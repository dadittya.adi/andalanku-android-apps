package com.lokavor.andalanku.utils

import org.junit.Assert
import org.junit.Test

class DateUtilUnitTest {
    @Test
    fun formated_test() {
        val date = "2019-03-27 15:15:44".stringToDate("yyyy-MM-dd HH:mm:ss")
        val formated = date.dateToString("dd MMM yyyy HH:mm")

        Assert.assertEquals(formated, "27 Mar 2019 15:15")
    }

    @Test
    fun formated_test_with_diff_format_date() {
        var formated = ""
        try {
            val date = "2019-03-27".stringToDate("yyyy-MM-dd HH:mm:ss")
            formated = date.dateToString("dd MMM yyyy HH:mm")
        } catch (error: Exception) {}

        Assert.assertEquals(formated, "")
    }

    @Test
    fun formated_string_test() {
        val date = "2019-03-27 15:15:44".stringToDate("yyyy-MM-dd HH:mm:ss")
        val formated = date.dateToString(arrayOf("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt","Nov", "Des"))
        System.out.println(formated)
        Assert.assertEquals(formated, "27 Mar 2019")
    }

    @Test
    fun datetimeToString() {
        val date = "2019-03-27 15:05:44".stringToDate("yyyy-MM-dd HH:mm:ss")
        val formated = date.datetimeToString(arrayOf("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt","Nov", "Des"))
        System.out.println(formated)
        Assert.assertEquals(formated, "27 Mar 2019 15:05")
    }

    @Test
    fun datetimeToString2() {
        val date = "2019-08-28 15:05:44".stringToDate("yyyy-MM-dd HH:mm:ss")
        val formated = date.datetimeToString("kemarin", arrayOf("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"), arrayOf("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt","Nov", "Des"))
        Assert.assertEquals(formated, "27 Mar 2019 15:05")
    }
}