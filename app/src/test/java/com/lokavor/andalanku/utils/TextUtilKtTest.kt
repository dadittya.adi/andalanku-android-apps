package com.lokavor.andalanku.utils

import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test

class TextUtilKtTest{
    @Test
    fun formated_to_rupiah_test() {
        val long: Long = 955850000
        val result = long.convertToRpFormat()
        val result_with_0 = (0.toLong()).convertToRpFormat()

        Assert.assertEquals(result, "Rp955.850.000,-")
        Assert.assertEquals(result_with_0, "Rp0,-")
    }

    @Test
    fun formated_decimal_test() {
        val double = 112830.91283
        val result = double.roundedNumber()

        Assert.assertEquals(result.toString(), 112830.91.toString())
    }

    @Test
    fun get_value_from_precentage_test() {
        val long: Long = 10000
        val result = long.getValue(1.0)
        val result_with_0 = long.getValue(0.0)

        Assert.assertEquals(result.toString(), 100.toString())
        Assert.assertEquals(result_with_0.toString(), 0.toString())
    }

    @Test
    fun get_percentage_test() {
        val long: Long = 10000
        val result = long.getPercentage(100000)

        Assert.assertEquals(result.toString(), 10.toString())
    }

    @Test
    fun get_number_from_text() {
        val result = "Rp955.850.000,-".getNumberOnly()
        val result_with_n = "Rp955.850\n.000,-".getNumberOnly()

        Assert.assertEquals(result.toString(), 955850000.toString())
        Assert.assertEquals(result_with_n.toString(), 955850000.toString())
    }
}