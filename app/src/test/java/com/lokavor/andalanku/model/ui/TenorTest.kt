package com.lokavor.andalanku.model.ui

import org.junit.Assert.*
import org.junit.Test

class TenorTest {
    @Test
    fun tenor_tes() {
        val tenor = _root_ide_package_.id.andalan.andalanku.model.ui.Tenor(3)

        assertEquals(tenor.getMonth("bulan"), "3 bulan")
        assertEquals(tenor.getYear("tahun"), "0.25 tahun")
        val tenor2 = _root_ide_package_.id.andalan.andalanku.model.ui.Tenor(12)
        assertEquals(tenor2.getMonth("bulan"), "12 bulan")
        assertEquals(tenor2.getYear("tahun"), "1 tahun")
    }
}