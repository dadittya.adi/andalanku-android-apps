package id.andalan.andalanku

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.MenuItem
import id.andalan.andalanku.deps.DaggerDeps
import id.andalan.andalanku.deps.Deps
import id.andalan.andalanku.networking.NetworkModule
import id.andalan.andalanku.preferences.PreferencesModule
import java.io.File

open class BaseApp: AppCompatActivity() {
    lateinit var deps: Deps
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val cacheFile = File(cacheDir, "responses")
        deps = DaggerDeps
            .builder()
            .networkModule(NetworkModule(application, cacheFile))
            .preferencesModule(PreferencesModule(application))
            .build()

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}