package id.andalan.andalanku.exception

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import id.andalan.andalanku.networking.NetManager
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.net.HttpURLConnection
import java.net.SocketTimeoutException
import java.net.UnknownHostException

internal class ErrorInterceptor(private val netManager: NetManager): Interceptor {
    private var response: Response? = null
    private var responseBody: String? = null
    private var responseJson: JsonObject? = null

    override fun intercept(chain: Interceptor.Chain): Response {
        if (!netManager.isConnectedToInternet) throw ErrorConnectionException()

        try {

            response = chain.proceed(chain.request())
            responseBody = response!!.body()!!.string()
            responseJson = JsonParser().parse(responseBody).asJsonObject

        } catch (e: Exception) {
            if (e is UnknownHostException) throw ErrorConnectionException()
            //if (e is SocketTimeoutException) throw ErrorConnectionException("Connection Timeout")
        }

        if (!response!!.isSuccessful) {
            val errorMessage = parseErrorMessage(loopbackParseErrorMessage(responseJson))
            if (response!!.code() >= HttpURLConnection.HTTP_INTERNAL_ERROR)
                throw ErrorConnectionException("Oops, Internal Server Error")

            if (response!!.code() == HttpURLConnection.HTTP_FORBIDDEN || response!!.code() == HttpURLConnection.HTTP_UNAUTHORIZED) {
//                EventBus.getDefault().post(AuthenticationException(errorMessage))
            }
            throw RuntimeException(errorMessage)
        }
        return response!!
            .newBuilder()
            .body(ResponseBody.create(response!!.body()!!.contentType(), responseBody!!))
            .build()
    }

    private fun parseErrorMessage(jsonObject: JsonObject?): String? {
        return jsonObject?.get("message")?.asString
    }

    private fun loopbackParseErrorMessage(jsonObject: JsonObject?): JsonObject? {
        return jsonObject?.get("error")?.asJsonObject
    }
}