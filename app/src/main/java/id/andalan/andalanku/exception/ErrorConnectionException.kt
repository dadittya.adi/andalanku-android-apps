package id.andalan.andalanku.exception

import java.io.IOException

class ErrorConnectionException: IOException {
    constructor() : super("No Connection, Please check your connection")
    constructor(message: String) : super(message)
}
