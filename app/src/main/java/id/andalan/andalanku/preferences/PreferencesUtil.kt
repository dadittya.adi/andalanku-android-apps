package id.andalan.andalanku.preferences

import android.content.SharedPreferences
import com.google.gson.Gson
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.PlafondSimulationRequest
import id.andalan.andalanku.model.response.AgentResponse
import id.andalan.andalanku.model.response.LegalDocumentResponse
import id.andalan.andalanku.model.response.LoginResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.ui.Occupation
import java.util.*
import javax.inject.Inject

class PreferencesUtil @Inject constructor(private val sharedPreferences: SharedPreferences) {
    fun putLoginDate(): Boolean {
        return sharedPreferences.edit().putString(LOGINDATE, Date().time.toString()).commit()
    }

    fun putJob(job: Occupation): Boolean {
        return sharedPreferences.edit().putString(JOB, Gson().toJson(job)).commit()
    }

    fun getJob(): Occupation? {
        val job = sharedPreferences.getString(JOB, "")?:""
        return if (job.isNotEmpty()) {
            Gson().fromJson(job, Occupation::class.java)
        } else {
            Occupation()
        }
    }

    fun putSideJob(job: String): Boolean {
        return sharedPreferences.edit().putString(SIDE_JOB, job).commit()
    }

    fun putAdditionalIncome(income: String): Boolean {
        return sharedPreferences.edit().putString(ADDITIONAL_INCOME, income).commit()
    }

    fun getSideJob(): String? {
        return sharedPreferences.getString(SIDE_JOB, "")
    }

    fun putSimulationPlafond(job: PlafondSimulationRequest): Boolean {
        return sharedPreferences.edit().putString(SIMULATION_PLAFOND, Gson().toJson(job)).commit()
    }

    fun getSimulationPlafond(): PlafondSimulationRequest? {
        return Gson().fromJson(sharedPreferences.getString(SIMULATION_PLAFOND, Gson().toJson(PlafondSimulationRequest())), PlafondSimulationRequest::class.java)
    }

    fun isLoginExpired(): Boolean {
        val timeString = sharedPreferences.getString(LOGINDATE, "0")
        val timeLogin = (timeString?:"0").toLong()
        val date = Date()
        return (date.time - timeLogin > ConfigVar.expiredLogin)
    }

    fun putLatitude(latitude: String): Boolean {
        if (latitude != "" || latitude != "0") {
            return sharedPreferences.edit().putString(LATITUDE, latitude).commit()
        }
        else {
            return true
        }
    }

    fun getLatitude(): String? {
        return sharedPreferences.getString(LATITUDE, "0")
    }

    fun putLongitude(longitude: String): Boolean {
        if (longitude != "" || longitude != "0") {
            return sharedPreferences.edit().putString(LONGITUDE, longitude).commit()
        }
        else {
            return true
        }
    }

    fun getLongitude(): String? {
        return sharedPreferences.getString(LONGITUDE, "0")
    }

    fun putAccessToken(accessToken: String): Boolean {
        return sharedPreferences.edit().putString(ACCESS_TOKEN, accessToken).commit()
    }

    fun getAccessToken(): String? {
        return sharedPreferences.getString(ACCESS_TOKEN, "")
    }

    fun putIdNumber(id: Int): Boolean {
        return sharedPreferences.edit().putInt(ID_NUMBER, id).commit()
    }

    fun getIdNumber(): Int? {
        return sharedPreferences.getInt(ID_NUMBER, 0)
    }

    fun putUser(loginResponse: LoginResponse): Boolean {
        return sharedPreferences.edit().putString(USER, Gson().toJson(loginResponse)).commit()
    }

    fun getUser(): LoginResponse? {
        return Gson().fromJson(sharedPreferences.getString(USER, Gson().toJson(LoginResponse())), LoginResponse::class.java)
    }

    fun putAgentCode(agentResponse: AgentResponse): Boolean {
        return sharedPreferences.edit().putString(AGENTCODE, Gson().toJson(agentResponse)).commit()
    }

    fun getAgentCode(): AgentResponse? {
        return Gson().fromJson(sharedPreferences.getString(AGENTCODE, Gson().toJson(AgentResponse())), AgentResponse::class.java)
    }

    fun putLogoutUser(loginResponse: LoginResponse): Boolean {
        return sharedPreferences.edit().putString(USER_TRASH, Gson().toJson(loginResponse)).commit()
    }

    fun getLogoutUser(): LoginResponse? {
        return Gson().fromJson(sharedPreferences.getString(USER_TRASH, Gson().toJson(LoginResponse())), LoginResponse::class.java)
    }

    fun putProfile(personalInformationResponse: PersonalInformationResponse): Boolean {
        return sharedPreferences.edit().putString(PROFILE, Gson().toJson(personalInformationResponse)).commit()
    }

    fun getProfile(): PersonalInformationResponse? {
        return Gson().fromJson(sharedPreferences.getString(PROFILE, Gson().toJson(PersonalInformationResponse())), PersonalInformationResponse::class.java)
    }

    fun putLegalDocument(legalDocumentResponse: LegalDocumentResponse): Boolean {
        return sharedPreferences.edit().putString(LEGAL_DOCUMENT, Gson().toJson(legalDocumentResponse)).commit()
    }

    fun getLegalDocument(): LegalDocumentResponse? {
        return Gson().fromJson(sharedPreferences.getString(LEGAL_DOCUMENT, Gson().toJson(LegalDocumentResponse())), LegalDocumentResponse::class.java)
    }

    fun putFirstOpen(firstOpen: Boolean): Boolean {
        return sharedPreferences.edit().putBoolean(FIRST_OPEN, firstOpen).commit()
    }

    fun getFirstOpen(): Boolean {
        return sharedPreferences.getBoolean(FIRST_OPEN, true)
    }

    fun putFirstOpenTips(firstOpenTips: Boolean): Boolean {
        return sharedPreferences.edit().putBoolean(FIRST_OPEN_TIPS, firstOpenTips).commit()
    }

    fun getFirstOpenTips(): Boolean {
        return sharedPreferences.getBoolean(FIRST_OPEN_TIPS, true)
    }

    fun putWebUrl(webUrl: String?): Boolean {
        return sharedPreferences.edit().putString(WEB_URL, webUrl).commit()
    }

    fun getWebUrl(): String? {
        return sharedPreferences.getString(WEB_URL, "https://andalanku.id/")
    }

    fun putApiUrl(apiUrl: String?): Boolean {
        return sharedPreferences.edit().putString(API_URL, apiUrl).commit()
    }

    fun getApiUrl(): String? {
        return sharedPreferences.getString(API_URL, "https://api.andalanku.id/")
    }

    fun putCmsUrl(cmsUrl: String?): Boolean {
        return sharedPreferences.edit().putString(CMS_URL, cmsUrl).commit()
    }

    fun getCmsUrl(): String? {
        return sharedPreferences.getString(CMS_URL, "https://cms.andalanku.id/")
    }

    fun putFcmToken(fcmToken: String?): Boolean {
        return sharedPreferences.edit().putString(FCM_TOKEN, fcmToken).commit()
    }

    fun getFcmToken(): String? {
        return sharedPreferences.getString(FCM_TOKEN, "")
    }


    companion object {
        val ACCESS_TOKEN = "access_token"
        val ID_NUMBER = "id_number"
        val LOGINDATE = "login_date"
        val USER = "user"
        val AGENTCODE = "agent_code"
        val LATITUDE = "latitude"
        val LONGITUDE = "longitude"
        val PROFILE = "PROFILE"
        val USER_TRASH = "user_trash"
        val JOB = "job"
        val SIDE_JOB = "side_job"
        val LEGAL_DOCUMENT = "legal_document"
        val SIMULATION_PLAFOND = "SimulationPlafond"
        val ADDITIONAL_INCOME = "additional_income"
        val FIRST_OPEN = "first_open"
        val FIRST_OPEN_TIPS = "first_open_tips"
        val WEB_URL = "web_url"
        val API_URL = "api_url"
        val CMS_URL = "cms_url"
        val FCM_TOKEN = "fcm_token"
    }
}