package id.andalan.andalanku.config

import id.andalan.andalanku.model.ui.PickerItem

class ConfigVar {
    companion object {
        val MENU_TYPE_E_COMPLAIN = "e_complain"
        val SUCCESS = "success"
        val FAILED = "failed"
        val NEW_CAR = "NEW"
        val USED_CARS = "USED"
        val KMG = "KMG"
        val LEGAL_ADDRESS_TYPE = "LEGAL"
        val DOMISILI_ADDRESS_TYPE = "DOMICILE"
        val OFFICE_ADDRESS_TYPE = "OFFICE"
        val SUBMITTED = "SUBMITTED"
        val ONPROCESS = "ON PROCESS"
        val SOLVED = "SOLVED"
        val BPKB = "BPKB"
        val SERTIFIKAT = "Sertifikat Tanah"
        val expiredLogin = 1000 * 60 * 60 * 60
        val listCategory: MutableList<PickerItem> = arrayListOf(PickerItem(id = "ARK", name = "All Risk"), PickerItem(id = "TLO", name = "TLO"))

        // type message
        val ECOMPLAIN = "e complain"
        val CLAIMINSURANCE = "insurance claim"
        val CREDITAPPLICATION = "credit application"
        val PLAFONDAPPLICATION = "plafond application"
        val REPAYMENT = "repayment"
        val REPAYMENTREQUEST = "repayment"
        val COLLATERALREQUEST = "collateral request"

        val MALE = "M"
        val FEMALE = "F"
    }
}