package id.andalan.andalanku.deps

import id.andalan.andalanku.networking.NetworkModule
import id.andalan.andalanku.preferences.PreferencesModule
import id.andalan.andalanku.ui.account.AccountSettingActivity
import id.andalan.andalanku.ui.account.UpdatePersonalInfoActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingDomicileActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingWorkPlaceActivity
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.complain.SubmissionComplainActivity
import id.andalan.andalanku.ui.contract.ContractDetailActivity
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.dana.ChooseDocumentAgunanActivity
import id.andalan.andalanku.ui.dana.SimulationDanaActivity
import id.andalan.andalanku.ui.payment.SimulationDanaResultActivity
import id.andalan.andalanku.ui.faq.FaqActivity
import id.andalan.andalanku.ui.guarantee.RequestTakeGuaranteeActivity
import id.andalan.andalanku.ui.guarantee.SubmissionRequestGuaranteeActivity
import id.andalan.andalanku.ui.help.HelpActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.installments.InstallmentScheduleDetailActivity
import id.andalan.andalanku.ui.insurance.EClaimInsuranceActivity
import id.andalan.andalanku.ui.insurance.SubmissionClaimInsuranceActivity
import id.andalan.andalanku.ui.loading_screen.LoadingScreenActivity
import id.andalan.andalanku.ui.loan_application.AccountDocumentActivity
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.newcar.CarChooserActivity
import id.andalan.andalanku.ui.newcar.CarsDetailActivity
import id.andalan.andalanku.ui.newcar.PaymentNewCarActivity
import id.andalan.andalanku.ui.news.NewsActivity
import id.andalan.andalanku.ui.otp.OtpVerificationActivity
import id.andalan.andalanku.ui.password.ForgotPasswordActivity
import id.andalan.andalanku.ui.password.UpdatePasswordActivity
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.ui.pin.ChangePinActivity
import id.andalan.andalanku.ui.pin.CreatePinActivity
import id.andalan.andalanku.ui.pin.RecreatePinActivity
import id.andalan.andalanku.ui.plafond.PlafondSubmissionActivity
import id.andalan.andalanku.ui.plafond.PlafondSubmissionSimulationActivity
import id.andalan.andalanku.ui.plafond.SimulationPlafondActivity
import id.andalan.andalanku.ui.preloader.PreloaderActivity
import id.andalan.andalanku.ui.promo.PromoActivity
import id.andalan.andalanku.ui.referral.ReferralActivity
import id.andalan.andalanku.ui.register.RegisterActivity
import id.andalan.andalanku.ui.repayment.RepaymentDetailActivity
import id.andalan.andalanku.ui.tnc.PrivacyPolicyActivity
import id.andalan.andalanku.ui.tnc.TNCActivity
import id.andalan.andalanku.ui.tutorial.TutorialActivity
import id.andalan.andalanku.ui.usedcar.UsedCarActivity
import dagger.Component
import id.andalan.andalanku.ui.agent.AgentRegisterActivity
import id.andalan.andalanku.ui.agent.AgentRegisterTncActivity
import id.andalan.andalanku.ui.contract.ContractActivity
import id.andalan.andalanku.ui.inbox.MessageReplyActivity
import id.andalan.andalanku.ui.installments.InstallmentScheduleActivity
import id.andalan.andalanku.ui.loan_application.CompleteDataActivity
import id.andalan.andalanku.ui.news.NewsDetailActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentDetailActivity
import id.andalan.andalanku.ui.ppob.*
import id.andalan.andalanku.ui.promo.DetailPromoActivity
import id.andalan.andalanku.ui.repayment.RepaymentSimulationActivity
import id.andalan.andalanku.ui.transactionHistory.PpobTransactionDetailActivity
import id.andalan.andalanku.ui.transactionHistory.TransactionHistoryFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [PreferencesModule::class, NetworkModule::class])
interface Deps {
    fun inject(accountSettingActivity: AccountSettingActivity)
    fun inject(loadingScreenActivity: LoadingScreenActivity)
    fun inject(mainHomeActivity: MainHomeActivity)
    fun inject(preloaderActivity: PreloaderActivity)
    fun inject(registerActivity: RegisterActivity)
    fun inject(createPinActivity: CreatePinActivity)
    fun inject(loginActivity: LoginActivity)
    fun inject(otpVerificationActivity: OtpVerificationActivity)
    fun inject(newsActivity: NewsActivity)
    fun inject(promoActivity: PromoActivity)
    fun inject(carsDetailActivity: CarsDetailActivity)
    fun inject(usedCarActivity: UsedCarActivity)
    fun inject(simulationDanaActivity: SimulationDanaActivity)
    fun inject(paymentSimulationActivity: PaymentSimulationActivity)
    fun inject(paymentDanaSimulationActivity: SimulationDanaResultActivity)
    fun inject(updatePersonalInfoActivity: UpdatePersonalInfoActivity)
    fun inject(helpActivity: HelpActivity)
    fun inject(changePinActivity: ChangePinActivity)
    fun inject(recreatePinActivity: RecreatePinActivity)
    fun inject(faqActivity: FaqActivity)
    fun inject(eComplainActivity: EComplainActivity)
    fun inject(submissionComplainActivity: SubmissionComplainActivity)
    fun inject(paymentNewCarActivity: PaymentNewCarActivity)
    fun inject(carChooserActivity: CarChooserActivity)
    fun inject(completeDocumentActivity: CompleteDocumentActivity)
    fun inject(chooseDocumentAgunanActivity: ChooseDocumentAgunanActivity)
    fun inject(updateAddressAccordingKtpActivity: UpdateAddressAccordingKtpActivity)
    fun inject(updateAddressAccordingWorkPlaceActivity: UpdateAddressAccordingWorkPlaceActivity)
    fun inject(updateAddressAccordingDomicileActivity: UpdateAddressAccordingDomicileActivity)
    fun inject(forgotPasswordActivity: ForgotPasswordActivity)
    fun inject(installmentScheduleDetailActivity: InstallmentScheduleDetailActivity)
    fun inject(repaymentDetailActivity: RepaymentDetailActivity)
    fun inject(contractDetailActivity: ContractDetailActivity)
    fun inject(submissionClaimInsuranceActivity: SubmissionClaimInsuranceActivity)
    fun inject(eClaimInsuranceActivity: EClaimInsuranceActivity)
    fun inject(requestTakeGuaranteeActivity: RequestTakeGuaranteeActivity)
    fun inject(submissionRequestGuaranteeActivity: SubmissionRequestGuaranteeActivity)
    fun inject(plafondSubmissionSimulationActivity: PlafondSubmissionSimulationActivity)
    fun inject(simulationPlafondActivity: SimulationPlafondActivity)
    fun inject(plafondSubmissionActivity: PlafondSubmissionActivity)
    fun inject(updatePasswordActivity: UpdatePasswordActivity)
    fun inject(accountDocumentActivity: AccountDocumentActivity)
    fun inject(referralActivity: ReferralActivity)
    fun inject(tncActivity: TNCActivity)
    fun inject(privacyPolicyActivity: PrivacyPolicyActivity)
    fun inject(tutorialActivity: TutorialActivity)
    fun inject(messageReplyActivity: MessageReplyActivity)
    fun inject(agentRegisterActivity: AgentRegisterActivity)
    fun inject(agentRegisterTncActivity: AgentRegisterTncActivity)
    fun inject(detailPromoActivity: DetailPromoActivity)
    fun inject(installmentPaymentDetailActivity: InstallmentPaymentDetailActivity)
    fun inject(installmentPaymentActivity: InstallmentPaymentActivity)
    fun inject(newsDetailActivity: NewsDetailActivity)
    fun inject(completeDataActivity: CompleteDataActivity)
    fun inject(contractActivity: ContractActivity)
    fun inject(installmentScheduleActivity: InstallmentScheduleActivity)
    fun inject(repaymentSimulationActivity: RepaymentSimulationActivity)
    fun inject(ppobPulsaActivity: PpobPulsaActivity)
    fun inject(transactionHistoryFragment: TransactionHistoryFragment)
    fun inject(ppobPaymentProcessActivity: PpobPaymentProcessActivity)
    fun inject(ppobPostpaidActivity: PpobPostpaidActivity)
    fun inject(ppobPlnActivity: PpobPlnActivity)
    fun inject(ppobPgnActivity: PpobPgnActivity)
    fun inject(ppobTelkomActivity: PpobTelkomActivity)
    fun inject(ppobGeneralActivity: PpobGeneralActivity)
    fun inject(ppobGeneralSubProductListActivity: PpobGeneralSubProductListActivity)
    fun inject(ppobBpjsActivity: PpobBpjsActivity)
    fun inject(ppobTransactionDetailActivity: PpobTransactionDetailActivity)
    fun inject(ppobCategoryListActivity: PpobCategoryListActivity)
    fun inject(ppobCreditCardActivity: PpobCreditCardActivity)
}