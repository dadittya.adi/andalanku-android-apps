package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.ui.base.BaseView

interface ContractView: BaseView {
    fun getAndalanFinencial(detailAndalanFinancialResponse: DetailAndalanFinancialResponse)
}