package id.andalan.andalanku.ui.register

import id.andalan.andalanku.model.response.RegisterResponse
import id.andalan.andalanku.ui.base.BaseView

interface RegisterView: BaseView {
    fun onGetResponse(registerResponse: RegisterResponse)
}