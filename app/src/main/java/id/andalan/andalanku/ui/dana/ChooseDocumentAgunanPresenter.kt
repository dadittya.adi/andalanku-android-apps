package id.andalan.andalanku.ui.dana

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.CarBrandsResponse
import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CarTypesResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class ChooseDocumentAgunanPresenter @Inject constructor(private val service: Services): BasePresenter<ChooseDocumentAgunanView>() {
    override fun onCreate() {
        getBrandsCar()
    }

    fun getBrandsCar() {
        view.showWait()
        val subscription = service.getBrandsCar()
            .subscribe(object : Subscriber<CarBrandsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(carBrandsResponse: CarBrandsResponse) {
                    if (carBrandsResponse.status == ConfigVar.SUCCESS) {
                        carBrandsResponse.CarBrandList?.let {
                            val list: MutableList<PickerItem> = arrayListOf()
                            it.forEach {
                                list.add(PickerItem(id = it.idCarBrand, name = it.name))
                            }
                            view.onGetCarBrands(list)
                        }
                    }
                }
            })
        subscriptions.add(subscription)
    }

    fun getTypesCar(id: String) {
        view.showWait()
        val subscription = service.getTypesCar(id)
            .subscribe(object : Subscriber<CarTypesResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(carTypesResponse: CarTypesResponse) {
                    if (carTypesResponse.status == ConfigVar.SUCCESS) {
                        carTypesResponse.carTypeList?.let {
                            val list: MutableList<PickerItem> = arrayListOf()
                            it.forEach {
                                list.add(PickerItem(id = it.idCarType, name = it.name))
                            }
                            view.onGetTypeCar(list)
                        }
                    }
                }
            })
        subscriptions.add(subscription)
    }

    fun getCarTrim(id: String) {
        view.showWait()
        val subscription = service.getTrimCar(id)
            .subscribe(object : Subscriber<CarTrimsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(carTrimsResponse: CarTrimsResponse) {
                    if (carTrimsResponse.status == ConfigVar.SUCCESS) {
                        carTrimsResponse.carTrimList?.let {
                            val list: MutableList<PickerItem> = arrayListOf()
                            it.forEach {
                                list.add(PickerItem(id = it.idCarTrim, name = it.name))
                            }
                            view.onGetCarTrim(list)
                        }
                    }
                }
            })
        subscriptions.add(subscription)
    }

    fun getUsedCarList(year: String, keyword: String) {
        view.showWait()
        val subscription = service.getUsedCarList(year = year, keyword = keyword)
            .subscribe(object : Subscriber<UsedCarListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(usedCarListResponse: UsedCarListResponse) {
                    view.onGetUsedCarsList(usedCarListResponse)
                }
            })
        subscriptions.add(subscription)
    }
}