@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.graphics.Color
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_ppob_group.view.*
import kotlinx.android.synthetic.main.item_trasaction.view.*

class TransactionHistoryAdapter(val onItemClickListener: OnClickItem): BaseAdapter<InstallmentPaymentHistory, TransactionHistoryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_trasaction
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<InstallmentPaymentHistory>(itemView) {
        override fun bind(installmentPaymentHistory: InstallmentPaymentHistory) {
            val data = installmentPaymentHistory.getInstallmentPaymentTransaction()
            itemView.btn_va.setOnClickListener {
                onItemClickListener.onClickVa(installmentPaymentHistory)
            }

            itemView.btn_detail.setOnClickListener {
                onItemClickListener.onClickDetail(installmentPaymentHistory)
            }

            val prefix = data.orderId.toString().substring(0,3)

            if (prefix == "INS") {
                val txtPeriode = data.info2.toString() + "-" + data.info3.toString()
                itemView.tv_category?.text = "Pembayaran Angsuran"
                //itemView.tv_lbl_info_1?.text = R.string.nomor_kontrak.
                itemView.tv_info_1?.text = data.info1.toString()
                //itemView.tv_lbl_info_2?.text = R.string.angsuran_ke.toString()
                itemView.tv_info_2?.text = txtPeriode

            }
            else {
                var category : String = ""
                if (data.info3 == "Gas Nagara") {
                    category = "Gas Negara"
                }
                else if (data.info3 == "Streaming1") {
                    category = "Streaming"
                }
                else {
                    category = data.info3.toString()
                }

                itemView.tv_category?.text = category
                var info1Label = "ID Pelanggan"
                if (data.info3.toString() == "Pulsa" ||
                    data.info3.toString() == "Paket Data" ||
                    data.info3.toString() == "Pascabayar") {
                    info1Label = "Nomor HP"
                }
                else if (data.info3.toString() == "Kartu Kredit"){
                    info1Label = "Nomor Kartu"
                }
                else if (data.info3.toString() == "Pembayaran Angsuran") {
                    info1Label = "No. Kontrak"
                }
                else if (data.info3.toString() == "Voucher Game" ||
                    data.info3.toString() == "Streaming1") {
                    info1Label = "Account ID"
                }
                else if (data.info3.toString() == "BPJS") {
                    info1Label = "No. BPJS"
                }

                itemView.tv_lbl_info_1?.text = info1Label
                itemView.tv_info_1?.text = data.info1.toString()
                itemView.tv_lbl_info_2?.text = "Product"
                itemView.tv_info_2?.text = data.info2.toString()

                if (data.info3 == "Pulsa" || data.info3 == "Pascabayar" || data.info3 == "Paket Data") {
                    itemView.ic_payment?.setImageResource(R.drawable.pulsa)
                }
                else if (data.info3 == "Listrik") {
                    itemView.ic_payment?.setImageResource(R.drawable.listrik)
                }
                else if (data.info3 == "PDAM") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.pdam)
                }
                else if (data.info3 == "BPJS") {
                    itemView.ic_payment?.setImageResource(R.drawable.bpjs)
                }
                else if (data.info3 == "Telkom") {
                    itemView.ic_payment?.setImageResource(R.drawable.telkom)
                }
                else if (data.info3 == "Gas Nagara" || data.info3 == "Gas Negara") {
                    itemView.ic_payment?.setImageResource(R.drawable.gasnegara)
                }
                else if (data.info3 == "Kartu Kredit") {
                    itemView.ic_payment?.setImageResource(R.drawable.ic_kartu_kredit)
                }
                else if (data.info3 == "Internet dan TV Kabel") {
                    itemView.ic_payment?.setImageResource(R.drawable.ic_tv_cable)
                }
                else if (data.info3 == "Pembayaran Angsuran") {
                    itemView.ic_payment?.setImageResource(R.drawable.ic_angsuran_kredit)
                }
                else if (data.info3 == "eMoney") {
                    itemView.ic_payment?.setImageResource(R.drawable.emoney)
                }
                else if (data.info3 == "Streaming1") {
                    itemView.ic_payment?.setImageResource(R.drawable.ic_streaming)
                }
                else if (data.info3 == "Voucher Game") {
                    itemView.ic_payment?.setImageResource(R.drawable.ic_game_voucher)
                }
                else if (data.info3 == "Voucher Digital") {
                    itemView.ic_payment?.setImageResource(R.drawable.ic_game_voucher)
                }

            }


            itemView.tv_date?.text = data.getDateUI(itemView.context.getMonths())
            itemView.tv_transaction_number?.text = data.orderId.toString()
            itemView.tv_total?.text = data.amount?.toLong()?.convertToRpFormat()
            itemView.guideline_bottom.visibility = View.GONE
            itemView.btn_detail?.visibility = View.GONE

            if (data.status == "INQUIRY") {
                itemView.tv_state?.text = "INQUIRY"
            }
            else if(data.status == "PAYMENT") {
                itemView.tv_state?.text = "MENUNGGU PEMBAYARAN"
                itemView.tv_state?.setTextColor(Color.parseColor("#FF9908"))
                itemView.guideline_bottom.visibility = View.VISIBLE
                itemView.btn_va?.visibility = View.VISIBLE
            }
            else if(data.status == "PAYMENT DONE") {
                itemView.tv_state?.text = "BERHASIL"
                
                itemView.tv_state?.setTextColor(Color.parseColor("#39B54A"))
                if (prefix == "AYO") {
                    itemView.guideline_bottom.visibility = View.VISIBLE
                    itemView.btn_detail?.visibility = View.VISIBLE
                }

            }
            else if(data.status == "PAYMENT FAILED") {
                itemView.tv_state?.text = "GAGAL"
            }
            else if(data.status == "EXPIRED") {
                itemView.tv_state?.text = "KADALUARSA"
                itemView.tv_state?.setTextColor(Color.RED)
            }
        }

    }

    interface OnClickItem {
        fun onClickVa(installmentPaymentHistory: InstallmentPaymentHistory)
        fun onClickDetail(installmentPaymentHistory: InstallmentPaymentHistory)
    }
}