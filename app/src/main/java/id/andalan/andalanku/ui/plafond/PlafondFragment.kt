package id.andalan.andalanku.ui.plafond

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Menu
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.MenuAdapterHome
import id.andalan.andalanku.ui.dana.SimulationDanaActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.newcar.PaymentNewCarActivity
import id.andalan.andalanku.ui.usedcar.UsedCarActivity
import kotlinx.android.synthetic.main.fragment_plafond.*
import kotlinx.android.synthetic.main.toolbar_login.*

class PlafondFragment : androidx.fragment.app.Fragment() {

    private lateinit var menuAdapterHome: MenuAdapterHome
    private lateinit var preferencesUtil: PreferencesUtil

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_plafond, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.plafond_andalanku)
        preferencesUtil = (activity as MainHomeActivity).preferencesUtil

        menuAdapterHome = MenuAdapterHome(object: MenuAdapterHome.onClickItem{
            override fun onClickMenu(menu: Menu) {
                when {
                    menu.name == getString(R.string.new_cars) -> startActivity(Intent(context, PaymentNewCarActivity::class.java))
                    menu.name == getString(R.string.used_car) -> startActivity(Intent(context, UsedCarActivity::class.java))
                    menu.name == getString(R.string.wallet_andalanku) -> startActivity(Intent(context, SimulationDanaActivity::class.java))
                }
            }
        })

        menuAdapterHome.add(
            Menu(
                name = getString(R.string.new_cars),
                image = R.drawable.ic_new_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.used_car),
                image = R.drawable.ic_used_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.wallet_andalanku),
                image = R.drawable.ic_dana_andalanku
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.e_commerce),
                image = R.drawable.ic_ecommerce
            )
        )

//        val layoutManager = GridLayoutManager(context, 3)
//        rv_menu_submission?.layoutManager = layoutManager
//        rv_menu_submission?.itemAnimator = DefaultItemAnimator()
//        rv_menu_submission?.adapter = menuAdapterHome
//        rv_menu_submission?.isNestedScrollingEnabled = true

        btn_submission?.setOnClickListener {
            if ((preferencesUtil.getUser()?.idCustomer?:0) > 0) {
                startActivity(Intent(context, PlafondSubmissionSimulationActivity::class.java))
            } else {
                startActivityForResult(Intent(context, LoginActivity::class.java), LoginActivity.REQUEST_LOGIN)
            }
        }

        toolbar_login.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
    }

    override fun onResume() {
        super.onResume()
        hi_text?.text = if (preferencesUtil.getProfile()?.customerName.isNullOrBlank()) "" else "Hi, ${preferencesUtil.getProfile()?.customerName?:""}"
    }
}
