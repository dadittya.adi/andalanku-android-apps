@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Promo
import id.andalan.andalanku.model.ui.PromoHome
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_list_news.view.*

class PromoAdapter(val onItemClickListener: PromoAdapter.OnClickItem): BaseAdapter<Promo, PromoAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_list_news
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Promo>(itemView) {
        override fun bind(promo: Promo) {
            val data = promo.getPromoHome()
            itemView.setOnClickListener {
                onItemClickListener.onClickPromo(promo)
            }
            try {
                loadImage(itemView.context, data.url?:"", itemView.iv_image_news)
            } catch (err: Exception) {
            }
            itemView.tv_periode_date?.text = data.getDateUI(itemView.context.getMonths())
        }

    }

    interface OnClickItem {
        fun onClickPromo(promo: Promo)
    }
}