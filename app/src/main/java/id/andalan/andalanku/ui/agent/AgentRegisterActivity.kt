@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.agent

import android.app.Activity
import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.preferences.PreferencesUtil
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import id.andalan.andalanku.model.request.SendAgenRegistrationRequest
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.ui.address.UpdateAddressAccordingDomicileActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity.Companion.IS_SETTING_ACCOUNT
import id.andalan.andalanku.ui.address.UpdateAddressAccordingWorkPlaceActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.DOMICILE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.DOMICILE_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.ISREFERENSI
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.IS_COMPLETE_DATA_FORM
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.OFFICE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.OFFICE_REQ
import id.andalan.andalanku.utils.*
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.activity_agent_register.*
import kotlinx.android.synthetic.main.layout_succes_send_update.view.*
import kotlinx.android.synthetic.main.top_drag_view.*
import kotlinx.android.synthetic.main.activity_agent_register.et_account_number
import kotlinx.android.synthetic.main.activity_agent_register.et_choose_bank
import kotlinx.android.synthetic.main.activity_agent_register.et_date
import kotlinx.android.synthetic.main.activity_agent_register.et_home_phone
import kotlinx.android.synthetic.main.activity_agent_register.et_in_the_name
import kotlinx.android.synthetic.main.activity_agent_register.et_ktp
import kotlinx.android.synthetic.main.activity_agent_register.et_label_home_phone
import kotlinx.android.synthetic.main.activity_agent_register.et_name
import kotlinx.android.synthetic.main.activity_agent_register.et_npwp
import kotlinx.android.synthetic.main.activity_agent_register.et_phone
import kotlinx.android.synthetic.main.activity_agent_register.et_pob
import kotlinx.android.synthetic.main.activity_agent_register.radio_female
import kotlinx.android.synthetic.main.activity_agent_register.radio_male
import kotlinx.android.synthetic.main.activity_agent_register.rg_container
import kotlinx.android.synthetic.main.activity_agent_register.rv_list_alamat
import kotlinx.android.synthetic.main.activity_agent_register.sliding_layout
import kotlinx.android.synthetic.main.activity_used_car.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.item_drag_view_bank.*
import kotlinx.android.synthetic.main.item_menu_home.view.title_text
import kotlinx.android.synthetic.main.layout_success.view.btn_ok
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.util.*
import java.text.SimpleDateFormat

class AgentRegisterActivity : BaseApp(), AgentRegisterView {
    override fun successUpdateAddress(updateAddressResponse: UpdateAddressResponse) {
        EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
    }

    companion object {
        val COMPLETE_DATA = "complete_data"
        val IS_ACCOUNT_SETTING = "is_account_setting"
        val REQUEST_IMAGE_BANK = 1003
        val REQUEST_IMAGE_NPWP = 1004

        val REQ_CODE_UPDATED_INFO = 1114
        val RES_CODE_UPDATED_INFO = 1115
        val UPDATED_INFO = "update_info"
        val HOME = "home"
        val MESSAGE = "message"
    }
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: AgentRegisterPresenter

    private lateinit var listBankAdapter: PickerAdapter
    private var bankChoosed = PickerItem()
    private val myCalendar = Calendar.getInstance()
    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom
    private lateinit var menuBottomAdapter: MenuAdapterProfile
    lateinit var branchAdapter: BranchAdapter
    lateinit var occupationAdapter: OccupationAdapter
    lateinit var marriageStatusAdapter: ArrayStaticAdapter
    lateinit var houseStatusAdapter: ArrayStaticAdapter
    lateinit var educationAdapter: ArrayStaticAdapter

    private var personalInfoUser: PersonalInformationResponse? = null

    private var choosedProvince: Location? = null
    private var choosedDistrict: Location? = null
    private var choosedCity: Location? = null
    private var choosedVillage: Location? = null
    private var choosedImage = ""
    private var choosedBankImage = ""
    private var choosedNPWPImage = ""
    private var choosedDateOfBirth = ""
    private var autoZipCode = ""

    private var choosedBank: Bank? = null
    private var choosedBankBranch: BankBranch? = null
    lateinit var bankAdapter: BankAdapter
    lateinit var bankBranchAdapter: BankBranchAdapter
    private var branchChoosed: Branch? = null
    private var occupationChoosed: Occupation? = null

    private var address: UpdateAddressRequest? = null
    private var addressDomicile: UpdateAddressRequest? = null
    private var addressOffice: UpdateAddressRequest? = null

    private var formatedDob: String = ""
    private lateinit var alertDialogProgress: AlertDialogCustom
    private var searchSender: String = ""
    private var marriageStatusList: MutableList<String> =  arrayListOf<String>("Single","Menikah","Janda", "Duda")
    private var houseStatusList: MutableList<String> =  arrayListOf<String>("Milik Sendiri","Kontrak","Milik Perusahaan", "Lain-lain")
    private var educationList: MutableList<String> =  arrayListOf<String>("SD","SMP","SMA", "S1", "S2", "S3", "Lain-lain")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        setContentView(R.layout.activity_agent_register)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        bankAdapter = BankAdapter(object: BankAdapter.OnClickItem{
            override fun onClickBank(bank: Bank) {
                et_choose_bank?.setText(bank.bankName)
                choosedBank = bank
                hideSlideLayout()
            }
        })

                    bankBranchAdapter = BankBranchAdapter(object: BankBranchAdapter.OnClickItem{
            override fun onClickBankBranch(bankBranch: BankBranch) {
                et_choose_bank_branch?.setText(bankBranch.bankBranchName)
                choosedBankBranch = bankBranch
                hideSlideLayout()
            }
        })

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.daftar_agent)

        val isAccountSetting = intent?.extras?.getBoolean(IS_ACCOUNT_SETTING, false)

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView()
        alertDialogCustom.create()
        alertDialogCustom.getView().btn_close.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra(AgentRegisterActivity.UPDATED_INFO, true)
            setResult(AgentRegisterActivity.RES_CODE_UPDATED_INFO, returnIntent)
            finish()
            alertDialogCustom.dismiss()
        }
        alertDialogCustom.getView().btn_back_to_home.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra(AgentRegisterActivity.UPDATED_INFO, true)
            setResult(AgentRegisterActivity.RES_CODE_UPDATED_INFO, returnIntent)
            finish()
            alertDialogCustom.dismiss()
        }
        alertDialogCustom.getView().sent_text.text = getString(R.string.information)

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false

        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        menuBottomAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.alamat_sesuai_ktp)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingKtpActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(ISREFERENSI, false)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(IS_SETTING_ACCOUNT, true)
                    startActivityForResult(intent, KTP_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_domisili)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingDomicileActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(ISREFERENSI, false)
                    intent.putExtra(DOMICILE_DATA, addressDomicile)
                    startActivityForResult(intent, DOMICILE_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_kantor)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingWorkPlaceActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(OFFICE_DATA, addressOffice)
                    intent.putExtra(ISREFERENSI, false)
                    startActivityForResult(intent, OFFICE_REQ)
                }
            }
        })

        listBankAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = listBankAdapter.getData().indexOf(bankChoosed)
                bankChoosed.isChoosed = false
                if (index > -1) listBankAdapter.update(index, bankChoosed)
                et_choose_bank?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                bankChoosed = pickerItem
                val indexChoosed = listBankAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) listBankAdapter.update(indexChoosed, bankChoosed)
                hideSlideLayout()
            }
        })

        et_choose_bank?.setOnClickListener {
            searchSender = "BANK"
            if (et_drag_view_bank.text.toString() == "") {
                presenter.getListBank(keyword = "")
            }
            tv_title_drag_view.text = getString(R.string.choose_bank)
            et_drag_view_bank.hint = getString(R.string.write_search)
            et_drag_view_bank.visibility = View.VISIBLE
            et_drag_view_bank.setText("")
            rv_drag_view_bank.adapter = bankAdapter
            showSlideLayout()
        }
        et_choose_bank_branch?.setOnClickListener {
            if(choosedBank != null) {
                searchSender = "BANK_BRANCH"
                presenter.getListBankBranch(choosedBank?.id, "")
                tv_title_drag_view.text = getString(R.string.choose_bank_branch)
                et_drag_view_bank.hint = getString(R.string.write_search)
                et_drag_view_bank.visibility = View.VISIBLE
                et_drag_view_bank.setText("")
                rv_drag_view_bank.adapter = bankBranchAdapter
                showSlideLayout()
            }
            else {
                Toast.makeText(this, "Pilih Bank Terlebih Dahulu", Toast.LENGTH_SHORT).show()
            }
        }


        et_drag_view_bank?.setOnEditorActionListener { _, i, _ ->
            return@setOnEditorActionListener if (i == EditorInfo.IME_ACTION_SEARCH) {
                if (searchSender == "BANK") {
                    presenter.getListBank(et_drag_view_bank.text.toString())
                }

                if (searchSender == "BANK_BRANCH") {
                    presenter.getListBankBranch(choosedBank?.id, et_drag_view_bank.text.toString())
                }
                true
            } else {
                false
            }
        }

        val btnUploadBank = bankImage?.findViewById<View>(R.id.btn_upload_photo2)
        val iv_preview = bankImage?.findViewById<ImageView>(R.id.iv_preview2)
        val tv_filename = bankImage?.findViewById<TextView>(R.id.tv_filename2)

        btnUploadBank?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_BANK)
        }

        val btnUploadNpwp = npwpImage?.findViewById<View>(R.id.btn_upload_photo2)
        val iv_preview_npwp = npwpImage?.findViewById<ImageView>(R.id.iv_preview2)
        val tv_filename_npwp = npwpImage?.findViewById<TextView>(R.id.tv_filename2)

        btnUploadNpwp?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_NPWP)
        }

        personalInfoUser = preferencesUtil.getProfile()
        //val user = preferencesUtil.getUser()
        //et_choose_bank?.setText(personalInfoUser?.bankName)
        et_account_number?.setText(personalInfoUser?.bankAccountNumber)
        et_in_the_name?.setText(personalInfoUser?.bankAccountHolder)
        address = personalInfoUser?.getAddressLegal()
        addressDomicile = personalInfoUser?.getAddressDomicile()
        addressOffice = personalInfoUser?.getAddressOffice()

//        if (!personalInfoUser?.bankAccountImage.isNullOrBlank()) {
//
//            loadImage(this@AgentRegisterActivity, personalInfoUser?.bankAccountImage?:"", iv_preview!!)
//            tv_filename?.text = getString(R.string.change_image)
//            choosedBankImage = personalInfoUser?.bankAccountImage?:""
//            //address?.typeIdCardImage = "url"
//        }
//
//
//        if (!personalInfoUser?.fotoNpwp.isNullOrBlank()) {
//            loadImage(this@AgentRegisterActivity, personalInfoUser?.fotoNpwp?:"", iv_preview_npwp!!)
//            tv_filename_npwp?.text = getString(R.string.change_image)
//            choosedNPWPImage = personalInfoUser?.fotoNpwp?:""
//        }

        et_name?.setText(personalInfoUser?.customerName)
        if (personalInfoUser?.gender == "M" || personalInfoUser?.gender == "L") {
            rg_container.check(radio_male.id)
        }

        else if (personalInfoUser?.gender == "P" || personalInfoUser?.gender == "F") {
            rg_container.check(radio_female.id)
        }

        et_pob?.setText(personalInfoUser?.placeOfBirth)
        et_date?.setText(personalInfoUser?.getDOBUI(this.getMonths()))
        choosedDateOfBirth = personalInfoUser?.dateOfBirth?:""
        et_ktp?.setText(personalInfoUser?.idNumber)
        et_home_phone?.setText(personalInfoUser?.homePhoneNumber)
        et_phone?.setText(personalInfoUser?.phoneNumber)
        et_label_home_phone?.setText(personalInfoUser?.areaCode)
        et_npwp?.setText(personalInfoUser?.npwp)
        et_email?.setText(personalInfoUser?.email)

        branchAdapter = BranchAdapter(object: BranchAdapter.OnClickItem{
            override fun onClick(data: Branch) {
                branchChoosed = data
                et_choose_branch?.setText(branchChoosed?.name)
                hideSlideLayout()
            }

        })

        et_choose_branch?.setOnClickListener {
            if (branchAdapter.getData().isEmpty()) { presenter.getListBranch(addressDomicile?.regencyId?:"") }
            tv_title_drag_view.text = "Pilih Cabang"
            et_drag_view_bank.visibility = View.GONE
            rv_drag_view_bank.adapter = branchAdapter
            showSlideLayout()
        }

        occupationAdapter = OccupationAdapter(object: OccupationAdapter.OnClickItem{
            override fun onClick(occupation: Occupation) {
                occupationChoosed = occupation
                et_job.setText(occupation.name)
                hideSlideLayout()
            }
        })

        et_job?.setOnClickListener {
            if (occupationAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.job)
            et_drag_view_bank.visibility = View.GONE
            rv_drag_view_bank.adapter = occupationAdapter
            showSlideLayout()
        }

        marriageStatusAdapter = ArrayStaticAdapter(object: ArrayStaticAdapter.OnClickItem{
            override fun onClick(marriageStatus: String) {
                et_choose_marriage_status.setText(marriageStatus)
                hideSlideLayout()
            }
        })

        marriageStatusAdapter.add(marriageStatusList)
        et_choose_marriage_status?.setOnClickListener {
            //if (marriageStatusAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.marriage_status)
            et_drag_view_bank.visibility = View.GONE
            rv_drag_view_bank.adapter = marriageStatusAdapter
            showSlideLayout()
        }

        houseStatusAdapter = ArrayStaticAdapter(object: ArrayStaticAdapter.OnClickItem{
            override fun onClick(houseStatus: String) {
                et_choose_house_status.setText(houseStatus)
                hideSlideLayout()
            }
        })

        houseStatusAdapter.add(houseStatusList)

        et_choose_house_status?.setOnClickListener {
            //if (marriageStatusAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.house_status)
            et_drag_view_bank.visibility = View.GONE
            rv_drag_view_bank.adapter = houseStatusAdapter
            showSlideLayout()
        }

        educationAdapter = ArrayStaticAdapter(object: ArrayStaticAdapter.OnClickItem{
            override fun onClick(education: String) {
                et_choose_education.setText(education)
                hideSlideLayout()
            }
        })

        educationAdapter.add(educationList)

        et_choose_education?.setOnClickListener {
            //if (marriageStatusAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.education)
            et_drag_view_bank.visibility = View.GONE
            rv_drag_view_bank.adapter = educationAdapter
            showSlideLayout()
        }


        btn_next_submit_agent?.setOnClickListener {
            val name = et_name?.text.toString()
            val selectedGender = rg_container?.checkedRadioButtonId
            val selectedGenderRadio: RadioButton? = findViewById(selectedGender?:0)
            val gender = selectedGenderRadio?.tag.toString()
            val pob = et_pob?.text.toString()
            val ktp = et_ktp?.text.toString()
            val npwp = et_npwp?.text.toString()
            val bankName = et_choose_bank?.text.toString()
            val bankBranchName = et_choose_bank_branch?.text.toString()
            val bankAccountNumber = et_account_number?.text.toString()
            val bankAccountHolder = et_in_the_name?.text.toString()
            val pendidikanTerakhir = et_choose_education.text.toString()
            val statusPernikahan = et_choose_marriage_status?.text.toString()
            val statusRumahTinggal = et_choose_house_status?.text.toString()
            val kerabatAfi = et_family_name?.text.toString()
            val kerabatAfiJabatan = et_family_position?.text.toString()
            val lamaTinggalTahun = et_length_of_stay_year?.text.toString()
            val lamaTinggalBulan = et_length_of_stay_months?.text.toString()

            val imageIdCard = address?.idCardImage
            var idCardImageType = ""
            if (imageIdCard?.isBlank() == false) {
                if (imageIdCard.take(4).equals("http")) {
                    idCardImageType = "url"
                }
                else {
                    idCardImageType = "file"
                }
            }

            var npwpImageType = ""
            if (choosedNPWPImage.isBlank() == false) {
                if (choosedNPWPImage.take(4).equals("http")) {
                    npwpImageType = "url"
                }
                else {
                    npwpImageType = "file"
                }
            }

            var bankImageType = ""
            if (choosedBankImage.isBlank() == false) {
                if (choosedBankImage.take(4).equals("http")) {
                    bankImageType = "url"
                }
                else {
                    bankImageType = "file"
                }
            }


            val sendAgenRegistrationRequest = SendAgenRegistrationRequest(
                imageIdCard = address?.idCardImage,
                typeImageIdCard = idCardImageType,
                provinceId = address?.provinceId,
                cityId = address?.regencyId,
                districtId = address?.districtId,
                villageId = address?.villageId,
                address = address?.address,
                zipCode = address?.zipCode,
                rt = address?.rt,
                rw = address?.rw,
                provinceIdDomicile = addressDomicile?.provinceId,
                cityIdDomicile = addressDomicile?.regencyId,
                districtIdDomicile = addressDomicile?.districtId,
                villageIdDomicile = addressDomicile?.villageId,
                addressDomicile = addressDomicile?.address,
                rtDomicile = addressDomicile?.rt,
                rwDomicile = addressDomicile?.rw,
                zipCodeDomicile = addressDomicile?.zipCode,
                provinceIdOffice = addressOffice?.provinceId,
                cityIdOffice = addressOffice?.regencyId,
                districtIdOffice = addressOffice?.districtId,
                villageIdOffice = addressOffice?.villageId,
                addressOffice = addressOffice?.address,
                rtOffice = addressOffice?.rt,
                rwOffice = addressOffice?.rw,
                zipCodeOffice = addressOffice?.zipCode,
                areaCode = et_label_home_phone?.text.toString(),
                phone = et_home_phone?.text.toString(),
                mobilePhone = et_phone?.text.toString(),
                email = et_email?.text.toString(),
                gender = gender,
                npwp = npwp,
                npwp_image = choosedNPWPImage,
                npwpImageType = npwpImageType,
                bankName = bankName,
                bankAccountNumber = bankAccountNumber,
                bankAccountHolder = bankAccountHolder,
                birthPlace = pob,
                birthDate = choosedDateOfBirth,
                idCardNumber = ktp,
                agentName = name,
                bankAccountImage = choosedBankImage,
                bankImageType = bankImageType,
                occupationId = occupationChoosed?.id,
                branchId = branchChoosed?.branchID,
                bankBranchName = bankBranchName,
                statusPernikahan = statusPernikahan,
                statusRumahTinggal = statusRumahTinggal,
                kerabatAfi = kerabatAfi,
                kerabatAfiJabatan = kerabatAfiJabatan,
                lamaTinggalBulan = lamaTinggalBulan,
                lamaTinggalTahun = lamaTinggalTahun,
                pendidikanTerakhir = pendidikanTerakhir,
                nikUpline = et_upline_No?.text.toString(),
                nameUpline = et_upline_name?.text.toString()
            )

            var msgValidation = sendAgenRegistrationRequest.isValidFormCompleteData()

            //msgValidation = ""
            if (msgValidation == "") {
                val intent = Intent(applicationContext, AgentRegisterTncActivity::class.java)
                intent.putExtra(COMPLETE_DATA, sendAgenRegistrationRequest)
                startActivityForResult(intent, 1234)
            }
            else {
                alertWarningCustom.getView().title_text?.text = "Peringatan"
                alertWarningCustom.getView().tv_message?.text = msgValidation

                alertWarningCustom.show()
            }


        }


        openDatePicker()
        initMenu()
        rv_drag_view_bank?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view_bank?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_drag_view_bank?.isNestedScrollingEnabled = false
        rv_list_alamat?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_list_alamat?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_list_alamat?.adapter = menuBottomAdapter
        rv_list_alamat?.isNestedScrollingEnabled = false

        et_upline_No?.setOnEditorActionListener { _, i, _ ->
            return@setOnEditorActionListener if (i == EditorInfo.IME_ACTION_SEARCH) {
                presenter.nipCheck(et_upline_No.text.toString())
                true
            } else {
                false
            }
        }

    }

    private fun initMenu() {
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_sesuai_ktp)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_domisili)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_kantor)))
    }

    private fun openDatePicker() {
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        et_date.setOnClickListener {
            if (personalInfoUser?.andalanCustomerId.isNullOrBlank()) {
                val datePickerDialog = DatePickerDialog(
                    this@AgentRegisterActivity, date, myCalendar
                        .get(Calendar.YEAR) - 10, myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)
                )
                datePickerDialog.show()
            }
        }
    }

    private fun updateLabel() {
        val myFormat = "dd/MM/yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        val tempDateText = sdf.format(myCalendar.time)
        et_date.setText(tempDateText.stringToDate("dd/MM/yyyy").dateToString(this.getMonths()))

        val myFormatChoosed = "yyyy-MM-dd"
        val sdfChoosed = SimpleDateFormat(myFormatChoosed, Locale.US)
        choosedDateOfBirth = sdfChoosed.format(myCalendar.time)
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        clearAllFocus()
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    private fun clearAllFocus() {
        hideKeyboard(this)
    }

    override fun onGetBank(bankResponse: BankResponse) {
        if (bankResponse.status == ConfigVar.SUCCESS) {
            bankResponse.bankList?.let {
                bankAdapter.removeAll()
                if (it.size > 0) {
                    bankAdapter.add(it)
                } else {
                    Toast.makeText(this, "Daftar Bank Kosong", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            Toast.makeText(this, "Gagal Mendapatkan Daftar Bank", Toast.LENGTH_SHORT).show()
        }
        hideKeyboard(this)
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onGetBankBranch(bankBranchResponse: BankBranchResponse) {
        if (bankBranchResponse.status == ConfigVar.SUCCESS) {
            bankBranchAdapter.removeAll()
            bankBranchResponse.bankBranchList?.let {
                if (it.size > 0) {
                    bankBranchAdapter.add(it)
                } else {
                    Toast.makeText(this, "Daftar Cabang Bank Kosong", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            Toast.makeText(this, "Gagal Mendapatkan Daftar Cabang Bank", Toast.LENGTH_SHORT).show()
        }
        hideKeyboard(this)
    }

    override fun onGetProvince(mutableList: MutableList<Location>) {
        choosedProvince = mutableList.find { it.name.equals(personalInfoUser?.provinceName, true) }
        presenter.getListCity(choosedProvince?.id?:"")
    }

    override fun onGetCity(mutableList: MutableList<Location>) {
        choosedCity = mutableList.find { it.name.equals(personalInfoUser?.cityName, true) }
        presenter.getListSubDistrict(choosedCity?.id?:"")
    }

    override fun onGetSubdistrict(mutableList: MutableList<Location>) {
        choosedDistrict = mutableList.find { it.name.equals(personalInfoUser?.districtName, true) }
        presenter.getListVillage(choosedDistrict?.id?:"")
    }

    override fun onGetVillage(mutableList: MutableList<Location>) {
        choosedVillage = mutableList.find { it.name.equals(personalInfoUser?.villageName, true) }
    }

    override fun onGetZipCode(zipCode: String) {
        autoZipCode = zipCode
    }

    override fun getResponseUpdatePersonalInfo(updateProfileResponse: UpdateAddressResponse) {
        if (updateProfileResponse.status == ConfigVar.SUCCESS) {
            if (personalInfoUser?.andalanCustomerId.isNullOrBlank()) {
                alertDialogCustom.getView().intro_text.text = getString(R.string.update_save)
                alertDialogCustom.show()
            } else {
                //btn_send?.text = "Kirim Perubahan"
            }


            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView {
                override fun onClickOk() {
                }

                override fun onClickCancel() {
                }

            }, this, getString(R.string.update_personal_info), updateProfileResponse.message?: "error")
        }
    }

    override fun getResponseUpdateAllAddress(updateAllAddressResponse: UpdateAddressResponse) {
        if (updateAllAddressResponse.status == ConfigVar.SUCCESS) {
            if (personalInfoUser?.andalanCustomerId == "") {
                alertDialogCustom.getView().intro_text.text = getString(R.string.update_save)
                alertDialogCustom.show()
            } else {
                alertDialogCustom.getView().intro_text.text = getString(R.string.update_sent)
                alertDialogCustom.show()
            }


            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView {
                override fun onClickOk() {
                }

                override fun onClickCancel() {
                }

            }, this, getString(R.string.update_personal_info), updateAllAddressResponse.message?: "error")
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            super.onBackPressed()
        }
    }

    private fun showImagePickerOptions(request: Int) {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent(request)
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent(request)
            }

        })
    }

    private fun launchCameraIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 800)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 800)

        startActivityForResult(intent, request)
    }

    private fun launchGalleryIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 800)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 800)

        startActivityForResult(intent, request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        e("tess", requestCode.toString())
        if (requestCode == REQUEST_IMAGE_BANK) {
            if (resultCode == Activity.RESULT_OK) {
                //val uploadButtonBank = bankImage?.findViewById<View>(R.id.btn_upload_photo2)
                val iv_preview = bankImage?.findViewById<ImageView>(R.id.iv_preview2)
                val tv_filename = bankImage?.findViewById<TextView>(R.id.tv_filename2)
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    choosedBankImage = it
                    loadImage(this, it, iv_preview!!)
                    loadImage(this@AgentRegisterActivity, it, iv_preview)
                    val file = File(it)
                    tv_filename!!.text = file.name
                }
            }
        }
        else if (requestCode == REQUEST_IMAGE_NPWP) {
            if (resultCode == Activity.RESULT_OK) {
                //val container_npwp = findViewById<View>(R.id.btn_upload_photo2)
                val iv_preview_npwp = npwpImage?.findViewById<ImageView>(R.id.iv_preview2)
                val tv_filename_npwp = npwpImage?.findViewById<TextView>(R.id.tv_filename2)
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    choosedNPWPImage = it
                    loadImage(this, it, iv_preview_npwp!!)
                    loadImage(this@AgentRegisterActivity, it, iv_preview_npwp)
                    val file = File(it)
                    tv_filename_npwp!!.text = file.name
                }
            }
        }

        if (requestCode == KTP_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            address = intentData?.getParcelable(KTP_DATA)
        } else if (requestCode == DOMICILE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressDomicile = intentData?.getParcelable(DOMICILE_DATA)
        } else if (requestCode == OFFICE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressOffice = intentData?.getParcelable(OFFICE_DATA)
        }
        else {
            val dataIntent = data?.extras
            val mainHomeActivity : MainHomeActivity = MainHomeActivity()
            if (dataIntent?.getBoolean(HOME, false) == true) {
                mainHomeActivity.moveViewNoAnim(0)
            } else if (dataIntent?.getBoolean(MESSAGE, false) == true) {
                mainHomeActivity.moveViewNoAnim(2)
            }
        }
    }

    override fun onGetBranch(branchListResponse: BranchListResponse) {
        branchAdapter.add(branchListResponse.branchList?: arrayListOf())
    }

    override fun onGetOccupation(list: MutableList<Occupation>) {
        occupationAdapter.add(list)
        if (occupationChoosed != null || occupationChoosed?.name?.isNotBlank() == true) {
            val item = list.find { it.name ==  occupationChoosed?.name }
            item?.let {
                occupationChoosed = it
            }
        }
    }

    override fun onNipCheck(nipCheckResponse: NipCheckResponse) {
        if (nipCheckResponse.status == "success") {
            et_upline_name.setText(nipCheckResponse.employeeName)
        }
        else {
            alertWarningCustom.getView().title_text?.text = "Peringatan"
            alertWarningCustom.getView().tv_message?.text = "NIK Tidak Ditemukan"

            alertWarningCustom.show()
        }
    }

}
