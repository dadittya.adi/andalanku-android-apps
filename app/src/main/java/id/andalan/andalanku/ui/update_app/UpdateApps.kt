package id.andalan.andalanku.ui.update_app

import android.content.ComponentName
import android.content.Intent
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.activity_update_apps.*
import kotlin.system.exitProcess


//import kotlinx.android.synthetic.main.activity_update_app.*

class UpdateApps : AppCompatActivity() {
    private var doubleBackToExitPressedOnce = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_apps)

        btn_update.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(
                    "https://play.google.com/store/apps/details?id=id.andalan.andalanku")
                setPackage("com.android.vending")
            }
            startActivity(intent)
        }

    }

    override fun onBackPressed() {
        //super.onBackPressed()

        if (doubleBackToExitPressedOnce) {
            moveTaskToBack(true)
            exitProcess(-1)
        }
        doubleBackToExitPressedOnce = true
        Toast.makeText(this, getString(R.string.press_back_to_exit), Toast.LENGTH_SHORT).show()

        Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }
}