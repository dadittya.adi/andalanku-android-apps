package id.andalan.andalanku.ui.installments


import androidx.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentPresenter
import id.andalan.andalanku.ui.payment.InstallmentPaymentView
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import kotlinx.android.synthetic.main.fragment_installment_schedule.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.layout_picker.*

class InstallmentScheduleFragment : androidx.fragment.app.Fragment(), InstallmentPaymentView {

    private lateinit var presenter: InstallmentPaymentPresenter
    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_installment_schedule, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.jadwal_angsuran)

        //presenter = InstallmentPaymentPresenter((activity as MainHomeActivity).services)
        //presenter.view = this

        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
        et_choosed_contract_number.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                et_choosed_contract_number?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)

                val intent = Intent(context, InstallmentScheduleDetailActivity::class.java)
                intent.putExtra(InstallmentScheduleDetailActivity.AGREEMENT_NUMBER, choosedPicker.name)
                intent.putExtra(InstallmentScheduleDetailActivity.LIST_AGREEMENT, pickerAdapter.getData().toTypedArray())
                startActivity(intent)
                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter

        val observerResponse = Observer<MutableList<PickerItem>> {
            pickerAdapter.removeAll()
            pickerAdapter.add(it?: arrayListOf())
        }
        (activity as MainHomeActivity).model.currentListAgreementNumber.observe(this, observerResponse)
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerAdapter.add(list)
            container_empty_state?.visibility = View.GONE
        } else {
            container_empty_state?.visibility = View.VISIBLE
        }
    }

    override fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse) {
        TODO("Not yet implemented")
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state?.visibility = View.VISIBLE
        Toast.makeText(context, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }
}
