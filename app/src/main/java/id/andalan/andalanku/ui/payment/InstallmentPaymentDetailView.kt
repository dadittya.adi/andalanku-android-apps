package id.andalan.andalanku.ui.payment

import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.ui.base.BaseView

interface InstallmentPaymentDetailView: BaseView {
    fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse)
}