package id.andalan.andalanku.ui.complain

import id.andalan.andalanku.model.response.ComplainListResponse
import id.andalan.andalanku.ui.base.BaseView

interface EComplainView: BaseView {
    fun getListResponse(complainListResponse: ComplainListResponse)
}