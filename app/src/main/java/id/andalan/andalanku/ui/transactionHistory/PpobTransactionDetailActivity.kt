package id.andalan.andalanku.ui.transactionHistory

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.response.PpobTransactionDetailResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.activity_ppob_general.*
import kotlinx.android.synthetic.main.activity_ppob_general.*
import kotlinx.android.synthetic.main.activity_ppob_general.sliding_layout
import kotlinx.android.synthetic.main.activity_ppob_transaction_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.ic_close
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobTransactionDetailActivity : BaseApp(), PpobTransactionDetailView {

    companion object {
        val TRANSACTION_ID = "transaction_id"
    }

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom


    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobTransactionDetailPresenter

    lateinit var ppobBillDetailCustomersAdapter: PpobTransactionDetailCustomersAdapter
    lateinit var ppobBillDetailBillAdapter: PpobTransactionDetailBillAdapter
    lateinit var ppobBillDetailProductAdapter: PpobTransactionDetailProductAdapter
    lateinit var ppobProductListAdapter: PpobProductListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_transaction_detail)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        val transactionId = intent?.extras?.getString(PpobTransactionDetailActivity.TRANSACTION_ID).toString()

        tv_title?.text = "DETAIL TRANSAKSI PPOB"

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this


        presenter.getTransactionDetail(transactionId)
        ppobBillDetailProductAdapter = PpobTransactionDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobTransactionDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobTransactionDetailBillAdapter()

    }

    override fun onGetDetailResponse(ppobTransactionDetailResponse: PpobTransactionDetailResponse) {

        if (ppobTransactionDetailResponse.status == ConfigVar.SUCCESS) {

            if (ppobTransactionDetailResponse.transactionStatus == true) {
                if (ppobTransactionDetailResponse.message.toString().contains("processed", true)) {
                    tv_pt_detail_status.text = "SEDANG DIPROSES"
                    tv_pt_detail_status.setTextColor(Color.BLUE)
                }
                else {
                    tv_pt_detail_status.text = "SUKSES"
                }
            }
            else {
                tv_pt_detail_status.text = "GAGAL"
                tv_pt_detail_status.setTextColor(Color.RED)
            }
            var info1Label = "ID Pelanggan"

            if (ppobTransactionDetailResponse.category.toString() == "Pulsa" ||
                ppobTransactionDetailResponse.category.toString() == "Paket Data" ||
                ppobTransactionDetailResponse.category.toString() == "Pascabayar") {
                info1Label = "Nomor HP"
            }
            else if (ppobTransactionDetailResponse.category.toString() == "Kartu Kredit"){
                info1Label = "Nomor Kartu"
            }
            else if (ppobTransactionDetailResponse.category.toString() == "Pembayaran Angsuran") {
                info1Label = "No Kontrak"
            }
            else if (ppobTransactionDetailResponse.category.toString() == "Voucher Game" ||
                ppobTransactionDetailResponse.category.toString() == "Streaming1") {
                info1Label = "Account ID"
            }
            else if (ppobTransactionDetailResponse.category.toString() == "BPJS") {
                info1Label = "No. BPJS"
            }

            tv_pt_detail_acc_number_label.text = info1Label

            tv_pt_detail_order_id.text = ppobTransactionDetailResponse.refNumber
            tv_pt_detail_date.text = ppobTransactionDetailResponse.getTransactionDate(getMonths())
            tv_pt_detail_acc_number.text = ppobTransactionDetailResponse.accountNumber
            tv_pt_detail_product.text = ppobTransactionDetailResponse.productName

            if (ppobTransactionDetailResponse.token != "") {
                tv_pt_detail_token.text = ppobTransactionDetailResponse.token
                if (ppobTransactionDetailResponse.type == "postpaid") {
                    tv_pt_detail_token_label.text = "INV Number"
                }
                else {
                    val category = ppobTransactionDetailResponse.category.toString()
                    if (category == "eMoney" || category == "Pulsa" || category == "Voucher Game" || category == "Paket Data") {
                        tv_pt_detail_token_label.text = "SN"
                    }
                }
            }
            else {
                tv_pt_detail_token.visibility = View.GONE
                tv_pt_detail_token_label.visibility = View.GONE
            }

            if (ppobTransactionDetailResponse.listBillCustomerDetails?.size ?: 0 > 0) {
                rv_pt_detail_customers.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailCustomersAdapter.removeAll()
                    ppobTransactionDetailResponse.listBillCustomerDetails?.let {
                        ppobBillDetailCustomersAdapter.add(
                            it
                        )
                    }

                    adapter = ppobBillDetailCustomersAdapter
                }
            }
            else {
                container_pt_detail_customer.visibility = View.GONE
            }

            if (ppobTransactionDetailResponse.listBillProductDetails?.size ?: 0 > 0 ) {
                rv_pt_detail_product.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailProductAdapter.removeAll()
                    ppobTransactionDetailResponse.listBillProductDetails?.let {
                        ppobBillDetailProductAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailProductAdapter
                }
            }
            else {
                container_pt_detail_product.visibility = View.GONE
            }

            if (ppobTransactionDetailResponse.listBillBillDetails?.size ?: 0 > 0) {
                rv_pt_detail_bills.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailBillAdapter.removeAll()
                    ppobTransactionDetailResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                    adapter = ppobBillDetailBillAdapter
                }
            }
            else {
                container_pt_detail_bills.visibility = View.GONE
            }

            tv_pt_detail_amount.text = ppobTransactionDetailResponse.amount?.toLong()?.convertToRpFormat()
        }
        else {
            alertWarningCustom.getView().tv_message?.text = "Gagal Mendapatkan Data"
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun showWait() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage ?: "", Toast.LENGTH_SHORT).show()
    }

    fun initProductItem() {
        rv_ppob_product_list.apply {
            layoutManager = GridLayoutManager(this@PpobTransactionDetailActivity, 1)
            adapter = ppobProductListAdapter
        }
    }

}