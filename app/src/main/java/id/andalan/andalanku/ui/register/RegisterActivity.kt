package id.andalan.andalanku.ui.register

import android.content.Intent
import android.os.Bundle
import android.util.Log.e
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.RegisterRequest
import id.andalan.andalanku.model.response.RegisterResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.pin.CreatePinActivity
import id.andalan.andalanku.utils.AlertDialogUtil
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject
import android.view.View

class RegisterActivity : BaseApp(), RegisterView, AlertDialogUtil.AlertDialogView {
    val TAG = RegisterActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: RegisterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        deps.inject(this)
        presenter.view = this

        setSupportActionBar(toolbar_login)
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.GONE
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.create_account)

        btn_register.setOnClickListener {
            val name = et_name?.text.toString()
            val idNumber = et_ktp?.text.toString()
            val email = et_email?.text.toString()
            val password = et_password?.text.toString()
            val phoneNumber = et_phone?.text.toString()
            val request = RegisterRequest(
                customerName = name,
                idNumber = idNumber,
                email = email,
                password = password,
                phoneNumber = phoneNumber
            )

            if (!onCheckFormRegistration(request)) {
                presenter.doRegister(request)
            }
        }
        btn_sign_in.setOnClickListener {
            startActivity(Intent(this@RegisterActivity, LoginActivity::class.java))
            finish()
        }
    }

    private fun onCheckFormRegistration(registerRequest: RegisterRequest): Boolean {
        var isError = false

        if (registerRequest.customerName?.isBlank() == true) {
            isError = true
            til_name.isErrorEnabled = true
            til_name.error = getString(R.string.blank_name)
        }
        if (registerRequest.idNumber?.isBlank() == true) {
            isError = true
            til_ktp.isErrorEnabled = true
            til_ktp.error = getString(R.string.blank_id_number)
        }
        if (registerRequest.email?.isBlank() == true) {
            isError = true
            til_email.isErrorEnabled = true
            til_email.error = getString(R.string.blank_email)
        }
        if (registerRequest.password?.isBlank() == true) {
            isError = true
            til_password.isErrorEnabled = true
            til_password.error = getString(R.string.blank_password)
        }
        if (registerRequest.phoneNumber?.isBlank() == true) {
            isError = true
            til_phone.isErrorEnabled = true
            til_phone.error = getString(R.string.blank_phone)
        }

        return isError
    }

    override fun onGetResponse(registerResponse: RegisterResponse) {
        e(TAG, registerResponse.message?: "error")
        if (registerResponse.status == ConfigVar.SUCCESS) {
            preferencesUtil.putIdNumber(registerResponse.idCustomer?:0)
            AlertDialogUtil.buildDefaultAlertDialog(this, this, getString(R.string.register_account), registerResponse.message?: "error")
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView {
                override fun onClickOk() {
                }

                override fun onClickCancel() {
                }

            }, this, getString(R.string.register_account), registerResponse.message?: "error")
        }
    }

    override fun showWait() {
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.VISIBLE
    }

    override fun removeWait() {
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.GONE
    }

    override fun onFailure(appErrorMessage: String?) {
        e(TAG, appErrorMessage ?: "error")
    }

    override fun onClickOk() {
        startActivity(Intent(this, CreatePinActivity::class.java))
        finish()
    }

    override fun onClickCancel() {
    }
}
