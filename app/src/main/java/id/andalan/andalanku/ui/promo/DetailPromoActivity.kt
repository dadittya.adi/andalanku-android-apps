package id.andalan.andalanku.ui.promo

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.DetailPromoResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.dana.SimulationDanaActivity
import id.andalan.andalanku.ui.inbox.DetailPromoPresenter
import id.andalan.andalanku.ui.newcar.CarChooserActivity
import id.andalan.andalanku.ui.newcar.PaymentNewCarActivity
import id.andalan.andalanku.ui.usedcar.UsedCarActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.convertStringToHTML
import id.andalan.andalanku.utils.ekstensions.loadImage
import id.andalan.andalanku.utils.getMonthsShort
import kotlinx.android.synthetic.main.activity_detail_promo.*
import kotlinx.android.synthetic.main.footer_share.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import javax.inject.Inject

class DetailPromoActivity : BaseApp(), DetailPromoView {
    companion object {
        val PROMOSLUG = "promoSlug"
    }

    lateinit var detailPromo: DetailPromoResponse
    @Inject
    lateinit var presenter: DetailPromoPresenter
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_promo)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        val slug: String = intent?.extras?.getString(PROMOSLUG)?:""

        deps.inject(this)
        presenter.view = this
            presenter.getPromoDetail(slug)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.detail_promo)

        btn_share.setOnClickListener{
            val shareIntent = Intent(Intent.ACTION_SEND)
            val webBasedUrl = preferencesUtil.getWebUrl()
            shareIntent.type = "text/plain"

            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bagikan Promo Ini")

            val appUrl =
                "$webBasedUrl/promo/$slug"

            shareIntent.putExtra(Intent.EXTRA_TEXT, appUrl)

            startActivity(Intent.createChooser(shareIntent, "Share via"))
        }
    }

    override fun onGetDetailPromo(detailPromoResponse: DetailPromoResponse) {
        if (detailPromoResponse.status == ConfigVar.SUCCESS) {
            loadImage(this, detailPromoResponse.image?:"", iv_promo)
            tv_title_promo.text = detailPromoResponse.title
            //val temp = "<p>Andalanku.id menawarkan program menarik khusus bagi konsumen yang mengajukan fasilitas multiguna dengan jaminan BPKB mobil yaitu dengan nama DNA ASLI, yaitu program bunga rendah sebesar 0,7% perbulan untuk tenor satu tahun. </p><p><br></p><p>Penawaran ini akan berlangsung sampai dengan bulan Desember 2019 dimana calon konsumen bisa mengajukan fasilitas tersebut dengan cara mendatangi kantor cabang Andalan Finance terdekat ataupun melalui Aplikasi Andalanku.id. Untuk informasi lebih lengkapnya silahkan hubungi Call Center kami di 1500 995 atau WA 0853 2 1500 995. Jadi tunggu apa lagi? </p><p><br></p><a href=\"google.com\"><p>Ajukan Sekarang!</p></a><p><br></p><p><br></p><p><br></p>"
            //tv_content.text = detailPromoResponse.content?.convertStringToHTML()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_content.text = Html.fromHtml(detailPromoResponse.contentHtml, Html.FROM_HTML_MODE_COMPACT)
            } else {
                tv_content.text = Html.fromHtml(detailPromoResponse.contentHtml)
            }
            val txtPeriode =  detailPromoResponse.getDateStartPromoUI(this.getMonthsShort()) + " - " +
                    detailPromoResponse.getDateEndPromoUI(this.getMonthsShort())
            tv_promo_periode.text = txtPeriode

            Log.d("PromoType", detailPromoResponse.type.toString())
            if (detailPromoResponse.type.isNullOrEmpty()) {
                btn_ajukan.visibility = View.GONE
            }
            else{
                btn_ajukan.setOnClickListener {
                    val promoTypeName = detailPromoResponse.type
                    if (promoTypeName == "NEW"){
                        val intentNewCar = Intent(this@DetailPromoActivity, PaymentNewCarActivity::class.java)
                        startActivity(intentNewCar)
                    }
                    else if (promoTypeName == "USED"){
                        val intentUsedCar = Intent(this@DetailPromoActivity, UsedCarActivity::class.java)
                        startActivity(intentUsedCar)
                    }
                    else if (promoTypeName == "DANA"){
                        val intentDana = Intent(this@DetailPromoActivity, SimulationDanaActivity::class.java)
                        startActivity(intentDana)
                    }
                }
            }
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {

    }

}
