package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.item_ppob_sub_product.view.*
import java.lang.NullPointerException

class PpobSubProductListAdapter(val onItemClickListener: OnClickItem): BaseAdapter<PpobProduct, PpobSubProductListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_sub_product
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobProduct>(itemView) {
        override fun bind(data: PpobProduct) {
            itemView.btn_ppob_sub_product_price.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }

            val productPrice = data.amount?.toLong()?.convertToRpFormat()
            itemView.tv_ppob_sub_product_name?.text = "Top Up " + data.name
            itemView.btn_ppob_sub_product_price?.text = productPrice
        }

    }

    interface OnClickItem {
        fun onClickMenu(ppobProduct: PpobProduct)
    }
}