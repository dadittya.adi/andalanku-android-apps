package id.andalan.andalanku.ui.partner

import android.annotation.TargetApi
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log.e
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebViewClient
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.activity_partner_detail.*
import kotlinx.android.synthetic.main.toolbar_login.*
import android.webkit.WebView
import android.widget.ProgressBar



class PartnerDetailActivity : BaseApp() {
    companion object {
        val LINK_PARTNER = "link_partner"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_partner_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.partner_andalanku)

        val url = intent?.extras?.getString(LINK_PARTNER, "")

        wb_partner.settings.javaScriptEnabled = true
        wb_partner.webViewClient = AppWebViewClients(pb_loading)
        wb_partner.loadUrl(url)
    }

    inner class AppWebViewClients(private val progressBar: ProgressBar) : WebViewClient() {

        init {
            progressBar.visibility = View.VISIBLE
        }


        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                view?.loadUrl(request?.url.toString())
            }
            return super.shouldOverrideUrlLoading(view, request)
        }

        @Suppress("OverridingDeprecatedMember")
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            progressBar.visibility = View.GONE
        }

    }
}
