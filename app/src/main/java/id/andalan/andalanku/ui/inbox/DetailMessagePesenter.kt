package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.response.DetailInboxResponse
import id.andalan.andalanku.model.response.InboxResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class DetailMessagePesenter @Inject constructor(private val service: Services): BasePresenter<DetailMessageView>() {
    override fun onCreate() {
    }

    fun getDetailMessage(idMessage: String) {
        view.showWait()
        val subscription = service.getDetailMessage(idMessage).subscribe(object : Subscriber<DetailInboxResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailInboxResponse: DetailInboxResponse) {
                view.onGetDetailMessage(detailInboxResponse)
            }
        })
        subscriptions.add(subscription)
    }
}