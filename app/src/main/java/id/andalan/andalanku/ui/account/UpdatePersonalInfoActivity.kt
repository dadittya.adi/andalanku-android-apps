@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.account

import android.app.Activity
import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.preferences.PreferencesUtil
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_update_personal_info.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import id.andalan.andalanku.model.request.SendAllAddressRequest
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.UpdateAddressResponse
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.address.UpdateAddressAccordingDomicileActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity.Companion.IS_SETTING_ACCOUNT
import id.andalan.andalanku.ui.address.UpdateAddressAccordingWorkPlaceActivity
import id.andalan.andalanku.ui.agent.AgentRegisterActivity
import id.andalan.andalanku.ui.agent.AgentRegisterTncActivity
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.DOMICILE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.DOMICILE_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.ISREFERENSI
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.IS_COMPLETE_DATA_FORM
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.OFFICE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.OFFICE_REQ
import id.andalan.andalanku.utils.*
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.form_upload_image.*
import kotlinx.android.synthetic.main.form_upload_image2.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.item_menu_home.view.*
import kotlinx.android.synthetic.main.item_menu_home.view.title_text
import kotlinx.android.synthetic.main.item_upload_document.*
import kotlinx.android.synthetic.main.layout_succes_send_update.view.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.layout_success.view.btn_ok
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.top_drag_view.*
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.io.IOException
import java.util.*
import java.text.SimpleDateFormat

class UpdatePersonalInfoActivity : BaseApp(), UpdatePersonalInfoView {
    override fun successUpdateAddress(updateAddressResponse: UpdateAddressResponse) {
        EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
    }

    companion object {
        val IS_ACCOUNT_SETTING = "is_account_setting"
        val PROFILE_IMAGE = 512
        val REQUEST_IMAGE_BANK = 1003
        val REQUEST_IMAGE_NPWP = 1004
    }
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: UpdatePersonalInfoPresenter

    private lateinit var listBankAdapter: PickerAdapter
    private var bankChoosed = PickerItem()
    private val myCalendar = Calendar.getInstance()
    private lateinit var alertDialogCustom: AlertDialogCustom
    //private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var menuBottomAdapter: MenuAdapterProfile

    private var personalInfoUser: PersonalInformationResponse? = null

    private var choosedProvince: Location? = null
    private var choosedDistrict: Location? = null
    private var choosedCity: Location? = null
    private var choosedVillage: Location? = null
    private var choosedImage = ""
    private var choosedBankImage = ""
    private var choosedNPWPImage = ""
    private var choosedDateOfBirth = ""
    private var avatar = ""
    private var autoZipCode = ""

    private var address: UpdateAddressRequest? = null
    private var addressDomicile: UpdateAddressRequest? = null
    private var addressOffice: UpdateAddressRequest? = null

    private var formatedDob: String = ""
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        setContentView(R.layout.activity_update_personal_info)

        tv_hint_photo_ktp?.text = "Upload Foto Cover Buku Tabungan"

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()



        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        //val loadView = findViewById<View>(R.id.loadingPanel)
        //loadView.visibility = View.GONE
        val isAccountSetting = intent?.extras?.getBoolean(IS_ACCOUNT_SETTING, false)
        if (isAccountSetting == true) {
            tv_title.text = getString(R.string.setting_account)

            container_layout_image?.visibility = View.VISIBLE
//            container_bottom_setting?.visibility = View.VISIBLE
        } else {
            tv_title.text = getString(R.string.update_personal_info)
        }

        /*alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)*/

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView()
        alertDialogCustom.create()
        alertDialogCustom.getView().btn_close.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra(MyAccountFragment.UPDATED_INFO, true)
            setResult(MyAccountFragment.RES_CODE_UPDATED_INFO, returnIntent)
            finish()
            alertDialogCustom.dismiss()
        }
        alertDialogCustom.getView().btn_back_to_home.setOnClickListener {
            val returnIntent = Intent()
            returnIntent.putExtra(MyAccountFragment.UPDATED_INFO, true)
            setResult(MyAccountFragment.RES_CODE_UPDATED_INFO, returnIntent)
            finish()
            alertDialogCustom.dismiss()
        }
        alertDialogCustom.getView().sent_text.text = getString(R.string.information)

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        menuBottomAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.alamat_sesuai_ktp)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingKtpActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(ISREFERENSI, false)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(IS_SETTING_ACCOUNT, true)
                    startActivityForResult(intent, KTP_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_domisili)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingDomicileActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(ISREFERENSI, false)
                    intent.putExtra(DOMICILE_DATA, addressDomicile)
                    startActivityForResult(intent, DOMICILE_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_kantor)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingWorkPlaceActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(OFFICE_DATA, addressOffice)
                    intent.putExtra(ISREFERENSI, false)
                    startActivityForResult(intent, OFFICE_REQ)
                }
            }
        })

        listBankAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = listBankAdapter.getData().indexOf(bankChoosed)

                bankChoosed.isChoosed = false
                if (index > -1) listBankAdapter.update(index, bankChoosed)
                et_choose_bank?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                bankChoosed = pickerItem
                val indexChoosed = listBankAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) listBankAdapter.update(indexChoosed, bankChoosed)

                hideSlideLayout()
            }
        })

        et_choose_bank?.setOnClickListener {
            tv_title_drag_view?.text = "Pilih Bank"
            rv_drag_view?.adapter = listBankAdapter
            et_drag_view?.visibility = View.GONE
            showSlideLayout()
        }

        btn_upload_photo?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_BANK)
        }

        btn_upload_photo2?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_NPWP)
        }

        /*btn_upload_photo?.setOnClickListener {
            showImagePickerOptions(UpdateAddressAccordingKtpActivity.REQUEST_IMAGE_KTP)
        }*/

        btn_edit_photo?.setOnClickListener {
            showImagePickerOptions(PROFILE_IMAGE)
        }

        personalInfoUser = preferencesUtil.getProfile()
        //val user = preferencesUtil.getUser()

        et_choose_bank?.setText(personalInfoUser?.bankName)
        et_account_number?.setText(personalInfoUser?.bankAccountNumber)
        et_in_the_name?.setText(personalInfoUser?.bankAccountHolder)
        address = personalInfoUser?.getAddressLegal()
        addressDomicile = personalInfoUser?.getAddressDomicile()
        addressOffice = personalInfoUser?.getAddressOffice()

        if (!personalInfoUser?.bankAccountImage.isNullOrBlank()) {
            val container = findViewById<View>(R.id.btn_upload_photo)
            val iv_preview = container.findViewById<ImageView>(R.id.iv_preview)
            val tv_filename = container.findViewById<TextView>(R.id.tv_filename)

            loadImage(this@UpdatePersonalInfoActivity, personalInfoUser?.bankAccountImage?:"", iv_preview)
            tv_filename.text = getString(R.string.change_image)
            choosedImage = address?.idCardImage?:""
            address?.typeIdCardImage = "url"
        }

        if (!personalInfoUser?.fotoNpwp.isNullOrBlank()) {
            val container_npwp = findViewById<View>(R.id.btn_upload_photo2)
            val iv_preview_npwp = container_npwp.findViewById<ImageView>(R.id.iv_preview2)
            val tv_filename_npwp = container_npwp.findViewById<TextView>(R.id.tv_filename2)
            loadImage(this@UpdatePersonalInfoActivity, personalInfoUser?.fotoNpwp?:"", iv_preview_npwp)
            tv_filename_npwp.text = getString(R.string.change_image)
            choosedImage = address?.npwp_image?:""
            //address?.typeIdCardImage = "url"
        }

        disableForm()

        if (isAccountSetting == true) {
            btn_send?.text = getString(R.string.capital_update_personal_info)
        } else {
            if (personalInfoUser?.andalanCustomerId.isNullOrBlank()) {
                btn_send?.text = "Simpan"
            } else {
                btn_send?.text = "Kirim Perubahan"
            }
            //isPembaruanDiri()
        }
        et_name?.setText(personalInfoUser?.customerName)
        if (personalInfoUser?.gender == "M" || personalInfoUser?.gender == "L") {
            rg_container.check(radio_male.id)
        }
        else if (personalInfoUser?.gender == "P" || personalInfoUser?.gender == "F") {
            rg_container.check(radio_female.id)
        }
        et_pob?.setText(personalInfoUser?.placeOfBirth)
        et_date?.setText(personalInfoUser?.getDOBUI(this.getMonths()))
        choosedDateOfBirth = personalInfoUser?.dateOfBirth?:""
        et_ktp?.setText(personalInfoUser?.idNumber)
        et_home_phone?.setText(personalInfoUser?.homePhoneNumber)
        et_phone?.setText(personalInfoUser?.phoneNumber)
        et_label_home_phone?.setText(personalInfoUser?.areaCode)
        et_npwp?.setText(personalInfoUser?.npwp)
        loadProfile(personalInfoUser?.avatar)
        //rg_container?.check(0)

        cb_is_karyawan

        cb_is_karyawan?.setOnCheckedChangeListener(null)

        cb_is_karyawan?.isChecked = personalInfoUser?.nip?.isNotBlank() == true
        til_nip?.visibility = if (personalInfoUser?.nip?.isNotBlank() == true) {
            View.VISIBLE
        } else {
            View.GONE
        }
        et_nip?.setText(personalInfoUser?.nip)

        cb_is_karyawan?.setOnCheckedChangeListener { _, isChecked ->
            til_nip?.visibility = if (isChecked) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        btn_send?.setOnClickListener {
            val name = et_name?.text.toString()
            val selectedGender = rg_container?.checkedRadioButtonId
            val selectedGenderRadio: RadioButton? = findViewById(selectedGender?:0)
            val gender = selectedGenderRadio?.tag.toString()
            val pob = et_pob?.text.toString()
            //val date = et_date?.text.toString()
            val ktp = et_ktp?.text.toString()
            //val phoneNumber = et_home_phone?.text.toString()
            //val mobileNumber = et_phone?.text.toString()
            val npwp = et_npwp?.text.toString()
            //val nip = if (cb_is_karyawan?.isChecked == true) et_nip?.text.toString() else ""
//            val containerBank = findViewById<View>(R.id.container_image)
            val bankName = et_choose_bank?.text.toString()
            val bankAccountNumber = et_account_number?.text.toString()
            val bankAccountHolder = et_in_the_name?.text.toString()

            //val postalCode = personalInfoUser?.zipCode
            //val fullAddress = personalInfoUser?.address //val rt = personalInfoUser?.rt
            //val rw = personalInfoUser?.rw


            val imageIdCard = address?.idCardImage
            var idCardImageType = ""
            if (imageIdCard?.isBlank() == false) {
                if (imageIdCard.take(4).equals("http")) {
                    idCardImageType = "url"
                }
                else {
                    idCardImageType = "file"
                }
            }
            val allDataAddress = SendAllAddressRequest(
                imageIdCard = address?.idCardImage,
                typeImageIdCard = idCardImageType,
                provinceId = address?.provinceId,
                cityId = address?.regencyId,
                districtId = address?.districtId,
                villageId = address?.villageId,
                address = address?.address,
                zipCode = address?.zipCode,
                rt = address?.rt,
                rw = address?.rw,
                provinceIdDomicile = addressDomicile?.provinceId,
                cityIdDomicile = addressDomicile?.regencyId,
                districtIdDomicile = addressDomicile?.districtId,
                villageIdDomicile = addressDomicile?.villageId,
                addressDomicile = addressDomicile?.address,
                rtDomicile = addressDomicile?.rt,
                rwDomicile = addressDomicile?.rw,
                zipCodeDomicile = addressDomicile?.zipCode,
                provinceIdOffice = addressOffice?.provinceId,
                cityIdOffice = addressOffice?.regencyId,
                districtIdOffice = addressOffice?.districtId,
                villageIdOffice = addressOffice?.villageId,
                addressOffice = addressOffice?.address,
                rtOffice = addressOffice?.rt,
                rwOffice = addressOffice?.rw,
                zipCodeOffice = addressOffice?.zipCode,
                areaCode = et_label_home_phone?.text.toString(),
                phone = et_home_phone?.text.toString(),
                mobilePhone = et_phone?.text.toString(),
                //idCustomer = user?.idCustomer,
                gender = gender,
                npwp = npwp,
                npwp_image = choosedNPWPImage,
                avatar = avatar,
                bankName = bankName,
                bankAccountNumber = bankAccountNumber,
                bankAccountHolder = bankAccountHolder,
                birthPlace = pob,
                birthDate = choosedDateOfBirth,
                idCardNumber = ktp,
                customer_name = name,
                bankAccountImage = choosedBankImage
            )

            var msgValidation = allDataAddress.validFormCompleteData()

            if (msgValidation == "") {
                presenter.sendUpdatedAddress(allDataAddress)
            }
            else {
                alertWarningCustom.getView().title_text?.text = "Peringatan"
                alertWarningCustom.getView().tv_message?.text = msgValidation

                alertWarningCustom.show()
            }
        }

        openDatePicker()

        initMenu()

        rv_drag_view?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
//        rv_drag_view?.adapter = addressAdapter
        rv_drag_view?.isNestedScrollingEnabled = false

        rv_list_alamat?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_list_alamat?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_list_alamat?.adapter = menuBottomAdapter
        rv_list_alamat?.isNestedScrollingEnabled = false
    }

    private fun disableForm () {
        if (!personalInfoUser?.andalanCustomerId.isNullOrBlank()) {
            et_name?.isFocusableInTouchMode = false
            et_name?.isFocusable = false
            rg_container?.isFocusableInTouchMode = false
            rg_container?.isFocusable = false
            et_pob?.isFocusableInTouchMode = false
            et_pob?.isFocusable = false
            et_ktp?.isFocusableInTouchMode = false
            et_ktp?.isFocusable = false
            for (i in 0 until rg_container.childCount) {
                rg_container.getChildAt(i).isEnabled = false
            }
        }
    }

    private fun isPembaruanDiri () {
        container_mid_3?.visibility = View.GONE
        container_mid_2?.visibility = View.GONE
        container_bottom?.visibility = View.GONE
    }
    private fun initMenu() {
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_sesuai_ktp)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_domisili)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_kantor)))
        listBankAdapter.add(PickerItem(id = "bca", name = "BCA"))
        listBankAdapter.add(PickerItem(id = "mandiri", name = "MANDIRI"))
        listBankAdapter.add(PickerItem(id = "cimb_niaga", name = "CIMB"))
    }

    private fun openDatePicker() {
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        et_date.setOnClickListener {
            if (personalInfoUser?.andalanCustomerId.isNullOrBlank()) {
                val datePickerDialog = DatePickerDialog(
                    this@UpdatePersonalInfoActivity, date, myCalendar
                        .get(Calendar.YEAR) - 10, myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)
                )
                datePickerDialog.show()
            }
        }
    }

    private fun updateLabel() {
        val myFormat = "dd/MM/yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        val tempDateText = sdf.format(myCalendar.time)
        et_date.setText(tempDateText.stringToDate("dd/MM/yyyy").dateToString(this.getMonths()))

        val myFormatChoosed = "yyyy-MM-dd"
        val sdfChoosed = SimpleDateFormat(myFormatChoosed, Locale.US)
        choosedDateOfBirth = sdfChoosed.format(myCalendar.time)
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        clearAllFocus()
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    private fun clearAllFocus() {
    }

    override fun onGetProvince(mutableList: MutableList<Location>) {
        choosedProvince = mutableList.find { it.name.equals(personalInfoUser?.provinceName, true) }
        presenter.getListCity(choosedProvince?.id?:"")
    }

    override fun onGetCity(mutableList: MutableList<Location>) {
        choosedCity = mutableList.find { it.name.equals(personalInfoUser?.cityName, true) }
        presenter.getListSubDistrict(choosedCity?.id?:"")
    }

    override fun onGetSubdistrict(mutableList: MutableList<Location>) {
        choosedDistrict = mutableList.find { it.name.equals(personalInfoUser?.districtName, true) }
        presenter.getListVillage(choosedDistrict?.id?:"")
    }

    override fun onGetVillage(mutableList: MutableList<Location>) {
        choosedVillage = mutableList.find { it.name.equals(personalInfoUser?.villageName, true) }
    }

    override fun onGetZipCode(zipCode: String) {
        autoZipCode = zipCode
    }

    override fun getResponseUpdatePersonalInfo(updateProfileResponse: UpdateAddressResponse) {
        if (updateProfileResponse.status == ConfigVar.SUCCESS) {
            if (personalInfoUser?.andalanCustomerId.isNullOrBlank()) {
                alertDialogCustom.getView().intro_text.text = getString(R.string.update_save)
                alertDialogCustom.show()
            } else {
                //btn_send?.text = "Kirim Perubahan"
            }


            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView {
                override fun onClickOk() {
                }

                override fun onClickCancel() {
                }

            }, this, getString(R.string.update_personal_info), updateProfileResponse.message?: "error")
        }
    }

    override fun getResponseUpdateAllAddress(updateAllAddressResponse: UpdateAddressResponse) {
        if (updateAllAddressResponse.status == ConfigVar.SUCCESS) {
            if (personalInfoUser?.andalanCustomerId == "") {
                alertDialogCustom.getView().intro_text.text = getString(R.string.update_save)
                alertDialogCustom.show()
            } else {
                alertDialogCustom.getView().intro_text.text = getString(R.string.update_sent)
                alertDialogCustom.show()
            }


            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView {
                override fun onClickOk() {
                }

                override fun onClickCancel() {
                }

            }, this, getString(R.string.update_personal_info), updateAllAddressResponse.message?: "error")
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }


    override fun onFailure(appErrorMessage: String?) {
    }

    private fun loadProfile(url: String?) {
        if (url.isNullOrBlank()) {
            GlideApp.with(this).load(this.resources.getIdentifier("ic_person", "drawable", this.packageName))
                .into(iv_photo_profile)
        } else {
            GlideApp.with(this).load(url)
                .into(iv_photo_profile)
        }
//        imgProfile.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent))
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            super.onBackPressed()
        }
    }

    private fun showImagePickerOptions(request: Int) {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent(request)
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent(request)
            }

        })
    }

    private fun launchCameraIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 800)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 800)

        startActivityForResult(intent, request)
    }

    private fun launchGalleryIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 800)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 800)

        startActivityForResult(intent, request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        e("tess", requestCode.toString())
        val container = findViewById<View>(R.id.btn_upload_photo)
        val iv_preview = container.findViewById<ImageView>(R.id.iv_preview)
        val tv_filename = container.findViewById<TextView>(R.id.tv_filename)
        if (requestCode == UpdateAddressAccordingKtpActivity.REQUEST_IMAGE_KTP) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    choosedImage = it
                    loadImage(this@UpdatePersonalInfoActivity, it, iv_preview)
                    val file = File(it)
                    tv_filename.text = file.name
                }
            }
        } else if (requestCode == PROFILE_IMAGE && resultCode == Activity.RESULT_OK) {
            val uri = data?.getParcelableExtra<Uri>("path")
            uri?.path?.let {
                avatar = it
            }
            try {
                loadProfile(uri.toString())
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        else if (requestCode == REQUEST_IMAGE_BANK) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    choosedBankImage = it
                    loadImage(this, it, iv_preview)
                    loadImage(this@UpdatePersonalInfoActivity, it, iv_preview)
                    val file = File(it)
                    tv_filename.text = file.name
                }
            }
        }
        else if (requestCode == REQUEST_IMAGE_NPWP) {
            if (resultCode == Activity.RESULT_OK) {
                val container_npwp = findViewById<View>(R.id.btn_upload_photo2)
                val iv_preview_npwp = container_npwp.findViewById<ImageView>(R.id.iv_preview2)
                val tv_filename_npwp = container_npwp.findViewById<TextView>(R.id.tv_filename2)
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    choosedNPWPImage = it
                    loadImage(this, it, iv_preview_npwp)
                    loadImage(this@UpdatePersonalInfoActivity, it, iv_preview_npwp)
                    val file = File(it)
                    tv_filename_npwp.text = file.name
                }
            }
        }

        if (requestCode == KTP_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            address = intentData?.getParcelable(KTP_DATA)
        } else if (requestCode == DOMICILE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressDomicile = intentData?.getParcelable(DOMICILE_DATA)
        } else if (requestCode == OFFICE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressOffice = intentData?.getParcelable(OFFICE_DATA)
        }
    }

}
