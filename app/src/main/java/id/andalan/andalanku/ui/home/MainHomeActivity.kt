package id.andalan.andalanku.ui.home

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.tabs.TabLayout
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessaging
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.BuildConfig
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.request.EventBus
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_AGGREMENT_LIST
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_INBOX
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_PERSONAL_INFO
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_UNREAD_MESSAGE
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.CreditSimulationO
import id.andalan.andalanku.model.ui.Inbox
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.onboarding.OnboardStart
import id.andalan.andalanku.ui.update_app.UpdateApps
import id.andalan.andalanku.ui.viewmodel.ViewModelMain
import kotlinx.android.synthetic.main.activity_main_home.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

//private lateinit var firebaseAnalytics: FirebaseAnalytics

class MainHomeActivity : BaseApp(), MainHomeView {

    override fun onGetLegalDocument(legalDocumentResponse: LegalDocumentResponse) {
        preferencesUtil.putLegalDocument(legalDocumentResponse)
    }

    override fun onGetCashValue(cashValueResponse: CashValueResponse) {
        model.currentCashValue.value = cashValueResponse
    }

    override fun onGetAgent(agentResponse: AgentResponse) {
        preferencesUtil.putAgentCode(agentResponse)
    }

    override fun onGetUnreadMessage(unreadMessageResponse: UnreadMessageResponse) {
        model.currentUnreadMessage.value = unreadMessageResponse
        //var status = ""
        //status = unreadMessageResponse.status!!
        val unreadMessage = unreadMessageResponse.unreadCount!!

        if (unreadMessage > 0) {
            setUnreadIcon()
        }
        else {
            setReadIcon()
        }

    }

    private lateinit var mainViewPagerAdapter: MainViewPagerAdapter
    private var lastPosition = 0

    val TAG = MainHomeActivity::class.java.simpleName

    companion object {

        private val TAGS = "LocationProvider"
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
        val EVENT_SENDER = "event_sender"
    }
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: MainHomePresenter

    var simulation: CreditSimulationO? = null
    var collateral: Collateral? = null
    var creditSimulationRequest: CreditSimulationRequest? = null
    lateinit var model: ViewModelMain
    var isReferensikan = false

    // selected
    var selectedMessage: Inbox = Inbox()
    var detailInboxResponse: DetailInboxResponse? = null
    var apiUrl: String = ""

    private var PRIVATE_MODE: Int = 0
    private val PREF_NAME: String = "firstStart"

    private var mFusedLocationClient: FusedLocationProviderClient? = null

    protected var mLastLocation: Location? = null

    private var latestAppVersion: Int = 0
    private var isInboxSelected: Boolean = false
    private var isUnreadMessage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main_home)

        //val window: Window = activity.getWindow()

        // make full transparent statusBar

        // make full transparent statusBar

            var windowManager = WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
            windowManager = windowManager or WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
            setWindowFlag(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
        try {
            window.statusBarColor = Color.BLACK
        } catch (e: Exception) {
        }

        org.greenrobot.eventbus.EventBus.getDefault().register(this)
        deps.inject(this)
        presenter.view = this
        model = ViewModelProviders.of(this).get(ViewModelMain::class.java)
        presenter.onCreate()

        //preferencesUtil.putFirstOpen(true);
        Log.i(TAG, "First Open = " + preferencesUtil.getFirstOpen().toString())
        if ((preferencesUtil.getFirstOpen()) == true) {
            preferencesUtil.putFirstOpen(false)
            val intentOnboard = Intent(this@MainHomeActivity, OnboardStart::class.java)
            startActivity(intentOnboard)
        }

        presenter.getLatestAppVersion()
        presenter.getUrlList()

        if ((preferencesUtil.getUser()?.idCustomer ?: 0) > 0) {
            presenter.getAgreementNumber()
            presenter.getPersonalInfo()
            presenter.getAgentCode()
            presenter.getListInbox("1")
            presenter.getCashValue()
            FirebaseMessaging.getInstance().subscribeToTopic("ALL_REGISTERED_USER")
            //FirebaseMessaging.getInstance().subscribeToTopic("TESTING");
            //presenter.getUnreadMessage()
        }

        presenter.getLegalDocument()

        mainViewPagerAdapter = MainViewPagerAdapter(supportFragmentManager)
        container.adapter = mainViewPagerAdapter
        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                if (lastPosition == 8 || lastPosition == 10) {
                    moveViewNoAnim(0)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                //tab.icon?.colorFilter = PorterDuffColorFilter(ContextCompat.getColor(this@MainHomeActivity ,R.color.brown_grey), PorterDuff.Mode.SRC_IN)
            }

            override fun onTabSelected(tab: TabLayout.Tab) {
                isInboxSelected = false
                when (tab.position) {
                    0 -> {
                        tab.setIcon(R.drawable.ic_home_active)
                        tabs.getTabAt(1)?.setIcon(R.drawable.ic_transaction)
                        tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail)
                        tabs.getTabAt(3)?.setIcon(R.drawable.ic_person)
                    }
                    1 -> {
                        tabs.getTabAt(0)?.setIcon(R.drawable.ic_home)
                        tab.setIcon(R.drawable.ic_transaction_active)
                        tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail)
                        tabs.getTabAt(3)?.setIcon(R.drawable.ic_person)
                    }
                    2 -> {
                        isInboxSelected = true
                        tabs.getTabAt(0)?.setIcon(R.drawable.ic_home)
                        tabs.getTabAt(1)?.setIcon(R.drawable.ic_transaction)
                        tab.setIcon(R.drawable.ic_mail_active)
                        tabs.getTabAt(3)?.setIcon(R.drawable.ic_person)
                    }
                    3 -> {
                        tabs.getTabAt(0)?.setIcon(R.drawable.ic_home)
                        tabs.getTabAt(1)?.setIcon(R.drawable.ic_transaction)
                        tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail)
                        tab.setIcon(R.drawable.ic_person_active)

                    }
                }
            }
        })
        container.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageSelected(position: Int) {
                if ((position == 2 || position == 3) && (preferencesUtil.getUser()?.idCustomer
                        ?: 0) == 0
                ) {
                    moveViewNoAnim(0)
                    startActivityForResult(
                        Intent(
                            this@MainHomeActivity,
                            LoginActivity::class.java
                        ), LoginActivity.REQUEST_LOGIN
                    )
                }
                if (position == 4) {
                    moveViewNoAnim(0)
                }
                if (position == 7) {
                    model.isNeedReset.value = true
                }
                if (lastPosition > 3 && position > 3) {
                    moveViewNoAnim(0)
                }
                lastPosition = position
            }

        })

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        Log.d("FOOBAR", EVENT_SENDER)
        if ( intent?.extras?.getString(EVENT_SENDER) == "notification") {

            moveViewNoAnim(2)
        }

        checkFcmToken()

    }

    public override fun onStart() {
        super.onStart()

        if (!checkPermissions()) {
            requestPermissions()
        } else {
            getLastLocation()
        }
    }

    private fun setWindowFlag(bits: Int, on: Boolean) {
        val win = window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    private fun checkPermissions(): Boolean {
        val permissionState = ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
        return permissionState == PackageManager.PERMISSION_GRANTED
    }

    private fun transparentStatusAndNavigation(
        systemUiScrim: Int = Color.parseColor("#00000000") // 25% black
    ) {
        var systemUiVisibility = 0
        // Use a dark scrim by default since light status is API 23+
        var statusBarColor = systemUiScrim
        //  Use a dark scrim by default since light nav bar is API 27+
        var navigationBarColor = systemUiScrim
        val winParams = window.attributes


        if (    Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            systemUiVisibility = systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
            statusBarColor = Color.TRANSPARENT
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            systemUiVisibility = systemUiVisibility or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
            navigationBarColor = Color.TRANSPARENT
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            systemUiVisibility = systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or
                    View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            window.decorView.systemUiVisibility = systemUiVisibility
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            winParams.flags = winParams.flags or
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS or
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            winParams.flags = winParams.flags and
                    (WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS or
                            WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION).inv()
            window.statusBarColor = statusBarColor
            window.navigationBarColor = navigationBarColor
        }

        window.attributes = winParams
    }

    private fun requestPermissions() {
        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                View.OnClickListener {
                    // Request permission
                    startLocationPermissionRequest()
                })

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest()
        }
    }

    private fun showSnackbar(
        mainTextStringId: Int, actionStringId: Int,
        listener: View.OnClickListener
    ) {

        Toast.makeText(this@MainHomeActivity, getString(mainTextStringId), Toast.LENGTH_LONG).show()
    }

    private fun startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(
            this@MainHomeActivity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_PERMISSIONS_REQUEST_CODE
        )
    }

    @SuppressLint("MissingPermission")
    private fun getLastLocation() {
        mFusedLocationClient!!.lastLocation
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful && task.result != null) {
                    mLastLocation = task.result

                    preferencesUtil.putLatitude((mLastLocation)!!.latitude.toString())
                    preferencesUtil.putLongitude((mLastLocation)!!.longitude.toString())
                    Log.w(TAG, "location found", task.exception)
                    Log.w(
                        TAG,
                        "location found" + (mLastLocation)!!.latitude.toString(),
                        task.exception
                    )
                    Log.w(
                        TAG,
                        "location found" + (mLastLocation)!!.longitude.toString(),
                        task.exception
                    )
                } else {
                    Log.w(TAG, "location not found", task.exception)
                }
            }
    }

    fun moveView(index: Int) {
        container.currentItem = index
    }
    fun moveViewNoAnim(index: Int) {
        container.setCurrentItem(index, false)
    }
    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        when {
            container.currentItem == 0 -> {
                if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
                    //Toast.makeText(this, "Back pressed when slide up expanded", Toast.LENGTH_SHORT).show()
                    sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
                }
                else {
                    if (doubleBackToExitPressedOnce) {
                        super.onBackPressed()
                        return
                    }

                    this.doubleBackToExitPressedOnce = true
                    Toast.makeText(this, getString(R.string.press_back_to_exit), Toast.LENGTH_SHORT)
                        .show()

                    Handler().postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
                }
            }
            container.currentItem == 11 -> {
                moveViewNoAnim(2)
            }
            else -> moveViewNoAnim(0)
        }
    }

    override fun onGetNewsResponse(newsResponse: NewsResponse) {
        model.currentNewsResponse.value = newsResponse
    }

    override fun onGetPromoResponse(promoResponse: PromoResponse) {
        model.currentPromoResponse.value = promoResponse
    }

    override fun onGetSliderResponse(sliderResponse: SliderResponse) {
        model.currentSliderResponse.value = sliderResponse
    }

    override fun onGetPartnerResponse(partnerListResponse: PartnerListResponse) {
        model.currentPartnerResponse.value = partnerListResponse
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        model.currentListAgreementNumber.value = list
    }

    override fun getProfileInformation(personalInformationResponse: PersonalInformationResponse) {
        preferencesUtil.putProfile(personalInformationResponse)
        model.currentProfile.value = personalInformationResponse
    }

    override fun onGetToken(tokenResponse: TokenResponse) {
        if (tokenResponse.status == ConfigVar.SUCCESS) {
            preferencesUtil.putAccessToken(tokenResponse.tokenNumber ?: "")
        } else {
            presenter.getTokenNumber()
        }
    }

    override fun onGetLatestAppVersion(appVersionResponse: AppVersionResponse) {
        if (appVersionResponse.status == ConfigVar.SUCCESS) {
            latestAppVersion = appVersionResponse.latestAndroidVersion?:0

            if (BuildConfig.VERSION_CODE < latestAppVersion) {
                val intentUpdate = Intent(this@MainHomeActivity, UpdateApps::class.java)
                startActivity(intentUpdate)
            }
        }
    }

    override fun onGetUrlList(urlListResponse: UrlListResponse) {
        if (urlListResponse.status == ConfigVar.SUCCESS) {
            preferencesUtil.putWebUrl(urlListResponse.webUrl.toString())
            preferencesUtil.putApiUrl(urlListResponse.apiUrl.toString())
            apiUrl = urlListResponse.apiUrl.toString()
            preferencesUtil.putCmsUrl(urlListResponse.cmsUrl.toString())

        }
    }

    override fun onSetFcmToken(updateTokenResponse: UpdateTokenResponse) {

    }

    override fun onGetResponseInbox(page: Int, inboxResponse: InboxResponse) {
        var oldList = model.currentInboxList.value?: arrayListOf()
        if (page == 1) oldList = arrayListOf()
        val newList = inboxResponse.listInbox?.toMutableList()?: arrayListOf()
        oldList.addAll(newList)
        model.currentInboxList.value = oldList
        val unreadMessage = inboxResponse.unreadCount!!
        if (unreadMessage > 0) {
            setUnreadIcon()
        }
        else {
            setReadIcon()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(eventBus: EventBus) {
        if (eventBus.type == LOAD_AGGREMENT_LIST) {
            presenter.getAgreementNumber()
            presenter.getCashValue()
        } else if (eventBus.type == LOAD_PERSONAL_INFO) {
            isReferensikan = false
            presenter.getPersonalInfo()
            presenter.getAgentCode()
        } else if (eventBus.type == LOAD_INBOX) {
            presenter.getListInbox("1")
            //presenter.getUnreadMessage()
        }
        else if (eventBus.type == LOAD_UNREAD_MESSAGE) {
            presenter.getUnreadMessage()
        }
    }

    override fun onResume() {
        super.onResume()
        if ((preferencesUtil.getUser()?.idCustomer?:0) > 0) {
            //presenter.getAgreementNumber()
            presenter.getPersonalInfo()
            //presenter.getAgentCode()
        }

        val isExpired = preferencesUtil.isLoginExpired()
        if (isExpired) {
            preferencesUtil.putLoginDate()
            presenter.getTokenNumber()
        }

        if ( intent?.extras?.getString(EVENT_SENDER) == "notification") {

            moveViewNoAnim(2)
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        org.greenrobot.eventbus.EventBus.getDefault().unregister(this)
    }

    private fun setUnreadIcon() {
        if (isInboxSelected) {
            tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail_badge_top_active)
        }
        else {
            tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail_badge_top)
        }
    }

    private fun setReadIcon() {
        if (isInboxSelected) {
            tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail_active)
        }
        else {
            tabs.getTabAt(2)?.setIcon(R.drawable.ic_mail)
        }
    }

    private fun checkFcmToken() {
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            this,
            object : OnSuccessListener<InstanceIdResult?> {

                override fun onSuccess(p0: InstanceIdResult?) {
                    val token: String = p0?.token ?: ""
                    //Toast.makeText(this@MainHomeActivity, token, Toast.LENGTH_SHORT).show()

                    presenter.setFcmToken(token)
                }
            })
    }

}
