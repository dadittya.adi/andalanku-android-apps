package id.andalan.andalanku.ui.payment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.ui.CreditSimulationO
import id.andalan.andalanku.model.ui.Tenor
import id.andalan.andalanku.ui.adapters.ConditionAdapter
import id.andalan.andalanku.ui.loan_application.CompleteDataActivity
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getNumberOnly
import kotlinx.android.synthetic.main.fragment_advance_type_payment.*

class AdvanceTypePaymentFragment : androidx.fragment.app.Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_advance_type_payment, container, false)
    }

    private lateinit var conditionAdapter: ConditionAdapter
    private var creditSimulation: CreditSimulationO? = null
    lateinit var creditSimulationRequest: CreditSimulationRequest
    private var isUsedCar = false
    private var year = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as PaymentSimulationActivity).creditSimulationResponse.simulationData?.let {
            creditSimulation = it.find { it.creditType == "ADVANCE" }
        }
        creditSimulationRequest = (activity as PaymentSimulationActivity).creditSimulationRequest
        isUsedCar = (activity as PaymentSimulationActivity).isUsedCar
        year = (activity as PaymentSimulationActivity).year
        conditionAdapter = ConditionAdapter()

        rv_condition?.adapter = conditionAdapter
        rv_condition?.isNestedScrollingEnabled = true
        rv_condition?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_condition?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        if (creditSimulation != null) {
            e("tag", creditSimulation?.premiAsuransi + (creditSimulation?.premiAsuransi?.getNumberOnly()?:0).toString())
            if (isUsedCar) {
                tv_main_total_loan.text = creditSimulation?.pokokHutang?.getNumberOnly()?.convertToRpFormat()
                tv_year.text = year
                main_tahun_kendaraan.visibility = View.VISIBLE
                tv_year.visibility = View.VISIBLE
                tv_tenor.text = Tenor(creditSimulation?.tenor?.getNumberOnly()?.toInt()).getMonth(getString(R.string.month))
                tv_first_bill.text = (creditSimulation?.dp?.getNumberOnly()?.plus(creditSimulation?.angsuran?.getNumberOnly()?:0)?.plus(creditSimulation?.premiAsuransi?.getNumberOnly()?:0))?.convertToRpFormat()
                tv_installment_bill.text = creditSimulation?.angsuran?.getNumberOnly()?.convertToRpFormat()
            } else {
                tv_main_total_loan.text = creditSimulation?.pokokHutang?.getNumberOnly()?.convertToRpFormat()
                tv_tenor.text = Tenor(creditSimulation?.tenor?.getNumberOnly()?.toInt()).getMonth(getString(R.string.month))
                tv_first_bill.text = (creditSimulation?.dp?.getNumberOnly()?.plus(creditSimulation?.angsuran?.getNumberOnly()?:0)?.plus(creditSimulation?.premiAsuransi?.getNumberOnly()?:0))?.convertToRpFormat()
                tv_installment_bill.text = creditSimulation?.angsuran?.getNumberOnly()?.convertToRpFormat()
            }
        }

        initConditionList()

        btn_apply.setOnClickListener {
            if (((activity as PaymentSimulationActivity).preferencesUtil.getUser()?.idCustomer?:0) > 0) {

                /*
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.APPLY, true)
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                intent.putExtra(CompleteDocumentActivity.COLLATERAL, (activity as PaymentSimulationActivity).collateral)
                intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, (activity as PaymentSimulationActivity).creditSimulationRequest)
                (activity as PaymentSimulationActivity).setResult(PaymentSimulationActivity.RESPONSE_PAYMENT, intent)
                (activity as PaymentSimulationActivity).finishActivity()
                 */

                val intent = Intent(context, CompleteDataActivity::class.java)
                intent.putExtra(CompleteDataActivity.SIMULATION, creditSimulation)
                intent.putExtra(CompleteDataActivity.COLLATERAL, (activity as PaymentSimulationActivity).collateral)
                intent.putExtra(CompleteDataActivity.CREDIT_SIMULATION_REQUEST, (activity as PaymentSimulationActivity).creditSimulationRequest)
                startActivity(intent)
            } else {
                startActivity(Intent(context, LoginActivity::class.java))
                (activity as PaymentSimulationActivity).finishActivity()
            }
        }
        btn_re_simulate.setOnClickListener {
            if (isUsedCar) {
//                startActivity(Intent(context, UsedCarActivity::class.java))
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.RESIMULATE, true)
                (activity as PaymentSimulationActivity).setResult(PaymentSimulationActivity.RESPONSE_PAYMENT, intent)
                (activity as PaymentSimulationActivity).finishActivity()
            } else {
//                startActivity(Intent(context, PaymentNewCarActivity::class.java))
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.RESIMULATE, true)
                (activity as PaymentSimulationActivity).setResult(PaymentSimulationActivity.RESPONSE_PAYMENT, intent)
                (activity as PaymentSimulationActivity).finishActivity()
            }
        }

        btn_change_tenor.setOnClickListener {
        }
    }

    private fun initConditionList() {
        conditionAdapter.add("Rincian simulasi di atas bersifat estimasi dan tidak mengikat  dan dapat berubah sewaktu-waktu mengikuti kebijakan yang berlaku.")
        conditionAdapter.add("Total Pembayaran Pertama sudah termasuk Down Payment (DP)Nett, biaya admin, dan biaya asuransi.")
        conditionAdapter.add("Untuk informasi lengkap, silakan menghubungi Kantor Cabang Andalan Finance terdekat.")
        conditionAdapter.notifyDataSetChanged()
    }
}
