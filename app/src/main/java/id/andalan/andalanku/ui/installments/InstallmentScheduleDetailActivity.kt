package id.andalan.andalanku.ui.installments

import android.Manifest
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.Toast
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_installment_schedule_detail.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject
import pub.devrel.easypermissions.EasyPermissions
import androidx.annotation.NonNull
import pub.devrel.easypermissions.AppSettingsDialog
import android.app.DownloadManager
import android.content.*
import androidx.core.content.FileProvider
import android.view.LayoutInflater
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import id.andalan.andalanku.BuildConfig
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.model.response.PrePaymentSimulationResponse
import id.andalan.andalanku.utils.*
import java.io.File


class InstallmentScheduleDetailActivity : BaseApp(), InstallmentsScheduleView,
    EasyPermissions.PermissionCallbacks {
    companion object {
        val AGREEMENT_NUMBER = "aggreement_number"
        val LIST_AGREEMENT = "list_aggreement"
    }

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: InstallmentsSchedulePresenter
    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()
    private var pdfUrl = ""
    private var path = ""
    private val RC_STORAGE = 123
    private val STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var agreementNumber = ""

    private var downloadManager: DownloadManager? = null
    private var refid: Long = 0
    private lateinit var onComplete: BroadcastReceiver
    private var filename = ""

    private lateinit var alertDialogCustom: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installment_schedule_detail)
        PRDownloader.initialize(applicationContext)
        path = FileUtils.getRootDirPath()

        downloadManager = getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.detail_jadwal_angsuran)

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogCustom.create()
        alertDialogCustom.setCancelable(true)

        if (!intent.hasExtra(AGREEMENT_NUMBER)) {
            finish()
        }

        agreementNumber = intent.getStringExtra(AGREEMENT_NUMBER)
        val parcel = intent?.extras?.getParcelableArray(LIST_AGREEMENT)
        val listAgreement: MutableList<PickerItem> = arrayListOf()

        choosedPicker.name = agreementNumber

        parcel?.forEach {
            listAgreement.add(it as PickerItem)
        }

        deps.inject(this)
        presenter.view = this
        presenter.getDetailPaymentHistory(agreementNumber)
//        presenter.getDetailFinancial(agreementNumber)

        btn_sisa_pinjaman.text = agreementNumber

        btn_sisa_pinjaman.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }

        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                btn_sisa_pinjaman?.text = pickerItem.name
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)

                presenter.getDetailPaymentHistory(pickerItem.name?:"")
//                presenter.getDetailFinancial(pickerItem.name?:"")
                agreementNumber = pickerItem.name?:""
                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        pickerAdapter.add(listAgreement)

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter

        btn_save?.setOnClickListener {
            if (hasStoragePermission()) {
                doDownload()
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.izinkan_aplikasi_mendownload_file),
                    RC_STORAGE,
                    STORAGE)
            }
        }

        onComplete = object: BroadcastReceiver() {
            override fun onReceive(p0: Context?, p1: Intent?) {
                Toast.makeText(this@InstallmentScheduleDetailActivity, getString(R.string.success_download), Toast.LENGTH_LONG).show()
                openFile(path, filename)
            }
        }
        registerReceiver(onComplete,
            IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(onComplete)
    }

    private fun doDownload() {
        if (pdfUrl.isNotBlank()) {
            filename = "jadwal-angsuran-$agreementNumber.pdf"
//            val download_Uri = Uri.parse(pdfUrl)
//
//            val request = DownloadManager.Request(download_Uri)
//            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
//            request.setAllowedOverRoaming(false)
//            request.setTitle(filename)
//            request.setDescription("Downloading...")
//            request.setVisibleInDownloadsUi(true)
//            request.setDestinationInExternalPublicDir(
//                "/",
//                filename
//            )
//
//            refid = downloadManager?.enqueue(request)?:0
            PRDownloader.download(pdfUrl, path, filename)
                .build()
                .start(object: OnDownloadListener{
                    override fun onDownloadComplete() {
                        Toast.makeText(this@InstallmentScheduleDetailActivity, getString(R.string.success_download), Toast.LENGTH_LONG).show()
                        openFile(path, filename)
                    }
                    override fun onError(error: Error?) {
                        if (error?.isConnectionError == true) {
                            Toast.makeText(this@InstallmentScheduleDetailActivity, getString(R.string.failed_download_connection_error), Toast.LENGTH_LONG).show()
                        } else if (error?.isServerError == true) {
                            Toast.makeText(this@InstallmentScheduleDetailActivity, getString(R.string.failed_download_server_error), Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(this@InstallmentScheduleDetailActivity, getString(R.string.failed_download), Toast.LENGTH_LONG).show()
                        }
                    }
                })
        } else {

        }
    }

    private fun openFile(path: String, filename: String) {
        this.let {
            val file = File("$path/$filename")
            val target = Intent(Intent.ACTION_VIEW)
            val uri = FileProvider.getUriForFile(it,
                BuildConfig.APPLICATION_ID + ".provider",
                file)
            target.setDataAndType(uri, "application/pdf")
            target.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            val intent = Intent.createChooser(target, "Buka Kartu Piutang")
            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                // Instruct the user to install a PDF reader here, or something
            }
        }
    }

    private fun hasStoragePermission (): Boolean {
        return EasyPermissions.hasPermissions(this, STORAGE)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun getResponseInstallment(detailPaymentHistoryResponse: DetailPaymentHistoryResponse) {
        pdfUrl=detailPaymentHistoryResponse.kartuPiutang?:""
        if (pdfUrl != "") {
            wb_schedule.settings.javaScriptEnabled = true
            //wb_schedule.settings.pluginState = WebSettings.PluginState.ON
            wb_schedule.settings.supportZoom()
            wb_schedule.webViewClient = Callback()
            wb_schedule.loadUrl("https://docs.google.com/gview?embedded=true&url=$pdfUrl")
            btn_save?.isEnabled = true
        } else {
            btn_save?.isEnabled = false
            Toast.makeText(this, "Data Belum Tersedia", Toast.LENGTH_SHORT).show()
        }
    }

    private class Callback: WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
            return false
        }

        override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
            return false
        }
    }

    override fun getResponsePrepaymentSimulation(prePaymentSimulationResponse: PrePaymentSimulationResponse) {
        tv_value_sisa_pinjaman?.text = (prePaymentSimulationResponse.sisaPinjaman?.getNumberOnly()?:0).convertToRpFormat()
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        TODO("Not yet implemented")
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        AppSettingsDialog.Builder(this).build().show()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (RC_STORAGE == requestCode) {
            doDownload()
        }
    }

    override fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse) {
//        tv_value_sisa_pinjaman?.text = (if (detailFinanceResponse.installmentAmount.isNullOrEmpty()) { "0" } else detailFinanceResponse.installmentAmount).toLong().convertToRpFormat()
        tv_value_jatuh_kontrak?.text = detailFinanceResponse.getMaturityDateUI(this.getMonths())
    }

    override fun showWait() {
        alertDialogCustom.show()
    }

    override fun removeWait() {
        alertDialogCustom.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
