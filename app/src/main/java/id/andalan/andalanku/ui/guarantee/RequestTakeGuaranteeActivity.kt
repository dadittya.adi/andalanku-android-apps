@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.guarantee

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.CollateralListResponse
import id.andalan.andalanku.model.ui.CollateralRequest
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.RequestGuaranteeAdapter
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.guarantee.DetailTakeGuaranteeActivity.Companion.COLLATERAL_REQUEST_DETAIL
import kotlinx.android.synthetic.main.activity_request_take_guarantee.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class RequestTakeGuaranteeActivity : BaseApp(), RequestCollateralView {
    private val TAG = RequestTakeGuaranteeActivity::class.java.simpleName

    private lateinit var requestGuaranteeAdapter: RequestGuaranteeAdapter

    @Inject
    lateinit var presenter: RequestCollateralPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_take_guarantee)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.e_request_take_guarantee)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        layout?.setBackgroundColor(ContextCompat.getColor(this, R.color.snow))
        btn_add?.visibility = View.VISIBLE
        tabs?.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
                Log.e(TAG, "reselect ${p0?.position}")
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                Log.e(TAG, "un selected ${p0?.position}")
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                onTabsPosition(p0?.position?:0)
            }

        })
        btn_add.setOnClickListener {
            startActivityForResult(Intent(this, SubmissionRequestGuaranteeActivity::class.java), EComplainActivity.REQUEST_SEND_COMPLAIN)
        }
        btn_request_collateral_submission.setOnClickListener {
            startActivityForResult(Intent(this, SubmissionRequestGuaranteeActivity::class.java), EComplainActivity.REQUEST_SEND_COMPLAIN)
        }
        requestGuaranteeAdapter = RequestGuaranteeAdapter(object: RequestGuaranteeAdapter.OnClickItem{
            override fun onClickRequest(requestGuarantee: CollateralRequest) {
                val intent = Intent(this@RequestTakeGuaranteeActivity, DetailTakeGuaranteeActivity::class.java)
                intent.putExtra(COLLATERAL_REQUEST_DETAIL, requestGuarantee)
                startActivity(intent)
            }
        })

        rv_complain?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_complain?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_complain?.adapter = requestGuaranteeAdapter

    }

    fun onTabsPosition (position: Int) {
        when (position) {
            0 -> {
                presenter.getListCollateral()
            }
            1 -> {
                presenter.getListCollateral(ConfigVar.SUBMITTED)
            }
            2 -> {
                presenter.getListCollateral(ConfigVar.ONPROCESS)
            }
            3 -> {
                presenter.getListCollateral(ConfigVar.SOLVED)
            }
        }
        requestGuaranteeAdapter.removeAll()
    }

    override fun getRequestCollateralList(collateralListResponse: CollateralListResponse) {
        if (collateralListResponse.status == ConfigVar.SUCCESS) {
            collateralListResponse.collaterallist?.let {
                if (it.size > 0) {
                    requestGuaranteeAdapter.add(it)
                    showEmptyState(false)
                } else {
                    showEmptyState(true)
                }
            }
        } else {
            showEmptyState(true)
        }
    }

    private fun showEmptyState(state: Boolean) {
        rv_complain?.visibility = if (state) View.GONE else View.VISIBLE
        container_empty_state?.visibility = if (state) View.VISIBLE else View.GONE
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == EComplainActivity.REQUEST_SEND_COMPLAIN && resultCode == EComplainActivity.RESPONSE_SEND_COMPLAIN) {
            onTabsPosition(tabs.selectedTabPosition)
        }
    }
}
