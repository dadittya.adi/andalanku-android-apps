package id.andalan.andalanku.ui.password

import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.ChangePasswordResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.activity_update_password.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class UpdatePasswordActivity : BaseApp(), UpdatePasswordView {
    @Inject
    lateinit var presenter: UpdatePasswordPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var alertDialogCustom: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_password)

        deps.inject(this)
        presenter.view = this

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            finish()
        }

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.update_password)

        btn_send?.setOnClickListener {
            val password = et_password?.text.toString()
            val newPassword = et_new_pin?.text.toString()
            val confirmNewPassword = et_new_pin_confirm?.text.toString()

            if (password.length < 8) {
                Toast.makeText(this, "Minimal 8 Karakter", Toast.LENGTH_SHORT).show()
            }
            if (password.isBlank() || newPassword.isBlank() || confirmNewPassword.isBlank()) {
                Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            } else {
                if (newPassword != confirmNewPassword) {
                    Toast.makeText(this, getString(R.string.password_not_equals), Toast.LENGTH_SHORT).show()
                } else {
                    presenter.doResetPassword(password, newPassword, confirmNewPassword)
                }
            }
        }
    }

    override fun onChangePassword(changePasswordResponse: ChangePasswordResponse) {
        if (changePasswordResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.getView().tv_intro_text?.text = changePasswordResponse.message?:""
            alertDialogCustom.show()
        } else {
            Toast.makeText(this, changePasswordResponse.message?:"", Toast.LENGTH_SHORT).show()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
