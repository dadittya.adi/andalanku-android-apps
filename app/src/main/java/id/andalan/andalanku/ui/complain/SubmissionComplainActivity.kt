package id.andalan.andalanku.ui.complain

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.InputComplainRequest
import id.andalan.andalanku.model.response.CategoryComplainResponse
import id.andalan.andalanku.model.response.InputComplainResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.CategoryComplainAdapter
import id.andalan.andalanku.ui.adapters.ImageTemporaryAdapter
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.ImagePickerActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_submission_complain.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import java.io.IOException
import javax.inject.Inject
import org.greenrobot.eventbus.EventBus


class SubmissionComplainActivity : BaseApp(), SubmissionComplainView {
    companion object {
        val REQUEST_IMAGE = 101
    }
    private val TAG = SubmissionComplainActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: SubmissionComplainPresenter

    private lateinit var categoryAdapter: CategoryComplainAdapter
    private lateinit var imageTempAdapter: ImageTemporaryAdapter
    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submission_complain)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.complain_submission)
        et_phone.setText(preferencesUtil.getProfile()?.phoneNumber)

        categoryAdapter = CategoryComplainAdapter(object: CategoryComplainAdapter.OnClickItem{
            override fun onClick(category: String) {
                et_category.setText(category)
                hideSlideLayout()
            }
        })

        imageTempAdapter = ImageTemporaryAdapter(object: ImageTemporaryAdapter.OnClickItem{
            override fun onClickDelete(image: String) {
                val index = imageTempAdapter.getData().indexOf(image)
                if (index > -1) imageTempAdapter.removeAt(index)
            }
        })

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        et_drag_view?.visibility = View.GONE
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }
        tv_title_drag_view.text = getString(R.string.choose_complain_category)
        rv_drag_view?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_drag_view?.adapter = categoryAdapter

        rv_image_media?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_image_media?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_image_media?.adapter = imageTempAdapter

        et_category?.setOnClickListener {
            showSlideLayout()
        }

        btn_container_photo.setOnClickListener {
            openImagePicker()
        }

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            setResult(EComplainActivity.RESPONSE_SEND_COMPLAIN)
            finish()
        }

        btn_send.setOnClickListener {

            val request = InputComplainRequest(
                category = et_category.text.toString(),
                phone = et_phone.text.toString(),
                message = et_message.text.toString()
            )
            if (request.isValid()) {
                btn_send.isClickable = false
                presenter.sendComplain(request, imageTempAdapter.getData())
            } else {
                Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    private fun clearAllFocus() {
    }

    override fun onSuccessSendComplain(inputComplainResponse: InputComplainResponse) {
        if (inputComplainResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.getView().tv_intro_text?.text = inputComplainResponse.message
            alertDialogCustom.show()
        } else {
            Toast.makeText(this, inputComplainResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onGetCategoryList(categoryComplainResponse: CategoryComplainResponse) {
        if (categoryComplainResponse.status == ConfigVar.SUCCESS) {
            categoryComplainResponse.categoryList?.let {
                categoryAdapter.add(it)
                categoryAdapter.notifyDataSetChanged()
                EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_INBOX))
            }
        } else {
            finish()
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }

        })
    }

    private fun openImagePicker() {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions()
                    } else {
                        // TODO - handle permission denied case
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                try {
                    imageTempAdapter.add(uri?.path?:"")
                    imageTempAdapter.notifyDataSetChanged()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
