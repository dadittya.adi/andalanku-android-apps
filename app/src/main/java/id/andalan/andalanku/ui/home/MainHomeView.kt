package id.andalan.andalanku.ui.home

import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.Agreement
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface MainHomeView: BaseView {
    fun getProfileInformation(personalInformationResponse: PersonalInformationResponse)
    fun onGetToken(tokenResponse: TokenResponse)
    fun onGetContractNumber(list: MutableList<PickerItem>)
    fun onGetNewsResponse(newsResponse: NewsResponse)
    fun onGetPromoResponse(promoResponse: PromoResponse)
    fun onGetPartnerResponse(partnerListResponse: PartnerListResponse)
    fun onGetResponseInbox(page: Int, inboxResponse: InboxResponse)
    fun onGetAgent(agentResponse: AgentResponse)
    fun onGetCashValue(cashValueResponse: CashValueResponse)
    fun onGetUnreadMessage(unreadMessageResponse: UnreadMessageResponse) {}
    fun onGetLegalDocument(legalDocumentResponse: LegalDocumentResponse)
    fun onGetSliderResponse(sliderResponse: SliderResponse)
    fun onGetLatestAppVersion(appVersionResponse: AppVersionResponse)
    fun onGetUrlList(urlListResponse: UrlListResponse)
    fun onSetFcmToken(updateTokenResponse: UpdateTokenResponse)
}