package id.andalan.andalanku.ui.agent

import id.andalan.andalanku.model.request.SendAgenRegistrationRequest
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.AddressName
import id.andalan.andalanku.model.ui.Bank
import id.andalan.andalanku.model.ui.BankBranch
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.ui.base.BaseView

interface AgentRegisterTncView: BaseView {
    fun onResponseSend(agentRegistrationResponse: AgentRegistrationResponse)

}