package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_attachment_complain.view.*

class AttachmentClaimAdapter(val onItemClickListener: OnClickItem): BaseAdapter<String, AttachmentClaimAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_attachment_complain
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<String>(itemView) {
        override fun bind(data: String) {
            itemView.setOnClickListener {
                onItemClickListener.onClickImage(data)
            }
            loadImage(itemView.context, data, itemView.iv_attachment_image)
        }

    }

    interface OnClickItem {
        fun onClickImage(urlImage: String)
    }
}