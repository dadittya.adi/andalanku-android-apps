package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.response.PpobUpdateAmountResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import id.andalan.andalanku.ui.home.MainHomeView
import rx.Subscriber
import rx.Subscription
import javax.inject.Inject

class PpobUpdateAmountPresenter @Inject constructor(private val service: Services): BasePresenter<PpobUpdateAmountView>(){
    override fun onCreate() {
        TODO("Not yet implemented")
    }

    fun ppobUpdateAmount(orderId: String, amount: String) {
        view.showWait()
        val subscription = service.PpobUpdateAmount(orderId, amount).subscribe(object : Subscriber<PpobUpdateAmountResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(ppobUpdateAmountResponse: PpobUpdateAmountResponse) {
                view.removeWait()
                view.onUpdateAmount(ppobUpdateAmountResponse)
            }
        })
        subscriptions.add(subscription)
    }

}