@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.item_ppob_pulsa_data.view.*


class PpobPulsaDataAdapter(val onItemClickListener: OnClickItem): BaseAdapter<PpobProduct, PpobPulsaDataAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_pulsa_data
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobProduct>(itemView) {
        override fun bind(ppobProduct: PpobProduct) {
            val data = ppobProduct.getPpobProduct()

            var productName = data.name

            val productPrice = "Harga ${data.amount?.toLong()?.convertToRpFormat()}"
            itemView.tv_ppob_data_product_name.text = productName.toString()
            itemView.tv_ppob_data_price.text = productPrice

            if (ppobProduct.validity != "") {
                itemView.tv_ppob_data_detail_validity.text = data.validity
                itemView.guideline_ppob_data.visibility = View.VISIBLE
                itemView.tv_ppob_data_detail_button.visibility = View.VISIBLE
                itemView.tv_ppob_data_detail_button.text = "Lihat Detail"
            }

            if (ppobProduct.note != "") {
                itemView.tv_ppob_data_detail_info.text = data.note
                itemView.guideline_ppob_data.visibility = View.VISIBLE
                itemView.tv_ppob_data_detail_button.visibility = View.VISIBLE
                itemView.tv_ppob_data_detail_button.text = "Lihat Detail"
            }

            itemView.tv_ppob_data_product_name.setOnClickListener {
                onItemClickListener.onClickProduct(data)
            }

            itemView.tv_ppob_data_price.setOnClickListener {
                onItemClickListener.onClickProduct(data)
            }

            itemView.tv_ppob_data_detail_button.setOnClickListener {
                // If the CardView is already expanded, set its visibility
                //  to gone and change the expand less icon to expand more.
                // If the CardView is already expanded, set its visibility
                //  to gone and change the expand less icon to expand more.
                if (itemView.ppob_pulsa_data_info_container.visibility == View.VISIBLE) {

                    // The transition of the hiddenView is carried out
                    //  by the TransitionManager class.
                    // Here we use an object of the AutoTransition
                    // Class to create a default transition.
                    TransitionManager.beginDelayedTransition(
                        itemView.cv_ppob_pulsa_data,
                        AutoTransition()
                    )
                    itemView.tv_ppob_data_detail_info.visibility = View.GONE
                    itemView.tv_ppob_data_detail_validity.visibility = View.GONE
                    itemView.ppob_pulsa_data_info_container.visibility = View.GONE
                    itemView.tv_ppob_data_detail_button.text = "Lihat Detail"
                } else {
                    TransitionManager.beginDelayedTransition(
                        itemView.cv_ppob_pulsa_data,
                        AutoTransition()
                    )
                    itemView.tv_ppob_data_detail_info.visibility = View.VISIBLE
                    itemView.tv_ppob_data_detail_validity.visibility = View.VISIBLE
                    itemView.ppob_pulsa_data_info_container.visibility = View.VISIBLE
                    itemView.tv_ppob_data_detail_button.text = "Sembunyikan"
                }
            }
        }

    }

    interface OnClickItem {
        fun onClickProduct(ppobProduct: PpobProduct)
    }
}