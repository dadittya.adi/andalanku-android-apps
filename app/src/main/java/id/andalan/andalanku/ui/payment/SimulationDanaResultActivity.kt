package id.andalan.andalanku.ui.payment

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.model.ui.CreditSimulation
import id.andalan.andalanku.model.ui.CreditSimulationO
import id.andalan.andalanku.model.ui.Tenor
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.loan_application.CompleteDataActivity
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getNumberOnly
import kotlinx.android.synthetic.main.activity_simulation_dana_result.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class SimulationDanaResultActivity : BaseApp() {
    private val TAG = SimulationDanaResultActivity::class.java.simpleName
    companion object {
        val CREDIT = "credit"
        val REQUEST_RESULT_DANA = 991
        val RESPONSE_RESULT_DANA = 992
        val COLLATERAL = "collateral"
        val CREDIT_SIMULATION_REQUEST = "credit_simulation_request"
    }
    lateinit var creditSimulationResponse: CreditSimulationResponse
    lateinit var collateral: Collateral
    lateinit var creditSimulationRequest: CreditSimulationRequest
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private var creditSimulation: CreditSimulationO? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simulation_dana_result)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.payment_simulation_small)

        creditSimulationResponse = intent?.extras?.getParcelable(CREDIT) as CreditSimulationResponse
        creditSimulation = creditSimulationResponse.simulationData?.find { it.creditType == "ADVANCE" }
        collateral = intent?.extras?.getParcelable(COLLATERAL) as Collateral
        creditSimulationRequest = intent?.extras?.getParcelable(CREDIT_SIMULATION_REQUEST) as CreditSimulationRequest

        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                if (p0?.position == 0) {
                    creditSimulation = creditSimulationResponse.simulationData?.find { it.creditType == "ADVANCE" }
                    loadData()
                } else {
                    creditSimulation = creditSimulationResponse.simulationData?.find { it.creditType == "ARREAR" }
                    loadData()
                }
            }
        })

        loadData()

        btn_apply.setOnClickListener {
            if ((preferencesUtil.getUser()?.idCustomer?:0) > 0) {
                /*val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                intent.putExtra(PaymentSimulationActivity.APPLY, true)
                setResult(RESPONSE_RESULT_DANA, intent)
                finish()
                 */
                val intent = Intent(this, CompleteDataActivity::class.java)
                intent.putExtra(CompleteDataActivity.SIMULATION, creditSimulation)
                intent.putExtra(CompleteDataActivity.COLLATERAL, collateral)
                intent.putExtra(CompleteDataActivity.CREDIT_SIMULATION_REQUEST, creditSimulationRequest)
                startActivity(intent)
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
        btn_re_simulate.setOnClickListener {
            val intent = Intent()
            intent.putExtra(PaymentSimulationActivity.RESIMULATE, true)
            setResult(RESPONSE_RESULT_DANA, intent)
            finish()
        }
    }

    fun loadData() {
        if (creditSimulation != null) {
            tv_pengajuan.text = creditSimulation!!.pokokHutang?.getNumberOnly()?.convertToRpFormat()
            tv_jangka_waktu.text = Tenor(creditSimulation!!.tenor?.getNumberOnly()?.toInt()).getMonth(getString(R.string.month))
            tv_angsuran.text = creditSimulation!!.angsuran?.getNumberOnly()?.convertToRpFormat()
            tv_administrasi.text = creditSimulation!!.biayaAdmin?.getNumberOnly()?.convertToRpFormat()
            tv_provisi.text = creditSimulation!!.provisi?.getNumberOnly()?.convertToRpFormat()
            tv_dana_diterima.text = creditSimulation!!.danaDiterima?.getNumberOnly()?.convertToRpFormat()
            tv_asuransi.text = creditSimulation!!.premiAsuransi?.getNumberOnly()?.convertToRpFormat()
        }
    }
}
