package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PpobGroup
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_ppob_group.view.*

class PpobGroupListAdapter(val onItemClickListener: OnClickItem): BaseAdapter<PpobGroup, PpobGroupListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_group
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobGroup>(itemView) {
        override fun bind(data: PpobGroup) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            if (data.icon?.isNotEmpty() == true){

                if (data.icon == "pulsa") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.pulsa)
                }
                else if (data.icon == "listrik") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.listrik)
                }
                else if (data.icon == "pdam") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.pdam)
                }
                else if (data.icon == "bpjs") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.bpjs)
                }
                else if (data.icon == "telkom") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.telkom)
                }
                else if (data.icon == "gasnegara") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.gasnegara)
                }
                else if (data.icon == "kartu_kredit") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_kartu_kredit)
                }
                else if (data.icon == "tv_cable") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_tv_cable)
                }
                else if (data.icon == "angsuran_kredit") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_angsuran_kredit)
                }
                else if (data.icon == "emoney") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.emoney)
                }
                else if (data.icon == "streaming") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_streaming)
                }
                else if (data.icon == "game_voucher") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_game_voucher)
                }
                else if (data.icon == "digital_voucher") {
                    itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_game_voucher)
                }

            }
            else {
                itemView.img_item_ppob_group_product_image?.setImageResource(R.drawable.ic_default_product)
            }

            itemView.tv_item_ppob_group_product_label?.text = data.name
        }

    }

    interface OnClickItem {
        fun onClickMenu(ppobGroup: PpobGroup)
    }
}

