package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.FaqParent
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_menu_profile.view.*

class FaqAdapter(val onItemClickListener: OnClickItem): BaseAdapter<FaqParent, FaqAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_menu_profile
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<FaqParent>(itemView) {
        override fun bind(data: FaqParent) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            itemView.tv_name.text = data.question
        }

    }

    interface OnClickItem {
        fun onClickMenu(faqParent: FaqParent)
    }
}