package id.andalan.andalanku.ui.inbox

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.ImageTemporaryAdapter
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.ImagePickerActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.model.request.InputMessageRequest
import id.andalan.andalanku.model.response.InputMessageResponse
import kotlinx.android.synthetic.main.activity_reply_message.*
import kotlinx.android.synthetic.main.activity_submission_complain.btn_container_photo
import kotlinx.android.synthetic.main.activity_submission_complain.btn_send
import kotlinx.android.synthetic.main.activity_submission_complain.et_message
import kotlinx.android.synthetic.main.activity_submission_complain.rv_image_media
import kotlinx.android.synthetic.main.activity_submission_complain.sliding_layout
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import java.io.IOException
import javax.inject.Inject


class MessageReplyActivity : BaseApp(), MessageReplyView {
    companion object {
        val REQUEST_IMAGE = 101
    }
    private val TAG = MessageReplyActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: MessageReplyPresenter

    private lateinit var imageTempAdapter: ImageTemporaryAdapter
    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reply_message)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.reply_message)

        imageTempAdapter = ImageTemporaryAdapter(object: ImageTemporaryAdapter.OnClickItem{
            override fun onClickDelete(image: String) {
                val index = imageTempAdapter.getData().indexOf(image)
                if (index > -1) imageTempAdapter.removeAt(index)
            }
        })

        rv_image_media?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_image_media?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_image_media?.adapter = imageTempAdapter

        et_title?.setText(intent?.extras?.getString("title", ""))
        et_title?.isEnabled = false
        //int value =getIntent().getIntExtra("key", 0)

        btn_container_photo.setOnClickListener {
            openImagePicker()
        }

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            //setResult(EComplainActivity.RESPONSE_SEND_COMPLAIN)
            finish()
        }

        btn_send.setOnClickListener {

            val request = InputMessageRequest(
                destination_id = "-2",
                title = et_title.text.toString(),
                content = et_message.text.toString()
            )
            if (request.isValid()) {
                btn_send.isClickable = false
                presenter.sendMessage(request, imageTempAdapter.getData())
            } else {
                Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    private fun clearAllFocus() {
    }

    override fun onSuccessSendMessage(inputMessageResponse: InputMessageResponse) {
        if (inputMessageResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.getView().tv_intro_text?.text = inputMessageResponse.message
            alertDialogCustom.show()
        } else {
            Toast.makeText(this, inputMessageResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }

        })
    }

    private fun openImagePicker() {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions()
                    } else {
                        // TODO - handle permission denied case
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                try {
                    imageTempAdapter.add(uri?.path?:"")
                    imageTempAdapter.notifyDataSetChanged()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
