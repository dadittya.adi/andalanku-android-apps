@file:Suppress("NAME_SHADOWING")
package id.andalan.andalanku.ui.address

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.UpdateAddressResponse
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.model.ui.TILExtension
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.LocationAdapter
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.ISREFERENSI
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.IS_COMPLETE_DATA_FORM
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_DATA
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.ImagePickerActivity
import id.andalan.andalanku.utils.ekstensions.loadImage
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.model.ui.Document
import kotlinx.android.synthetic.main.activity_update_address_according_ktp.*
import kotlinx.android.synthetic.main.form_upload_image.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.layout_confirmation.view.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.layout_success.view.btn_ok
import kotlinx.android.synthetic.main.layout_success.view.tv_intro_text
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class UpdateAddressAccordingKtpActivity : BaseApp(), UpdateAddressView {
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: UpdateAddressPresenter

    lateinit var provinceAdapter: LocationAdapter
    lateinit var cityAdapter: LocationAdapter
    lateinit var subDistrictAdapter: LocationAdapter
    lateinit var villageAdapter: LocationAdapter

    private var provinceChoosed: Location? = null
    private var cityChoosed: Location? = null
    private var subDistrictChoosed: Location? = null
    private var villageChoosed: Location? = null
    private var choosedImage: String = ""
    private val documentDataBottom: MutableList<Document> = arrayListOf()

    private var personalInfoUser: PersonalInformationResponse? = null
    private var address: UpdateAddressRequest? = null

    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertDialogBack: AlertDialogCustom
    private var isSettingAccount = false

    private val listErrorForm: MutableList<TILExtension> = arrayListOf()

    companion object {
        val REQUEST_IMAGE_KTP = 1002
        val IS_SETTING_ACCOUNT = "is_setting_account"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_address_according_ktp)

        address = intent.extras?.getParcelable(KTP_DATA)
        isSettingAccount = intent.extras?.getBoolean(IS_SETTING_ACCOUNT)?:false

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        alertDialogBack = AlertDialogCustom(this, null)
        alertDialogBack.setView(LayoutInflater.from(this).inflate(R.layout.layout_confirmation, null, false))
        alertDialogBack.create()
        alertDialogBack.getView().btn_ok?.setOnClickListener {
            alertDialogBack.dismiss()
            finish()
        }
        alertDialogBack.getView().btn_cancel?.setOnClickListener {
            alertDialogBack.dismiss()
        }

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()
        personalInfoUser = preferencesUtil.getProfile()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.edit_alamat_sesuai_ktp)

        rv_drag_view?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        provinceAdapter = LocationAdapter(object: LocationAdapter.OnClickItem{
            override fun onClickPromo(location: Location) {
                et_province.setText(location.name)
                provinceChoosed = location
                presenter.getListCity(location.id?:"")
                resetLocation()
                hideSlideLayout()
            }
        })

        cityAdapter = LocationAdapter(object: LocationAdapter.OnClickItem{
            override fun onClickPromo(location: Location) {
                et_city?.setText(location.name)
                cityChoosed = location
                presenter.getListSubDistrict(location.id?:"")
                hideSlideLayout()
            }
        })

        subDistrictAdapter = LocationAdapter(object: LocationAdapter.OnClickItem{
            override fun onClickPromo(location: Location) {
                et_sub_district?.setText(location.name)
                subDistrictChoosed = location
                presenter.getListVillage(location.id?:"")
                hideSlideLayout()
            }
        })

        villageAdapter = LocationAdapter(object: LocationAdapter.OnClickItem{
            override fun onClickPromo(location: Location) {
                villageChoosed = location
                et_village?.setText(location.name)
                presenter.getZipCode(location.id?:"")
                hideSlideLayout()
            }
        })

//        val isCompleteDataForm = intent?.extras?.getBoolean(IS_COMPLETE_DATA_FORM, false)
        val isCompleteDataForm = false
        val isBlockedSlideUp = (preferencesUtil.getProfile()?.andalanCustomerId?.isNotEmpty() == true && isCompleteDataForm == true)

        et_province?.setOnClickListener {
            tv_title_drag_view?.text = getString(R.string.province)
            et_drag_view?.visibility = View.GONE
            rv_drag_view?.adapter = provinceAdapter
            if (provinceAdapter.getData().isEmpty()) { presenter.getListProvince() }
            showSlideLayout(isBlockedSlideUp)
        }

        et_city?.setOnClickListener {
            tv_title_drag_view?.text = getString(R.string.city)
            et_drag_view?.visibility = View.GONE
            rv_drag_view?.adapter = cityAdapter
            showSlideLayout(isBlockedSlideUp)
        }

        et_sub_district?.setOnClickListener {
            tv_title_drag_view?.text = getString(R.string.sub_district)
            et_drag_view?.visibility = View.GONE
            rv_drag_view?.adapter = subDistrictAdapter
            showSlideLayout(isBlockedSlideUp)
        }

        et_village?.setOnClickListener {
            tv_title_drag_view?.text = getString(R.string.city)
            et_drag_view?.visibility = View.GONE
            rv_drag_view?.adapter = villageAdapter
            showSlideLayout(isBlockedSlideUp)
        }

        btn_upload_photo?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_KTP)
        }

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            finish()
        }

        btn_save?.setOnClickListener {
            if (cb_term_condition?.isChecked == true) {
                var updateAddressRequest = UpdateAddressRequest(
                    idCustomer = preferencesUtil.getUser()?.idCustomer,
                    addressType = ConfigVar.LEGAL_ADDRESS_TYPE,
                    address = et_full_address.text.toString(),
                    rt = et_rt.text.toString(),
                    rw = et_rw.text.toString(),
                    provinceId = provinceChoosed?.id,
                    regencyId = cityChoosed?.id,
                    districtId = subDistrictChoosed?.id,
                    villageId = villageChoosed?.id,
                    zipCode = et_postal_code.text.toString(),
                    phoneNumber = et_phone.text.toString(),
                    idCardImage = choosedImage,
                    provinceName = et_province.text.toString(),
                    cityName = et_city.text.toString(),
                    districtName = et_sub_district.text.toString(),
                    villageName = et_village.text.toString()
                )
                if (updateAddressRequest.isValid()) {
                    val isCompleteDataForm = intent?.extras?.getBoolean(IS_COMPLETE_DATA_FORM, false)
                    if (isCompleteDataForm == true) {
                        val intent = Intent()
                        intent.putExtra(KTP_DATA, updateAddressRequest)
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    } else {
                        presenter.sendUpdatedDataAddress(updateAddressRequest)
                    }
                } else {
                    showError()
                    Toast.makeText(this@UpdateAddressAccordingKtpActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this@UpdateAddressAccordingKtpActivity, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
            }
        }

        setDefaultData()
    }

    private fun setDefaultData() {
        val user = preferencesUtil.getProfile()
        val isRefensikan = intent.extras?.getBoolean(ISREFERENSI, false)

        if (address == null) {
            address = user?.getAddressLegal()
        }
        if (isRefensikan == null || isRefensikan == false) {
            address?.let {
                et_phone?.setText(it.phoneNumber)
                et_province?.setText(it.provinceName)
                et_city?.setText(it.cityName)
                et_sub_district?.setText(it.districtName)
                et_village?.setText(it.villageName)
                et_postal_code?.setText(it.zipCode)
                et_full_address?.setText(it.address)
                et_rt?.setText(it.rt)
                et_rw?.setText(it.rw)
                if (isSettingAccount) {
                    text_photo_ktp?.visibility = View.VISIBLE
                    container_image?.visibility = View.VISIBLE

                    if (!address?.idCardImage.isNullOrBlank()) {
                        loadImage(this@UpdateAddressAccordingKtpActivity, address?.idCardImage?:"", iv_preview)
                        tv_filename.text = getString(R.string.change_image)
                        choosedImage = address?.idCardImage?:""
                        address?.typeIdCardImage = "url"
                    }
                }
                provinceChoosed = Location(id = it.provinceId)
                cityChoosed = Location(id = it.cityName)
                subDistrictChoosed = Location(id = it.districtId)
                villageChoosed = Location(id = it.villageId)
            }
        } else {
            address = intent.extras?.getParcelable(KTP_DATA)
            address?.let {
                et_phone?.setText(it.phoneNumber)
                et_province?.setText(it.provinceName)
                et_city?.setText(it.cityName)
                et_sub_district?.setText(it.districtName)
                et_village?.setText(it.villageName)
                et_postal_code?.setText(it.zipCode)
                et_full_address?.setText(it.address)
                et_rt?.setText(it.rt)
                et_rw?.setText(it.rw)
                provinceChoosed = Location(id = it.provinceId)
                cityChoosed = Location(id = it.cityName)
                subDistrictChoosed = Location(id = it.districtId)
                villageChoosed = Location(id = it.villageId)
            }
        }
    }

    private fun focusableForm(state: Boolean) {
        et_phone?.isFocusable = state
        et_phone?.isFocusableInTouchMode = state
        et_postal_code?.isFocusable = state
        et_postal_code?.isFocusableInTouchMode = state
        et_full_address?.isFocusable = state
        et_full_address?.isFocusableInTouchMode = state
        et_rt?.isFocusable = state
        et_rt?.isFocusableInTouchMode = state
        et_rw?.isFocusable = state
        et_rw?.isFocusableInTouchMode = state
    }

    private fun resetLocation() {
        et_city?.setText("")
        et_sub_district?.setText("")
        et_village?.setText("")
        et_postal_code?.setText("")
    }

    override fun onGetResponseUpdate(updateAddressResponse: UpdateAddressResponse) {
        if (updateAddressResponse.status == ConfigVar.SUCCESS) {
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
//            alertDialogCustom.getView().tv_intro_text?.text = "${updateAddressResponse.message} - ${updateAddressResponse.ticketNumber}"
            alertDialogCustom.getView().tv_intro_text?.text = "${updateAddressResponse.message}"
            alertDialogCustom.show()
        } else {
            Toast.makeText(this@UpdateAddressAccordingKtpActivity, updateAddressResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onGetCity(mutableList: MutableList<Location>) {
        cityAdapter.removeAll()
        cityAdapter.add(mutableList)

    }

    override fun onGetSubdistrict(mutableList: MutableList<Location>) {
        subDistrictAdapter.removeAll()
        subDistrictAdapter.add(mutableList)
    }

    override fun onGetVillage(mutableList: MutableList<Location>) {
        villageAdapter.removeAll()
        villageAdapter.add(mutableList)
    }

    override fun onGetProvince(mutableList: MutableList<Location>) {
        provinceAdapter.add(mutableList)
    }

    override fun onGetZipCode(zipCode: String) {
        et_postal_code?.setText(zipCode)
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout(state: Boolean = false) {
        if (!state) sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            alertDialogBack.show()
        }
    }

    private fun showImagePickerOptions(request: Int) {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent(request)
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent(request)
            }

        })
    }

    private fun launchCameraIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, request)
    }

    private fun launchGalleryIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_KTP) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    choosedImage = it
                    loadImage(this@UpdateAddressAccordingKtpActivity, it, iv_preview)
                    tv_filename.text = getString(R.string.change_image)
                    address?.typeIdCardImage = "file"
                }
            }
        }
    }

    private fun showError() {
        listErrorForm.clear()
        if (et_phone?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_phone, "Form masih kosong"))
        if (et_province?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_province, "Form masih kosong"))
        if (et_city?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_city, "Form masih kosong"))
        if (et_sub_district?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_sub_district, "Form masih kosong"))
        if (et_village?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_village, "Form masih kosong"))
        if (et_postal_code?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_postal_code, "Form masih kosong"))
        if (et_full_address?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_postal_code, "Form masih kosong"))
        if (et_rt?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_rt, "Form masih kosong"))
        if (et_rw?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_rw, "Form masih kosong"))

        toggleError(list = listErrorForm, state = true)
        hideError(list = listErrorForm)
    }

    private fun toggleError(list: MutableList<TILExtension>, state: Boolean) {
        if (state) {
            list.forEach {
                it.til.error = it.error
                it.til.isErrorEnabled = true
            }
        } else {
            list.forEach {
                it.til.error = ""
                it.til.isErrorEnabled = false
            }
        }
    }

    private fun hideError(list: MutableList<TILExtension>) {
        GlobalScope.launch(Dispatchers.Main) {
            delay(2000)
            toggleError(list, false)
        }
    }
}
