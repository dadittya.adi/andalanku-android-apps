package id.andalan.andalanku.ui.base

interface BaseView {
    fun showWait()

    fun removeWait()

    fun onFailure(appErrorMessage: String?)
}