package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Occupation
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*

class OccupationAdapter(val onItemClickListener: OnClickItem): BaseAdapter<Occupation, OccupationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_default_text
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Occupation>(itemView) {
        override fun bind(data: Occupation) {
            itemView.setOnClickListener {
                onItemClickListener.onClick(data)
            }
            itemView.tv_name.text = data.name
        }

    }

    interface OnClickItem {
        fun onClick(occupation: Occupation)
    }
}