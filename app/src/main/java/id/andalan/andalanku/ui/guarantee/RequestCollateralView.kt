package id.andalan.andalanku.ui.guarantee

import id.andalan.andalanku.model.response.CollateralListResponse
import id.andalan.andalanku.ui.base.BaseView

interface RequestCollateralView: BaseView {
    fun getRequestCollateralList(collateralListResponse: CollateralListResponse)
}