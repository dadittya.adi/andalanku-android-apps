package id.andalan.andalanku.ui.plafond

import android.content.Intent
import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_simulation_plafond.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class SimulationPlafondActivity : BaseApp() {
    companion object {
        val RESULT_PLAFOND = "result_plafond"
    }

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simulation_plafond)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.plafond_simulation_2)

        val plafondValue = intent?.extras?.getString(RESULT_PLAFOND)
        plafondValue?.let {
            tv_plafond_value?.text = it
        }

        btn_back?.setOnClickListener {
            finish()
        }

        btn_submission?.setOnClickListener {
            if ((preferencesUtil.getUser()?.idCustomer?:0) > 0) {
                startActivity(Intent(this, PlafondSubmissionActivity::class.java))
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }
        }
    }
}
