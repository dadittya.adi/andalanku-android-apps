package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Cars
import id.andalan.andalanku.model.ui.UsedCar
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_car_type.view.*

class CarsTypeAdapter(val onItemClickListener: OnClickItem): BaseAdapter<UsedCar, CarsTypeAdapter.ViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarsTypeAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_car_type
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<UsedCar>(itemView) {
        override fun bind(data: UsedCar) {
            itemView.setOnClickListener {
                onItemClickListener.onClickType(data)
            }
            itemView.tv_name_car?.text = data.AssetName
        }

    }

    interface OnClickItem {
        fun onClickType(cars: UsedCar)
    }
}