package id.andalan.andalanku.ui.adapters

import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Document
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_upload_document.view.*
import java.io.File

class DocumentAdapter(val onItemClickListener: OnClick): BaseAdapter<Document, DocumentAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_upload_document
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Document>(itemView) {
        override fun bind(data: Document) {
            itemView.setOnClickListener {
                onItemClickListener.onClickItem(data)
            }

            itemView.tv_hint_photo_ktp.text = data.hintText
            itemView.tv_info_text.visibility = if (data.infoText.isNullOrBlank()) View.GONE else View.VISIBLE
            itemView.tv_info_text.text = data.infoText
            if (data.urlImage?.isNotBlank() == true) {
                loadImage(itemView.context, data.urlImage?:"", itemView.iv_preview)
                itemView.tv_filename.text = "Ubah Foto/Gambar"
            }

        }

    }

    interface OnClick {
        fun onClickItem(document: Document)
    }
}