package id.andalan.andalanku.ui.adapters

import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.AddressName
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*

class AddressAdapter(val onItemClickListener: OnClickItem): BaseAdapter<AddressName, AddressAdapter.ViewHolder>() {
    private var addressName: AddressName? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_default_text
    }

    fun setAddressName(data: AddressName?) {
        addressName = data
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<AddressName>(itemView) {
        override fun bind(data: AddressName) {
            itemView.setOnClickListener {
                onItemClickListener.onClickPromo(data)
            }
            itemView.tv_name.text = data.name
        }

    }

    interface OnClickItem {
        fun onClickPromo(addressName: AddressName)
    }
}