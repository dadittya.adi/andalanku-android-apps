package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_condition.view.*

class ConditionAdapter : BaseAdapter<String, ConditionAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConditionAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_condition
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<String>(itemView) {
        override fun bind(data: String) {
            itemView.tv_conditioon_item.text = data
        }
    }
}