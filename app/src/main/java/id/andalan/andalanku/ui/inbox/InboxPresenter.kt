package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.response.InboxResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class InboxPresenter @Inject constructor(private val service: Services): BasePresenter<InboxView>() {
    override fun onCreate() {
        getListInbox("1")
    }

    fun getListInbox(page: String) {
        view.showWait()
        val subscription = service.getInboxList(page).subscribe(object : Subscriber<InboxResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(inboxResponse: InboxResponse) {
                view.onGetResponse(inboxResponse)
            }
        })
        subscriptions.add(subscription)
    }
}