package id.andalan.andalanku.ui.loading_screen

import id.andalan.andalanku.model.response.TokenResponse
import id.andalan.andalanku.ui.base.BaseView

interface PreloaderView: BaseView {
    fun onGetToken(tokenResponse: TokenResponse)
}