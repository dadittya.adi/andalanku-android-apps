package id.andalan.andalanku.ui.newcar

import id.andalan.andalanku.model.response.CarBrandsResponse
import id.andalan.andalanku.ui.base.BaseView

interface PaymentNewCarView: BaseView {
    fun onGetCarBrands(carBrandsResponse: CarBrandsResponse)
}