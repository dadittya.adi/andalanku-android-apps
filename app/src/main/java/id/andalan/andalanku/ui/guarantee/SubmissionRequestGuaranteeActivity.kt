package id.andalan.andalanku.ui.guarantee

import android.app.Activity
import android.app.DatePickerDialog
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.CollateralRequestResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_submission_request_guarantee.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class SubmissionRequestGuaranteeActivity : BaseApp(), SubmissionRequestGuaranteeView {
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: SubmissionRequestGuaranteePresenter

    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var pickerContractAdapter: PickerAdapter

    private var choosedContract = PickerItem()
    private var choosedDateEvent = ""

    private val calendarEvent = Calendar.getInstance()
    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submission_request_guarantee)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.submission_take_guarantee_title)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        ic_close?.setOnClickListener {
            hideSlideLayout()
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        pickerContractAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerContractAdapter.getData().indexOf(choosedContract)

                choosedContract.isChoosed = false
                if (index > -1) pickerContractAdapter.update(index, choosedContract)
                et_contract?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedContract = pickerItem
                val indexChoosed = pickerContractAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerContractAdapter.update(indexChoosed, choosedContract)

                hideSlideLayout()
            }
        })

        et_contract?.setOnClickListener {
            showSlideLayout()
            rv_item?.adapter = pickerContractAdapter
            tv_title_text?.text = getString(R.string.choose_contract_number)
        }

        openDatePicker()

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            setResult(EComplainActivity.RESPONSE_SEND_COMPLAIN)
            finish()
        }

        btn_send?.setOnClickListener {
            btn_send?.isClickable = false

            val agreementNumber = et_contract?.text.toString()

            if (agreementNumber.isNotBlank() && choosedDateEvent.isNotBlank()) {
                presenter.sendCollateral(agreementNumber, choosedDateEvent)
            } else {
                Toast.makeText(this@SubmissionRequestGuaranteeActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    private fun openDatePicker() {
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendarEvent.set(Calendar.YEAR, year)
            calendarEvent.set(Calendar.MONTH, monthOfYear)
            calendarEvent.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        et_take_guarantee_date.setOnClickListener {
            val dateDialog = DatePickerDialog(
                this@SubmissionRequestGuaranteeActivity, date, calendarEvent
                    .get(Calendar.YEAR), calendarEvent.get(Calendar.MONTH),
                calendarEvent.get(Calendar.DAY_OF_MONTH)
            )
            dateDialog.datePicker.minDate = Date().time + (1000*60*60*24*7)
            dateDialog.show()
        }
    }

    private fun updateLabel() {
        val dateFormat = "dd/MM/yyyy"
        val sdf = SimpleDateFormat(dateFormat, Locale.US)

        val dateEventFormat = "yyyy-MM-dd"
        val sdf2 = SimpleDateFormat(dateEventFormat, Locale.US)

        choosedDateEvent = sdf2.format(calendarEvent.time)
        et_take_guarantee_date.setText(sdf.format(calendarEvent.time))
    }

    override fun onGetResponseSubmission(collateralRequestResponse: CollateralRequestResponse) {
        if (collateralRequestResponse.status == ConfigVar.SUCCESS) {
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_INBOX))
            alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.collateral_request_success)
            alertDialogCustom.show()
        } else {
            if (collateralRequestResponse.message == "already submit") {
                alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.collateral_request_double)
            }
            else {
                alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.collateral_request_failed)
            }
            alertDialogCustom.show()
            //Toast.makeText(this, collateralRequestResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerContractAdapter.add(list)
            container_empty_state.visibility = View.GONE
        } else {
            container_empty_state.visibility = View.VISIBLE
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state.visibility = View.VISIBLE
        Toast.makeText(this, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
