package id.andalan.andalanku.ui.usedcar

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Cars
import id.andalan.andalanku.ui.adapters.CarsTypeAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.fragment_used_car.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*

class UsedCarFragment : androidx.fragment.app.Fragment() {

    private val TAG = UsedCarFragment::class.java.simpleName

    private lateinit var carsTypeAdapter: CarsTypeAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_used_car, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.cars_used_payment)
        toolbar_login.setNavigationOnClickListener { (activity as MainHomeActivity).finish() }

//        carsTypeAdapter = CarsTypeAdapter(object: CarsTypeAdapter.OnClickItem{
//            override fun onClickType(cars: Cars) {
//                hideSlideLayout()
//            }
//        })
        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false

        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        et_type?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.what_is_your_fav_car)
            et_drag_view.hint = getString(R.string.write_search)
            showSlideLayout()
        }

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_drag_view.adapter = carsTypeAdapter

//        initFakeType()
    }

//    private fun initFakeType() {
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 G MT", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 V MT", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 V MT DIESEL", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 G MT", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 G MT", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 G MT", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.add(Cars(id = "123", type = "Toyota NEW KIJANG INNOVA 2.4 G MT", urlImage = "https://via.placeholder.com/600/92c952"))
//        carsTypeAdapter.notifyDataSetChanged()
//    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }
}
