package id.andalan.andalanku.ui.contract


import androidx.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.DetailAndalanFinancialResponse
import id.andalan.andalanku.model.response.DetailUnitResponse
import kotlinx.android.synthetic.main.fragment_detail_unit.*

class DetailUnitFragment : androidx.fragment.app.Fragment(), DetailUnitView {

    lateinit var presenter: DetailUnitPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_unit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let {
            val observerResponse = Observer<DetailAndalanFinancialResponse> {
                // Update the UI, in this case, a TextView.
                it?.andalanDetailUnit?.let {list ->
                    if (list.isNotEmpty()) {
                        val detailUnitResponse = list[0]
                        getDetailUnit(detailUnitResponse)
                    }
                }
            }
            (activity as ContractDetailActivity).model.currentAndalanFinancialResponse.observe(this, observerResponse)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            activity?.let {
                getDetailUnit((activity as ContractDetailActivity).detailUnitResponse?: DetailUnitResponse())
            }
        }
    }

//    fun getDetailUnit(detailUnitResponse: List<DetailUnitResponse>?) {
//        et_merk_type?.setText(detailUnitResponse.assetDescription)
//        et_tahun_pembuatan?.setText(detailUnitResponse.manufacturingYear)
//        et_colour?.setText(detailUnitResponse.colour)
//        et_no_polisi?.setText(detailUnitResponse.licensePlate)
//        et_nomer_rangka?.setText(detailUnitResponse.chasisNo)
//        et_mesin_hint?.setText(detailUnitResponse.engineNo)
//    }

    override fun getDetailUnit(detailUnitResponse: DetailUnitResponse) {
        et_merk_type?.setText(detailUnitResponse.assetDescription)
        et_tahun_pembuatan?.setText(detailUnitResponse.manufacturingYear)
        et_colour?.setText(detailUnitResponse.colour)
        et_no_polisi?.setText(detailUnitResponse.licensePlate)
        et_nomer_rangka?.setText(detailUnitResponse.chasisNo)
        et_mesin_hint?.setText(detailUnitResponse.engineNo)
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }

}
