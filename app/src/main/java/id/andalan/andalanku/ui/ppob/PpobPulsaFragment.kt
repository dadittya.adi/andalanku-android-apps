package id.andalan.andalanku.ui.ppob


import android.Manifest
import android.app.DownloadManager
import android.content.*
import android.os.Bundle
import androidx.core.content.FileProvider
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BuildConfig

import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PartnerListResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.model.ui.Promo
import id.andalan.andalanku.ui.adapters.PpobPulsaPulsaAdapter
import id.andalan.andalanku.ui.adapters.PromoHomeAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.promo.DetailPromoActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.fragment_ppob_pulsa_pulsa.*
import java.io.File
import androidx.lifecycle.Observer
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.payment.InstallmentPaymentProcessActivity
import id.andalan.andalanku.utils.AlertWarningCustom
import kotlinx.android.synthetic.main.layout_warning_alert.view.*

class PpobPulsaFragment : androidx.fragment.app.Fragment(), PpobView{

    private lateinit var onComplete: BroadcastReceiver
    private lateinit var alertDialogProgress: AlertDialogCustom
    lateinit var ppobPulsaPulsaAdapter: PpobPulsaPulsaAdapter
    private lateinit var alertWarningCustom: AlertWarningCustom

    var preferencesUtil: PreferencesUtil? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ppob_pulsa_pulsa, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        alertDialogProgress = AlertDialogCustom(context!!, null)
        alertDialogProgress.setView(LayoutInflater.from(context!!).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(context!!, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        ppobPulsaPulsaAdapter = PpobPulsaPulsaAdapter(object : PpobPulsaPulsaAdapter.OnClickItem {
            override fun onClickProduct(ppobProduct: PpobProduct) {
                val accountNumber = (activity as PpobPulsaActivity).accountNumber

                if (accountNumber == "") {
                    alertWarningCustom.getView().tv_message?.text = "Nomor Ponsel Belum Diisi"
                    alertWarningCustom.show()
                }
                else {
                    (activity as PpobPulsaActivity).productLogo = ppobProduct.logo.toString()
                    ppobProduct.code?.let {
                        (activity as PpobPulsaActivity).presenter.ppobinquiry(
                            (activity as PpobPulsaActivity).accountNumber, it,"top_up",
                            "",""
                        )
                    }
                }
            }
        })

        val observerProductListResponse = Observer<PpobProductListResponse> {
            it?.let {
                onGetProductListResponse(it)
            }
        }
        (activity as PpobPulsaActivity).model.currentPpobProduct.observe(
            viewLifecycleOwner,
            observerProductListResponse
        )

        initProductItem()

    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        if (ppobProductListResponse.status == ConfigVar.SUCCESS) {
            if (ppobProductListResponse.ppobProduct.isNullOrEmpty()) {
                //empty response
            } else {
                ppobPulsaPulsaAdapter.removeAll()
                ppobPulsaPulsaAdapter.add(ppobProductListResponse.ppobProduct)

                initProductItem()
            }
        }
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

    }


    override fun showWait() {
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    fun initProductItem() {
        rv_ppob_pulsa_pulsa.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = ppobPulsaPulsaAdapter
        }
    }

}
