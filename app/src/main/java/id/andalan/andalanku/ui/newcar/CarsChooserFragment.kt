package id.andalan.andalanku.ui.newcar


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Cars
import id.andalan.andalanku.ui.adapters.CarsAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import kotlinx.android.synthetic.main.fragment_cars_chooser.*
import kotlinx.android.synthetic.main.toolbar_login.*

class CarsChooserFragment : androidx.fragment.app.Fragment() {
    private val TAG = CarsChooserFragment::class.java.simpleName

    private lateinit var carsAdapter: CarsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cars_chooser, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.choose_your_favorite_car)
        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
//
//        carsAdapter = CarsAdapter(object: CarsAdapter.OnClickItem {
//            override fun onClickImage(cars: Cars) {
//                val intent = Intent(context, CarsDetailActivity::class.java)
//                startActivity(intent)
//            }
//
//        })

        rv_cars?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_cars?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_cars?.adapter = carsAdapter

//        initFakeCars()
    }
//
//    private fun initFakeCars() {
//        carsAdapter.add(Cars(id = "1", name = "Alpard", brand = "Toyota", price = "999,9 jt"))
//        carsAdapter.add(Cars(id = "1", name = "Alpard", brand = "Toyota", price = "999,9 jt"))
//        carsAdapter.add(Cars(id = "1", name = "Alpard", brand = "Toyota", price = "999,9 jt"))
//        carsAdapter.add(Cars(id = "1", name = "Alpard", brand = "Toyota", price = "999,9 jt"))
//    }
}
