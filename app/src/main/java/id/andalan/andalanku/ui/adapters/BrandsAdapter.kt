@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Brands
import id.andalan.andalanku.model.ui.CarBrand
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_brand_cars.view.*
import java.lang.Exception

class BrandsAdapter(val onItemClickListener: OnClickItem): BaseAdapter<CarBrand, BrandsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_brand_cars
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<CarBrand>(itemView) {
        override fun bind(carBrand: CarBrand) {
            val data = carBrand.getBrands()
            itemView.setOnClickListener {
                onItemClickListener.onClickImage(carBrand.getBrands(), carBrand)
            }
            try {
                loadImage(itemView.context, data.urlImage?:"", itemView.iv_brands)
            } catch (e: Exception) { }
        }

    }

    interface OnClickItem {
        fun onClickImage(brands: Brands, carBrand: CarBrand)
    }
}