package id.andalan.andalanku.ui.adapters

import android.content.Context
import id.andalan.andalanku.utils.ekstensions.loadImage
import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class CarsDetailSliderAdapter(val context: Context, private val listImage: MutableList<String>): SliderAdapter() {
    override fun getItemCount(): Int {
        return listImage.size
    }

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder) {
        loadImage(context, listImage[position], imageSlideViewHolder.imageView)
    }
}