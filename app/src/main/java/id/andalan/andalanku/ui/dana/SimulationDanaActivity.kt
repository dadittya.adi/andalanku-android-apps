package id.andalan.andalanku.ui.dana

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.util.Log.e
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.TenorAdapter
import id.andalan.andalanku.ui.adapters.ZoneAreaAdapter
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.newcar.CarsDetailPresenter
import id.andalan.andalanku.ui.newcar.CarsDetailView
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.ui.payment.SimulationDanaResultActivity
import id.andalan.andalanku.utils.AlertDialogUtil
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getNumberOnly
import id.andalan.andalanku.ui.adapters.PremiAdapter
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_simulation_dana.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.layout_agunan_extra.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import javax.inject.Inject



class SimulationDanaActivity : BaseApp(), CarsDetailView {
    override fun onGetUsedCarsList(usedCarListResponse: UsedCarListResponse) {
    }

    companion object {
        val REQUEST_DANA_SIMULATION = 981
        val RESPONSE_DANA_SIMULATION = 982
        val CODE_AGUNAN_TYPE_REQUEST = 142
        val CODE_AGUNAN_TYPE_RESULT = 175
        val BPKB = "bpkb"
        val SERTIPIKAT = "sertipikat"
        val CHOOSED_INDEX = "index"
        val COLLATERAL = "collateral"
    }
    private val TAG = SimulationDanaActivity::class.java.simpleName

    private var choosedItem = -1
    private var sertipikat = Sertipikat()
    private var bpkb = Bpkb()
    private lateinit var tenorAdapter: TenorAdapter
    private lateinit var zoneAreaAdapter: ZoneAreaAdapter
    private lateinit var premiAdapter: PremiAdapter

    private var choosedTenor: Tenor? = null
    private var choosedZone: ZoneArea? = null
    private var choosedPremi: Premi? = null
    private var carPrice: String? = null
    private lateinit var creditSimulationRequest: CreditSimulationRequest
    private lateinit var creditSimulation: CreditSimulationO
    private lateinit var collateral: Collateral

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: CarsDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simulation_dana)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.dana_andalanku)

        deps.inject(this)
        presenter.view = this

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        btn_fill_data.setOnClickListener {
            val intent = Intent(this, ChooseDocumentAgunanActivity::class.java)
            intent.putExtra(CHOOSED_INDEX, choosedItem)
            intent.putExtra(SERTIPIKAT, sertipikat)
            intent.putExtra(BPKB, bpkb)
            startActivityForResult(intent, CODE_AGUNAN_TYPE_REQUEST)
        }

        et_kebutuhan_dana.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    e(TAG, price.toString())
                    if (et_kebutuhan_dana.text.toString() != price.convertToRpFormat()) et_kebutuhan_dana.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_kebutuhan_dana.setSelection(text.length - 2)
                }
            }

        })

        tenorAdapter = TenorAdapter(object: TenorAdapter.OnClickItem{
            override fun onClickPromo(tenor: Tenor) {
                choosedTenor = tenor
                et_jangka_waktu?.setText(tenor.getMonth(getString(R.string.month)))
                hideSlideLayout()
            }
        })
        zoneAreaAdapter = ZoneAreaAdapter(object: ZoneAreaAdapter.OnClickItem{
            override fun onClickPromo(zoneArea: ZoneArea) {
                choosedZone = zoneArea
                et_zone?.setText(zoneArea.name)
                hideSlideLayout()
            }
        })

        et_jangka_waktu?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.pilih_waktu_peminjaman)
            et_drag_view.visibility = View.GONE
            rv_drag_view?.adapter = tenorAdapter
            showSlideLayout()
        }
        et_zone?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_area_zone)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = zoneAreaAdapter
            showSlideLayout()
        }
        et_premi?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_insurance)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = premiAdapter
            showSlideLayout()
        }


        btn_calc.setOnClickListener {
            val otrText = et_kebutuhan_dana.text.toString()
            val otrPrice = otrText.getNumberOnly()
            val tenor = choosedTenor?.id?:0
            val zone = choosedZone?.id?:0
            val premi = choosedPremi?.id?:0

            creditSimulationRequest = CreditSimulationRequest(
                type = ConfigVar.KMG,
                otr = otrPrice.toString(),
                dp = "0",
                tenor = tenor.toString(),
                wilayah = zone.toString(),
                asuransi = premi.toString(),
                car_price = carPrice
            )

            if (creditSimulationRequest.isValidKMG()) {
                var intPrice: Int
                var intCarPrice: Int
                try {
                    intPrice = otrPrice.toInt()
                    intCarPrice = carPrice!!.toInt()
                } catch (nfe: NumberFormatException) {
                    intPrice = 0
                    intCarPrice = 0
                }

                //var intLimit = 0
                //intLimit = intCarPrice * (0.85)

                Log.e("carPrice", intCarPrice.toString())
                Log.e("otr", intPrice.toString())
                Log.e("limit", intCarPrice.times(0.85).toInt().toString())
                if (intPrice > intCarPrice.times(0.85)) {
                    Toast.makeText(this@SimulationDanaActivity, "Pengajuan Dana Andalanku Maksimal 85% Dari Harga Mobil",
                        Toast.LENGTH_SHORT).show()
                    et_kebutuhan_dana.setText(intCarPrice.times(0.85).toInt().toString())
                } else {
                    presenter.getCreditSimulation(creditSimulationRequest)
                }
            } else {
                Toast.makeText(this@SimulationDanaActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }

        premiAdapter = PremiAdapter(object: PremiAdapter.OnClickItem{
            override fun onClickPromo(premi: Premi) {
                choosedPremi = premi
                et_premi?.setText(premi.name)
                hideSlideLayout()
            }
        })

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        initFakeData()
    }

    private fun initFakeData() {
        tenorAdapter.add(Tenor(id = 3))
        tenorAdapter.add(Tenor(id = 6))
        tenorAdapter.add(Tenor(id = 12))
        tenorAdapter.add(Tenor(id = 18))
        tenorAdapter.add(Tenor(id = 24))
        tenorAdapter.add(Tenor(id = 36))
        tenorAdapter.add(Tenor(id = 48))

        premiAdapter.add(Premi(id = 1, type = getString(R.string.tlo), name = getString(R.string.tlo)))
        premiAdapter.add(Premi(id = 2, type = getString(R.string.ark), name = getString(R.string.all_risk)))

        zoneAreaAdapter.add(ZoneArea(id = 1, name = getString(R.string.zone_1)))
        zoneAreaAdapter.add(ZoneArea(id = 2, name = getString(R.string.zone_2)))
        zoneAreaAdapter.add(ZoneArea(id = 3, name = getString(R.string.zone_3)))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CODE_AGUNAN_TYPE_REQUEST && resultCode == CODE_AGUNAN_TYPE_RESULT) {
            til_kebutuhan_dana?.visibility = View.VISIBLE
            val index = data?.extras?.getInt(CHOOSED_INDEX)
            choosedItem = index?:-1
            if (index == 0) {
                val bpkb: Bpkb= data.extras?.getParcelable(BPKB)?: Bpkb()
                this.bpkb = bpkb
                tv_agunan_text.text = "BPKB ${bpkb.brand} An. ${bpkb.owners}"
                carPrice = bpkb.price
                setToUpdateButton()
            } else if (index == 1) {
                val sertipikat: Sertipikat = data.extras?.getParcelable(SERTIPIKAT)?:Sertipikat()
                this.sertipikat = sertipikat
                tv_agunan_text.text = "${sertipikat.type} An. ${sertipikat.ownersArea}"
                setToUpdateButton()
            }
            collateral = data?.extras?.getParcelable(CompleteDocumentActivity.COLLATERAL)?:Collateral()
        }
        if (requestCode == SimulationDanaResultActivity.REQUEST_RESULT_DANA
            && resultCode == SimulationDanaResultActivity.RESPONSE_RESULT_DANA) {
            val isApply = data?.getBooleanExtra(PaymentSimulationActivity.APPLY, false)
            creditSimulation = data?.getParcelableExtra(PaymentSimulationActivity.CREDIT)?: CreditSimulationO()
            if (isApply == true) {
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
                intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
                setResult(RESPONSE_DANA_SIMULATION, intent)
                finish()
            }
        }
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        hideKeyboard(this)
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    private fun setToUpdateButton() {
        ic_next.visibility = View.GONE
        btn_fill_data.text = getString(R.string.update_text)
        btn_calc.isEnabled = true
        container_form_extra.visibility = View.VISIBLE
    }

    override fun onGetCreditSimulation(creditSimulationResponse: CreditSimulationResponse) {
        if (creditSimulationResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this@SimulationDanaActivity, SimulationDanaResultActivity::class.java)
            intent.putExtra(SimulationDanaResultActivity.CREDIT, creditSimulationResponse)
            intent.putExtra(SimulationDanaResultActivity.COLLATERAL, collateral)
            intent.putExtra(SimulationDanaResultActivity.CREDIT_SIMULATION_REQUEST, creditSimulationRequest)
            startActivityForResult(intent, SimulationDanaResultActivity.REQUEST_RESULT_DANA)
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }
                override fun onClickOk() {
                }
            }, this, getString(R.string.dana_andalanku), creditSimulationResponse.message?:"")
        }
    }

    override fun onGetCarTrim(carTrimsResponse: CarTrimsResponse) {
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
