package id.andalan.andalanku.ui.onboarding

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.activity_onboard_start.*


class OnboardStart : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboard_start)

        btn_onboarding_start.setOnClickListener {
            finish()
            val intentOnboard = Intent(this@OnboardStart, OnboardPage1::class.java)
            startActivity(intentOnboard)
        }
    }
}