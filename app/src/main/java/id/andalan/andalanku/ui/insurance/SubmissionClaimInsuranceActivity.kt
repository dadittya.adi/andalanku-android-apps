package id.andalan.andalanku.ui.insurance

import android.Manifest
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.annotation.Nullable
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.InsuranceClaimRequest
import id.andalan.andalanku.model.response.InsuranceClaimResponse
import id.andalan.andalanku.model.ui.Agreement
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.ImageTemporaryAdapter
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.complain.SubmissionComplainActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.ImagePickerActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.*
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.btn_container_photo
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.btn_send
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.et_category
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.et_message
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.rv_image_media
import kotlinx.android.synthetic.main.activity_submission_claim_insurance.sliding_layout
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class SubmissionClaimInsuranceActivity : BaseApp(), SubmissionClaimInsuranceView {
    @Inject
    lateinit var presenter: SubmissionClaimInsurancePresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var pickerContractAdapter: PickerAdapter
    private lateinit var pickerEventCaseAdapter: PickerAdapter
    private lateinit var imageTempAdapter: ImageTemporaryAdapter

    private var choosedContract = PickerItem()
    private var choosedEventCase = PickerItem()
    private var choosedCategory = PickerItem()

    private val calendarEvent = Calendar.getInstance()
    private var choosedDateEvent = ""
    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submission_claim_insurance)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.claim_insurance_submission)

        ic_close?.setOnClickListener {
            collapsePanel()
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        pickerContractAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerContractAdapter.getData().indexOf(choosedContract)

                choosedContract.isChoosed = false
                if (index > -1) pickerContractAdapter.update(index, choosedContract)
                pickerItem.isChoosed = true
                setContractEditText(pickerItem)
                choosedCategory = PickerItem(id = (choosedContract.realObject as Agreement).insuranceCategory)
                presenter.onGetCategory()
                val indexChoosed = pickerContractAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerContractAdapter.update(indexChoosed, choosedContract)

                collapsePanel()
            }
        })

        imageTempAdapter = ImageTemporaryAdapter(object: ImageTemporaryAdapter.OnClickItem{
            override fun onClickDelete(image: String) {
                val index = imageTempAdapter.getData().indexOf(image)
                if (index > -1) imageTempAdapter.removeAt(index)
            }
        })

        et_contract?.setOnClickListener {
            showPanel()
            rv_item?.adapter = pickerContractAdapter
            tv_title_text?.text = getString(R.string.choose_contract_number)
        }

//        et_category?.setOnClickListener {
//            showPanel()
//            rv_item?.adapter = pickerCategoryAdapter
//            tv_title_text?.text = getString(R.string.choose_insurance_category)
//        }

        et_case?.setOnClickListener {
            showPanel()
            rv_item?.adapter = pickerEventCaseAdapter
            tv_title_text?.text = getString(R.string.choose_event_case)
        }

        openDatePicker()

        rv_image_media?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_image_media?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_image_media?.adapter = imageTempAdapter

        btn_container_photo.setOnClickListener {
            openImagePicker()
        }

        btn_send.setOnClickListener {
            val insuranceClaimRequest = InsuranceClaimRequest(
                idCustomer = preferencesUtil.getUser()?.idCustomer.toString(),
                agreement_number = choosedContract.name,
                claim_type = choosedCategory.id,
                event_case = choosedEventCase.id,
                event_date = choosedDateEvent,
                location = et_location.text.toString(),
                notes = et_message?.text.toString()
            )

            if (insuranceClaimRequest.isValid()){
                btn_send.isClickable = false
                presenter.sendData(insuranceClaimRequest, imageTempAdapter.getData())
            } else {
                Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            setResult(EComplainActivity.RESPONSE_SEND_COMPLAIN)
            finish()
        }
    }

    private fun setContractEditText(pickerItem: PickerItem) {
        choosedContract = pickerItem
        et_contract?.setText(pickerItem.name)
    }

    private fun openDatePicker() {
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendarEvent.set(Calendar.YEAR, year)
            calendarEvent.set(Calendar.MONTH, monthOfYear)
            calendarEvent.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        et_date_case.setOnClickListener {
            val dateDialog = DatePickerDialog(
                this@SubmissionClaimInsuranceActivity, date, calendarEvent
                    .get(Calendar.YEAR), calendarEvent.get(Calendar.MONTH),
                calendarEvent.get(Calendar.DAY_OF_MONTH)
            )
            dateDialog.datePicker.maxDate = Date().time
            dateDialog.show()
        }
    }

    private fun updateLabel() {
        val dateFormat = "dd/MM/yyyy"
        val sdf = SimpleDateFormat(dateFormat, Locale.US)

        val dateEventFormat = "yyyy-MM-dd"
        val sdf2 = SimpleDateFormat(dateEventFormat, Locale.US)

        choosedDateEvent = sdf2.format(calendarEvent.time)
        et_date_case.setText(sdf.format(calendarEvent.time))
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerContractAdapter.add(list)
            container_empty_state.visibility = View.GONE
        } else {
            container_empty_state.visibility = View.VISIBLE
        }
    }

    override fun onGetCategoryInsurance(list: MutableList<PickerItem>) {
        val item = list.find { it ->
            e("tag", choosedCategory.id?:"" + " ," + it.id)
            it.id == choosedCategory.id
        }
        e("tag", choosedCategory.id?:"")
        et_category?.setText(item?.name)
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }

        })
    }

    private fun openImagePicker() {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions()
                    } else {
                        // TODO - handle permission denied case
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, SubmissionComplainActivity.REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, SubmissionComplainActivity.REQUEST_IMAGE)
    }

    override fun onGetEventCase(list: MutableList<PickerItem>) {
        pickerEventCaseAdapter= PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerEventCaseAdapter.getData().indexOf(choosedEventCase)

                choosedEventCase.isChoosed = false
                if (index > -1) pickerEventCaseAdapter.update(index, choosedEventCase)
                et_case?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedEventCase = pickerItem
                val indexChoosed = pickerEventCaseAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerEventCaseAdapter.update(indexChoosed, choosedEventCase)

                collapsePanel()
            }
        })
        pickerEventCaseAdapter.add(list)
    }

    private fun showPanel() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    private fun collapsePanel() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    override fun onGetResponseClaimInsurance(insuranceClaimResponse: InsuranceClaimResponse) {
        if (insuranceClaimResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.getView().tv_intro_text?.text = insuranceClaimResponse.message
            alertDialogCustom.show()
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_INBOX))
        } else {
            Toast.makeText(this, insuranceClaimResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        if (requestCode == SubmissionComplainActivity.REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                try {
                    imageTempAdapter.add(uri?.path?:"")
                    imageTempAdapter.notifyDataSetChanged()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state.visibility = View.VISIBLE
        Toast.makeText(this, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
