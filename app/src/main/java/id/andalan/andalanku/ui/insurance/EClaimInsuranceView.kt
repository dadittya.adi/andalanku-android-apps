package id.andalan.andalanku.ui.insurance

import id.andalan.andalanku.model.response.ClaimInsuranceListResponse
import id.andalan.andalanku.ui.base.BaseView

interface EClaimInsuranceView: BaseView {
    fun onGetEClaimResponse(claimInsuranceListResponse: ClaimInsuranceListResponse)
}