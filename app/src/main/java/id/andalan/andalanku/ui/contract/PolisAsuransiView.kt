package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailInsuranceResponse
import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.ui.base.BaseView

interface PolisAsuransiView: BaseView {
    fun getDetailPolis(detailInsuranceResponse: DetailInsuranceResponse)
}