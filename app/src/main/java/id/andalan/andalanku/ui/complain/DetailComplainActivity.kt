package id.andalan.andalanku.ui.complain

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Complain
import id.andalan.andalanku.ui.adapters.AttachmentComplainAdapter
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.activity_detail_complain.*
import kotlinx.android.synthetic.main.toolbar_login.*

class DetailComplainActivity : BaseApp() {
    companion object {
        val COMPLAIN = "complain"
    }
    private lateinit var attachmentComplainAdapter: AttachmentComplainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_complain)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.detail_complain)

        val complain: Complain? = intent?.extras?.getParcelable(COMPLAIN)
        e("taG image", complain?.mediaList.toString())

        attachmentComplainAdapter = AttachmentComplainAdapter(object: AttachmentComplainAdapter.OnClickItem{
            override fun onClickImage(urlImage: String) {
            }
        })

        rv_attachment?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_attachment?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                false
            )
        rv_attachment?.adapter = attachmentComplainAdapter

        complain?.let {
            tv_reported_number?.text = it.ticketNumber
            tv_category?.text = it.category
            tv_date_sent?.text = it.getSubmittedDateUI(this.getMonths())
            tv_status_complain?.text = it.status
            tv_message?.text = it.message
            tv_phone?.text = it.phone
            tv_follow_up_status?.text = it.solution?:"-"
            attachmentComplainAdapter.add(it.mediaList?: arrayListOf())
        }
    }
}
