package id.andalan.andalanku.ui.plafond

import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.PlafondApplicationRequest
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.response.BranchListResponse
import id.andalan.andalanku.model.response.PlafondApplicationResponse
import id.andalan.andalanku.model.ui.Branch
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.model.ui.Occupation
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.BranchAdapter
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.adapters.OccupationAdapter
import id.andalan.andalanku.ui.address.UpdateAddressAccordingDomicileActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingWorkPlaceActivity
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.DOMICILE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.DOMICILE_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.ISREFERENSI
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.IS_COMPLETE_DATA_FORM
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.KTP_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.OFFICE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.OFFICE_REQ
import id.andalan.andalanku.ui.loan_application.CompleteDataPresenter
import id.andalan.andalanku.ui.loan_application.CompleteDataView
import id.andalan.andalanku.utils.*
import id.andalan.andalanku.utils.ekstensions.loadImage
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_plafond_submission.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class PlafondSubmissionActivity : BaseApp(), CompleteDataView {
    companion object {
        val REQUEST_IMAGE_KTP = 11121
        val REQUEST_IMAGE_SLIP_GAJI = 11122
    }
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: CompleteDataPresenter
    lateinit var occupationAdapter: OccupationAdapter
    lateinit var branchAdapter: BranchAdapter
    private lateinit var menuBottomAdapter: MenuAdapterProfile
    private var occupationChoosed: Occupation? = null
    private var branchChoosed: Branch? = null
    private var dateChoosed: String = ""
    private var dateOfBirthChoosed: String = ""
    private var address: UpdateAddressRequest? = null
    private var addressDomicile: UpdateAddressRequest? = null
    private var addressOffice: UpdateAddressRequest? = null
    private val dataCalendar = Calendar.getInstance()
    private val dobCalendar = Calendar.getInstance()

    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    private var imageKtp = ""
    private var typeImageKtp = ""
    private var imageSlipGaji = ""
    private var typeImageSlipGaji = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plafond_submission)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.submission_plafond)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            finish()
        }

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        occupationAdapter = OccupationAdapter(object: OccupationAdapter.OnClickItem{
            override fun onClick(occupation: Occupation) {
                occupationChoosed = occupation
                et_job.setText(occupation.name)
                hideSlideLayout()
            }
        })

        menuBottomAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.alamat_sesuai_ktp)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingKtpActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(ISREFERENSI, false)
                    intent.putExtra(KTP_DATA, address)
                    startActivityForResult(intent, KTP_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_domisili)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingDomicileActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(ISREFERENSI, false)
                    intent.putExtra(DOMICILE_DATA, addressDomicile)
                    startActivityForResult(intent, DOMICILE_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_kantor)) {
                    val intent = Intent(baseContext, UpdateAddressAccordingWorkPlaceActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(OFFICE_DATA, addressOffice)
                    intent.putExtra(ISREFERENSI, false)
                    startActivityForResult(intent, OFFICE_REQ)
                }
            }
        })

        branchAdapter = BranchAdapter(object: BranchAdapter.OnClickItem{
            override fun onClick(data: Branch) {
                branchChoosed = data
                et_choose_branch?.setText(branchChoosed?.address)
                hideSlideLayout()
            }
        })

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        val isBlockedSlideUp = false
        et_job?.setOnClickListener {
            if (occupationAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.job)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = occupationAdapter
            showSlideLayout(isBlockedSlideUp)
        }

        et_choose_branch?.setOnClickListener {
            if (branchAdapter.getData().isEmpty()) { presenter.getListBranch(address?.regencyId?:"") }
            tv_title_drag_view.text = "Pilih Cabang"
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = branchAdapter
            showSlideLayout()
        }

        et_salary?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    if (et_salary.text.toString() != price.convertToRpFormat()) et_salary.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_salary.setSelection(text.length - 2)
                }
            }
        })

        et_additional_salary?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    if (et_additional_salary.text.toString() != price.convertToRpFormat()) et_additional_salary.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_additional_salary.setSelection(text.length - 2)
                }
            }
        })

        btn_next?.setOnClickListener {
            val plafondApplicationRequest = PlafondApplicationRequest(
                occupationId = occupationChoosed?.id?:"",
                monthlyIncome = et_salary.text.toString().getNumberOnly().toString(),
                sideJob = et_source_income?.text.toString(),
                additionalIsncome = et_additional_salary.text.toString().getNumberOnly().toString(),
                andalanBranchId = branchChoosed?.branchID?:"",
                surveyDate = dateChoosed,
                surveyTime = et_time.text.toString(),

                provinceId = address?.provinceId,
                cityId = address?.regencyId,
                districtId = address?.districtId,
                villageId = address?.villageId,
                address = address?.address,
                rt = address?.rt,
                rw = address?.rw,
                zipCode = address?.zipCode,

                provinceIdDomicile = addressDomicile?.provinceId,
                cityIdDomicile = addressDomicile?.regencyId,
                districtIdDomicile = addressDomicile?.districtId,
                villageIdDomicile = addressDomicile?.villageId,
                addressDomicile = addressDomicile?.address,
                rtDomicile = addressDomicile?.rt,
                rwDomicile = addressDomicile?.rw,
                zipCodeDomicile = addressDomicile?.zipCode,

                provinceIdOffice = addressOffice?.provinceId,
                cityIdOffice = addressOffice?.regencyId,
                districtIdOffice = addressOffice?.districtId,
                villageIdOffice = addressOffice?.villageId,
                addressOffice = addressOffice?.address,
                rtOffice = addressOffice?.rt,
                rwOffice = addressOffice?.rw,
                zipCodeOffice = addressOffice?.zipCode,

                fotoKtp = imageKtp,
                typeFotoKtp = typeImageKtp,
                fotoSlipGaji = imageSlipGaji,
                typeFotoSlipGaji = typeImageSlipGaji
            )
            if (plafondApplicationRequest.isValid()) {
                presenter.getResponsePlafondResponse(plafondApplicationRequest)
            } else {
                Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }

        rv_list_alamat?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_list_alamat?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_list_alamat?.adapter = menuBottomAdapter
        rv_list_alamat?.isNestedScrollingEnabled = false

        initMenu()

        openDatePicker()
        loadPersonalInfo()
    }

    override fun onResponseApplicationPlafond(plafondApplicationResponse: PlafondApplicationResponse) {
        if (plafondApplicationResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.getView().tv_intro_text?.text = plafondApplicationResponse.message
            alertDialogCustom.show()
        } else {
            Toast.makeText(this, plafondApplicationResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun initMenu() {
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_sesuai_ktp)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_domisili)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_kantor)))
    }

    private fun loadPersonalInfo() {
        presenter.getOccupationList()
        val dataPersonal = preferencesUtil.getProfile()
        val dataSimulationPlafond = preferencesUtil.getSimulationPlafond()
        address = dataPersonal?.getAddressLegal()
        addressDomicile = dataPersonal?.getAddressDomicile()
        addressOffice = dataPersonal?.getAddressOffice()

        val btnUploadKtp = container_ktp?.findViewById<LinearLayout>(R.id.btn_upload_photo)
        btnUploadKtp?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_KTP)
        }
        val btnUploadImageSlipGaji = container_image_slip_gaji?.findViewById<LinearLayout>(R.id.btn_upload_photo)
        btnUploadImageSlipGaji?.setOnClickListener {
            showImagePickerOptions(REQUEST_IMAGE_SLIP_GAJI)
        }

        val tvImageKtp = container_ktp?.findViewById<ImageView>(R.id.iv_preview)?: ImageView(this)
        val tvNameKtp = container_ktp?.findViewById<TextView>(R.id.tv_filename)
        imageKtp = dataPersonal?.fotoKtp?:""
        if (dataPersonal?.fotoKtp?.isNotBlank() == true) {
            loadImage(this@PlafondSubmissionActivity, imageKtp, tvImageKtp)
            tvNameKtp?.text = this@PlafondSubmissionActivity.getString(R.string.change_image)
        }

        val tvImageSlipGaji = container_image_slip_gaji?.findViewById<ImageView>(R.id.iv_preview)?:ImageView(this)
        val tvNameSlipGaji = container_image_slip_gaji?.findViewById<TextView>(R.id.tv_filename)
        imageSlipGaji = dataPersonal?.fotoSlipGaji?:""
        if (dataPersonal?.fotoSlipGaji?.isNotBlank() == true) {
            loadImage(this@PlafondSubmissionActivity, imageSlipGaji, tvImageSlipGaji)
            tvNameSlipGaji?.text = this@PlafondSubmissionActivity.getString(R.string.change_image)
        }

        et_full_name?.setText(dataPersonal?.customerName)
        if (dataPersonal?.gender == "M" || dataPersonal?.gender == "L") {
            rg_container.check(radio_male.id)
        }
        else if (dataPersonal?.gender === "P" || dataPersonal?.gender === "F") {
            rg_container.check(radio_female.id)
        }
        et_email?.setText(dataPersonal?.email)
        et_place_of_birth?.setText(dataPersonal?.placeOfBirth)
        this.let {
            et_date_of_birth?.setText(dataPersonal?.getDOBUI(it.getMonths()))
        }
        dateOfBirthChoosed = dataPersonal?.dateOfBirth?:""
        et_ktp_number?.setText(dataPersonal?.idNumber)
        et_mother_name?.setText(dataPersonal?.motherName)
        et_label_home_phone?.setText(dataPersonal?.areaCode)
        et_telephone_number?.setText(dataPersonal?.homePhoneNumber)
        et_phone_number?.setText(dataPersonal?.phoneNumber)
        val salary = (dataSimulationPlafond?.monthlyIncome?.getNumberOnly()?:0).convertToRpFormat()
        et_salary?.setText(salary)
        val additionalSalary = (dataSimulationPlafond?.additionalIsncome?.getNumberOnly()?:0).convertToRpFormat()
        et_additional_salary?.setText(additionalSalary)
        if (dataPersonal?.sideJob.isNullOrEmpty()) {
            et_source_income?.setText(preferencesUtil.getSideJob())
        } else {
            et_source_income?.setText(dataPersonal?.sideJob)
        }

        if (dataPersonal?.occupation.isNullOrEmpty()) {
            occupationChoosed = preferencesUtil.getJob()
            et_job?.setText(occupationChoosed?.name)
        } else {
            occupationChoosed = Occupation(name = dataPersonal?.occupation)
            et_job?.setText(occupationChoosed?.name)
        }
    }

    private fun focusableForm(state: Boolean) {
        et_full_name?.isFocusable = state
        et_full_name?.isFocusableInTouchMode = state
        et_email?.isFocusable = state
        et_email?.isFocusableInTouchMode = state
        et_place_of_birth?.isFocusable = state
        et_place_of_birth?.isFocusableInTouchMode = state
        et_ktp_number?.isFocusable = state
        et_ktp_number?.isFocusableInTouchMode = state
        et_mother_name?.isFocusable = state
        et_mother_name?.isFocusableInTouchMode = state
        et_telephone_number?.isFocusable = state
        et_telephone_number?.isFocusableInTouchMode = state
        et_phone_number?.isFocusable = state
        et_phone_number?.isFocusableInTouchMode = state
        et_salary?.isFocusable = state
        et_salary?.isFocusableInTouchMode = state
        et_additional_salary?.isFocusable = state
        et_additional_salary?.isFocusableInTouchMode = state
        et_source_income?.isFocusable = state
        et_source_income?.isFocusableInTouchMode = state
    }

    private fun openDatePicker() {
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            dataCalendar.set(Calendar.YEAR, year)
            dataCalendar.set(Calendar.MONTH, monthOfYear)
            dataCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val textFormat = "dd/MM/yyyy"
            val sdfText = SimpleDateFormat(textFormat, Locale.US)
            val dataFormat = "yyyy-MM-dd"
            val sdfData = SimpleDateFormat(dataFormat, Locale.US)
            dateChoosed = sdfData.format(dataCalendar.time)

            this.let {
                et_date?.setText(sdfText.format(dataCalendar.time).stringToDate("dd/MM/yyyy").dateToString(it.getMonths()))
            }
        }

        val dateOfBirth = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            dobCalendar.set(Calendar.YEAR, year)
            dobCalendar.set(Calendar.MONTH, monthOfYear)
            dobCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val textFormat = "dd/MM/yyyy"
            val sdfText = SimpleDateFormat(textFormat, Locale.US)
            val dataFormat = "yyyy-MM-dd"
            val sdfData = SimpleDateFormat(dataFormat, Locale.US)
            dateOfBirthChoosed = sdfData.format(dobCalendar.time)

            this.let {
                et_date_of_birth.setText(sdfText.format(dobCalendar.time).stringToDate("dd/MM/yyyy").dateToString(it.getMonths()))
            }
        }

        et_date.setOnClickListener {
            val datePicker = DatePickerDialog(
                this, date, dataCalendar
                    .get(Calendar.YEAR), dataCalendar.get(Calendar.MONTH),
                dataCalendar.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.datePicker.minDate = Date().time
            datePicker.show()
        }

        et_date_of_birth.setOnClickListener {
            DatePickerDialog(
                this, dateOfBirth, dobCalendar
                    .get(Calendar.YEAR)-10, dobCalendar.get(Calendar.MONTH),
                dobCalendar.get(Calendar.DAY_OF_MONTH)
            ).show()
        }

        et_time.setOnClickListener {
            openTimePicker()
        }
    }

    private fun openTimePicker() {
        val hour = dataCalendar.get(Calendar.HOUR_OF_DAY)
        val minute = dataCalendar.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(this,
            TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                et_time.setText(
                    String.format("%02d:%02d", selectedHour, selectedMinute)
                )
            }, hour, minute, true
        )
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    override fun onGetProvince(mutableList: MutableList<Location>) {
//        provinceAdapter.add(mutableList)
//        if (provinceChoosed != null || provinceChoosed?.name?.isNotBlank() == true) {
//            val item = mutableList.find { it.name == provinceChoosed?.name }
//            item?.let {
//                provinceChoosed = it
//                presenter.getListCity(it.id?:"")
//            }
//        }
    }

    override fun onGetCity(mutableList: MutableList<Location>) {
//        cityAdapter.removeAll()
//        cityAdapter.add(mutableList)
//
//        if (cityChoosed != null || cityChoosed?.name?.isNotBlank() == true) {
//            val item = mutableList.find { it.name == cityChoosed?.name }
//            item?.let {
//                cityChoosed = it
//                presenter.getListSubDistrict(it.id?:"")
//            }
//        }
    }

    override fun onGetSubdistrict(mutableList: MutableList<Location>) {
//        subDistrictAdapter.removeAll()
//        subDistrictAdapter.add(mutableList)
//
//        if (subDistrictChoosed != null || subDistrictChoosed?.name?.isNotBlank() == true) {
//            val item = mutableList.find { it.name == subDistrictChoosed?.name }
//            item?.let {
//                subDistrictChoosed = it
//                presenter.getListVillage(it.id?:"")
//            }
//        }
    }

    override fun onGetVillage(mutableList: MutableList<Location>) {
//        villageAdapter.removeAll()
//        villageAdapter.add(mutableList)
//
//        if (villageChoosed != null || villageChoosed?.name?.isNotBlank() == true) {
//            val item = mutableList.find { it.name == villageChoosed?.name }
//            item?.let {
//                villageChoosed = it
//            }
//        }
    }

    override fun onGetOccupation(list: MutableList<Occupation>) {
        occupationAdapter.add(list)
        if (occupationChoosed != null || occupationChoosed?.name?.isNotBlank() == true) {
            val item = list.find { it.name == occupationChoosed?.name }
            item?.let {
                et_job?.setText(it.name)
                occupationChoosed = it
            }
        }
    }

    override fun onGetBranch(branchListResponse: BranchListResponse) {
        branchAdapter.add(branchListResponse.branchList?: arrayListOf())
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout(isBlocked: Boolean = false) {
        if (!isBlocked) {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            hideKeyboard(this)
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == KTP_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            address = intentData?.getParcelable(KTP_DATA)
        } else if (requestCode == DOMICILE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressDomicile = intentData?.getParcelable(DOMICILE_DATA)
        } else if (requestCode == OFFICE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressOffice = intentData?.getParcelable(OFFICE_DATA)
        }

        if (requestCode == REQUEST_IMAGE_KTP) {
            if (resultCode == Activity.RESULT_OK) {
                val imgKtp = container_ktp?.findViewById<ImageView>(R.id.iv_preview)?: ImageView(this@PlafondSubmissionActivity)
                val tvName = container_ktp?.findViewById<TextView>(R.id.tv_filename)
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    imageKtp = it
                    loadImage(this@PlafondSubmissionActivity, it, imgKtp)
                    tvName?.text = this@PlafondSubmissionActivity.getString(R.string.change_image)
                    typeImageKtp = "file"
                }
            }
        } else if (requestCode == REQUEST_IMAGE_SLIP_GAJI) {
            if (resultCode == Activity.RESULT_OK) {
                val imgSlipGaji = container_image_slip_gaji?.findViewById<ImageView>(R.id.iv_preview)?: ImageView(this@PlafondSubmissionActivity)
                val tvName = container_image_slip_gaji?.findViewById<TextView>(R.id.tv_filename)
                val uri = data?.getParcelableExtra<Uri>("path")
                uri?.path?.let {
                    imageSlipGaji = it
                    loadImage(this@PlafondSubmissionActivity, it, imgSlipGaji)
                    tvName?.text = this@PlafondSubmissionActivity.getString(R.string.change_image)
                    typeImageSlipGaji = "file"
                }
            }
        }
    }

    private fun showImagePickerOptions(request: Int) {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent(request)
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent(request)
            }

        })
    }

    private fun launchCameraIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, request)
    }

    private fun launchGalleryIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, request)
    }
}
