package id.andalan.andalanku.ui.ppob

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.response.PpobUpdateAmountResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.utils.*
import kotlinx.android.synthetic.main.activity_ppob_credit_card.*
import kotlinx.android.synthetic.main.activity_ppob_credit_card.rv_ppob_product_list
import kotlinx.android.synthetic.main.activity_ppob_credit_card.sliding_layout
import kotlinx.android.synthetic.main.activity_used_car.*
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.*
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.btn_bill_detail_payment
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.btn_bill_detail_show_bill
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.container_bill_detail_bills
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.container_bill_detail_input
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.et_ppob_bill_detail_account_number
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.guideline3
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.guideline4
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.guideline6
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.ic_close
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.ic_ppob_bill_detail_clear_button
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.label_ppob_bill_detail_account_number_label
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.rv_ppob_bill_detail_bills
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.rv_ppob_bill_detail_customers
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.rv_ppob_bill_detail_product
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.tv_bill_detail_header_name
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.tv_bill_detail_tv_title_text
import kotlinx.android.synthetic.main.layout_bill_detail_credit_card.*
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobCreditCardActivity : BaseApp(), PpobView, PpobUpdateAmountView {

    companion object {
        val CATEGORY = "category"
        val TITLE = "title"
        val ACC_NUMBER_LABEL = "acc_number_label"
    }

    var biller:String = ""
    var accountNumber = ""
    var logo: String = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    var category: String = ""
    var title: String = ""
    var accNumberLabel: String = ""
    var minLength: Int = 0
    var orderId: String = ""

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom


    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter
    @Inject
    lateinit var presenterUpdateAmount: PpobUpdateAmountPresenter

    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter
    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter
    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter
    lateinit var ppobProductListAdapter: PpobProductListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_credit_card)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        category = intent?.extras?.getString(PpobGeneralActivity.CATEGORY).toString()
        title = intent?.extras?.getString(PpobGeneralActivity.TITLE).toString()
        accNumberLabel = intent?.extras?.getString(PpobGeneralActivity.ACC_NUMBER_LABEL).toString()

        if (accNumberLabel != "") {
            label_ppob_bill_detail_account_number_label.text = accNumberLabel
        }

        tv_title?.text = title

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this
        presenterUpdateAmount.view = this

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }


        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        btn_bill_detail_show_bill.setOnClickListener {

            accountNumber = et_ppob_bill_detail_account_number.text.toString()
            if (validateNumber(accountNumber)) {
                presenter.ppobinquiry(accountNumber, productCode, "Tagihan", "", "")
            }
            else {
                biller = ""
                alertWarningCustom.getView().tv_message?.text = accNumberLabel + " Belum Lengkap"
                alertWarningCustom.show()
            }
        }

        ic_ppob_bill_detail_clear_button.setOnClickListener {
            et_ppob_bill_detail_account_number.setText("")
        }
        btn_bill_detail_payment.setOnClickListener {
            var txtAmount = et_ppob_bill_detail_amount.text.toString()
            val re = Regex("[^0-9]")
            txtAmount = txtAmount.let { re.replace(it, "") }

            if (txtAmount != "") {
                totalAmount = txtAmount
                presenterUpdateAmount.ppobUpdateAmount(orderId, txtAmount)
            }
            else {
                alertWarningCustom.getView().tv_message?.text = "Jumlah Pembayaran Belum Diisi"
                alertWarningCustom.show()
            }
        }

        presenter.getPpobProductList(category, "", "")
        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

        if (category == "emoney"
            || category == "streaming 1"
            || category == "streaming 1"
            || category == "voucher digital"
            || category == "voucher game") {
            ppobProductListAdapter =
                PpobProductListAdapter(object : PpobProductListAdapter.OnClickItem {
                    override fun onClickMenu(ppobProduct: PpobProduct) {
                        val intent = Intent(this@PpobCreditCardActivity, PpobGeneralSubProductListActivity::class.java)
                        intent.putExtra("category", category)
                        intent.putExtra("biller", ppobProduct.biller)
                        intent.putExtra("title", ppobProduct.name)
                        intent.putExtra("acc_number_label", "")
                        startActivity(intent)
                    }
                })

            et_ppob_pdam_biller_search.visibility = View.GONE
        }
        else {
            ppobProductListAdapter =
                PpobProductListAdapter(object : PpobProductListAdapter.OnClickItem {
                    override fun onClickMenu(ppobProduct: PpobProduct) {
                        productCode = ppobProduct.code.toString()
                        productName = ppobProduct.name.toString()

                        et_ppob_bill_detail_account_number.setText("")
                        tv_bill_detail_tv_title_text.text = productName
                        if (accNumberLabel != "") {
                            label_ppob_bill_detail_account_number_label.text =
                                accNumberLabel
                        }
                        else {
                            label_ppob_bill_detail_account_number_label.text =
                                getString(R.string.account_number)
                        }
                        container_bill_detail_bills.visibility = View.GONE
                        container_bill_detail_input.visibility = View.VISIBLE

                        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
                    }
                })
        }

        et_ppob_bill_detail_account_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                accountNumber = et_ppob_bill_detail_account_number.text.toString()
                if (validateNumber(accountNumber)) {
                    presenter.ppobinquiry(accountNumber, productCode, "Tagihan", "", "")
                }
                else {
                    biller = ""
                    alertWarningCustom.getView().tv_message?.text = accNumberLabel + " Belum Lengkap"
                    alertWarningCustom.show()
                }
            }
            false
        }

        et_ppob_pdam_biller_search.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val keyword = et_ppob_pdam_biller_search.text.toString()
                presenter.getPpobProductList(
                    category, "", keyword)
            }
            false
        }
        et_ppob_bill_detail_account_number.filters += InputFilter.LengthFilter(19)
        minLength = 4

        et_ppob_bill_detail_amount.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()

                    if (et_ppob_bill_detail_amount.text.toString() != price.convertToRpFormat()) et_ppob_bill_detail_amount.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_ppob_bill_detail_amount.setSelection(text.length - 2)
                }
            }
        })

    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        if (ppobProductListResponse.status == ConfigVar.SUCCESS) {
            if (ppobProductListResponse.ppobProduct.isNullOrEmpty()) {
                //empty response
            } else {
                ppobProductListAdapter.removeAll()
                ppobProductListAdapter.add(ppobProductListResponse.ppobProduct)

                initProductItem()
            }
        }
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {
            container_bill_detail_bills.visibility = View.VISIBLE
            container_bill_detail_input.visibility = View.GONE

            orderId = ppobInquiryResponse.orderId.toString()

            tv_bill_detail_tv_title_text?.text = "Detail Tagihan Kartu Kredit"

            tv_bill_detail_header_name?.text = "${preferencesUtil.getUser()?.customerName},"

            if (ppobInquiryResponse.listBillCustomerDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_customers.visibility = View.VISIBLE
                guideline3.visibility = View.VISIBLE
                rv_ppob_bill_detail_customers.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailCustomersAdapter.removeAll()
                    ppobInquiryResponse.listBillCustomerDetails?.let {
                        ppobBillDetailCustomersAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailCustomersAdapter
                }
            }
            else {
                rv_ppob_bill_detail_customers.visibility = View.GONE
                guideline3.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillProductDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_product.visibility = View.VISIBLE
                guideline4.visibility = View.VISIBLE

                rv_ppob_bill_detail_product.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailProductAdapter.removeAll()
                    ppobInquiryResponse.listBillProductDetails?.let {
                        ppobBillDetailProductAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailProductAdapter
                }
            }
            else {
                rv_ppob_bill_detail_product.visibility = View.GONE
                guideline4.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillBillDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_bills.visibility = View.VISIBLE
                guideline6.visibility = View.VISIBLE

                rv_ppob_bill_detail_bills.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailBillAdapter.removeAll()
                    ppobInquiryResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                    adapter = ppobBillDetailBillAdapter
                }
            }
            else {
                rv_ppob_bill_detail_bills.visibility = View.GONE
                guideline6.visibility = View.GONE
            }

            var hintText = tv_ppob_bill_detail_hint.text.toString()
            val productName = ppobInquiryResponse.productName.toString()

            hintText = if (productName.contains("cimb", true) || productName.contains("citi", true )) {
                hintText + "Rp 15.000"
            } else {
                hintText + "Rp 10.000"
            }

            tv_ppob_bill_detail_hint.text = hintText

            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            //totalAmount = ppobInquiryResponse.amount.toString()

            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun onUpdateAmount(ppobUpdateAmountResponse: PpobUpdateAmountResponse) {
        if (ppobUpdateAmountResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, logo)
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobUpdateAmountResponse.mesage
            alertWarningCustom.show()
        }
    }

    override fun showWait() {
        //sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }

    fun initProductItem() {
        rv_ppob_product_list.apply {
            layoutManager = GridLayoutManager(this@PpobCreditCardActivity, 1)
            adapter = ppobProductListAdapter
        }
    }

    private fun validateNumber(number: String) : Boolean {
        if ((number == "") or (number.length < minLength)) {
            return false
        }

        return true
    }

}