package id.andalan.andalanku.ui.address

import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.response.LocationResponse
import id.andalan.andalanku.model.response.UpdateAddressResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class UpdateAddressPresenter @Inject constructor(private val service: Services): BasePresenter<UpdateAddressView>() {
    override fun onCreate() {
        getListProvince()
    }

    fun sendUpdateAddress(updateAddressRequest: UpdateAddressRequest) {
        view.showWait()
        val subscription = service.sendUpdateAddress(updateAddressRequest)
            .subscribe(object : Subscriber<UpdateAddressResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(updateAddressResponse: UpdateAddressResponse) {
                    view.onGetResponseUpdate(updateAddressResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun sendUpdatedDataAddress(updateAddressRequest: UpdateAddressRequest) {
        view.showWait()
        val subscription = service.sendUpdateOnlyAddress(updateAddressRequest).subscribe(object : Subscriber<UpdateAddressResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(updateAddressResponse: UpdateAddressResponse) {
                view.onGetResponseUpdate(updateAddressResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getListProvince() {
        view.showWait()
        val subscription = service.getProvince()
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetProvince(locationResponse.provinceList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListCity(id: String) {
        view.showWait()
        val subscription = service.getCity(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetCity(locationResponse.cityList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListSubDistrict(id: String) {
        view.showWait()
        val subscription = service.getDistrict(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetSubdistrict(locationResponse.districtList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListVillage(id: String) {
        view.showWait()
        val subscription = service.getVillage(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetVillage(locationResponse.villageList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getZipCode(id: String) {
        view.showWait()
        val subscription = service.getZipCode(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetZipCode(locationResponse.zipCode)
                }
            })
        subscriptions.add(subscription)
    }
}