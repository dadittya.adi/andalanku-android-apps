package id.andalan.andalanku.ui.pin

import android.content.Intent
import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.register.SuccessRegisterActivity
import id.andalan.andalanku.utils.AlertDialogUtil
import kotlinx.android.synthetic.main.activity_create_pin.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class CreatePinActivity : BaseApp(), CreatePinView {
    val TAG = CreatePinActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: CreatePinPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_pin)

        deps.inject(this)
        presenter.view = this

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.security_account)

        btn_next.setOnClickListener {
            val isChecked = cb_term_condition.isChecked
            val pin = et_pin.text.toString()
            val pinConfirm = et_pin_confirm.text.toString()
            if (!isChecked) {
                AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                    override fun onClickCancel() {
                    }

                    override fun onClickOk() {
                    }

                }, this, getString(R.string.security_account), getString(R.string.info_check_term_condition))
            } else if (pin.isNotBlank() && pin == pinConfirm) {
                presenter.createPin(et_pin.text.toString())
            } else {
                AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                    override fun onClickCancel() {
                    }

                    override fun onClickOk() {
                    }

                }, this, getString(R.string.security_account), getString(R.string.pin_not_equals))
            }
        }
    }

    override fun onGetResponse(createPinResponse: CreatePinResponse) {
        if (createPinResponse.status == ConfigVar.SUCCESS) {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }

                override fun onClickOk() {
                    startActivity(Intent(this@CreatePinActivity, SuccessRegisterActivity::class.java))
                    finish()
                }

            }, this, getString(R.string.security_account), createPinResponse.message?:"")
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }

                override fun onClickOk() {
                }

            }, this, getString(R.string.security_account), createPinResponse.message?:"")
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
