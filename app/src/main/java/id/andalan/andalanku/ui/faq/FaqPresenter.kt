package id.andalan.andalanku.ui.faq

import id.andalan.andalanku.model.response.FaqResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class FaqPresenter @Inject constructor(private val service: Services): BasePresenter<FaqView>() {
    override fun onCreate() {
        getListFaq()
    }

    private fun getListFaq() {
        view.showWait()
        val subscription = service.getListFaq()
            .subscribe(object : Subscriber<FaqResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(faqResponse: FaqResponse) {
                    view.getFaqList(faqResponse)
                }
            })
        subscriptions.add(subscription)
    }
}