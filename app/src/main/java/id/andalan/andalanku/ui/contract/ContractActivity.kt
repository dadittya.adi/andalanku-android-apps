package id.andalan.andalanku.ui.contract

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentPresenter
import id.andalan.andalanku.ui.payment.InstallmentPaymentView
import kotlinx.android.synthetic.main.fragment_contract.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import kotlinx.android.synthetic.main.toolbar_plafond.toolbar_login
import kotlinx.android.synthetic.main.toolbar_plafond.tv_title
import javax.inject.Inject

class ContractActivity : BaseApp(), InstallmentPaymentView {

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: InstallmentPaymentPresenter

    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.fragment_contract)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.dokument_contract)

        deps.inject(this)
        presenter.view = this
        presenter.getAggreementNumber()

        et_choosed_contract_number.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                et_choosed_contract_number?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)

                val intent = Intent(this@ContractActivity, ContractDetailActivity::class.java)
                intent.putExtra(ContractDetailActivity.AGREEMENT_NUMBER, choosedPicker.name)
                intent.putExtra(ContractDetailActivity.LIST_AGREEMENT, pickerAdapter.getData().toTypedArray())
                startActivity(intent)

                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerAdapter.removeAll()
            pickerAdapter.add(list)
            container_empty_state?.visibility = View.GONE
        } else {
            container_empty_state?.visibility = View.VISIBLE
        }
    }

    override fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse) {
        TODO("Not yet implemented")
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state?.visibility = View.VISIBLE
        Toast.makeText(this, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }
}