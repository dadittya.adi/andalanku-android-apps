package id.andalan.andalanku.ui.insurance

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.util.Log.e
import android.view.View
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.ClaimInsuranceListResponse
import id.andalan.andalanku.model.ui.ClaimInsurance
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.ClaimInsuranceAdapter
import id.andalan.andalanku.ui.adapters.RequestGuaranteeAdapter
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.guarantee.DetailTakeGuaranteeActivity
import id.andalan.andalanku.ui.guarantee.SubmissionRequestGuaranteeActivity
import id.andalan.andalanku.ui.insurance.DetailClaimInsuranceActivity.Companion.CLAIM_INSURANCE
import kotlinx.android.synthetic.main.activity_eclaim_insurance.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class EClaimInsuranceActivity : BaseApp(), EClaimInsuranceView {
    private val TAG = EClaimInsuranceActivity::class.java.simpleName

    private lateinit var claimInsuranceAdapter: ClaimInsuranceAdapter

    @Inject
    lateinit var presenter: EClaimInsurancePresenter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eclaim_insurance)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.e_claim_insurance)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        layout?.setBackgroundColor(ContextCompat.getColor(this, R.color.snow))
        btn_add?.visibility = View.VISIBLE
        tabs?.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
                Log.e(TAG, "reselect ${p0?.position}")
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                Log.e(TAG, "un selected ${p0?.position}")
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                onTabsPosition(p0?.position?:0)
            }

        })
        btn_add?.setOnClickListener {
            startActivityForResult(Intent(this, SubmissionClaimInsuranceActivity::class.java), EComplainActivity.REQUEST_SEND_COMPLAIN)
        }
        btn_complain_submission?.setOnClickListener {
            startActivityForResult(Intent(this, SubmissionClaimInsuranceActivity::class.java), EComplainActivity.REQUEST_SEND_COMPLAIN)
        }

        claimInsuranceAdapter = ClaimInsuranceAdapter(object: ClaimInsuranceAdapter.OnClickItem{
            override fun onClickClaim(claimInsurance: ClaimInsurance) {
                val intent = Intent(this@EClaimInsuranceActivity, DetailClaimInsuranceActivity::class.java)
                intent.putExtra(CLAIM_INSURANCE, claimInsurance)
                startActivity(intent)
            }
        })

        rv_claim?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_claim?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_claim?.adapter = claimInsuranceAdapter
    }

    fun onTabsPosition (position: Int) {
        when (position) {
            0 -> {
                presenter.getListClaimInsurance()
            }
            1 -> {
                presenter.getListClaimInsurance(ConfigVar.SUBMITTED)
            }
            2 -> {
                presenter.getListClaimInsurance(ConfigVar.ONPROCESS)
            }
            3 -> {
                presenter.getListClaimInsurance(ConfigVar.SOLVED)
            }
        }
        claimInsuranceAdapter.removeAll()
    }

    override fun onGetEClaimResponse(claimInsuranceListResponse: ClaimInsuranceListResponse) {
        if (claimInsuranceListResponse.status == ConfigVar.SUCCESS) {
            claimInsuranceListResponse.insuranceClaimList?.let {
                if (it.size > 0) {
                    claimInsuranceAdapter.add(claimInsuranceListResponse.insuranceClaimList)
                    showEmptyState(false)
                } else {
                    showEmptyState(true)
                }
            }
        } else {
            showEmptyState(true)
            Toast.makeText(this@EClaimInsuranceActivity, claimInsuranceListResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun showEmptyState(state: Boolean) {
        rv_claim?.visibility = if (state) View.GONE else View.VISIBLE
        container_empty_state?.visibility = if (state) View.VISIBLE else View.GONE
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        e("tag", requestCode.toString()+ ", " + resultCode)
        if (requestCode == EComplainActivity.REQUEST_SEND_COMPLAIN && resultCode == EComplainActivity.RESPONSE_SEND_COMPLAIN) {
            onTabsPosition(tabs.selectedTabPosition)
        }
    }
}
