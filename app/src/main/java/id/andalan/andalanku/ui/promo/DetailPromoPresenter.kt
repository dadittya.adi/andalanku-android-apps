package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.response.DetailPromoResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import id.andalan.andalanku.ui.promo.DetailPromoView
import rx.Subscriber
import javax.inject.Inject

class DetailPromoPresenter @Inject constructor(private val service: Services): BasePresenter<DetailPromoView>() {
    override fun onCreate() {
    }

    fun getPromoDetail(slug: String) {
        view.showWait()
        val subscription = service.getDetailPromo(slug).subscribe(object : Subscriber<DetailPromoResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailPromoResponse: DetailPromoResponse) {
                view.onGetDetailPromo(detailPromoResponse)
            }
        })
        subscriptions.add(subscription)
    }
}