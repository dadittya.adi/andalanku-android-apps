@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.News
import id.andalan.andalanku.model.ui.NewsHome
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_list_news.view.*

class NewsAdapter(val onItemClickListener: OnClickItem): BaseAdapter<News, NewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_list_news
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<News>(itemView) {
        override fun bind(news: News) {
            val data = news.getNewsHome()
            itemView.setOnClickListener {
                onItemClickListener.onClickNews(news)
            }
            try {
                loadImage(itemView.context, data.urlImage?:"", itemView.iv_image_news)
            } catch (err: Exception) {}
            itemView.tv_periode_date?.text = data.getDateUI(itemView.context.getMonths())
        }

    }

    interface OnClickItem {
        fun onClickNews(news: News)
    }
}