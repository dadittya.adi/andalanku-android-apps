package id.andalan.andalanku.ui.guarantee

import id.andalan.andalanku.model.response.CollateralRequestResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface SubmissionRequestGuaranteeView: BaseView {
    fun onGetResponseSubmission(collateralRequestResponse: CollateralRequestResponse)
    fun onGetContractNumber(list: MutableList<PickerItem>)
}