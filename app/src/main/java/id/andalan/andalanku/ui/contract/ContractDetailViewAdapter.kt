package id.andalan.andalanku.ui.contract

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

class ContractDetailViewAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
//class ContractDetailViewAdapter(fragmentManager: androidx.fragment.app.FragmentManager): FragmentPagerAdapter(fragmentManager) {
    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return getFragments()[position]!!
    }

    override fun getCount(): Int {
        return getFragments().size
    }

    private fun getFragments(): Hashtable<Int, androidx.fragment.app.Fragment> {
        val ht = Hashtable<Int, androidx.fragment.app.Fragment>()
        ht[0] = DetailUnitFragment()
        ht[1] = DetailKontrakFragment()
        ht[2] = PaymentHistoryFragment()
        ht[3] = PolisAsuransiFragment()
        ht[4] = AgreementContractFragment()
        return ht
    }
}