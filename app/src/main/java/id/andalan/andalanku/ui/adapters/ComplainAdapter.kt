@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Complain
//import id.andalan.andalanku.model.ui.Complainservices
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_ecomplain.view.*

class ComplainAdapter(val onItemClickListener: OnClickItem): BaseAdapter<Complain, ComplainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ecomplain
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Complain>(itemView) {
        override fun bind(complain: Complain) {
            val data = complain.getComplainService()
            itemView.setOnClickListener {
                onItemClickListener.onClickComplain(complain)
            }
            itemView.tv_category?.text = data.category
            itemView.tv_date?.text = data.getDateUI(itemView.context.getMonths())
            itemView.tv_id_complain?.text = itemView.context.getString(R.string.complain_number, data.id)
            itemView.tv_state?.text = data.state
        }

    }

    interface OnClickItem {
        fun onClickComplain(complain: Complain)
    }
}