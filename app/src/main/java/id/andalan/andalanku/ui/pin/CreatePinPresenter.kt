package id.andalan.andalanku.ui.pin

import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.model.response.TokenResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class CreatePinPresenter @Inject constructor(private val service: Services): BasePresenter<CreatePinView>() {
    override fun onCreate() {
    }

    fun createPin(pinNumber: String) {
        view.showWait()
        val subscription = service.doCreatePin(pinNumber).subscribe(object : Subscriber<CreatePinResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(createPinResponse: CreatePinResponse) {
                view.onGetResponse(createPinResponse)
            }
        })
        subscriptions.add(subscription)
    }
}