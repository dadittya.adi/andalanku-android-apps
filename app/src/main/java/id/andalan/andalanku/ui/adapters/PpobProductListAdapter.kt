package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_menu_profile_2.view.*
import kotlinx.android.synthetic.main.item_menu_profile_2.view.tv_name
import java.lang.NullPointerException

class PpobProductListAdapter(val onItemClickListener: OnClickItem): BaseAdapter<PpobProduct, PpobProductListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_product
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobProduct>(itemView) {
        override fun bind(data: PpobProduct) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            if (data.logo?.isNotEmpty() == true){
                try {
                    Glide
                        .with(itemView.context)
                        .load(data.logo)
                        .into(itemView.ic_left)
                } catch (error: NullPointerException) {}
            }
            else {
                itemView.ic_left?.setImageResource(R.drawable.ic_default_product)
            }

            itemView.tv_name?.text = data.name
        }

    }

    interface OnClickItem {
        fun onClickMenu(ppobProduct: PpobProduct)
    }
}