package id.andalan.andalanku.ui.home

import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class HomeFragmentPresenter @Inject constructor(private val service: Services): BasePresenter<HomeFragmentView>() {
    override fun onCreate() {
        getListSlider()
    }

    fun getListSlider() {
        view.showWait()
        val subscription = service.getSlider().subscribe(object : Subscriber<SliderResponse>() {
            override fun onCompleted() {
                view.onRemoveWaitGetSlider()
            }

            override fun onError(e: Throwable) {
                view.onRemoveWaitGetSlider()
                view.onErrorGetSlider(e.localizedMessage?:"")
            }

            override fun onNext(sliderResponse: SliderResponse) {
                view.onGetSliderResponse(sliderResponse)
                getListNews()
            }
        })
        subscriptions.add(subscription)
    }

    fun getListNews() {
        view.showWait()
        val subscription = service.getNews(1, "").subscribe(object : Subscriber<NewsResponse>() {
            override fun onCompleted() {
                view.onRemoveWaitGetNews()
            }

            override fun onError(e: Throwable) {
                view.onRemoveWaitGetNews()
                view.onErrorGetNews(e.localizedMessage?:"")
            }

            override fun onNext(newsResponse: NewsResponse) {
                view.onGetNewsResponse(newsResponse)
                getListPromo()
            }
        })
        subscriptions.add(subscription)
    }

    fun getListPromo() {
        view.showWait()
        val subscription = service.getPromo(1, "").subscribe(object : Subscriber<PromoResponse>() {
            override fun onCompleted() {
                view.onRemoveWaitGetPromo()
                getListPartner()
            }

            override fun onError(e: Throwable) {
                view.onRemoveWaitGetNews()
                view.onErrorGetNews(e.localizedMessage?:"")
            }

            override fun onNext(promoResponse: PromoResponse) {
                view.onGetPromoResponse(promoResponse)
                getAgentCode()
            }
        })
        subscriptions.add(subscription)
    }

    fun getListPartner() {
        view.showWait()
        val subscription = service.getPartner().subscribe(object : Subscriber<PartnerListResponse>() {
            override fun onCompleted() {
                view.onRemoveWaitGetPartner()
            }

            override fun onError(e: Throwable) {
                view.onRemoveWaitGetNews()
                view.onErrorGetNews(e.localizedMessage?:"")
            }

            override fun onNext(partnerListResponse: PartnerListResponse) {
                view.onGetPartnerResponse(partnerListResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getAgentCode() {
        view.showWait()
        val subscription = service.getAgentCode().subscribe(object : Subscriber<AgentResponse>() {
            override fun onCompleted() {
                view.showWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(agentResponse: AgentResponse) {
                view.onGetAgent(agentResponse)
                getListPartner()
            }
        })
        subscriptions.add(subscription)
    }
}