package id.andalan.andalanku.ui.ecommerce

import android.os.Bundle
import id.andalan.andalanku.BaseApp

import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.toolbar_login.*

class EcommerceComingSoonActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ecommerce_coming_soon)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.e_commerce)
    }
}