package id.andalan.andalanku.ui.transactionHistory

import id.andalan.andalanku.model.response.InstallmentPaymentHistoryListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class TransactionHistoryPresenter @Inject constructor(private val service: Services): BasePresenter<TransactionHistoryView>() {
    override fun onCreate() {
        //getListTransaction()
    }

    fun getListTransaction(category: String, status: String) {
        view.showWait()
        val subscription = service.getPaymentHistoryList(category, status)
            .subscribe(object : Subscriber<InstallmentPaymentHistoryListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(installmentPaymentHistoryListResponse: InstallmentPaymentHistoryListResponse) {
                    view.getListResponse(installmentPaymentHistoryListResponse)
                }
            })
        subscriptions.add(subscription)
    }
}