package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Bank
import id.andalan.andalanku.model.ui.BankBranch
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_bank.view.*
import kotlinx.android.synthetic.main.item_default_text.view.*

class BankBranchAdapter(val onItemClickListener: OnClickItem): BaseAdapter<BankBranch, BankBranchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_bank
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<BankBranch>(itemView) {
        override fun bind(data: BankBranch) {
            itemView.setOnClickListener {
                onItemClickListener.onClickBankBranch(data)
            }
            itemView.tv_bank_name?.text = data.bankBranchName
        }

    }

    interface OnClickItem {
        fun onClickBankBranch(bankBranch: BankBranch)
    }
}