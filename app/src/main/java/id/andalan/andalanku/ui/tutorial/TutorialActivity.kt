package id.andalan.andalanku.ui.tutorial

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.FaqParent
import id.andalan.andalanku.model.response.FaqResponse
import id.andalan.andalanku.model.response.TutorialResponse
import id.andalan.andalanku.model.ui.TutorialParent
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.FaqAdapter
import id.andalan.andalanku.ui.adapters.TutorialAdapter
import kotlinx.android.synthetic.main.activity_faq.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class TutorialActivity : BaseApp(), TutorialView {
    private val TAG = TutorialActivity::class.java.simpleName
    private lateinit var menuMidAdapter: TutorialAdapter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: TutorialPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.tutorial)

        menuMidAdapter = TutorialAdapter(object: TutorialAdapter.OnClickItem{
            override fun onClickMenu(tutorialParent: TutorialParent) {
                val intent = Intent(this@TutorialActivity, TutorialDetailActivity::class.java)
                intent.putExtra(TutorialDetailActivity.CONTENT_TUTORIAL, tutorialParent)
                startActivity(intent)
            }

        })

        rv_menu?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu?.adapter = menuMidAdapter
        rv_menu?.isNestedScrollingEnabled = false
    }

    override fun getTutorialist(tutorialResponse: TutorialResponse) {
        if (tutorialResponse.status == ConfigVar.SUCCESS) {
            menuMidAdapter.add(tutorialResponse.tutorialParentList?: arrayListOf())
            menuMidAdapter.notifyDataSetChanged()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
