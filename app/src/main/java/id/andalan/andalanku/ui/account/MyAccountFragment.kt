package id.andalan.andalanku.ui.account

import android.content.Intent
import android.os.Bundle
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.model.response.LoginResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.ui.account.UpdatePersonalInfoActivity.Companion.IS_ACCOUNT_SETTING
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile2
import id.andalan.andalanku.ui.help.HelpActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.loading_screen.LoadingScreenActivity
import id.andalan.andalanku.ui.pin.ChangePinActivity
import id.andalan.andalanku.ui.referral.ReferralActivity
import id.andalan.andalanku.ui.security.SecuritySettingActivity
import id.andalan.andalanku.ui.tnc.PrivacyPolicyActivity
import id.andalan.andalanku.ui.tnc.TNCActivity
import id.andalan.andalanku.ui.tutorial.TutorialActivity
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.fragment_my_account.*
import kotlinx.android.synthetic.main.toolbar_login.*
import id.andalan.andalanku.BuildConfig
import javax.inject.Inject

class MyAccountFragment : androidx.fragment.app.Fragment() {
    companion object {
        val REQ_CODE_UPDATED_INFO = 1114
        val RES_CODE_UPDATED_INFO = 1115
        val UPDATED_INFO = "update_info"
    }

    private val TAG = MyAccountFragment::class.java.simpleName

    private lateinit var menuTopAdapter: MenuAdapterProfile2
    private lateinit var menuMid2Adapter: MenuAdapterProfile
    private lateinit var menuMidAdapter: MenuAdapterProfile
    private lateinit var menuBottomAdapter: MenuAdapterProfile

    @Inject
    lateinit var presenter: UpdatePersonalInfoPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.my_akun)

        val user = (activity as MainHomeActivity).preferencesUtil.getUser()
        val profile = (activity as MainHomeActivity).preferencesUtil.getProfile()
        tv_name?.text = user?.customerName
        tv_email?.text = user?.email
//        if (profile?.agentCode != "000" || profile?.agentCode != "-") {
//            tv_agent?.text = profile?.agentCode
//        }
        context?.let {context ->
            profile?.avatar?.let {
                if (it.isNotBlank()) {
                    loadImage(context, it, iv_photo_profile)
                }
            }
        }
        profile?.andalanCustomerId?.let {
            if (it.isEmpty()) {
                btn_update_personal_info?.visibility = View.INVISIBLE
            }
            else {
                btn_setting_account?.visibility = View.INVISIBLE
            }
        }

        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }

        btn_setting_account?.setOnClickListener {
            val intent = Intent(context, UpdatePersonalInfoActivity::class.java)
            intent.putExtra(IS_ACCOUNT_SETTING, true)
            startActivityForResult(intent, REQ_CODE_UPDATED_INFO)
        }

        btn_update_personal_info?.setOnClickListener {
            val intent = Intent(context, UpdatePersonalInfoActivity::class.java)
            startActivityForResult(intent, REQ_CODE_UPDATED_INFO)
        }
        initAdapter()
        initMenuTop()
        initMenuMid()
        initMenuMid2()
        initMenuBottom()
    }

    private fun initAdapter() {
        menuTopAdapter = MenuAdapterProfile2(object: MenuAdapterProfile2.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == context?.getString(R.string.plafond_andalanku)) {
                    (activity as MainHomeActivity).moveViewNoAnim(5)
                }
            }
        })
        menuMidAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
//                e("TAG", menuProfile.type)
                if (context?.getString(R.string.security_setting) == menuProfile.name) {
                    startActivity(Intent(context, SecuritySettingActivity::class.java))
                } else if (context?.getString(R.string.change_pin) == menuProfile.name) {
                    startActivity(Intent(context, ChangePinActivity::class.java))
                } else if (getString(R.string.referral_code_menu) == menuProfile.name) {
                    startActivity(Intent(context, ReferralActivity::class.java))
                } else if ("Dokumen Kontrak" == menuProfile.name) {
                    (activity as MainHomeActivity).moveViewNoAnim(10)
                }
            }
        })
        menuMid2Adapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name === context?.getString(R.string.help)) {
                    startActivity(Intent(context, HelpActivity::class.java))
                } else if (menuProfile.name === context?.getString(R.string.term_condition_small)) {
                    startActivity(Intent(context, TNCActivity::class.java))
                } else if (menuProfile.name === context?.getString(R.string.privacy_policy)) {
                    startActivity(Intent(context, PrivacyPolicyActivity::class.java))
                }
                else if (menuProfile.name === context?.getString(R.string.tutorial)) {
                    startActivity(Intent(context, TutorialActivity::class.java))
                }
            }
        })
        menuBottomAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == context?.getString(R.string.logout)) {
                    (activity as MainHomeActivity).preferencesUtil.getUser()?.let {
                        (activity as MainHomeActivity).preferencesUtil.putLogoutUser(it)
                    }
                    (activity as MainHomeActivity).preferencesUtil.putUser(LoginResponse())
                    (activity as MainHomeActivity).preferencesUtil.putProfile(PersonalInformationResponse())
                    startActivity(Intent(context, LoadingScreenActivity::class.java))
                    (activity as MainHomeActivity).finish()
                }
            }
        })
    }

    private fun initMenuTop() {
        rv_menu_top?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu_top?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu_top?.adapter = menuTopAdapter
        rv_menu_top?.isNestedScrollingEnabled = false
        menuTopAdapter.add(
            MenuProfile(
                name = context?.getString(R.string.plafond_andalanku),
                leftDrawable = R.drawable.ic_wallet
            )
        )
        menuTopAdapter.add(
            MenuProfile(
                name = context?.getString(R.string.point_andalanku),
                leftDrawable = R.drawable.ic_point
            )
        )
    }

    private fun initMenuMid() {
        rv_menu_mid?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu_mid?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu_mid?.adapter = menuMidAdapter
        rv_menu_mid?.isNestedScrollingEnabled = false
        //menuMidAdapter.add(MenuProfile(name = getString(R.string.referral_code_menu)))
        menuMidAdapter.add(MenuProfile(name = "Dokumen Kontrak"))
        menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.change_pin)))
        menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.security_setting)))
    }

    private fun initMenuMid2() {
        rv_menu_mid_2?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu_mid_2?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu_mid_2?.adapter = menuMid2Adapter
        rv_menu_mid_2?.isNestedScrollingEnabled = false
        menuMid2Adapter.add(MenuProfile(name = context?.getString(R.string.tutorial)))
        menuMid2Adapter.add(MenuProfile(name = context?.getString(R.string.help)))
        menuMid2Adapter.add(MenuProfile(name = context?.getString(R.string.term_condition_small)))
        menuMid2Adapter.add(MenuProfile(name = context?.getString(R.string.privacy_policy)))
    }

    private fun initMenuBottom() {
        rv_menu_bottom?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu_bottom?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu_bottom?.adapter = menuBottomAdapter
        rv_menu_bottom?.isNestedScrollingEnabled = false
        menuBottomAdapter.add(
            MenuProfile(

                name = context?.getString(R.string.app_version),
                rightText = BuildConfig.VERSION_NAME
            )
        )
        menuBottomAdapter.add(
            MenuProfile(
                name = context?.getString(R.string.logout),
                rightText = ""
            )
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_CODE_UPDATED_INFO && resultCode == RES_CODE_UPDATED_INFO) {
            e(TAG, "got it")
        }
    }

    override fun onResume() {
        super.onResume()

        val user = (activity as MainHomeActivity).preferencesUtil.getUser()
        val profile = (activity as MainHomeActivity).preferencesUtil.getProfile()
        tv_name?.text = user?.customerName
        tv_email?.text = user?.email
        if (profile?.agentCode.equals("000")) {

        }
        else {
            tv_agent?.text = profile?.agentCode
        }
        context?.let {context ->
            profile?.avatar?.let {
                if (it.isNotBlank()) {
                    iv_photo_profile.setImageURI(null)
                    loadImage(context, it, iv_photo_profile)
                }
            }
        }
        profile?.andalanCustomerId?.let {
            if (it.isEmpty()) {
                btn_update_personal_info?.visibility = View.INVISIBLE
            }
            else {
                btn_setting_account?.visibility = View.INVISIBLE
            }
        }
    }

}
