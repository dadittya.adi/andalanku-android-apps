package id.andalan.andalanku.ui.payment

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import java.util.*

class PaymentSimulationAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return getFragments()[position]!!
    }

    override fun getCount(): Int {
        return getFragments().size
    }

    private fun getFragments(): Hashtable<Int, androidx.fragment.app.Fragment> {
        val ht = Hashtable<Int, androidx.fragment.app.Fragment>()
        ht[0] = AdvanceTypePaymentFragment()
        ht[1] = ArrearTypeFragment()
        return ht
    }
}