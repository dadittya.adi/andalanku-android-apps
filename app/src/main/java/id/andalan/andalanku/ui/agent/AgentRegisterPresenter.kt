package id.andalan.andalanku.ui.agent

import id.andalan.andalanku.model.request.*
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class AgentRegisterPresenter @Inject constructor(private val service: Services): BasePresenter<AgentRegisterView>() {
    override fun onCreate() {
        getListProvince()
        //getListBank()
        getPersonalInfoUser()
    }

    fun sendUpdatedAddress(sendAllAddressRequest: SendAllAddressRequest) {
        view.showWait()
        val subscription = service.sendAllAddress(sendAllAddressRequest).subscribe(object : Subscriber<UpdateAddressResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                //view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(updateAllAddressResponse: UpdateAddressResponse) {
                view.getResponseUpdateAllAddress(updateAllAddressResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getListBank(keyword: String) {
        view.showWait()
        val subscription = service.getBank(keyword)
            .subscribe(object : Subscriber<BankResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(bankResponse: BankResponse) {
                    view.onGetBank(bankResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun getListBankBranch(id: String?, keyword: String) {
        view.showWait()
        val subscription = service.getBankBranch(id, keyword)
            .subscribe(object : Subscriber<BankBranchResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(bankBranchResponse: BankBranchResponse) {
                    view.onGetBankBranch(bankBranchResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun getListProvince() {
        view.showWait()
        val subscription = service.getProvince()
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetProvince(locationResponse.provinceList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListCity(id: String) {
        view.showWait()
        val subscription = service.getCity(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetCity(locationResponse.cityList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListSubDistrict(id: String) {
        view.showWait()
        val subscription = service.getDistrict(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetSubdistrict(locationResponse.districtList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListVillage(id: String) {
        view.showWait()
        val subscription = service.getVillage(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetVillage(locationResponse.villageList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getZipCode(id: String) {
        view.showWait()
        val subscription = service.getZipCode(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetZipCode(locationResponse.zipCode)
                }
            })
        subscriptions.add(subscription)
    }

    fun getPersonalInfoUser() {
        view.showWait()
        val subscription = service.getPersonalInfo().subscribe(object : Subscriber<PersonalInformationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(personalInformationResponse: PersonalInformationResponse) {
                //view.getProfileInformation(personalInformationResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }

    fun getListBranch(idCity: String) {
        view.showWait()
        val subscription = service.getAdvanceBranchList(idCity)
            .subscribe(object : Subscriber<BranchListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(getBranchListResponse: BranchListResponse) {
                    view.onGetBranch(getBranchListResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun getOccupationList() {
        view.showWait()
        val subscription = service.getOccupation()
            .subscribe(object : Subscriber<OccupationsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(occupationsResponse: OccupationsResponse) {
                    view.onGetOccupation(occupationsResponse.occupationList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun nipCheck(nip: String) {
        view.showWait()
        val subscription = service.NipCheck(nip)
            .subscribe(object : Subscriber<NipCheckResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(nipCheckResponse: NipCheckResponse) {
                    view.onNipCheck(nipCheckResponse)
                }
            })
        subscriptions.add(subscription)
    }


}