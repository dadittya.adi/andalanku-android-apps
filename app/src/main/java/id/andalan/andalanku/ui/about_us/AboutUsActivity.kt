package id.andalan.andalanku.ui.about_us

import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.activity_about_us.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import android.content.Intent
import android.net.Uri


class AboutUsActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.about_us)

        btn_branch.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.brach_web)))
            startActivity(browserIntent)
        }
    }
}
