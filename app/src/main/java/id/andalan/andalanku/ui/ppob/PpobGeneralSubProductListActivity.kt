package id.andalan.andalanku.ui.ppob

import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.activity_ppob_general_sub_product.*
import kotlinx.android.synthetic.main.activity_ppob_general_sub_product.sliding_layout
import kotlinx.android.synthetic.main.fragment_ppob_pln_prepaid.*
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.ic_close
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobGeneralSubProductListActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val BILLER = "biller"
        val TITLE = "title"
        val ACC_NUMBER_LABEL = "acc_number_label"
    }

    var biller:String = ""
    var accountNumber = ""
    var logo: String = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    var category: String = ""
    var title: String = ""
    var accNumberLabel: String = ""
    var minLength: Int = 0
    var zoneId: String = ""

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom


    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter

    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter
    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter
    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter
    lateinit var ppobProductListAdapter: PpobSubProductListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_general_sub_product)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        biller = intent?.extras?.getString(PpobGeneralSubProductListActivity.BILLER).toString()
        category = intent?.extras?.getString(PpobGeneralSubProductListActivity.CATEGORY).toString()
        title = intent?.extras?.getString(PpobGeneralSubProductListActivity.TITLE).toString()
        accNumberLabel = intent?.extras?.getString(PpobGeneralSubProductListActivity.ACC_NUMBER_LABEL).toString()

        if (category == "emoney") {
            et_ppob_general_sub_product_acc_number.inputType = InputType.TYPE_CLASS_PHONE
            et_ppob_general_sub_product_acc_number.filters += InputFilter.LengthFilter(13)
            minLength = 10
            if (biller != "Top Up (Customer)") {
                label_acc_number.text = "Masukkan Nomor " + biller + " Anda"
            }
            else {
                label_acc_number.text = "Masukkan Nomor Gopay Anda"
            }
        }


        tv_title?.text = title

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }

        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        btn_bill_detail_payment.setOnClickListener {


        }

        presenter.getPpobProductList(category, biller, "")
        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

        ppobProductListAdapter =
            PpobSubProductListAdapter(object : PpobSubProductListAdapter.OnClickItem {
                override fun onClickMenu(ppobProduct: PpobProduct) {
                    productCode = ppobProduct.code.toString()
                    productName = ppobProduct.name.toString()
                    val productCategory = ppobProduct.category.toString()

                    if (biller == "Mobile Legends Diamonds") {
                        accountNumber = et_ppob_general_sub_product_acc_number_ml.text.toString()
                        zoneId = et_ppob_general_sub_product_zone.text.toString()

                    }
                    else {
                        accountNumber = et_ppob_general_sub_product_acc_number.text.toString()
                    }


                    if (validateNumber(category, accountNumber)) {
                        var groupCategory = "top_up"

                        if (productCategory == "Voucher Game" || productCategory == "Streaming 1"
                            || productCategory == "Voucher Digital") {
                           groupCategory = "hiburan"
                        }
                        presenter.ppobinquiry(accountNumber,
                                    productCode, groupCategory, zoneId, "")
                    }
                    else {
                        alertWarningCustom.getView().tv_message?.text = "ID Pelanggan Belum Diisi atau Belum Lengkap"
                        alertWarningCustom.show()
                    }
                }
            })

        if (category == "voucher game") {
            if (biller == "Mobile Legends Diamonds") {
                label_acc_number.text = "Account ID"
                et_ppob_general_sub_product_acc_number.visibility = View.INVISIBLE
                ct_ppob_general_sub_product_acc_number_ml.visibility = View.VISIBLE
            }
            else {
                label_acc_number.text = "Email / No. Handphone"
            }
        }

    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        if (ppobProductListResponse.status == ConfigVar.SUCCESS) {
            if (ppobProductListResponse.ppobProduct.isNullOrEmpty()) {
                //empty response
            } else {
                ppobProductListAdapter.removeAll()
                ppobProductListAdapter.add(ppobProductListResponse.ppobProduct)

                initProductItem()
            }
        }
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {
            productCode = ppobInquiryResponse.productCode.toString()
            productName = ppobInquiryResponse.productName.toString()
            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            totalAmount = ppobInquiryResponse.amount.toString()

            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, logo)
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun showWait() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }

    fun initProductItem() {
        rv_ppob_product_list.apply {
            layoutManager = GridLayoutManager(this@PpobGeneralSubProductListActivity, 1)
            adapter = ppobProductListAdapter
        }
    }

    fun validateNumber(category: String, number: String) : Boolean{
        if ((number == "") or (number.length < minLength)) {
            return false
        }

        if (category == "emoney") {
            val prefix = number.take(3)

            return (prefix == "081") or
                    (prefix == "082") or
                    (prefix == "085") or
                    (prefix == "087") or
                    (prefix == "087") or
                    (prefix == "083") or
                    (prefix == "089") or
                    (prefix == "088")
        }

        return true
    }

}