package id.andalan.andalanku.ui.ppob

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PpobBillDetailBillAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailCustomersAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailProductAdapter
import id.andalan.andalanku.ui.viewmodel.ViewModelPpob
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.activity_ppob_ponsel_pascabayar.btn_ppob_postpaid_show_bill
import kotlinx.android.synthetic.main.activity_ppob_ponsel_pascabayar.ic_ppob_postpaid_clear_button
import kotlinx.android.synthetic.main.activity_ppob_ponsel_pascabayar.sliding_layout
import kotlinx.android.synthetic.main.activity_ppob_telkom.*
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.ic_close
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobTelkomActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
        val REQUEST_PICK_CONTACT = 100
    }

    var biller:String = ""
    var accountNumber = ""
    var logo: String = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom


    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter

    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter

    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter

    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter

    lateinit var model: ViewModelPpob

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_telkom)

        deps.inject(this)
        model = ViewModelProviders.of(this).get(ViewModelPpob::class.java)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tv_title?.text = getString(R.string.telkom)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        et_ppob_telkom_account_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val accountNumber = et_ppob_telkom_account_number.text.toString()
                presenter.ppobinquiry(accountNumber, "TLIH", "Tagihan", "", "")
            }
            false
        }

        ic_ppob_postpaid_clear_button.setOnClickListener {
            et_ppob_telkom_account_number.setText("")
            accountNumber = ""
        }

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        btn_ppob_postpaid_show_bill.setOnClickListener {
            if (et_ppob_telkom_account_number.text.toString() != "") {
                val accountNumber = et_ppob_telkom_account_number.text.toString()
                presenter.ppobinquiry(accountNumber, "TLIH", "Tagihan", "", "")
            }
            else {
                biller = ""
                alertWarningCustom.getView().tv_message?.text = "Nomor ID Pelanggan Belum Diisi"
                alertWarningCustom.show()
            }
        }

        btn_bill_detail_payment.setOnClickListener {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, "telkom")
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }

        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {
            container_bill_detail_bills.visibility = View.VISIBLE
            container_bill_detail_input.visibility = View.GONE

            productName = ppobInquiryResponse.productName.toString()
            accountNumber = ppobInquiryResponse.accountNumber.toString()

            tv_bill_detail_tv_title_text?.text = getString(R.string.telkom)
            tv_bill_detail_header_name?.text = "${preferencesUtil.getUser()?.customerName},"

            if (ppobInquiryResponse.listBillCustomerDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_customers.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailCustomersAdapter.removeAll()
                    ppobInquiryResponse.listBillCustomerDetails?.let {
                        ppobBillDetailCustomersAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailCustomersAdapter
                }
            }
            else {
                rv_ppob_bill_detail_customers.visibility = View.GONE
                guideline3.visibility = View.GONE
            }


            if (ppobInquiryResponse.listBillProductDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_product.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailProductAdapter.removeAll()
                    ppobInquiryResponse.listBillProductDetails?.let {
                        ppobBillDetailProductAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailProductAdapter
                }
            }
            else {
                rv_ppob_bill_detail_product.visibility = View.GONE
                guideline4.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillBillDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_bills.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailBillAdapter.removeAll()
                    ppobInquiryResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                    adapter = ppobBillDetailBillAdapter
                }
            }
            else {
                rv_ppob_bill_detail_bills.visibility = View.GONE
                guideline6.visibility = View.GONE
            }


            tv_bill_detail_amount_total?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()
            tv_bill_detail_amount_total_payment?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()

            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            totalAmount = ppobInquiryResponse.amount.toString()

            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }


}