package id.andalan.andalanku.ui.promo

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PromoResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PromoPresenter @Inject constructor(private val service: Services): BasePresenter<PromoView>() {
    override fun onCreate() {
        getListPromo(1, "")
    }

    fun getListPromo(page: Int, keyword: String) {
        view.showWait()
        val subscription = service.getPromo(page, keyword).subscribe(object : Subscriber<PromoResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(promoResponse: PromoResponse) {
                if (promoResponse.status == ConfigVar.SUCCESS) {
                    view.onGetListPromo(promoResponse.promoList)
                } else {
                    view.onFailure(promoResponse.status?:"")
                }
            }
        })
        subscriptions.add(subscription)
    }
}