package id.andalan.andalanku.ui.ppob

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.ui.agent.AgentRegisterTncActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.activity_installment_payment_process.*
import kotlinx.android.synthetic.main.activity_installment_payment_process.web_view
import kotlinx.android.synthetic.main.activity_ppob_payment_process.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobPaymentProcessActivity : BaseApp(), PpobPaymentView {

    companion object {
        val PRODUCT_CODE = "product_code"
        val ACCOUNT_NUMBER = "account_number"
        val ZONE_ID = "zone_id"
        val MONTH = "month"
        val PRODUCT_IMAGE = "product_image"
        val PRODUCT_NAME = "product_name"
        val PAYMENT_URL = "payment_url"
        val TOTAL_AMOUNT = "total_amount"
    }

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var productCode: String
    private lateinit var accountNumber: String
    private lateinit var productImage: String
    private lateinit var productName: String
    private lateinit var paymentUrl: String
    var totalAmount: Long = 0

    @Inject
    lateinit var presenter: PpobPaymentPresenter



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_payment_process)

        cd_ppob_payment_review.visibility = View.GONE

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Metode Pembayaran"

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(  false)
        alertDialogProgress.dismiss()

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().tv_intro_text?.text = "Terima Kasih. Segera Lakukan Pembayaran Sebelum Waktu Habis"
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            //intent.putExtra(AgentRegisterTncActivity.HOME, true)
            val intent = Intent(this, MainHomeActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }

        productCode = intent.getStringExtra(PRODUCT_CODE)
        accountNumber = intent.getStringExtra(ACCOUNT_NUMBER)
        productImage = intent.getStringExtra(PRODUCT_IMAGE)
        productName = intent.getStringExtra(PRODUCT_NAME)
        paymentUrl = intent.getStringExtra(PAYMENT_URL)
        totalAmount = intent.getStringExtra(TOTAL_AMOUNT).toLong()

        deps.inject(this)
        presenter.view = this

        initData()
    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {

    }

    fun initData() {
        var loadingCounter = 0
        web_view.settings.javaScriptEnabled = true
        web_view.webViewClient = WebViewClient()
        web_view.loadUrl(paymentUrl)
        web_view.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                val endpoint = url?.takeLast(14)

                if (endpoint == "payment_finish") {
                    if (!isFinishing) {
                        alertDialogCustom.show()
                    }
                }
                alertDialogProgress.show()
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                loadingCounter ++

                if (loadingCounter == 2) {
                    cd_ppob_payment_review.visibility = View.GONE
                }
                if (!this@PpobPaymentProcessActivity.isFinishing) {
                    alertDialogProgress.dismiss()
                }

                super.onPageFinished(view, url)
            }

        }

        if (productImage != "") {
            if (productImage == "Telkomsel") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.telkomsel_logo)
            }
            else if (productImage == "Indosat") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.indosat_logo)
            }
            else if (productImage == "XL") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.xl_logo)
            }
            else if (productImage == "Axis") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.axis_logo)
            }
            else if (productImage == "Three") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.tri_logo)
            }
            else if (productImage == "Smartfren") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.smartfren_logo)
            }
            else if (productImage == "bpjs") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.bpjs_logo)
            }
            else if (productImage == "pln") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.pln_logo)
            }
            else if (productImage == "pgn") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.pgn_logo)
            }
            else if (productImage == "telkom") {
                img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.telkom_logo)
            }
            else {
                loadImage(this, productImage, img_cd_ppob_payment_review_product_logo)
            }
        }
        else {
            img_cd_ppob_payment_review_product_logo.visibility = View.INVISIBLE
        }

        cd_ppob_payment_review.visibility = View.VISIBLE



        tv_cd_ppob_payment_review_amount.text = totalAmount.convertToRpFormat()
        tv_cd_ppob_payment_review_header.text = getString(R.string.payment_title)
        tv_cd_ppob_payment_review_product.text = productName
        tv_cd_ppob_payment_review_account_number.text = accountNumber

        cd_ppob_payment_review.visibility = View.VISIBLE
    }
}