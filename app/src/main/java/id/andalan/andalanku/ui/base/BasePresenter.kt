package id.andalan.andalanku.ui.base

import rx.subscriptions.CompositeSubscription

abstract class BasePresenter<T: BaseView> {
    lateinit var view: T
    protected val subscriptions: CompositeSubscription = CompositeSubscription()
    fun onStop() {
        subscriptions.unsubscribe()
    }
    abstract fun onCreate()
}