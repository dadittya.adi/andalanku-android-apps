@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Promo
import id.andalan.andalanku.model.ui.PromoHome
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_promo_home.view.*

class PromoHomeAdapter(val onItemClickListener: OnClickItem): BaseAdapter<Promo, PromoHomeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoHomeAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_promo_home
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Promo>(itemView) {
        override fun bind(promo: Promo) {
            val data = promo.getPromoHome()

            itemView.setOnClickListener {
                onItemClickListener.onClickPromo(promo)
            }
            loadImage(itemView.context, data.url?:"", itemView.iv_promo_home)
        }

    }

    interface OnClickItem {
        fun onClickPromo(promo: Promo)
    }
}