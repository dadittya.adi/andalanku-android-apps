package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailUnitResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class DetailUnitPresenter @Inject constructor(private val service: Services): BasePresenter<DetailUnitView>() {
    override fun onCreate() {
    }

    fun getDetailUnit(agreementNo: String) {
        view.showWait()
        val subscription = service.getDetailUnit(agreementNo).subscribe(object : Subscriber<DetailUnitResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailUnitResponse: DetailUnitResponse) {
                view.getDetailUnit(detailUnitResponse)
            }
        })
        subscriptions.add(subscription)
    }
}