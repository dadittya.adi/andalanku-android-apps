package id.andalan.andalanku.ui.installments

import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R

class InstallmentsScheduleActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installments_schedule)
    }
}
