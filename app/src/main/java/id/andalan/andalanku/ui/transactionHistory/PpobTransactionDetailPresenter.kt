package id.andalan.andalanku.ui.transactionHistory

import id.andalan.andalanku.model.response.PpobTransactionDetailResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PpobTransactionDetailPresenter @Inject constructor(private val service: Services): BasePresenter<PpobTransactionDetailView>() {
    override fun onCreate() {
        //getListTransaction()
    }

    fun getTransactionDetail(transactionId: String) {
        view.showWait()
        val subscription = service.PpobTransactionDetail(transactionId)
            .subscribe(object : Subscriber<PpobTransactionDetailResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(ppobTransactionDetailResponse: PpobTransactionDetailResponse) {
                    view.onGetDetailResponse(ppobTransactionDetailResponse)
                }
            })
        subscriptions.add(subscription)
    }
}