package id.andalan.andalanku.ui.promo

import id.andalan.andalanku.model.response.DetailPromoResponse
import id.andalan.andalanku.ui.base.BaseView

interface DetailPromoView: BaseView {
    fun onGetDetailPromo(detailPromoResponse: DetailPromoResponse)
}