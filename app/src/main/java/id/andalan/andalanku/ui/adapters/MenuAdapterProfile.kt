package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_menu_profile.view.*

class MenuAdapterProfile(val onItemClickListener: OnClickItem): BaseAdapter<MenuProfile, MenuAdapterProfile.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_menu_profile
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<MenuProfile>(itemView) {
        override fun bind(data: MenuProfile) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            if (data.rightText != null) {
                itemView.tv_right?.text = data.rightText
                itemView.tv_right?.visibility = View.VISIBLE
                itemView.ic_right?.visibility = View.GONE
            }
            itemView.tv_name.text = data.name
        }

    }

    interface OnClickItem {
        fun onClickMenu(menuProfile: MenuProfile)
    }
}