package id.andalan.andalanku.ui.cash_value

import android.os.Bundle
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.home.MainHomeActivity
import kotlinx.android.synthetic.main.toolbar_login.*

class CashValueComingSoonActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cash_value_coming_soon)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.cash_value)
    }
}
