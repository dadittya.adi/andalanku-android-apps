package id.andalan.andalanku.ui.loan_application

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_INBOX
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_PERSONAL_INFO
import id.andalan.andalanku.model.request.SendLoanApplicationRequest
import id.andalan.andalanku.model.response.LoanApplicationResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.ui.CreditSimulationO
import id.andalan.andalanku.model.ui.Document
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.DocumentAdapter
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.ImagePickerActivity
import kotlinx.android.synthetic.main.activity_complete_document.*
import kotlinx.android.synthetic.main.layout_success_payment_submission.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class CompleteDocumentActivity : BaseApp(), CompleteDocumentView {
    companion object {
        val REQUEST_IMAGE_KTP = 1261
        val REQUEST_IMAGE_KK = 1262
        val REQUEST_IMAGE_SLIP = 1263
        val REQUEST_IMAGE_KORAN = 1264
        val REQUEST_IMAGE_LAIN = 1265
        val COLLATERAL = "collateral"
        val CREDITRESQUEST = "credit_request"
        val HOME = "home"
        val MESSAGE = "message"
        val CREDIT = "credit"
    }

    private lateinit var documentAdapterTop: DocumentAdapter
    private lateinit var documentAdapterBottom: DocumentAdapter
    private val documentDataTop: MutableList<Document> = arrayListOf()
    private val documentDataBottom: MutableList<Document> = arrayListOf()
    private var personalInformation: PersonalInformationResponse? = null

    private lateinit var simulation: CreditSimulationO
    private lateinit var sendLoanApplicationRequest: SendLoanApplicationRequest
    private lateinit var collateral: Collateral
    private lateinit var creditSimulationRequest: CreditSimulationRequest

    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: CompleteDocumentPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_document)
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.kelengkapan_dokumen)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        deps.inject(this)
        presenter.view = this

        sendLoanApplicationRequest = intent.getParcelableExtra(CompleteDataFragment.COMPLETE_DATA)
        simulation = intent.getParcelableExtra(CREDIT)
        collateral = intent.getParcelableExtra(COLLATERAL)
        creditSimulationRequest = intent.getParcelableExtra(CREDITRESQUEST)
        personalInformation = preferencesUtil.getProfile()

        documentAdapterTop = DocumentAdapter(object: DocumentAdapter.OnClick{
            override fun onClickItem(document: Document) {
                if (document.id == "1") {
                    openImagePicker(REQUEST_IMAGE_KTP)
                } else if (document.id == "2") {
                    openImagePicker(REQUEST_IMAGE_KK)
                }
            }
        })

        documentAdapterBottom = DocumentAdapter(object: DocumentAdapter.OnClick{
            override fun onClickItem(document: Document) {
                if (document.id == "1") {
                    openImagePicker(REQUEST_IMAGE_SLIP)
                } else if (document.id == "2") {
                    openImagePicker(REQUEST_IMAGE_KORAN)
                } else if (document.id == "3") {
                    openImagePicker(REQUEST_IMAGE_LAIN)
                }

            }
        })

        initData()

        rv_form_top.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_form_top.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_form_top.adapter = documentAdapterTop

        rv_form_bottom.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_form_bottom.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_form_bottom.adapter = documentAdapterBottom

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success_payment_submission, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = "Pengajuan Pembiayaan"
        alertDialogCustom.getView().tv_message_content?.text = "Formulir Pengajuan pembiayaan Andalanku Anda Sudah kami terima. Untuk mengetahui status pengajuan anda lihat progres nya di Halaman Pesan."
        alertDialogCustom.getView().btn_back?.setOnClickListener {
            alertDialogCustom.dismiss()
            val intent = intent
            intent.putExtra(HOME, true)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
        alertDialogCustom.getView().btn_see_result?.setOnClickListener {
            alertDialogCustom.dismiss()
            val intent = intent
            intent.putExtra(MESSAGE, true)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        btn_submission.setOnClickListener {
            if (cb_term_condition.isChecked) {
                val fotoKTP = documentDataTop[0].urlImage

                if (fotoKTP != null) {
                    val fotoKK = documentDataTop[1].urlImage
                    val fotoSlip = documentDataBottom[0].urlImage
                    val fotoKoran = documentDataBottom[1].urlImage
                    val fotoLain = documentDataBottom[2].urlImage

                    sendLoanApplicationRequest.product = creditSimulationRequest.type
                    sendLoanApplicationRequest.otr = creditSimulationRequest.otr
                    sendLoanApplicationRequest.dpPercent = creditSimulationRequest.dp
                    sendLoanApplicationRequest.tenor = creditSimulationRequest.tenor
                    sendLoanApplicationRequest.insurance = creditSimulationRequest.asuransi
                    sendLoanApplicationRequest.region = creditSimulationRequest.wilayah

                    sendLoanApplicationRequest.collateralType = collateral.collateralType
                    sendLoanApplicationRequest.collateral = collateral.collateral
                    sendLoanApplicationRequest.collateralOwner = collateral.collateralOwner
                    sendLoanApplicationRequest.collateralYear = collateral.collateralYear
                    sendLoanApplicationRequest.collateralPaperNumber =
                        collateral.collateralPaperNumber

                    sendLoanApplicationRequest.fotoKtp = fotoKTP
                    sendLoanApplicationRequest.typeFotoKtp = documentDataTop[0].typeUrlImage
                    sendLoanApplicationRequest.fotoKk = fotoKK
                    sendLoanApplicationRequest.typeFotoKk = documentDataTop[1].typeUrlImage
                    sendLoanApplicationRequest.fotoSlipGaji = fotoSlip
                    sendLoanApplicationRequest.typeFotoSlipGaji = documentDataBottom[0].typeUrlImage
                    sendLoanApplicationRequest.fotoRekeningKoran = fotoKoran
                    sendLoanApplicationRequest.typeFotoRekeningKoran =
                        documentDataBottom[1].typeUrlImage
                    sendLoanApplicationRequest.fotoKeuanganLainya = fotoLain
                    sendLoanApplicationRequest.typeFotoKeuanganLainya =
                        documentDataBottom[2].typeUrlImage

                    presenter.sendLoanapplication(sendLoanApplicationRequest)
                } else {
                    Toast.makeText(this@CompleteDocumentActivity, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this@CompleteDocumentActivity, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initData() {
        documentDataTop.add(Document(id = "1", hintText = "Upload Foto KTP", urlImage = if (sendLoanApplicationRequest.loanApplicationReferal == "N") personalInformation?.fotoKtp else ""))
        documentDataTop.add(Document(id = "2", hintText = "Upload Foto Kartu Keluarga", urlImage = if (sendLoanApplicationRequest.loanApplicationReferal == "N") personalInformation?.fotoKk else ""))
        documentDataBottom.add(Document(id = "1", hintText = "Upload Foto Slip Gaji", urlImage = if (sendLoanApplicationRequest.loanApplicationReferal == "N") personalInformation?.fotoSlipGaji else ""))
        documentDataBottom.add(Document(id = "2", hintText = "Upload Foto Rekening Koran", infoText = "*  Rekening Koran  1 bulan terakhir", urlImage = if (sendLoanApplicationRequest.loanApplicationReferal == "N") personalInformation?.fotoRekeningKoran else ""))
        documentDataBottom.add(Document(id = "3", hintText = "Upload Foto Data Keuangan Lainnya", infoText = "*  khusus untuk pemohon bukan pekerja", urlImage = if (sendLoanApplicationRequest.loanApplicationReferal == "N") personalInformation?.fotoKeuanganLainnya else ""))
        documentAdapterTop.add(documentDataTop)
        documentAdapterBottom.add(documentDataBottom)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_KTP) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                documentDataTop[0] = Document(id = "1", hintText = "Upload Foto KTP", urlImage = uri?.path, typeUrlImage = "file")
                documentAdapterTop.removeAll()
                documentAdapterTop.add(documentDataTop)
            }
        }
        if (requestCode == REQUEST_IMAGE_KK) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                documentDataTop[1] = Document(id = "2", hintText = "Upload Foto Kartu Keluarga", urlImage = uri?.path, typeUrlImage = "file")
                documentAdapterTop.removeAll()
                documentAdapterTop.add(documentDataTop)
            }
        }
        if (requestCode == REQUEST_IMAGE_SLIP) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                documentDataBottom[0] = Document(id = "1", hintText = "Upload Foto Slip Gaji", urlImage = uri?.path, typeUrlImage = "file")
                documentAdapterBottom.removeAll()
                documentAdapterBottom.add(documentDataBottom)
            }
        }
        if (requestCode == REQUEST_IMAGE_KORAN) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                documentDataBottom[1] = Document(id = "2", hintText = "Upload Foto Rekening Koran", infoText = "*  Rekening Koran  1 bulan terakhir", urlImage = uri?.path, typeUrlImage = "file")
                documentAdapterBottom.removeAll()
                documentAdapterBottom.add(documentDataBottom)
            }
        }
        if (requestCode == REQUEST_IMAGE_LAIN) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")
                documentDataBottom[2] = Document(id = "3", hintText = "Upload Foto Data Keuangan Lainnya", infoText = "*  khusus untuk pemohon bukan pekerja", urlImage = uri?.path, typeUrlImage = "file")
                documentAdapterBottom.removeAll()
                documentAdapterBottom.add(documentDataBottom)
            }
        }
    }

    private fun openImagePicker(request: Int) {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions(request)
                    } else {
                        // TODO - handle permission denied case
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun showImagePickerOptions(request: Int) {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent(request)
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent(request)
            }

        })
    }

    private fun launchCameraIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, request)
    }

    private fun launchGalleryIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, request)
    }

    override fun onResponseSend(sendLoanApplicationResponse: LoanApplicationResponse) {
        if (sendLoanApplicationResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.show()
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_INBOX))
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_PERSONAL_INFO))
        } else {
            Toast.makeText(this@CompleteDocumentActivity, sendLoanApplicationResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this@CompleteDocumentActivity, "Gagal mengirim data", Toast.LENGTH_SHORT).show()
    }
}
