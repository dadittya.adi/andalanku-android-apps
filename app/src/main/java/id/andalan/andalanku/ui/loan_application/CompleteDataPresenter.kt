package id.andalan.andalanku.ui.loan_application

import id.andalan.andalanku.model.request.PlafondApplicationRequest
import id.andalan.andalanku.model.response.BranchListResponse
import id.andalan.andalanku.model.response.LocationResponse
import id.andalan.andalanku.model.response.OccupationsResponse
import id.andalan.andalanku.model.response.PlafondApplicationResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class CompleteDataPresenter @Inject constructor(private val service: Services): BasePresenter<CompleteDataView>() {
    override fun onCreate() {
        getOccupationList()
    }

    fun getOccupationList() {
        view.showWait()
        val subscription = service.getOccupation()
            .subscribe(object : Subscriber<OccupationsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(occupationsResponse: OccupationsResponse) {
                    getListProvice()
                    view.onGetOccupation(occupationsResponse.occupationList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListProvice() {
        view.showWait()
        val subscription = service.getProvince()
            .subscribe(object : Subscriber<LocationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(locationResponse: LocationResponse) {
                view.onGetProvince(locationResponse.provinceList?: arrayListOf())
            }
        })
        subscriptions.add(subscription)
    }

    fun getListCity(id: String) {
        view.showWait()
        val subscription = service.getCity(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetCity(locationResponse.cityList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListSubDistrict(id: String) {
        view.showWait()
        val subscription = service.getDistrict(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetSubdistrict(locationResponse.districtList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListVillage(id: String) {
        view.showWait()
        val subscription = service.getVillage(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetVillage(locationResponse.villageList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListBranch(idCity: String) {
        view.showWait()
        val subscription = service.getAdvanceBranchList(idCity)
            .subscribe(object : Subscriber<BranchListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(getBranchListResponse: BranchListResponse) {
                    view.onGetBranch(getBranchListResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun getResponsePlafondResponse(plafondApplicationRequest: PlafondApplicationRequest) {
        view.showWait()
        val subscription = service.sendPlafondApplication(plafondApplicationRequest)
            .subscribe(object : Subscriber<PlafondApplicationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(plafondApplicationResponse: PlafondApplicationResponse) {
                    getListProvice()
                    view.onResponseApplicationPlafond(plafondApplicationResponse)
                }
            })
        subscriptions.add(subscription)
    }
}