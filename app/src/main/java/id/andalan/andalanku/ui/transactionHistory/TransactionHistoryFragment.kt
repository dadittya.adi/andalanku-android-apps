package id.andalan.andalanku.ui.transactionHistory

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.ComplainListResponse
import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.model.response.InstallmentPaymentHistoryListResponse
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.TransactionHistoryAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentProcessActivity
import kotlinx.android.synthetic.main.activity_ppob_category_list.*
import kotlinx.android.synthetic.main.fragment_transaction_history.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class TransactionHistoryFragment : androidx.fragment.app.Fragment(), TransactionHistoryView {
    companion object {
        val REQUEST_SEND_COMPLAIN = 113
        val RESPONSE_SEND_COMPLAIN = 213
    }
    private val TAG = TransactionHistoryFragment::class.java.simpleName

    private lateinit var transactionHistoryAdapter: TransactionHistoryAdapter

    var preferencesUtil: PreferencesUtil? = null
    var category: String = "all"
    var paymentStatus: String = "all"

    @Inject
    lateinit var presenter: TransactionHistoryPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.transaction_history)

        presenter = TransactionHistoryPresenter((activity as MainHomeActivity).services)
        presenter.view = this

        tv_title?.text = getString(R.string.transaction_history)
        context?.let { ContextCompat.getColor(it, R.color.snow) }?.let {
            layout?.setBackgroundColor(
                it
            )
        }

        radio_group_status.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radio_all -> paymentStatus = "all"
                R.id.radio_waiting -> paymentStatus = "waiting"
                R.id.radio_done -> paymentStatus = "done"
                else -> paymentStatus = "all"
            }
            presenter.getListTransaction(category, paymentStatus)
        }
        btn_add?.visibility = View.INVISIBLE
        tabs?.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
                //e(TAG, "reselect ${p0?.position}")
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                //e(TAG, "un selected ${p0?.position}")
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                //onTabsPosition(p0?.position?:0)
                if (p0 != null) {
                    when (p0.position) {
                        0 -> {
                            category = "all"
                        }
                        1 -> {
                            category = "installment"
                        }
                        2 -> {
                            category = "top_up"
                        }
                        3 -> {
                            category = "tagihan"
                        }
                        4  -> {
                            category = "hiburan"
                        }
                    }
                }
                presenter.getListTransaction(category, paymentStatus)
            }

        })

        (activity as MainHomeActivity).apiUrl
        transactionHistoryAdapter = TransactionHistoryAdapter(object: TransactionHistoryAdapter.OnClickItem{
            override fun onClickVa(installmentPaymentHistory: InstallmentPaymentHistory) {
                val prefix = installmentPaymentHistory.orderId.toString().substring(0,3)
                val intent = Intent(context, InstallmentPaymentProcessActivity::class.java)
                var paymentUrl = (activity as MainHomeActivity).apiUrl + "/carspay?transaction_code=" + installmentPaymentHistory.orderId
                if (prefix == "AYO") {
                    paymentUrl += "&type=ppob"
                }
                intent.putExtra(InstallmentPaymentProcessActivity.TRANSACTION_URL, paymentUrl)
                intent.putExtra(InstallmentPaymentProcessActivity.PAY_PINALTY, 'Y')
                startActivity(intent)
            }
            override fun onClickDetail(installmentPaymentHistory: InstallmentPaymentHistory) {
                val prefix = installmentPaymentHistory.orderId.toString().substring(0,3)

                if (prefix == "AYO") {
                    val intent = Intent(context, PpobTransactionDetailActivity::class.java)

                    intent.putExtra(PpobTransactionDetailActivity.TRANSACTION_ID, installmentPaymentHistory.orderId)
                    startActivity(intent)
                }
            }
        })

        rv_transaction?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_transaction?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_transaction?.adapter = transactionHistoryAdapter

        swipe_layout?.setOnRefreshListener {
            presenter.getListTransaction(category, paymentStatus)
        }

    }


    fun onTabsPosition (position: Int) {
        when (position) {
            0 -> {
                presenter.getListTransaction(category, paymentStatus)
            }
            1 -> {
                presenter.getListTransaction(category, paymentStatus)
            }
        }
        transactionHistoryAdapter.removeAll()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SEND_COMPLAIN && resultCode == RESPONSE_SEND_COMPLAIN) {
            onTabsPosition(tabs.selectedTabPosition)
        }
    }

    override fun getListResponse(installmentPaymentHistoryResponse: InstallmentPaymentHistoryListResponse) {
        if (installmentPaymentHistoryResponse.status == ConfigVar.SUCCESS) {
            transactionHistoryAdapter.removeAll()
            installmentPaymentHistoryResponse.listInstallmentPaymentHistory?.let {
                if (it.size > 0) {
                    transactionHistoryAdapter.add(it)
                    transactionHistoryAdapter.notifyDataSetChanged()
                    showEmptyState(false)
                } else {
                    showEmptyState(true)
                }
            }
        } else {
            showEmptyState(true)
        }
    }

    private fun showEmptyState(state: Boolean) {
        //rv_complain?.visibility = if (state) View.GONE else View.VISIBLE
        //container_empty_state?.visibility = if (state) View.VISIBLE else View.GONE
    }

    override fun showWait() {
        swipe_layout.isRefreshing = true
    }

    override fun removeWait() {
        swipe_layout.isRefreshing = false
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(context, appErrorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onResume() {
        super.onResume()

        presenter.getListTransaction(category, paymentStatus)
    }
}
