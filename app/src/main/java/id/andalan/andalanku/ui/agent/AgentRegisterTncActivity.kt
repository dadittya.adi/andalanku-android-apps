package id.andalan.andalanku.ui.agent

import android.app.Activity
import android.os.Bundle
import android.text.Editable
import android.text.Html.TagHandler
import android.view.LayoutInflater
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.SendAgenRegistrationRequest
import id.andalan.andalanku.model.response.AgentRegistrationResponse
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.agent_register_tnc_activity.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.layout_success_payment_submission.view.tv_title_text
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import org.xml.sax.XMLReader
import javax.inject.Inject


class AgentRegisterTncActivity : BaseApp(), AgentRegisterTncView  {
    companion object {
        val HOME = "home"
    }
    @Inject
    lateinit var presenter: AgentRegisterTncPresenter
    
    private lateinit var sendAgentRegisterRequest: SendAgenRegistrationRequest
    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    var agreementHtml: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setContentView(R.layout.agent_register_tnc_activity)

        sendAgentRegisterRequest = intent.getParcelableExtra(AgentRegisterActivity.COMPLETE_DATA)
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        web_view.loadUrl("file:///android_asset/agent_agreement.html")
        tv_title.text = getString(R.string.head_tnc_agent)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        btn_submit_agent.isEnabled = false

        cb_term_condition?.setOnCheckedChangeListener{_, isChecked ->
            btn_submit_agent.isEnabled = cb_term_condition.isChecked == true
        }

        btn_submit_agent?.setOnClickListener {
            //val name = et_name?.text.toString()
            //sendAgentRegisterRequest = intent.getParcelableExtra(AgentRegisterActivity.COMPLETE_DATA)
            presenter.sendAgentRegistration(sendAgentRegisterRequest)
        }

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().tv_intro_text?.text = "Formulir pendaftaran AgentAnda Sudah kami terima. Untuk mengetahui status pendaftaran dapat anda lihat progres nya di Halaman Pesan."
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            val intent = intent
            intent.putExtra(HOME, true)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }
    override fun onResponseSend(agentRegistrationResponse: AgentRegistrationResponse) {
        if (agentRegistrationResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.show()
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_INBOX))
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_PERSONAL_INFO))
        } else {
            Toast.makeText(this@AgentRegisterTncActivity, agentRegistrationResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this@AgentRegisterTncActivity, "Gagal mengirim data", Toast.LENGTH_SHORT).show()
    }

}

class MyTagHandler : TagHandler {
    var first = true
    var parent: String? = null
    var index = 1
    override fun handleTag(
        opening: Boolean,
        tag: String,
        output: Editable,
        xmlReader: XMLReader?
    ) {
        if (tag == "ul") {
            parent = "ul"
        } else if (tag == "ol") {
            parent = "ol"
        }
        if (tag == "li") {
            if (parent == "ul") {
                first = if (first) {
                    output.append("\n\t•")
                    false
                } else {
                    true
                }
            } else {
                if (first) {
                    output.append("\n\t$index. ")
                    first = false
                    index++
                } else {
                    first = true
                }
            }
        }
    }
}