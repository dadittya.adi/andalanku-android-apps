package id.andalan.andalanku.ui.preloader

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Menu
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.MenuAdapter
import id.andalan.andalanku.ui.adapters.MenuAdapterHome
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.dana.SimulationDanaActivity
import kotlinx.android.synthetic.main.activity_preloader.*
import javax.inject.Inject
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.newcar.PaymentNewCarActivity
import id.andalan.andalanku.ui.news.NewsActivity
import id.andalan.andalanku.ui.promo.PromoActivity
import id.andalan.andalanku.ui.usedcar.UsedCarActivity
import id.andalan.andalanku.utils.ekstensions.setHeight
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.top_drag_view.*


class PreloaderActivity : BaseApp() {

    val TAG = PreloaderActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private var menuAdapter: MenuAdapter? = null
    private lateinit var menuAdapterHome: MenuAdapterHome

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preloader)

        deps.inject(this)

        e(TAG, preferencesUtil.getIdNumber().toString())

        menuAdapter = MenuAdapter(object: MenuAdapter.onClickItem{
            override fun onClickMenu(menu: Menu) {
                when {
                    menu.name == getString(R.string.promo_andalanku) -> {
                        startActivity(Intent(this@PreloaderActivity, PromoActivity::class.java))
                    }
                    menu.name == getString(R.string.payment_simulation) -> {
                        et_drag_view.visibility = View.GONE
                        showMenuSubmission()
                        showSlideLayout()
                    }
                    menu.name == getString(R.string.info_andalanku) -> {
                        startActivity(Intent(this@PreloaderActivity, NewsActivity::class.java))
                    }
                }
            }
        })
        menuAdapterHome = MenuAdapterHome(object: MenuAdapterHome.onClickItem{
            override fun onClickMenu(menu: Menu) {
                when {
                    menu.type == ConfigVar.MENU_TYPE_E_COMPLAIN -> startActivity(Intent(this@PreloaderActivity, EComplainActivity::class.java))
                    menu.name == getString(R.string.new_cars) -> startActivity(Intent(this@PreloaderActivity, PaymentNewCarActivity::class.java))
                    menu.name == getString(R.string.used_car) -> startActivity(Intent(this@PreloaderActivity, UsedCarActivity::class.java))
                    menu.name == getString(R.string.wallet_andalanku) -> startActivity(Intent(this@PreloaderActivity, SimulationDanaActivity::class.java))
                }
                hideSlideLayout()
            }
        })

        btn_login.setOnClickListener {
            val myIntent = Intent(this, LoginActivity::class.java)
            startActivity(myIntent)
        }

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        btn_close_drag_view?.visibility = View.GONE
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }
        val height = resources.getDimension(R.dimen.height_drag_view_content_300)
        container_content_drag_view.setHeight(height.toInt())

        rv_menu.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            baseContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        rv_menu.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu.adapter = menuAdapter

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.GridLayoutManager(this, 3)
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_drag_view.adapter = menuAdapterHome

        loadMenu()
    }

    private fun loadMenu() {
        menuAdapter?.add(
            Menu(
                name = getString(R.string.promo_andalanku),
                image = R.drawable.ic_promo_preloader
            )
        )
        menuAdapter?.add(
            Menu(
                name = getString(R.string.payment_simulation),
                image = R.drawable.ic_payment_simulation
            )
        )
        menuAdapter?.add(
            Menu(
                name = getString(R.string.info_andalanku),
                image = R.drawable.ic_info_andalanku
            )
        )
    }

    private fun showMenuSubmission() {
        tv_title_drag_view?.text = getString(R.string.financial_submission)
        menuAdapterHome.removeAll()
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.new_cars),
                image = R.drawable.ic_new_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.used_car),
                image = R.drawable.ic_used_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.wallet_andalanku),
                image = R.drawable.ic_dana_andalanku
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.e_commerce),
                image = R.drawable.ic_ecommerce
            )
        )
        menuAdapterHome.notifyDataSetChanged()
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            super.onBackPressed()
        }
    }
}
