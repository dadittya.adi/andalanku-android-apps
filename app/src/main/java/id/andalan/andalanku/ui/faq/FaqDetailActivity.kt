package id.andalan.andalanku.ui.faq

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.FaqParent
import id.andalan.andalanku.ui.adapters.FaqAnswerAdapter
import kotlinx.android.synthetic.main.activity_faq_detail.*
import kotlinx.android.synthetic.main.toolbar_login.*

class FaqDetailActivity : BaseApp() {
    companion object {
        val CONTENT_FAQS = "content_faqs"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.faqs)

        val content: FaqParent? = intent?.extras?.getParcelable(CONTENT_FAQS)

        if (content == null) {
            finish()
        }
        val adapter = FaqAnswerAdapter()
        adapter.add(content?.faqAnswerList?: arrayListOf())
        tv_title_text?.text = content?.question
        rv_content.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_content.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_content.adapter = adapter

    }
}
