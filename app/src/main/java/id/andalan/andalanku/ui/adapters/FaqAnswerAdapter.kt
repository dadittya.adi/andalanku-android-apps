package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Faq
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_faq.view.*
import java.lang.NullPointerException

class FaqAnswerAdapter : BaseAdapter<Faq, FaqAnswerAdapter.ViewHolder>() {
    var i = 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
       //i = 1
        return R.layout.item_faq
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Faq>(itemView) {

        override fun bind(data: Faq) {
            itemView.tv_number.text = data.counter
            itemView.tv_name.text = data.description
            if (data.image?.isNotEmpty() == true){
                try {
                    Glide
                        .with(itemView.context)
                        .load(data.image)
                        .into(itemView.iv_faq)
                } catch (error: NullPointerException) {}
            }
            else {
                itemView.iv_faq.layoutParams.height = 0
                itemView.iv_faq.requestLayout()
            }
            i++
        }

    }
}