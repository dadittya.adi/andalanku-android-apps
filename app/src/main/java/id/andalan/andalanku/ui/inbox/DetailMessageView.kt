package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.response.DetailInboxResponse
import id.andalan.andalanku.ui.base.BaseView

interface DetailMessageView: BaseView {
    fun onGetDetailMessage(detailInboxResponse: DetailInboxResponse)
}