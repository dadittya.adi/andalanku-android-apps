package id.andalan.andalanku.ui.agent

import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.ui.base.BaseView

interface AgentRegisterView: BaseView {
    fun onGetProvince(mutableList: MutableList<Location>)
    fun onGetCity(mutableList: MutableList<Location>)
    fun onGetSubdistrict(mutableList: MutableList<Location>)
    fun onGetVillage(mutableList: MutableList<Location>)
    fun getResponseUpdatePersonalInfo(updateAddressResponse: UpdateAddressResponse)
    fun successUpdateAddress(updateAddressResponse: UpdateAddressResponse)
    fun getResponseUpdateAllAddress(updateAllAddressResponse: UpdateAddressResponse)
    fun onGetZipCode(zipCode: String)
    fun onGetBank(bankResponse: BankResponse)
    fun onGetBankBranch(bankBranchResponse: BankBranchResponse)
    fun onGetBranch(branchListResponse: BranchListResponse)
    fun onGetOccupation(list: MutableList<Occupation>)
    fun onNipCheck(nipCheckResponse: NipCheckResponse)
    //fun getProfileInformation(personalInformationResponse: PersonalInformationResponse)
    //fun onGetPersonalInformation12(personalInformationResponse: PersonalInformationResponse)
    //fun foo()
}