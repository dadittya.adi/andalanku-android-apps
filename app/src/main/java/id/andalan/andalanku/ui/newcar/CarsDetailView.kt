package id.andalan.andalanku.ui.newcar

import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.ZoneArea
import id.andalan.andalanku.ui.base.BaseView

interface CarsDetailView: BaseView {
    fun onGetCreditSimulation(creditSimulationResponse: CreditSimulationResponse)
    fun onGetCarTrim(carTrimsResponse: CarTrimsResponse)
    fun onGetUsedCarsList(usedCarListResponse: UsedCarListResponse)
}