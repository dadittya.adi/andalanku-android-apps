package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class DetailKontrakPresenter @Inject constructor(private val service: Services): BasePresenter<DetailKontrakView>() {
    override fun onCreate() {
    }

    fun getDetailFinancial(agreementNo: String) {
        view.showWait()
        val subscription = service.getDetailFinancial(agreementNo).subscribe(object : Subscriber<DetailFinanceResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailFinanceResponse: DetailFinanceResponse) {
                view.onGetDetailContract(detailFinanceResponse)
            }
        })
        subscriptions.add(subscription)
    }

}