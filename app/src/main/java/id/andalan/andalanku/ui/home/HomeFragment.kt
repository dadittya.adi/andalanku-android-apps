package id.andalan.andalanku.ui.home

//import kotlinx.android.synthetic.main.home_carouselview.carouselView
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.skydoves.balloon.balloon
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState
import com.synnapps.carouselview.CarouselView
import com.synnapps.carouselview.ImageListener
import id.andalan.andalanku.ui.ppob.PpobCategoryListActivity
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.factory.*
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.Menu
import id.andalan.andalanku.model.ui.News
import id.andalan.andalanku.model.ui.PartnerHome
import id.andalan.andalanku.model.ui.Promo
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.account.MyAccountFragment
import id.andalan.andalanku.ui.account.UpdatePersonalInfoActivity
import id.andalan.andalanku.ui.adapters.MenuAdapterHome
import id.andalan.andalanku.ui.adapters.NewsHomeAdapter
import id.andalan.andalanku.ui.adapters.PartnerHomeAdapter
import id.andalan.andalanku.ui.adapters.PromoHomeAdapter
import id.andalan.andalanku.ui.agent.AgentRegisterActivity
import id.andalan.andalanku.ui.cash_value.CashValueComingSoonActivity
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.contract.ContractActivity
import id.andalan.andalanku.ui.dana.SimulationDanaActivity
import id.andalan.andalanku.ui.ecommerce.EcommerceComingSoonActivity
import id.andalan.andalanku.ui.guarantee.RequestTakeGuaranteeActivity
import id.andalan.andalanku.ui.installments.InstallmentScheduleActivity
import id.andalan.andalanku.ui.insurance.EClaimInsuranceActivity
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.newcar.PaymentNewCarActivity
import id.andalan.andalanku.ui.news.NewsActivity
import id.andalan.andalanku.ui.news.NewsDetailActivity
import id.andalan.andalanku.ui.news.NewsDetailActivity.Companion.NEWS
import id.andalan.andalanku.ui.partner.PartnerDetailActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentActivity
import id.andalan.andalanku.ui.ppob.PpobPlnActivity
import id.andalan.andalanku.ui.ppob.PpobPulsaActivity
import id.andalan.andalanku.ui.promo.DetailPromoActivity
import id.andalan.andalanku.ui.promo.PromoActivity
import id.andalan.andalanku.ui.repayment.RepaymentSimulationActivity
import id.andalan.andalanku.ui.usedcar.UsedCarActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.PositionOnScrollListener
import id.andalan.andalanku.utils.convertToMoneyFormat
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.sliding_layout
import kotlinx.android.synthetic.main.home_carouselview.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.sub_header_home.*
import me.toptas.fancyshowcase.FancyShowCaseView
import me.toptas.fancyshowcase.FocusShape
import me.toptas.fancyshowcase.listener.DismissListener

class HomeFragment : androidx.fragment.app.Fragment(), HomeFragmentView {

    private val TAG: String = HomeFragment::class.java.simpleName

    private var position = 0
    private var imageIndicator = arrayOf<ImageView?>()
    private lateinit var promoHomeAdapter: PromoHomeAdapter
    private lateinit var partnerHomeAdapter: PartnerHomeAdapter
    private lateinit var newsHomeAdapter: NewsHomeAdapter
    private lateinit var menuAdapterHome: MenuAdapterHome
    private val submissionBalloon by balloon(SubmissionTipsBalloonFactory::class)
    private val servicesBalloon by balloon(ServicesTipsBalloonFactory::class)
    private val simulationBalloon by balloon(SimulationTipsBalloonFactory::class)
    private val cashValueTipsBalloonFactory by balloon(CashValueTipsBalloonFactory::class)
    private val scheduleTipsBalloonFactory by balloon(ScheduleTipsBalloonFactory::class)
    private val bannerTipsBalloonFactory by balloon(BannerTipsBalloonFactory::class)
    private var mContext: Context? = null

    var preferencesUtil: PreferencesUtil? = null
    lateinit var presenter: HomeFragmentPresenter

    private lateinit var alertDialogCustom: AlertDialogCustom
    lateinit var mFancyShowCaseView: FancyShowCaseView


    companion object {

        private val TAG = "LocationProvider"

        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return LayoutInflater.from(mContext).inflate(R.layout.fragment_home, container, false)
    }

    override fun onResume() {
        super.onResume()
        (activity as MainHomeActivity).isReferensikan = false
        if (preferencesUtil !== null) {
            tv_user_name?.text =
                if (preferencesUtil?.getUser()?.customerName?.isNotBlank() == true) "Hi, ${preferencesUtil?.getUser()?.customerName}" else "Selamat datang"
        } else {
            tv_user_name?.text = "Selamat datang"
        }

    }

    var slideImages: MutableList<String> = ArrayList()
    var slidePromoSlug: MutableList<String> = ArrayList()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preferencesUtil = (activity as MainHomeActivity).preferencesUtil
        presenter = HomeFragmentPresenter((activity as MainHomeActivity).services)
        presenter.view = this

        alertDialogCustom = AlertDialogCustom(mContext!!, null)
        alertDialogCustom.setView(
            LayoutInflater.from(mContext).inflate(R.layout.layout_success, null, false)
        )
        alertDialogCustom.create()
        //alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
        }

//        carouselViewHome.setImageListener(imagesListener)
//        carouselViewHome.pageCount = sampleImages.size

        tv_user_name?.text =
            if (preferencesUtil?.getUser()?.customerName?.isNotBlank() == true) preferencesUtil?.getUser()?.customerName else "Selamat datang"

        btn_plafond?.setOnClickListener {
            (activity as MainHomeActivity).moveViewNoAnim(5)
        }
        btn_cash_value?.setOnClickListener {
            val intent = Intent(context, CashValueComingSoonActivity::class.java)
            startActivity(intent)
        }

        btn_submission_home.setOnClickListener {
            showMenuSubmission(context?.getString(R.string.financial_submission) ?: "")
            showMenuSubmission(context?.getString(R.string.financial_submission) ?: "")
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        btn_cash_transfer?.setOnClickListener {
            //(activity as MainHomeActivity).moveViewNoAnim(12)
            val intent = Intent(mContext, CashValueComingSoonActivity::class.java)
            startActivity(intent)
        }
        promoHomeAdapter = PromoHomeAdapter(object : PromoHomeAdapter.OnClickItem {
            override fun onClickPromo(promo: Promo) {
                val intent = Intent(mContext, DetailPromoActivity::class.java)
                intent.putExtra(DetailPromoActivity.PROMOSLUG, promo.slug)
                startActivity(intent)
            }
        })
        partnerHomeAdapter = PartnerHomeAdapter(object : PartnerHomeAdapter.OnClickItem {
            override fun onClickPartner(partnerHome: PartnerHome) {
                val intent = Intent(mContext, PartnerDetailActivity::class.java)
                intent.putExtra(PartnerDetailActivity.LINK_PARTNER, partnerHome.link)
                startActivity(intent)
            }
        })
        newsHomeAdapter = NewsHomeAdapter(object : NewsHomeAdapter.OnClickItem {
            override fun onClickRead(news: News) {
                val intent = Intent(mContext, NewsDetailActivity::class.java)
                intent.putExtra(NEWS, news)
                startActivity(intent)
            }

        })
        menuAdapterHome = MenuAdapterHome(object : MenuAdapterHome.onClickItem {
            override fun onClickMenu(menu: Menu) {
                when {
                    menu.name == context?.getString(R.string.e_complain) -> {
                        if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                            startActivityForResult(
                                Intent(mContext, LoginActivity::class.java),
                                LoginActivity.REQUEST_LOGIN
                            )
                        } else {
                            startActivity(Intent(mContext, EComplainActivity::class.java))
                        }
                    }
                    menu.name == context?.getString(R.string.e_claim_asurance) -> {
                        if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                            startActivityForResult(
                                Intent(mContext, LoginActivity::class.java),
                                LoginActivity.REQUEST_LOGIN
                            )
                        } else {
                            startActivity(Intent(mContext, EClaimInsuranceActivity::class.java))
                        }
                    }
                    menu.name == context?.getString(R.string.e_request_guarantee) -> {
                        if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                            startActivityForResult(
                                Intent(mContext, LoginActivity::class.java),
                                LoginActivity.REQUEST_LOGIN
                            )
                        } else {
                            startActivity(Intent(mContext, RequestTakeGuaranteeActivity::class.java))
                        }
                    }
                    menu.name == getString(R.string.new_cars) -> startActivityForResult(
                        Intent(
                            mContext,
                            PaymentNewCarActivity::class.java
                        ), PaymentNewCarActivity.REQUEST_PAYMENT_SIMULATION
                    )
                    menu.name == getString(R.string.used_car) -> startActivityForResult(
                        Intent(
                            mContext,
                            UsedCarActivity::class.java
                        ), UsedCarActivity.REQUEST_USED_CAR
                    )
                    menu.name == getString(R.string.wallet_andalanku) -> startActivityForResult(
                        Intent(mContext, SimulationDanaActivity::class.java),
                        SimulationDanaActivity.REQUEST_DANA_SIMULATION
                    )
                    menu.name == getString(R.string.e_commerce) -> startActivityForResult(
                        Intent(
                            mContext,
                            EcommerceComingSoonActivity::class.java
                        ), 1
                    )
                }
                sliding_layout.panelState = PanelState.COLLAPSED
            }
        })

        btn_see_all_news.setOnClickListener {
            startActivity(Intent(mContext, NewsActivity::class.java))
        }
        btn_see_all_promo.setOnClickListener {
            startActivity(Intent(mContext, PromoActivity::class.java))
        }
        btn_services.setOnClickListener {
            showMenuServices()
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        btn_credit_simulation.setOnClickListener {
            showMenuSubmission(context?.getString(R.string.credit_simulate) ?: "")
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }

        btn_payment.setOnClickListener {
            if ((preferencesUtil?.getUser()?.idCustomer?:0) == 0) {
                startActivityForResult(Intent(mContext, LoginActivity::class.java), LoginActivity.REQUEST_LOGIN)
            } else {
                startActivity(Intent(mContext, InstallmentPaymentActivity::class.java))
            }
        }

        btn_schedule?.setOnClickListener {
            if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                startActivityForResult(
                    Intent(mContext, LoginActivity::class.java),
                    LoginActivity.REQUEST_LOGIN
                )
            } else {
                val intent = Intent(mContext, InstallmentScheduleActivity::class.java)
                startActivity(intent)
            }
        }

        btn_repayment_simulation?.setOnClickListener {
            if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                startActivityForResult(
                    Intent(mContext, LoginActivity::class.java),
                    LoginActivity.REQUEST_LOGIN
                )
            } else {
                val intent = Intent(mContext, RepaymentSimulationActivity::class.java)
                startActivity(intent)
            }
        }

        btn_document?.setOnClickListener {
            if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                startActivityForResult(
                    Intent(mContext, LoginActivity::class.java),
                    LoginActivity.REQUEST_LOGIN
                )
            } else {
                val intent = Intent(mContext, ContractActivity::class.java)
                startActivity(intent)
            }
        }

        btn_view_services?.setOnClickListener {
            if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                startActivityForResult(
                    Intent(mContext, LoginActivity::class.java),
                    LoginActivity.REQUEST_LOGIN
                )
            } else {
                val intent = Intent(mContext, PpobCategoryListActivity::class.java)
                startActivity(intent)
            }
        }


        sliding_layout?.addPanelSlideListener(object : SlidingUpPanelLayout.PanelSlideListener {
            override fun onPanelSlide(panel: View, slideOffset: Float) {
                Log.i(TAG, "onPanelSlide, offset $slideOffset")
            }

            override fun onPanelStateChanged(
                panel: View,
                previousState: PanelState,
                newState: PanelState
            ) {
                Log.i(TAG, "onPanelStateChanged $newState")
            }
        })

        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = PanelState.COLLAPSED }

        initPromoHome()
        initPartnerHome()
        initNewsHome()
        initMenu()


        val observerNewsResponse = Observer<NewsResponse> {
            it?.let {
                onGetNewsResponse(it)
            }
        }
        (activity as MainHomeActivity).model.currentNewsResponse.observe(viewLifecycleOwner, observerNewsResponse)

        val observerSliderResponse = Observer<SliderResponse> {
            it?.let {
                onGetSliderResponse(it)
                var imageListener: ImageListener = object : ImageListener {
                    override fun setImageForPosition(position: Int, imageView: ImageView) {
                        // You can use Glide or Picasso here
                        mContext?.let { it1 ->
                            Glide
                                .with(it1)
                                .load(slideImages[position])
                                .into(imageView)
                        }
                        //Picasso.get().load(slideImages[position]).into(imageView)
                    }
                }

                val carouselView = view.findViewById<CarouselView>(R.id.carouselViewHome)

                carouselViewHome.setImageListener(imageListener)
                carouselView.pageCount = slideImages.size

                carouselView.setImageClickListener { position ->
                    if (slidePromoSlug[position] != "") {
                        val intent = Intent(mContext, DetailPromoActivity::class.java)
                        intent.putExtra(DetailPromoActivity.PROMOSLUG, slidePromoSlug[position])
                        startActivity(intent)
                    }
                }
            }
        }
        (activity as MainHomeActivity).model.currentSliderResponse.observe(
            viewLifecycleOwner,
            observerSliderResponse
        )


        val observerPromoResponse = Observer<PromoResponse> {
            it?.let {
                onGetPromoResponse(it)
            }
        }
        (activity as MainHomeActivity).model.currentPromoResponse.observe(
            viewLifecycleOwner,
            observerPromoResponse
        )

        val observerPartnerListResponse = Observer<PartnerListResponse> {
            it?.let {
                onGetPartnerResponse(it)
            }
        }
        (activity as MainHomeActivity).model.currentPartnerResponse.observe(
            viewLifecycleOwner,
            observerPartnerListResponse
        )

        val observerCashValueResponse = Observer<CashValueResponse> {
            it?.let {
                tv_cash_value.text = it.totalCashValue?.toLong()?.convertToMoneyFormat()
            }
        }
        (activity as MainHomeActivity).model.currentCashValue.observe(
            viewLifecycleOwner,
            observerCashValueResponse
        )

        btn_reg_agent?.setOnClickListener {

            if ((preferencesUtil?.getUser()?.idCustomer ?: 0) == 0) {
                startActivityForResult(
                    Intent(mContext, LoginActivity::class.java),
                    LoginActivity.REQUEST_LOGIN
                )
            } else {
                if ((preferencesUtil?.getAgentCode()?.agentCode ?: "") != "") {
                    if ((preferencesUtil?.getAgentCode()?.agentCode ?: "") == "000") {
                        alertDialogCustom.getView().tv_intro_text?.text =
                            "Pendaftaran Agent Anda Sedang di Proses"
                        alertDialogCustom.show()
                    } else {
                        alertDialogCustom.getView().tv_intro_text?.text =
                            "Anda Sudah Terdaftar Sebagai Agent Andalanku"
                        alertDialogCustom.show()
                    }
                } else {
                    val intent = Intent(context, AgentRegisterActivity::class.java)
                    intent.putExtra(UpdatePersonalInfoActivity.IS_ACCOUNT_SETTING, true)
                    startActivityForResult(intent, MyAccountFragment.REQ_CODE_UPDATED_INFO)
                }
            }

        }

        swipe_layout?.setOnRefreshListener {
            presenter.onCreate()
        }

        if ((preferencesUtil!!.getFirstOpenTips()) == true) {
            showSubmissionTips()
        }

        btn_isi_ulang_ponsel?.setOnClickListener {
            val intent = Intent(mContext, PpobPulsaActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "pulsa")
            startActivity(intent)
        }

        btn_listrik_pln?.setOnClickListener {
            val intent = Intent(mContext, PpobPlnActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "prabayar")
            startActivity(intent)
        }

        btn_air_pdam?.setOnClickListener {
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(mContext, activityClass)
            intent.putExtra("category", "pdam")
            intent.putExtra("title", "PDAM")
            intent.putExtra("acc_number_label", "Nomor Pelanggan")
            startActivity(intent)
        }

    }

    private fun resetShowCase() {
        this.mFancyShowCaseView.hide()
        //this.mFancyShowCaseView = null
    }

    private fun showSubmissionTips() {
        if (isAdded) {
            this.mFancyShowCaseView = FancyShowCaseView.Builder(this.requireActivity())
                .focusOn(this.requireView().findViewById<View>(R.id.btn_submission_home_shadow))
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(10)
                .focusCircleRadiusFactor(1.5)
                .backgroundColor(Color.parseColor("#AA0F0F0F"))
                .dismissListener(object : DismissListener {
                    override fun onDismiss(id: String?) {
                        showServicesTips()
                    }

                    override fun onSkipped(id: String?) {
                        TODO("Not yet implemented")
                    }
                })
                .build()

            this.mFancyShowCaseView.show()
            this.submissionBalloon?.showAlignTop(
                this.requireView().findViewById<View>(R.id.btn_submission_home)
            )

            val buttonEdit: Button =
                this.submissionBalloon?.getContentView()!!.findViewById(R.id.button_edit)
            buttonEdit.setOnClickListener {
                this.submissionBalloon!!.dismiss()
                resetShowCase()
            }
        }
    }

    private fun showServicesTips() {
        //resetShowCase()
        if (isAdded) {
            this.mFancyShowCaseView = FancyShowCaseView.Builder(this.requireActivity())
                .focusOn(this.requireView().findViewById<View>(R.id.btn_services_shadow))
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(10)
                .focusCircleRadiusFactor(1.5)
                .backgroundColor(Color.parseColor("#AA0F0F0F"))
                .dismissListener(object : DismissListener {
                    override fun onDismiss(id: String?) {
                        showSimulationTips()
                    }

                    override fun onSkipped(id: String?) {
                        TODO("Not yet implemented")
                    }
                })
                .build()

            this.mFancyShowCaseView.show()
            this.servicesBalloon?.showAlignBottom(
                this.requireView().findViewById<View>(R.id.btn_services)
            )

            val buttonEdit: Button =
                this.servicesBalloon?.getContentView()!!.findViewById(R.id.button_edit)
            buttonEdit.setOnClickListener {
                this.servicesBalloon!!.dismiss()
                resetShowCase()
            }
        }
    }

    private fun showSimulationTips() {
        //resetShowCase()
        if (isAdded) {
            this.mFancyShowCaseView = FancyShowCaseView.Builder(this.requireActivity())
                .focusOn(this.requireView().findViewById<View>(R.id.btn_credit_simulation_shadow))
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(10)
                .focusCircleRadiusFactor(1.5)
                .backgroundColor(Color.parseColor("#AA0F0F0F"))
                .dismissListener(object : DismissListener {
                    override fun onDismiss(id: String?) {
                        showCashValueTips()
                    }

                    override fun onSkipped(id: String?) {
                        TODO("Not yet implemented")
                    }
                })
                .build()

            this.mFancyShowCaseView.show()
            this.simulationBalloon?.showAlignTop(
                this.requireView().findViewById<View>(R.id.btn_credit_simulation)
            )

            val buttonEdit: Button =
                this.simulationBalloon?.getContentView()!!.findViewById(R.id.button_edit)
            buttonEdit.setOnClickListener {
                this.simulationBalloon!!.dismiss()
                resetShowCase()
            }
        }
    }

    private fun showCashValueTips() {
        //resetShowCase()
        if (isAdded) {
            this.mFancyShowCaseView = FancyShowCaseView.Builder(this.requireActivity())
                .focusOn(this.requireView().findViewById<View>(R.id.container_cash_value_shadow))
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(10)
                .focusCircleRadiusFactor(1.5)
                .backgroundColor(Color.parseColor("#AA0F0F0F"))
                .dismissListener(object : DismissListener {
                    override fun onDismiss(id: String?) {
                        showScheduleTips()
                    }

                    override fun onSkipped(id: String?) {
                        TODO("Not yet implemented")
                    }
                })
                .build()

            this.mFancyShowCaseView.show()
            this.cashValueTipsBalloonFactory?.showAlignTop(
                this.requireView().findViewById<View>(R.id.container_cash_value)
            )

            val buttonEdit: Button =
                this.cashValueTipsBalloonFactory?.getContentView()!!.findViewById(R.id.button_edit)
            buttonEdit.setOnClickListener {
                this.cashValueTipsBalloonFactory!!.dismiss()
                resetShowCase()
            }
        }
    }

    private fun showScheduleTips() {
        //resetShowCase()
        if (isAdded) {
            this.mFancyShowCaseView = FancyShowCaseView.Builder(this.requireActivity())
                .focusOn(this.requireView().findViewById<View>(R.id.btn_schedule_shadow))
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(10)
                .focusCircleRadiusFactor(1.5)
                .backgroundColor(Color.parseColor("#AA0F0F0F"))
                .dismissListener(object : DismissListener {
                    override fun onDismiss(id: String?) {
                        showBannerTips()
                    }

                    override fun onSkipped(id: String?) {
                        TODO("Not yet implemented")
                    }
                })
                .build()

            this.mFancyShowCaseView.show()
            this.scheduleTipsBalloonFactory?.showAlignTop(
                this.requireView().findViewById<View>(R.id.btn_schedule)
            )

            val buttonEdit: Button =
                this.scheduleTipsBalloonFactory?.getContentView()!!.findViewById(R.id.button_edit)
            buttonEdit.setOnClickListener {
                this.scheduleTipsBalloonFactory!!.dismiss()
                resetShowCase()
            }
        }
    }

    private fun showBannerTips() {
        //resetShowCase()
        if (isAdded) {
            this.mFancyShowCaseView = FancyShowCaseView.Builder(this.requireActivity())
                .focusOn(this.requireView().findViewById<View>(R.id.banner_shadow))
                .focusShape(FocusShape.ROUNDED_RECTANGLE)
                .roundRectRadius(10)
                .focusCircleRadiusFactor(1.5)
                .backgroundColor(Color.parseColor("#AA0F0F0F"))
                .build()

            this.mFancyShowCaseView.show()
            this.bannerTipsBalloonFactory?.showAlignBottom(
                this.requireView().findViewById<View>(R.id.banner_shadow2)
            )

            val buttonEdit: Button =
                this.bannerTipsBalloonFactory?.getContentView()!!.findViewById(R.id.button_edit)
            buttonEdit.setOnClickListener {
                this.bannerTipsBalloonFactory!!.dismiss()
                this.mFancyShowCaseView.hide()
                preferencesUtil!!.putFirstOpenTips(false)
            }
        }
    }

    private fun initMenu() {
        val layoutManager =
            androidx.recyclerview.widget.GridLayoutManager(mContext, 3)
        rv_menu_submission?.layoutManager = layoutManager
        rv_menu_submission?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu_submission?.adapter = menuAdapterHome
        rv_menu_submission?.isNestedScrollingEnabled = true
    }

    private fun showMenuServices() {
        tv_title_text?.text = mContext?.getString(R.string.e_services_andalanku)
        menuAdapterHome.removeAll()
        menuAdapterHome.add(
            Menu(
                name = mContext?.getString(R.string.e_complain),
                image = R.drawable.ic_complain
            )
        )
        menuAdapterHome.add(
            Menu(
                name = mContext?.getString(R.string.e_claim_asurance),
                image = R.drawable.ic_claim_insurance
            )
        )
        menuAdapterHome.add(
            Menu(
                name = mContext?.getString(R.string.e_request_guarantee),
                image = R.drawable.ic_request_take_guarantee
            )
        )
        menuAdapterHome.notifyDataSetChanged()
    }

    private fun showMenuSubmission(title: String) {
        tv_title_text?.text = title
        menuAdapterHome.removeAll()
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.new_cars),
                image = R.drawable.ic_new_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.used_car),
                image = R.drawable.ic_used_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.wallet_andalanku),
                image = R.drawable.ic_dana_andalanku
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.e_commerce),
                image = R.drawable.ic_ecommerce
            )
        )
        menuAdapterHome.notifyDataSetChanged()
    }

    private fun initNewsHome() {
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            mContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        rv_news?.layoutManager = layoutManager
        rv_news?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_news?.adapter = newsHomeAdapter
    }

    private fun initPartnerHome() {
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            mContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        rv_partner?.layoutManager = layoutManager
        rv_partner?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_partner?.adapter = partnerHomeAdapter
    }

    private fun initPromoHome() {
        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            mContext,
            androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
            false
        )
        rv_promo_home?.layoutManager = layoutManager
        rv_promo_home?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_promo_home?.adapter = promoHomeAdapter
        rv_promo_home?.addOnScrollListener(object : PositionOnScrollListener(layoutManager) {
            override fun currentPosition(position: Int, isToLeft: Boolean, isToRight: Boolean) {
                imageIndicator[this@HomeFragment.position]?.isEnabled = true

                this@HomeFragment.position =
                    if ((this@HomeFragment.position <= 1 && isToLeft) || imageIndicator.size == 1) {
                        0
                    } else {
                        position + 1
                    }

                imageIndicator[this@HomeFragment.position]?.isEnabled = false
            }
        })
        val snapHelper = androidx.recyclerview.widget.PagerSnapHelper()
        snapHelper.attachToRecyclerView(rv_promo_home)
    }

    private fun generateIndicator(totalIndicators: Int) {
        imageIndicator = arrayOfNulls(totalIndicators)
        val lp = LinearLayout.LayoutParams(
            resources.getDimension(R.dimen.width_indicator).toInt(),
            resources.getDimension(R.dimen.height_indicator).toInt()
        )
        lp.setMargins(5, 5, 5, 5)
        line_indicator?.removeAllViews()
        for (i in 0 until totalIndicators) {
            imageIndicator[i] = ImageView(mContext)
            imageIndicator[i]?.layoutParams = lp
            imageIndicator[i]?.setImageDrawable(
                ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.selector,
                    null
                )
            )
            line_indicator?.addView(imageIndicator[i])
        }
        imageIndicator[this.position]?.isEnabled = false
    }

    override fun onGetNewsResponse(newsResponse: NewsResponse) {
        if (newsResponse.status == ConfigVar.SUCCESS) {
            newsHomeAdapter.removeAll()
            newsHomeAdapter.add(newsResponse.newsList ?: arrayListOf())
        }
    }

    override fun onGetSliderResponse(sliderResponse: SliderResponse) {
        if (sliderResponse.status == ConfigVar.SUCCESS) {
            var sliders = sliderResponse.sliderList
            if (!sliders.isNullOrEmpty()) {
                for (i in 0 until sliders.size) {
                    sliders[i].image?.let { slideImages.add(it) }
                    sliders[i].promoSlug?.let { slidePromoSlug.add(it) }
                }
            }
        }
    }

    override fun onRemoveWaitGetNews() {
        //swipe_layout.isRefreshing = false
    }

    override fun onRemoveWaitGetSlider() {
        //swipe_layout.isRefreshing = false
    }

    override fun onErrorGetNews(message: String) {
        //swipe_layout.isRefreshing = false
        e(TAG, message)
    }

    override fun onErrorGetSlider(message: String) {
        //swipe_layout.isRefreshing = false
        e(TAG, message)
    }

    override fun onGetPromoResponse(promoResponse: PromoResponse?) {
        if (promoResponse != null) {
            if (promoResponse.status == ConfigVar.SUCCESS) {
                if (promoResponse.promoList.isNullOrEmpty()) {
                    //empty response
                } else {
                    promoHomeAdapter.removeAll()
                    promoHomeAdapter.add(promoResponse.promoList)
                    generateIndicator(promoResponse.promoList.size)
                }
            }
        }
    }

    override fun onGetPartnerResponse(partnerListResponse: PartnerListResponse) {
        if (partnerListResponse.status == ConfigVar.SUCCESS) {
            partnerHomeAdapter.removeAll()
            partnerHomeAdapter.add(partnerListResponse.partnerList ?: arrayListOf())
            partnerHomeAdapter.notifyDataSetChanged()
        }
    }

    override fun onRemoveWaitGetPromo() {
        //swipe_layout.isRefreshing = false
    }

    override fun onRemoveWaitGetPartner() {
        if (isAdded && swipe_layout != null) {
            swipe_layout.isRefreshing = false
        }
    }

    override fun onErrorGetPromo(message: String) {
        //swipe_layout.isRefreshing = false
        e(TAG, message)
    }

    override fun showWait() {
        swipe_layout.isRefreshing = true
    }

    override fun removeWait() {
        //swipe_layout.isRefreshing = false
    }

    override fun onFailure(appErrorMessage: String?) {
        //swipe_layout.isRefreshing = false
    }

    private fun hideSlider() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        hideSlider()
    }

    override fun onGetAgent(agentResponse: AgentResponse) {
        preferencesUtil?.putAgentCode(agentResponse)
    }

    fun androidx.fragment.app.Fragment.hideKeyboard() {
        view?.let { activity?.hideKeyboard(it) }
    }

    fun Activity.hideKeyboard() {
        hideKeyboard(currentFocus ?: View(this))
    }

    fun Context.hideKeyboard(view: View) {
        val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }
}
