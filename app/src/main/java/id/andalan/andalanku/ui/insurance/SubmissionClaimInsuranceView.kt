package id.andalan.andalanku.ui.insurance

import id.andalan.andalanku.model.response.InsuranceClaimResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface SubmissionClaimInsuranceView: BaseView {
    fun onGetResponseClaimInsurance(insuranceClaimResponse: InsuranceClaimResponse)
    fun onGetContractNumber(list: MutableList<PickerItem>)
    fun onGetCategoryInsurance(list: MutableList<PickerItem>)
    fun onGetEventCase(list: MutableList<PickerItem>)
}