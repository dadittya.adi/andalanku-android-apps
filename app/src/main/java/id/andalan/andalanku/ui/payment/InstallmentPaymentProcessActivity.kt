package id.andalan.andalanku.ui.payment

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.agent.AgentRegisterTncActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.activity_installment_payment_process.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class InstallmentPaymentProcessActivity : BaseApp() {

    companion object {
        val TRANSACTION_URL = "transaction_url"
        var PAY_PINALTY = "pay_pinalty"
    }

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertDialogCustom: AlertDialogCustom
    private var agreementNumber = ""
    @Inject
    lateinit var presenter: InstallmentPaymentDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installment_payment_process)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Pembayaran Angsuran"

        if (!intent.hasExtra(TRANSACTION_URL)) {
            finish()
        }

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(  false)
        alertDialogProgress.dismiss()

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().tv_intro_text?.text = "Terima Kasih. Segera Lakukan Pembayaran Sebelum Waktu Habis"
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            intent.putExtra(AgentRegisterTncActivity.HOME, true)
            val intent = Intent(this, MainHomeActivity::class.java)
            startActivity(intent)
            finishAffinity()
        }

        var url = intent.getStringExtra(TRANSACTION_URL)
        var pinalty = intent.getStringExtra(PAY_PINALTY)

        url = url + "&penalty_pay=" + pinalty

        web_view.settings.javaScriptEnabled = true
        web_view.webViewClient = WebViewClient()
        web_view.loadUrl(url)
        web_view.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request?.url.toString())
                return super.shouldOverrideUrlLoading(view, request)
            }

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                val endpoint = url?.takeLast(14)

                if (endpoint == "payment_finish") {
                    alertDialogCustom.show()
                }
                alertDialogProgress.show()
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                alertDialogProgress.dismiss()

                super.onPageFinished(view, url)
            }

        }
    }
}