package id.andalan.andalanku.ui.repayment

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.AgreementListResponse
import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.model.response.PrePaymentSimulationResponse
import id.andalan.andalanku.model.response.PrepaymentRequest
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class RepaymentDetailPresenter @Inject constructor(private val service: Services): BasePresenter<RepaymentDetailView>() {
    override fun onCreate() {
    }

    fun sendRequestPrepayment(agreementNumber: String, prePaymentDate: String) {
        view.showWait()
        val subscription = service.getPrePaymentRequest(agreementNumber, prePaymentDate).subscribe(object : Subscriber<PrepaymentRequest>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(prepaymentRequest: PrepaymentRequest) {
                view.getResponsePrepayment(prepaymentRequest)
            }
        })
        subscriptions.add(subscription)
    }

    fun getDetailPrepaymentSimulation(agreementNumber: String, prePaymentDate: String, isFirst: Boolean) {
        view.showWait()
        val subscription = service.getPrePaymentSimulation(agreementNumber, prePaymentDate).subscribe(object : Subscriber<PrePaymentSimulationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(prePaymentSimulationResponse: PrePaymentSimulationResponse) {
                view.getResponsePrepaymentSimulation(prePaymentSimulationResponse, isFirst)
            }
        })
        subscriptions.add(subscription)
    }

    fun getDetailFinancial(agreementNo: String) {
        view.showWait()
        val subscription = service.getDetailFinancial(agreementNo).subscribe(object : Subscriber<DetailFinanceResponse>() {
            override fun onCompleted() {
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailFinanceResponse: DetailFinanceResponse) {
                view.onGetDetailContract(detailFinanceResponse)
                getDetailPrepaymentSimulation(agreementNo, detailFinanceResponse.getMaturityDateReq(), true)
            }
        })
        subscriptions.add(subscription)
    }

    fun getAggreementNumber() {
        view.showWait()
        val subscription = service.getAgreementList().subscribe(object : Subscriber<AgreementListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(agreementListResponse: AgreementListResponse) {
                if (agreementListResponse.status == ConfigVar.SUCCESS) {
                    val list: MutableList<PickerItem> = arrayListOf()
                    agreementListResponse.agreementList?.let {
                        it.forEach {item ->
                            list.add(PickerItem(name = item.agreementNo, contract_status = item.contract_status, isChoosed = false))
                        }
                    }
                    view.onGetContractNumber(list)
                }
            }
        })
        subscriptions.add(subscription)
    }
}