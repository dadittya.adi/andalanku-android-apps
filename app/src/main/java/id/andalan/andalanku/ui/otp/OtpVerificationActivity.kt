package id.andalan.andalanku.ui.otp

import android.content.Intent
import android.os.Bundle
import android.util.Log.e
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_AGGREMENT_LIST
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_INBOX
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_PERSONAL_INFO
import id.andalan.andalanku.model.response.LoginResponse
import id.andalan.andalanku.model.response.OtpVerificationResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.pin.RecreatePinActivity
import id.andalan.andalanku.ui.register.SuccessRegisterActivity
import id.andalan.andalanku.utils.AlertDialogUtil
import kotlinx.android.synthetic.main.activity_otp_verification.*
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject

class OtpVerificationActivity : BaseApp(), OtpVerificationView {
    companion object {
        val IS_FORGOT_PIN = "is_forgot_pin"
        val REQUEST_VERIFICATION = 411
        val RESPONSE_VERIFICATION = 412
    }
    private val TAG = OtpVerificationActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: OtpVerificationPresenter

    private var otp = ""
    private var isForgotPin = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)

        isForgotPin = intent?.extras?.getBoolean(IS_FORGOT_PIN, false)?: false

        deps.inject(this)
        presenter.view = this

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = if (isForgotPin) getString(R.string.lupa_pin) else getString(R.string.verification_account)

        otp_view.setOtpCompletionListener {
            otp = it
        }

//        val no_hp = preferencesUtil.getUser().
//        tv_info_text?.text = "SMS berupa kode verifikasi baru saja dikirimkan ke no "

        btn_send.setOnClickListener {
            if (otp.isNotBlank()) {
                if (isForgotPin) {
                    startActivity(Intent(this@OtpVerificationActivity, RecreatePinActivity::class.java))
                } else {
                    presenter.doVerificationOtp(otp)
                }
            } else {
                AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                    override fun onClickCancel() {
                    }
                    override fun onClickOk() {
                    }
                }, this, getString(R.string.security_account), getString(R.string.form_otp_is_blank))
            }
        }
    }

    override fun getProfileInformation(personalInformationResponse: PersonalInformationResponse) {
        preferencesUtil.putProfile(personalInformationResponse)
    }

    override fun onGetResponse(otpVerificationResponse: OtpVerificationResponse) {
        if (otpVerificationResponse.status == ConfigVar.SUCCESS) {
            val user = preferencesUtil.getUser()
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_PERSONAL_INFO))
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_AGGREMENT_LIST))
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_INBOX))
            user?.isVerify = true
            preferencesUtil.putUser(user?: LoginResponse())
            finish()
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }

                override fun onClickOk() {
                }

            }, this, getString(R.string.security_account), otpVerificationResponse.message?:"")
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
