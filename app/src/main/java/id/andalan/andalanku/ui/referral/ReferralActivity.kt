package id.andalan.andalanku.ui.referral

import android.content.Context
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Faq
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.FaqAnswerAdapter
import kotlinx.android.synthetic.main.activity_referral.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class ReferralActivity : BaseApp() {

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referral)

        deps.inject(this)

        val user = preferencesUtil.getProfile()
        tv_code?.text = user?.referralCode
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.kode_referral_andalanku)

        val adapterFitur = FaqAnswerAdapter()
        adapterFitur.add(Faq(description = "Buka Aplikasi Andalanku, login dengan akun yang anda buat."))
        adapterFitur.add(Faq(description = "Masuk ke tab My Account"))
        //adapterFitur.add(Faq(description = "Klik “Kode Referral Andalanku”"))
        adapterFitur.add(Faq(description = "Klik tombol “COPY” untuk menyalin kode referral Andalanku dan bagikan ke teman, kerabat dan keluarga untuk Mendaftar di aplikasi ANDALANKU! dan melakukan pengajuan pembiayaan di AFI."))
        rv_fitur.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_fitur.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_fitur.adapter = adapterFitur

        val adapter = FaqAnswerAdapter()
        adapter.add(Faq(description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse elementum sem nec maximus laoreet. Donec ornare arcu nec turpis interdum consequat. Ut pharetra semper facilisis. Fusce porta, velit at rutrum iaculis, nunc velit convallis nisi, at dictum justo libero eget massa. Nulla sed faucibus libero."))
        adapter.add(Faq(description = "Aliquam erat volutpat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Suspendisse in eros egestas, sodales nisl tempor, pharetra mauris."))
        adapter.add(Faq(description = "Suspendisse accumsan nec risus vitae tempor. Proin sit amet vulputate mi, at accumsan urna."))
        adapter.add(Faq(description = "Morbi vel luctus ligula, vitae tempus ligula. Cras et varius ante. Proin placerat mi et viverra finibus. Proin efficitur mauris ut finibus eleifend. Phasellus ut tortor porta,"))
        rv_term.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        rv_term.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        rv_term.adapter = adapter

        btn_copy?.setOnClickListener {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
            val clip = android.content.ClipData.newPlainText("text label", tv_code.text.toString())
            clipboard.primaryClip = clip
            Toast.makeText(this, "Code Referral berhasil di copy", Toast.LENGTH_SHORT).show()
        }
    }
}
