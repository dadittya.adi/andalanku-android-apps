package id.andalan.andalanku.ui.installments

import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.model.response.PrePaymentSimulationResponse
import id.andalan.andalanku.model.response.PrepaymentRequest
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface InstallmentsScheduleView: BaseView {
    fun getResponseInstallment(detailPaymentHistoryResponse: DetailPaymentHistoryResponse)
    fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse)
    fun getResponsePrepaymentSimulation(prePaymentSimulationResponse: PrePaymentSimulationResponse)
    fun onGetContractNumber(list: MutableList<PickerItem>)
}