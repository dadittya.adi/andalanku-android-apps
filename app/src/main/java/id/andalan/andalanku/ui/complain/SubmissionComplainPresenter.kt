package id.andalan.andalanku.ui.complain

import id.andalan.andalanku.model.request.InputComplainRequest
import id.andalan.andalanku.model.response.CategoryComplainResponse
import id.andalan.andalanku.model.response.InputComplainResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import rx.Subscriber
import javax.inject.Inject

class SubmissionComplainPresenter @Inject constructor(private val service: Services): BasePresenter<SubmissionComplainView>() {
    override fun onCreate() {
        getListComplain()
    }

    private fun getListComplain() {
        view.showWait()
        val subscription = service.getCategoryComplain()
            .subscribe(object : Subscriber<CategoryComplainResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(categoryComplainResponse: CategoryComplainResponse) {
                    view.onGetCategoryList(categoryComplainResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun sendComplain(request: InputComplainRequest, media: List<String>) {
        view.showWait()
        val subscription = service.sendComplain(request, media)
            .subscribe(object : Subscriber<InputComplainResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(inputComplainResponse: InputComplainResponse) {
                    view.onSuccessSendComplain(inputComplainResponse)
                }
            })
        subscriptions.add(subscription)
    }
}