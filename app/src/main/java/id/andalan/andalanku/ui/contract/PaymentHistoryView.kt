package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.ui.base.BaseView

interface PaymentHistoryView: BaseView {
    fun getPaymentHistory(detailPaymentHistoryResponse: DetailPaymentHistoryResponse)
}