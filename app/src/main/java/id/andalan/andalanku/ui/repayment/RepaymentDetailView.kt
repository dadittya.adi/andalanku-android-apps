package id.andalan.andalanku.ui.repayment

import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.model.response.PrePaymentSimulationResponse
import id.andalan.andalanku.model.response.PrepaymentRequest
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface RepaymentDetailView: BaseView {
    fun getResponsePrepayment(prepaymentRequest: PrepaymentRequest)
    fun getResponsePrepaymentSimulation(prePaymentSimulationResponse: PrePaymentSimulationResponse, isFirst: Boolean)
    fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse)
    fun onGetContractNumber(list: MutableList<PickerItem>)
}