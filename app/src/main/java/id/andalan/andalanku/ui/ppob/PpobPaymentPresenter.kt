package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PpobPaymentPresenter @Inject constructor(private val service: Services): BasePresenter<PpobPaymentView>(){
    override fun onCreate() {
        TODO("Not yet implemented")
    }

    fun ppobinquiry(accountNumber: String, productCode: String, zoneId: String, month: String) {
        view.showWait()
        val subscription = service.PpobInquiry(accountNumber, productCode, zoneId, month).subscribe(object : Subscriber<PpobInquiryResponse>() {
            override fun onCompleted() {
                view.showWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(ppobInquiryResponse: PpobInquiryResponse) {
                view.onPpobInquiryResponse(ppobInquiryResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }

}