@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.News
//import id.andalan.andalanku.model.ui.NewsHome
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_news_home.view.*
import org.jsoup.Jsoup

class NewsHomeAdapter(val onItemClickListener: OnClickItem): BaseAdapter<News, NewsHomeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_news_home
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<News>(itemView) {
        override fun bind(news: News) {
            val data = news.getNewsHome()
            itemView.btn_read.setOnClickListener {
                onItemClickListener.onClickRead(news)
            }
            try {
                loadImage(itemView.context, data.urlImage?:"", itemView.iv_news_image)
            } catch (err: Exception) {

            }
            itemView.tv_news_title?.text = data.title
            itemView.tv_news_summary?.text = Jsoup.parse(data.subTitle).text()

        }

    }

    interface OnClickItem {
        fun onClickRead(news: News)
    }
}