package id.andalan.andalanku.ui.ppob

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobGroupResponse
import id.andalan.andalanku.model.ui.PpobGroup
import id.andalan.andalanku.ui.adapters.PpobGroupListAdapter
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.activity_ppob_category_list.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class PpobCategoryListActivity : BaseApp(), PpobCategoryListView {

    @Inject
    lateinit var presenter: PpobCategoryListPresenter

    lateinit var ppobGroupListAdapter: PpobGroupListAdapter

    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_category_list)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Semua Layanan Andalanku"
        layout?.setBackgroundColor(ContextCompat.getColor(this, R.color.snow))
        btn_add?.visibility = View.INVISIBLE

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(  false)
        alertDialogProgress.dismiss()

        radio_group_category.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.radio_all -> categoryFilter("all")
                R.id.radio_last_purchase -> categoryFilter("last_purchase")
                R.id.radio_top_up -> categoryFilter("top_up")
                R.id.radio_tagihan -> categoryFilter("tagihan")
                R.id.radio_hiburan -> categoryFilter("hiburan")
                else -> categoryFilter("all")
            }
        }

        btn_pulsa?.setOnClickListener {
            val intent = Intent(this, PpobPulsaActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "pulsa")
            startActivity(intent)
        }

        btn_paket_data?.setOnClickListener {
            val intent = Intent(this, PpobPulsaActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "data")
            startActivity(intent)
        }

        btn_pasca_bayar?.setOnClickListener {
            val intent = Intent(this, PpobPostpaidActivity::class.java)
            startActivity(intent)
        }

        btn_listrik_pln?.setOnClickListener {
            val intent = Intent(this, PpobPlnActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "pascabayar")
            startActivity(intent)
        }

        btn_token_listrik?.setOnClickListener {
            val intent = Intent(this, PpobPlnActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "prabayar")
            startActivity(intent)
        }


        btn_gas_pgn?.setOnClickListener {
            val intent = Intent(this, PpobPgnActivity::class.java)
            startActivity(intent)
        }

        btn_telkom?.setOnClickListener {
            val intent = Intent(this, PpobTelkomActivity::class.java)
            startActivity(intent)
        }

        btn_air_pdam?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "pdam")
            intent.putExtra("title", "PDAM")
            intent.putExtra("acc_number_label", "Nomor Pelanggan")
            startActivity(intent)
        }

        btn_kartu_kredit?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobCreditCardActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "Kartu Kredit")
            intent.putExtra("title", "Kartu Kredit")
            intent.putExtra("acc_number_label", "Nomor Kartu Kredit")

            startActivity(intent)
        }

        btn_angsuran_kredit?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "Pembayaran Angsuran")
            intent.putExtra("title", "Angsuran Kredit")
            intent.putExtra("acc_number_label", "Nomor Kontrak")
            startActivity(intent)
        }

        btn_internet_tv?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "Internet dan TV Kabel")
            intent.putExtra("title", "Internet dan TV Kabel")
            intent.putExtra("acc_number_label", "ID Pelanggan")
            startActivity(intent)
        }

        btn_emoney?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "emoney")
            intent.putExtra("title", "Top Up e-Money")
            intent.putExtra("acc_number_label", "")
            startActivity(intent)
        }

        btn_streaming?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "streaming 1")
            intent.putExtra("title", "Streaming")
            intent.putExtra("acc_number_label", "")
            startActivity(intent)
        }

        btn_digital_voucher?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "voucher digital")
            intent.putExtra("title", "Digital Voucher")
            intent.putExtra("acc_number_label", "")
            startActivity(intent)
        }

        btn_game_voucher?.setOnClickListener{
            val activityClass = Class.forName("id.andalan.andalanku.ui.ppob.PpobGeneralActivity")

            val intent = Intent(this, activityClass)
            intent.putExtra("category", "voucher game")
            intent.putExtra("title", "Game Voucher")
            intent.putExtra("acc_number_label", "")
            startActivity(intent)
        }

        btn_bpjs?.setOnClickListener {
            val intent = Intent(this, PpobBpjsActivity::class.java)
            startActivity(intent)
        }

        presenter.view = this
        presenter.getLastPurchase()
        ppobGroupListAdapter = PpobGroupListAdapter(object : PpobGroupListAdapter.OnClickItem {
            override fun onClickMenu(ppobGroup: PpobGroup) {
                val activityClass = ppobGroup.className?.let { Class.forName(it) }

                val intent = Intent(this@PpobCategoryListActivity, activityClass)
                intent.putExtra("category", ppobGroup.extra1)
                intent.putExtra("title", ppobGroup.title)
                intent.putExtra("acc_number_label", ppobGroup.extra2)
                startActivity(intent)
            }

        })

    }

    private fun categoryFilter(category: String) {
        when (category) {
            "last_purchase" -> {
                container_last_purchase.visibility = View.VISIBLE
                container_top_up.visibility = View.GONE
                container_tagihan.visibility = View.GONE
                container_hiburan.visibility = View.GONE
            }
            "top_up" -> {
                container_last_purchase.visibility = View.GONE
                container_top_up.visibility = View.VISIBLE
                container_tagihan.visibility = View.GONE
                container_hiburan.visibility = View.GONE
            }
            "tagihan" -> {
                container_last_purchase.visibility = View.GONE
                container_top_up.visibility = View.GONE
                container_tagihan.visibility = View.VISIBLE
                container_hiburan.visibility = View.GONE
            }
            "hiburan" -> {
                container_last_purchase.visibility = View.GONE
                container_top_up.visibility = View.GONE
                container_tagihan.visibility = View.GONE
                container_hiburan.visibility = View.VISIBLE
            }
            "all" -> {
                container_last_purchase.visibility = View.VISIBLE
                container_top_up.visibility = View.VISIBLE
                container_tagihan.visibility = View.VISIBLE
                container_hiburan.visibility = View.VISIBLE
            }
        }
    }

    override fun onGetLastPurchaseResponse(ppobGroupResponse: PpobGroupResponse) {
        if (ppobGroupResponse.status == ConfigVar.SUCCESS) {
            if (ppobGroupResponse.lastPurchase?.size ?: 0 == 4) {
                ppobGroupListAdapter.removeAll()
                ppobGroupResponse.lastPurchase?.let { ppobGroupListAdapter.add(it) }

                rv_ppob_category_list_last_purchase.apply {
                    layoutManager = GridLayoutManager(this@PpobCategoryListActivity, 4)
                    adapter = ppobGroupListAdapter
                }
            } else {

            }
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this@PpobCategoryListActivity, appErrorMessage, Toast.LENGTH_LONG).show()
    }


}