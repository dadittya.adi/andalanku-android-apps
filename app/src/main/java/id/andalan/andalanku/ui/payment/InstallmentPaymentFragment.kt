package id.andalan.andalanku.ui.payment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import kotlinx.android.synthetic.main.fragment_installment_payment.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*

class InstallmentPaymentFragment : androidx.fragment.app.Fragment(), InstallmentPaymentView {
    private val TAG = InstallmentPaymentFragment::class.java.simpleName

    private lateinit var presenter: InstallmentPaymentPresenter
    private lateinit var agreementNumber: String
    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_installment_payment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.pembayaran_angsuran)

        alertDialogProgress = AlertDialogCustom(context!!, null)
        alertDialogProgress.setView(LayoutInflater.from(context!!).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(  false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(context!!, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter = InstallmentPaymentPresenter((activity as MainHomeActivity).services)
        presenter.view = this

        presenter.onCreate()

        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
        et_choosed_contract_number.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                et_choosed_contract_number?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)

                agreementNumber = choosedPicker.name?:""

               // presenter.getPaymentDetail(agreementNumber)
                val intent = Intent(context, InstallmentPaymentDetailActivity::class.java)
                intent.putExtra(InstallmentPaymentDetailActivity.AGREEMENT_NUMBER, agreementNumber)
                startActivity(intent)

                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerAdapter.add(list)
            container_empty_state?.visibility = View.GONE
        } else {
            container_empty_state?.visibility = View.VISIBLE
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state?.visibility = View.VISIBLE
        Toast.makeText(context, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }

    override fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse) {
        if (paymentInquiryResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(context, InstallmentPaymentDetailActivity::class.java)
                intent.putExtra(InstallmentPaymentDetailActivity.AGREEMENT_NUMBER, agreementNumber)
                startActivity(intent)
        }
        else {

            //alertWarningCustom.getView().tv_t?.text = "Peringatan"
            alertWarningCustom.getView().tv_message?.text = paymentInquiryResponse.message
            alertWarningCustom.show()
        }
    }
}
