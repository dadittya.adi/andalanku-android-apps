package id.andalan.andalanku.ui.home

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class MainHomePresenter @Inject constructor(private val service: Services): BasePresenter<MainHomeView>()  {
    override fun onCreate() {
        getListSlider()
       //getListNews()
    }

    fun getPersonalInfo() {
        view.showWait()
        val subscription = service.getPersonalInfo().subscribe(object : Subscriber<PersonalInformationResponse>() {
            override fun onCompleted() {
                view.showWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(personalInformationResponse: PersonalInformationResponse) {
                view.getProfileInformation(personalInformationResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }

    fun getAgentCode() {
        view.showWait()
        val subscription = service.getAgentCode().subscribe(object : Subscriber<AgentResponse>() {
            override fun onCompleted() {
                view.showWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(agentResponse: AgentResponse) {
                view.onGetAgent(agentResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }

    fun getLegalDocument() {
        view.showWait()
        val subscription = service.getLegalDocument().subscribe(object : Subscriber<LegalDocumentResponse>() {
            override fun onCompleted() {
                view.showWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(t: LegalDocumentResponse) {
                view.onGetLegalDocument(t)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }

    fun getTokenNumber() {
        view.showWait()
        val subscription = service.getTokenNumber().subscribe(object : Subscriber<TokenResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(tokenResponse: TokenResponse) {
                view.onGetToken(tokenResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getAgreementNumber() {
        view.showWait()
        val subscription = service.getAgreementList().subscribe(object : Subscriber<AgreementListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(agreementListResponse: AgreementListResponse) {
                if (agreementListResponse.status == ConfigVar.SUCCESS) {
                    val list: MutableList<PickerItem> = arrayListOf()
                    agreementListResponse.agreementList?.let {
                        it.forEach {item ->
                            val pickerItem = PickerItem(name = item.agreementNo, contract_status = item.contract_status, isChoosed = false)
                            pickerItem.realObject = it
                            list.add(PickerItem(name = item.agreementNo, contract_status = item.contract_status, isChoosed = false))
                        }
                    }
                    view.onGetContractNumber(list)
                } else {
                    getAgreementNumber()
                }
            }
        })
        subscriptions.add(subscription)
    }

    fun getListNews() {
        view.showWait()
        val subscription = service.getNews(1, "").subscribe(object : Subscriber<NewsResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(newsResponse: NewsResponse) {
                view.onGetNewsResponse(newsResponse)
                getListPromo()
            }
        })
        subscriptions.add(subscription)
    }

    fun getListPromo() {
        view.showWait()
        val subscription = service.getPromo(1, "").subscribe(object : Subscriber<PromoResponse>() {
            override fun onCompleted() {
                view.removeWait()
                getListPartner()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(promoResponse: PromoResponse) {
                view.onGetPromoResponse(promoResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getListSlider() {
        view.showWait()
        val subscription = service.getSlider().subscribe(object : Subscriber<SliderResponse>() {
            override fun onCompleted() {
                view.removeWait()
                getListNews()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(sliderResponse: SliderResponse) {
                view.onGetSliderResponse(sliderResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getListPartner() {
        view.showWait()
        val subscription = service.getPartner().subscribe(object : Subscriber<PartnerListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(partnerListResponse: PartnerListResponse) {
                view.onGetPartnerResponse(partnerListResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getListInbox(page: String) {
        view.showWait()
        val subscription = service.getInboxList(page).subscribe(object : Subscriber<InboxResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(inboxResponse: InboxResponse) {
                view.onGetResponseInbox(page.toInt(), inboxResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getCashValue() {
        view.showWait()
        val subscription = service.getCashValue().subscribe(object : Subscriber<CashValueResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(cashValueResponse: CashValueResponse) {
                view.onGetCashValue(cashValueResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getUnreadMessage() {
        view.showWait()
        val subscription = service.getUnreadMessage().subscribe(object : Subscriber<UnreadMessageResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(unreadMessageResponse: UnreadMessageResponse) {
                view.onGetUnreadMessage(UnreadMessageResponse())
            }
        })
        subscriptions.add(subscription)
    }

    fun getLatestAppVersion() {
        view.showWait()
        val subscription = service.getLatestAppVersion().subscribe(object : Subscriber<AppVersionResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(appVersionResponse: AppVersionResponse) {
                view.onGetLatestAppVersion(appVersionResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getUrlList() {
        view.showWait()
        val subscription = service.getUrlList().subscribe(object : Subscriber<UrlListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(urlListResponse: UrlListResponse) {
                view.onGetUrlList(urlListResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun setFcmToken(fcmToken: String) {
        view.showWait()

        val subscription = service.updateFcmToken(fcmToken).subscribe(object : Subscriber<UpdateTokenResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(updateTokenResponse: UpdateTokenResponse) {
                view.onSetFcmToken(updateTokenResponse)
            }
        })
        subscriptions.add(subscription)
    }
}