package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import id.andalan.andalanku.ui.home.MainHomeView
import rx.Subscriber
import rx.Subscription
import javax.inject.Inject

class PpobPresenter @Inject constructor(private val service: Services): BasePresenter<PpobView>(){
    override fun onCreate() {
        TODO("Not yet implemented")
    }

    fun getPpobProductList(category:String, biller:String, keyword:String = "") {
        view.showWait()
        var subscription : Subscription
        if ((category == "emoney" || category == "streaming 1"
                    || category == "streaming 1"
                    || category == "voucher digital"
                    || category == "voucher game") && biller == "") {
            subscription = service.PpobEmoneyBiller(category)
                .subscribe(object : Subscriber<PpobProductListResponse>() {
                    override fun onCompleted() {
                        view.removeWait()
                    }

                    override fun onError(e: Throwable) {
                        view.onFailure(e.localizedMessage ?: "")
                        view.removeWait()
                    }

                    override fun onNext(ppobProductListResponse: PpobProductListResponse) {
                        view.onGetProductListResponse(ppobProductListResponse)
                        view.removeWait()
                    }
                })
        }
        else {
            subscription = service.PpobProductList(category, biller, keyword)
                .subscribe(object : Subscriber<PpobProductListResponse>() {
                    override fun onCompleted() {
                        view.removeWait()
                    }

                    override fun onError(e: Throwable) {
                        view.onFailure(e.localizedMessage ?: "")
                        view.removeWait()
                    }

                    override fun onNext(ppobProductListResponse: PpobProductListResponse) {
                        view.onGetProductListResponse(ppobProductListResponse)
                        view.removeWait()
                    }
                })
        }
        subscriptions.add(subscription)
    }

    fun getPpobProductList2(category:String, biller:String, keyword:String = "") {
        view.showWait()
        val subscription = service.PpobProductList(category, biller, keyword).subscribe(object : Subscriber<PpobProductListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(ppobProductListResponse: PpobProductListResponse) {
                view.onGetProductListResponse2(ppobProductListResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }

    fun ppobinquiry(accountNumber: String, productCode: String, category: String, zoneId: String, month: String) {
        view.showWait()
        val subscription = service.PpobInquiry(accountNumber, productCode, category, zoneId, month).subscribe(object : Subscriber<PpobInquiryResponse>() {
            override fun onCompleted() {
                //view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(ppobInquiryResponse: PpobInquiryResponse) {
                //view.removeWait()
                view.onPpobInquiryResponse(ppobInquiryResponse)
            }
        })
        subscriptions.add(subscription)
    }

}