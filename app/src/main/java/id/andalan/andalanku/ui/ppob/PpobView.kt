package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.ComplainListResponse
import id.andalan.andalanku.model.response.InstallmentPaymentHistoryListResponse
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.ui.base.BaseView

interface PpobView: BaseView {
    fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse)
    fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse)
    fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse)
}