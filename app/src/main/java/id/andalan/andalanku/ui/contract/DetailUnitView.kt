package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailUnitResponse
import id.andalan.andalanku.ui.base.BaseView

interface DetailUnitView: BaseView {
    fun getDetailUnit(detailUnitResponse: DetailUnitResponse)
}