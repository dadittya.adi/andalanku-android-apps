package id.andalan.andalanku.ui.tutorial

import id.andalan.andalanku.model.response.TutorialResponse
import id.andalan.andalanku.ui.base.BaseView

interface TutorialView: BaseView {
    fun getTutorialist(tutorialResponse: TutorialResponse)
}