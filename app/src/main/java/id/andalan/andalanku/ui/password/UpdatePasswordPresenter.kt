package id.andalan.andalanku.ui.password

import id.andalan.andalanku.model.response.ChangePasswordResponse
import id.andalan.andalanku.model.response.ResetPasswordResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class UpdatePasswordPresenter@Inject constructor(private val service: Services): BasePresenter<UpdatePasswordView>() {
    override fun onCreate() {
    }

    fun doResetPassword(password: String, newPassword: String, confirmNewPassword: String) {
        view.showWait()
        val subscription = service.doChangePassword(password, newPassword, confirmNewPassword).subscribe(object : Subscriber<ChangePasswordResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(changePasswordResponse: ChangePasswordResponse) {
                view.onChangePassword(changePasswordResponse)
            }
        })
        subscriptions.add(subscription)
    }
}