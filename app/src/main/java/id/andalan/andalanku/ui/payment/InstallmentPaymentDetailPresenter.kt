package id.andalan.andalanku.ui.payment

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.AgreementListResponse
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class InstallmentPaymentDetailPresenter @Inject constructor(private val service: Services): BasePresenter<InstallmentPaymentDetailView>() {
    override fun onCreate() {
        //getPaymentDetail()
    }

    fun getPaymentDetail(agreementNumber: String) {
        view.showWait()
        val subscription = service.paymentInquiry(agreementNumber).subscribe(object : Subscriber<PaymentInquiryResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(paymentInquiryResponse: PaymentInquiryResponse) {
                view.showWait()
                view.onGetPaymentDetail(paymentInquiryResponse)

            }
        })
        subscriptions.add(subscription)
    }
}