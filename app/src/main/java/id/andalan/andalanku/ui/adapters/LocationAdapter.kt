package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*

class LocationAdapter(val onItemClickListener: LocationAdapter.OnClickItem): BaseAdapter<Location, LocationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_default_text
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Location>(itemView) {
        override fun bind(data: Location) {
            itemView.setOnClickListener {
                onItemClickListener.onClickPromo(data)
            }
            itemView.tv_name.text = data.name
        }

    }

    interface OnClickItem {
        fun onClickPromo(location: Location)
    }
}