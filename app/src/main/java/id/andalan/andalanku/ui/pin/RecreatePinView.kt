package id.andalan.andalanku.ui.pin

import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.ui.base.BaseView

interface RecreatePinView: BaseView {
    fun onGetResponse(createPinResponse: CreatePinResponse)
    fun callbackVerificationInput(code: Int)
}