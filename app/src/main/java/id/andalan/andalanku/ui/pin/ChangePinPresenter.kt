package id.andalan.andalanku.ui.pin

import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.model.response.ResetPinResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class ChangePinPresenter @Inject constructor(private val service: Services): BasePresenter<ChangePinView>() {
    override fun onCreate() {
    }

    fun doVerificationInput(oldPin: String, newPin: String, newPinConfirm: String) {
        when {
            oldPin.isBlank() -> {
                view.callbackVerificationInput(1)
                return
            }
            newPin.isBlank() -> {
                view.callbackVerificationInput(2)
                return
            }
            newPin != newPinConfirm -> {
                view.callbackVerificationInput(2)
                return
            }
            else -> doChangePin(oldPin, newPin)
        }
    }

    private fun doChangePin(oldPin: String, newPin: String) {
        view.showWait()
        val subscription = service.doChangePin(oldPin, newPin)
            .subscribe(object : Subscriber<CreatePinResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(createPinResponse: CreatePinResponse) {
                    view.onGetResponse(createPinResponse)
                }
            })
        subscriptions.add(subscription)
    }
    fun doResetPin() {
        view.showWait()
        val subscription = service.doResetPin()
            .subscribe(object : Subscriber<ResetPinResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(resetPinResponse: ResetPinResponse) {
                    view.onGetResponseResetPin(resetPinResponse)
                }
            })
        subscriptions.add(subscription)
    }
}