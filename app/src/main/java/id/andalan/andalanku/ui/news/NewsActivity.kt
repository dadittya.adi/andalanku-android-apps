package id.andalan.andalanku.ui.news

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.News
import id.andalan.andalanku.model.ui.NewsHome
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.NewsAdapter
import id.andalan.andalanku.utils.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.activity_news.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class NewsActivity : BaseApp(), NewsView {

    private val TAG = NewsActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: NewsPresenter

    private lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    private var isLastItem = false

    private lateinit var newsAdapter: NewsAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news)

        deps.inject(this)
        presenter.view = this

        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.andalanku_news)

        newsAdapter = NewsAdapter(object: NewsAdapter.OnClickItem{
            override fun onClickNews(news: News) {
                val intent = Intent(this@NewsActivity, NewsDetailActivity::class.java)
                intent.putExtra(NewsDetailActivity.NEWS, news)
                startActivity(intent)
            }
        })

        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        endlessRecyclerOnScrollListener = object: EndlessRecyclerOnScrollListener(linearLayoutManager) {
            override fun onLoadMore(currentPage: Int) {
                if (!isLastItem) presenter.getListNews(currentPage+1, "")
            }
        }
        rv_news.layoutManager = linearLayoutManager
        rv_news.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        rv_news.adapter = newsAdapter
        rv_news.addOnScrollListener(endlessRecyclerOnScrollListener)
    }

    override fun onGetListNews(listNews: MutableList<News>) {
        if (listNews.size == 0) isLastItem = true
        e(TAG, listNews.size.toString())
        newsAdapter.add(listNews)
        newsAdapter.notifyDataSetChanged()
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        e(TAG, appErrorMessage)
    }

}
