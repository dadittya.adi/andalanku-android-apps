package id.andalan.andalanku.ui.pin

import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.model.response.ResetPinResponse
import id.andalan.andalanku.ui.base.BaseView

interface ChangePinView: BaseView {
    fun callbackVerificationInput(code: Int)
    fun onGetResponse(createPinResponse: CreatePinResponse)
    fun onGetResponseResetPin(resetPinResponse: ResetPinResponse)
}