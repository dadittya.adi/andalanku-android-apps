package id.andalan.andalanku.ui.pin

import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R

class InputPinActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_input_pin)
    }
}
