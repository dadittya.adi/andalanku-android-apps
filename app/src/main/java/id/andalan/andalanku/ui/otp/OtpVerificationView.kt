package id.andalan.andalanku.ui.otp

import id.andalan.andalanku.model.response.OtpVerificationResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.ui.base.BaseView

interface OtpVerificationView: BaseView {
    fun onGetResponse(otpVerificationResponse: OtpVerificationResponse)
    fun getProfileInformation(personalInformationResponse: PersonalInformationResponse)
}