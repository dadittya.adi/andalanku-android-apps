package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_picker.view.*

class ArrayStaticAdapter(val onItemClickListener: OnClickItem): BaseAdapter<String, ArrayStaticAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArrayStaticAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_picker
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<String>(itemView) {
        override fun bind(data: String) {
            itemView.tv_item_title.text = data
            itemView.setOnClickListener {
                onItemClickListener.onClick(data)
            }
        }
    }

    interface OnClickItem {
        fun onClick(category: String)
    }
}