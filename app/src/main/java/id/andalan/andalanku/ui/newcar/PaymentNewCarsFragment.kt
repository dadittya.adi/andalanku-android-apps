package id.andalan.andalanku.ui.newcar


import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Brands
import id.andalan.andalanku.ui.adapters.BrandsAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import kotlinx.android.synthetic.main.activity_payment_new_car.*
import kotlinx.android.synthetic.main.toolbar_login.*

class PaymentNewCarsFragment : androidx.fragment.app.Fragment() {
    private val TAG = PaymentNewCarsFragment::class.java.simpleName

    private lateinit var brandsAdapter: BrandsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_payment_new_cars, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.new_cars_payment)
        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }

//        brandsAdapter = BrandsAdapter(object: BrandsAdapter.OnClickItem{
//            override fun onClickImage(brands: Brands) {
//                (activity as MainHomeActivity).moveViewNoAnim(1)
//            }
//        })

        val gridLayoutManager =
            androidx.recyclerview.widget.GridLayoutManager(context, 2)
        rv_merk_mobil?.layoutManager = gridLayoutManager
        rv_merk_mobil?.adapter = brandsAdapter
        rv_merk_mobil?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

    }
}
