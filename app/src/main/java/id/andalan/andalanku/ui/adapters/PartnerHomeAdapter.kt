@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Partner
import id.andalan.andalanku.model.ui.PartnerHome
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_partner.view.*

class PartnerHomeAdapter(val onItemClickListener: OnClickItem): BaseAdapter<Partner, PartnerHomeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_partner
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Partner>(itemView) {
        override fun bind(partner: Partner) {
            val data = partner.getPartnerHome()
            itemView.setOnClickListener {
                onItemClickListener.onClickPartner(data)
            }
            loadImage(itemView.context, data.url?:"", itemView.iv_partner_home)
        }

    }

    interface OnClickItem {
        fun onClickPartner(partnerHome: PartnerHome)
    }
}