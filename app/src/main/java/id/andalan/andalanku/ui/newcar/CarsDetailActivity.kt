package id.andalan.andalanku.ui.newcar

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.utils.*
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_cars_detail.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import javax.inject.Inject

class CarsDetailActivity : BaseApp(), CarsDetailView {
    override fun onGetUsedCarsList(usedCarListResponse: UsedCarListResponse) {
    }

    companion object {
        val CARS = "cars"
        val REQUEST_CAR = 923
        val RESPONSE_CAR = 924
    }
    private val TAG = CarsDetailActivity::class.java.simpleName

    private lateinit var carsDetailSliderAdapter: CarsDetailSliderAdapter
    private lateinit var tenorAdapter: TenorAdapter
    private lateinit var premiAdapter: PremiAdapter
    private lateinit var zoneAreaAdapter: ZoneAreaAdapter
    private lateinit var carTrimAdapter: CarTrimAdapter

    private var choosedCarTrim: CarTrim? = null
    private var choosedTenor: Tenor? = null
    private var choosedPremi: Premi? = null
    private var choosedZone: ZoneArea? = null
    private var cars: CarType? = null
    private var carBrand: CarBrand? = null
    private var price = "0"
    private lateinit var collateral: Collateral
    private lateinit var creditSimulationRequest: CreditSimulationRequest
    private var isDpEdited = false
    private var isDpPriceEdited = false

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: CarsDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cars_detail)

        cars = intent?.extras?.getParcelable(CARS)
        carBrand = intent?.extras?.getParcelable(CarChooserActivity.CARBRANDS)
        if (cars?.isEmpty() == true) {
            finish()
        }

        deps.inject(this)
        presenter.view = this

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = cars?.name
        tv_cars_name.text = "${carBrand?.name} ${cars?.name}"
        carsDetailSliderAdapter = CarsDetailSliderAdapter(this, cars?.images?: arrayListOf())
        slider.setAdapter(carsDetailSliderAdapter)
        presenter.getCarTrim(cars?.idCarType?:"")

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        btn_type_chooser?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_cars)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = carTrimAdapter
            showSlideLayout()
        }

        et_tenor?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_tenor)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = tenorAdapter
            showSlideLayout()
        }

        et_premi?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_insurance)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = premiAdapter
            showSlideLayout()
        }

        et_zone?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_area_zone)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = zoneAreaAdapter
            showSlideLayout()
        }

        tenorAdapter = TenorAdapter(object: TenorAdapter.OnClickItem{
            override fun onClickPromo(tenor: Tenor) {
                choosedTenor = tenor
                et_tenor?.setText(tenor.getYear(getString(R.string.year)))
                hideSlideLayout()
            }
        })

        premiAdapter = PremiAdapter(object: PremiAdapter.OnClickItem{
            override fun onClickPromo(premi: Premi) {
                choosedPremi = premi
                et_premi?.setText(premi.name)
                hideSlideLayout()
            }
        })

        zoneAreaAdapter = ZoneAreaAdapter(object: ZoneAreaAdapter.OnClickItem{
            override fun onClickPromo(zoneArea: ZoneArea) {
                choosedZone = zoneArea
                et_zone?.setText(zoneArea.name)
                hideSlideLayout()
            }
        })

        carTrimAdapter = CarTrimAdapter(object: CarTrimAdapter.OnClickItem{
            override fun onClick(carTrim: CarTrim) {
                choosedCarTrim = carTrim
                tv_type?.text = carTrim.name
                reloadCarTrim()
                hideSlideLayout()
            }
        })

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        tv_dp_percent.setOnFocusChangeListener { _, hasFocus ->
            isDpEdited = hasFocus
        }

        tv_dp_percent.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (isDpEdited && !isDpPriceEdited) {
                    val numString = s.toString()
                    if (numString.isNotEmpty()) {
                        val dp = numString.toDouble()
                        if (dp <= 100) {
                            val dpPrice = price.toLong().getValue(dp)
                            tv_dp_price.setText(dpPrice.convertToRpFormat())
                        } else {
                            tv_dp_percent.setText(100.toString())
                        }
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        tv_dp_price.setOnFocusChangeListener { _, hasFocus ->
            isDpPriceEdited = hasFocus
        }

        tv_dp_price.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (!isDpEdited && isDpPriceEdited) {
                    val numString = s.toString()
                    if (numString.isNotEmpty()) {
                        val pricee = numString.getNumberOnly()
                        if (pricee <= price.toLong()) {
                            if (tv_dp_price.text.toString() != pricee.convertToRpFormat()) tv_dp_price.setText(pricee.convertToRpFormat())
                            val text = pricee.convertToRpFormat()
                            tv_dp_price.setSelection(text.length - 2)
                            tv_dp_percent.setText(pricee.getPercentage(price.toLong()))
                        } else {
                            if (tv_dp_price.text.toString() != price.toLong().convertToRpFormat()) tv_dp_price.setText(
                                price.toLong().convertToRpFormat()
                            )
                            val text = price.toLong().convertToRpFormat()
                            tv_dp_price.setSelection(text.length - 2)
                            tv_dp_percent.setText(price.toLong().getPercentage(price.toLong()))
                        }
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

        })

        btn_kalkulasi.setOnClickListener {
            val dpText = tv_dp_percent.text.toString()
            //val dpAmountText = tv_dp_price.text.toString()
            val dpPrice = dpText.getNumberOnly()
            //val dpAmount = dpAmountText.getNumberOnly()
            val tenor = choosedTenor?.id?:0
            val premi = choosedPremi?.type?:""
            val zone = choosedZone?.id?:0
            if (onCheckForm(dpPrice, tenor, premi, zone)) {
                collateral = Collateral(
                    collateralType = ConfigVar.BPKB,
                    collateral = "${carBrand?.name?:""}/${cars?.name}/${choosedCarTrim?.name?:""}",
                    collateralYear = "-",
                    collateralOwner = preferencesUtil.getUser()?.customerName,
                    collateralPaperNumber = "-",
                    assetCode = choosedCarTrim?.assetCode?:""
                )
                creditSimulationRequest = CreditSimulationRequest(
                    type = ConfigVar.NEW_CAR,
                    otr = choosedCarTrim?.price,
                    dp = dpPrice.toString(),
                    tenor = tenor.toString(),
                    asuransi = premi,
                    wilayah = zone.toString()
                )

                presenter.getCreditSimulation(creditSimulationRequest)
            } else {
                Toast.makeText(this@CarsDetailActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }

        initFakeData()
    }

    private fun onCheckForm(dpPrice: Long, tenor: Int, premi: String, zone: Int): Boolean {
        var result = true

        if (dpPrice == 0.toLong()) {
            result = false
        }
        if (tenor == 0) {
            result = false
        }
        if (premi.isBlank()) {
            result = false
        }
        if (zone == 0) {
            result = false
        }

        return result
    }

    private fun initFakeData() {
        tenorAdapter.add(Tenor(id = 12))
        tenorAdapter.add(Tenor(id = 18))
        tenorAdapter.add(Tenor(id = 24))
        tenorAdapter.add(Tenor(id = 36))
        tenorAdapter.add(Tenor(id = 48))
        tenorAdapter.add(Tenor(id = 60))

        premiAdapter.add(Premi(id = 1, type = getString(R.string.tlo), name = getString(R.string.tlo)))
        premiAdapter.add(Premi(id = 2, type = getString(R.string.ark), name = getString(R.string.all_risk)))

        zoneAreaAdapter.add(ZoneArea(id = 1, name = getString(R.string.zone_1)))
        zoneAreaAdapter.add(ZoneArea(id = 2, name = getString(R.string.zone_2)))
        zoneAreaAdapter.add(ZoneArea(id = 3, name = getString(R.string.zone_3)))
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun reloadCarTrim() {
        tv_type?.text = choosedCarTrim?.name
        tv_price_otr.text = choosedCarTrim?.price?.toLong()?.convertToRpFormat()
        price = choosedCarTrim?.price.toString()
    }

    override fun onGetCarTrim(carTrimsResponse: CarTrimsResponse) {
        if (carTrimsResponse.status == ConfigVar.SUCCESS) {
            carTrimsResponse.carTrimList?.let {
                carTrimAdapter.add(it)
                if (it.size > 0) {
                    choosedCarTrim = it[0]
                    reloadCarTrim()
                }
            }
        } else {
            Toast.makeText(this@CarsDetailActivity, carTrimsResponse.status, Toast.LENGTH_LONG).show()
        }
    }

    override fun onGetCreditSimulation(creditSimulationResponse: CreditSimulationResponse) {
        if (creditSimulationResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this@CarsDetailActivity, PaymentSimulationActivity::class.java)
            intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulationResponse)
            intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
            intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
            startActivityForResult(intent, PaymentSimulationActivity.REQUEST_PAYMENT)
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }
                override fun onClickOk() {
                }
            }, this, cars?.name?:"", creditSimulationResponse.message?:"")
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            super.onBackPressed()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PaymentSimulationActivity.REQUEST_PAYMENT && resultCode == PaymentSimulationActivity.RESPONSE_PAYMENT) {
            val isResimulate = data?.extras?.getBoolean(PaymentSimulationActivity.RESIMULATE, false)
            if (isResimulate == true) {
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.RESIMULATE, true)
                setResult(RESPONSE_CAR, intent)
                finish()
            }
            val isApply = data?.extras?.getBoolean(PaymentSimulationActivity.APPLY, false)
            if (isApply == true) {
                val creditSimulation: CreditSimulationO? = data.extras?.getParcelable(PaymentSimulationActivity.CREDIT)
                val intent = Intent()

                intent.putExtra(PaymentSimulationActivity.APPLY, true)
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
                intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
                setResult(RESPONSE_CAR, intent)
                finish()
            }
        }
    }
}
