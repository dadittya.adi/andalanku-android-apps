package id.andalan.andalanku.ui.newcar

import id.andalan.andalanku.model.response.CarTypesResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class CarChooserPresenter @Inject constructor(private val service: Services): BasePresenter<CarChooserView>() {
    override fun onCreate() {
    }

    fun getTypesCar(id: String) {
        view.showWait()
        val subscription = service.getTypesCar(id)
            .subscribe(object : Subscriber<CarTypesResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(carTypesResponse: CarTypesResponse) {
                    view.onGetTypeCar(carTypesResponse)
                }
            })
        subscriptions.add(subscription)
    }
}