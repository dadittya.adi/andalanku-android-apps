package id.andalan.andalanku.ui.complain

import id.andalan.andalanku.model.response.CategoryComplainResponse
import id.andalan.andalanku.model.response.InputComplainResponse
import id.andalan.andalanku.ui.base.BaseView

interface SubmissionComplainView: BaseView {
    fun onSuccessSendComplain(inputComplainResponse: InputComplainResponse)
    fun onGetCategoryList(categoryComplainResponse: CategoryComplainResponse)
}