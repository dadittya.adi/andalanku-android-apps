package id.andalan.andalanku.ui.promo

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.util.Log.e
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Promo
import id.andalan.andalanku.model.ui.PromoHome
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PromoAdapter
import id.andalan.andalanku.utils.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.activity_promo.*
import kotlinx.android.synthetic.main.activity_promo.swipe_layout
import kotlinx.android.synthetic.main.activity_transaction_history.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class PromoActivity : BaseApp(), PromoView {
    val TAG = PromoActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: PromoPresenter

    private lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private lateinit var linearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager
    private var isLastItem = false

    private lateinit var promoAdapter: PromoAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_promo)

        deps.inject(this)
        presenter.view = this

        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.andalanku_promo)

        promoAdapter = PromoAdapter(object: PromoAdapter.OnClickItem{
            override fun onClickPromo(promo: Promo) {
                val intent = Intent(this@PromoActivity, DetailPromoActivity::class.java)
                intent.putExtra(DetailPromoActivity.PROMOSLUG, promo.slug)
                startActivity(intent)
            }
        })
        linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            this,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        endlessRecyclerOnScrollListener = object: EndlessRecyclerOnScrollListener(linearLayoutManager) {
            override fun onLoadMore(currentPage: Int) {
                if (!isLastItem) presenter.getListPromo(currentPage+1, "")
            }
        }
        rv_promo.layoutManager = linearLayoutManager
        rv_promo.addOnScrollListener(endlessRecyclerOnScrollListener)
        rv_promo.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_promo.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_promo.adapter = promoAdapter

        swipe_layout?.setOnRefreshListener {
            presenter.onCreate()
        }
    }
    override fun onGetListPromo(list: MutableList<Promo>) {
        promoAdapter.removeAll()
        if (list.size == 0) isLastItem = true
        promoAdapter.add(list)
        promoAdapter.notifyDataSetChanged()
    }

    override fun showWait() {
        swipe_layout.isRefreshing = true
    }

    override fun removeWait() {
        swipe_layout.isRefreshing = false
    }

    override fun onFailure(appErrorMessage: String?) {
        e(TAG, appErrorMessage)
    }
}
