package id.andalan.andalanku.ui.newcar

import id.andalan.andalanku.model.response.CarBrandsResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PaymentNewCarPresenter @Inject constructor(private val service: Services): BasePresenter<PaymentNewCarView>() {
    override fun onCreate() {
        getBrandsCar()
    }

    fun getBrandsCar() {
        view.showWait()
        val subscription = service.getBrandsCar()
            .subscribe(object : Subscriber<CarBrandsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(carBrandsResponse: CarBrandsResponse) {
                    view.onGetCarBrands(carBrandsResponse)
                }
            })
        subscriptions.add(subscription)
    }
}