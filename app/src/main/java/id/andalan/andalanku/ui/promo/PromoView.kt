package id.andalan.andalanku.ui.promo

import id.andalan.andalanku.model.ui.Promo
import id.andalan.andalanku.ui.base.BaseView

interface PromoView: BaseView {
    fun onGetListPromo(list: MutableList<Promo>)
}