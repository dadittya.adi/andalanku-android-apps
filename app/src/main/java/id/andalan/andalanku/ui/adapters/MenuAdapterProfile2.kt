package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_menu_profile_2.view.*

class MenuAdapterProfile2(val onItemClickListener: OnClickItem): BaseAdapter<MenuProfile, MenuAdapterProfile2.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_menu_profile_2
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<MenuProfile>(itemView) {
        override fun bind(data: MenuProfile) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            itemView.ic_left?.setImageResource(data.leftDrawable?: R.drawable.ic_wallet)
            itemView.tv_name?.text = data.name
            itemView.tv_point?.text = data.point
        }

    }

    interface OnClickItem {
        fun onClickMenu(menuProfile: MenuProfile)
    }
}