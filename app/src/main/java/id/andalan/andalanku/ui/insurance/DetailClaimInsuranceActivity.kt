package id.andalan.andalanku.ui.insurance

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.ClaimInsurance
import id.andalan.andalanku.ui.adapters.AttachmentClaimAdapter
import id.andalan.andalanku.ui.adapters.AttachmentComplainAdapter
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.activity_detail_claim_insurance.*
import kotlinx.android.synthetic.main.toolbar_login.*

class DetailClaimInsuranceActivity : BaseApp() {
    companion object {
        val CLAIM_INSURANCE = "claim_insurance"
    }
    private lateinit var attachmentClaimAdapter: AttachmentClaimAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_claim_insurance)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.detail_claim)

        val claimInsurance: ClaimInsurance? = intent?.extras?.getParcelable(CLAIM_INSURANCE)
        Log.e("taG image", claimInsurance?.mediaList.toString())

        attachmentClaimAdapter = AttachmentClaimAdapter(object: AttachmentClaimAdapter.OnClickItem{
            override fun onClickImage(urlImage: String) {
            }
        })

        rv_attachment.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_attachment.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                false
            )
        rv_attachment.adapter = attachmentClaimAdapter

        claimInsurance?.let {
            tv_submission_number?.text = it.ticketNumber
            tv_category_insurance?.text = it.getInsuranceType() + "(" + it.getClaimEventCase() + ")"
            tv_date_sent?.text = it.getClaimDateUI(this.getMonths())
            tv_status_submission?.text = it.status
            hint_info?.visibility = if (it.pic.isNullOrBlank()) View.GONE else View.VISIBLE
            tv_info?.visibility = if (it.pic.isNullOrBlank()) View.GONE else View.VISIBLE
            tv_info?.text = "Telah di handle oleh " + it.pic
            tv_date_incident?.text = it.getEventDateUI(this.getMonths())
            tv_location?.text = it.location
            tv_message?.text = it.notes
            attachmentClaimAdapter.add(it.mediaList?: arrayListOf())
        }
    }
}
