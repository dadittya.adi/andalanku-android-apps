package id.andalan.andalanku.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.Inbox
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.model.ui.Occupation
import id.andalan.andalanku.model.ui.PickerItem

class ViewModelMain: ViewModel() {
    val currentListAgreementNumber: MutableLiveData<MutableList<PickerItem>> by lazy {
        MutableLiveData<MutableList<PickerItem>>()
    }

    val currentNewsResponse: MutableLiveData<NewsResponse> by lazy {
        MutableLiveData<NewsResponse>()
    }

    val currentPromoResponse: MutableLiveData<PromoResponse> by lazy {
        MutableLiveData<PromoResponse>()
    }

    val currentSliderResponse: MutableLiveData<SliderResponse> by lazy {
        MutableLiveData<SliderResponse>()
    }

    val currentPartnerResponse: MutableLiveData<PartnerListResponse> by lazy {
        MutableLiveData<PartnerListResponse>()
    }

    val currentProfile: MutableLiveData<PersonalInformationResponse> by lazy {
        MutableLiveData<PersonalInformationResponse>()
    }

    val currentInboxList: MutableLiveData<MutableList<Inbox>> by lazy {
        MutableLiveData<MutableList<Inbox>>()
    }

    val currentProvinceList: MutableLiveData<MutableList<Location>> by lazy {
        MutableLiveData<MutableList<Location>>()
    }

    val currentCityList: MutableLiveData<MutableList<Location>> by lazy {
        MutableLiveData<MutableList<Location>>()
    }

    val currentDistrictList: MutableLiveData<MutableList<Location>> by lazy {
        MutableLiveData<MutableList<Location>>()
    }

    val currentVillageList: MutableLiveData<MutableList<Location>> by lazy {
        MutableLiveData<MutableList<Location>>()
    }

    val currentOccupationList: MutableLiveData<MutableList<Occupation>> by lazy {
        MutableLiveData<MutableList<Occupation>>()
    }

    val choosedContract: MutableLiveData<PickerItem> by lazy {
        MutableLiveData<PickerItem>()
    }

    val currentCashValue: MutableLiveData<CashValueResponse> by lazy {
        MutableLiveData<CashValueResponse>()
    }

    val currentSlider: MutableLiveData<SliderResponse> by lazy {
        MutableLiveData<SliderResponse>()
    }

    val isNeedReset: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }

    val currentUnreadMessage: MutableLiveData<UnreadMessageResponse> by lazy {
        MutableLiveData<UnreadMessageResponse>()
    }

}