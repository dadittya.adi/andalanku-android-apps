 package id.andalan.andalanku.ui.loan_application

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.request.SendLoanApplicationRequest
import id.andalan.andalanku.model.ui.CreditSimulationO
import id.andalan.andalanku.model.ui.Document
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment.Companion.COMPLETE_DATA
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity.Companion.HOME
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity.Companion.MESSAGE
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.utils.ImagePickerActivity
import id.andalan.andalanku.utils.ekstensions.loadImage
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_account_document.*
import kotlinx.android.synthetic.main.item_upload_document.*
import kotlinx.android.synthetic.main.item_upload_document.view.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.toolbar_login.*
import java.io.File
import javax.inject.Inject

class AccountDocumentActivity : BaseApp() {

    private lateinit var listBankAdapter: PickerAdapter
    private var bankChoosed = PickerItem()

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var sendLoanApplicationRequest: SendLoanApplicationRequest
    private lateinit var simulation: CreditSimulationO
    private lateinit var collateral: Collateral
    private lateinit var creditSimulationRequest: CreditSimulationRequest
    private var image = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_document)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.data_rekening)
        tv_hint_photo_ktp?.text = "Upload Foto Cover Buku Tabungan"

        sendLoanApplicationRequest = intent.getParcelableExtra(CompleteDataFragment.COMPLETE_DATA)
        simulation = intent.getParcelableExtra(PaymentSimulationActivity.CREDIT)
        collateral = intent.getParcelableExtra(CompleteDocumentActivity.COLLATERAL)
        creditSimulationRequest = intent.getParcelableExtra(CompleteDocumentActivity.CREDITRESQUEST)

//        if (preferencesUtil?.getProfile().fo) {
//            image = it
//            loadImage(this, it, iv_preview)
//            tv_filename.text = getString(R.string.change_image)
//        }

        deps.inject(this)

        tv_title_text?.text = "Pilih Bank"
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        listBankAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = listBankAdapter.getData().indexOf(bankChoosed)

                bankChoosed.isChoosed = false
                if (index > -1) listBankAdapter.update(index, bankChoosed)
                et_choose_bank?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                bankChoosed = pickerItem
                val indexChoosed = listBankAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) listBankAdapter.update(indexChoosed, bankChoosed)

                hideSlideLayout()
            }
        })

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = listBankAdapter

        initBank()

        et_choose_bank?.setOnClickListener {
            showSlideLayout()
        }

        btn_next?.setOnClickListener {
            if (cb_term_condition.isChecked) {
                sendLoanApplicationRequest.bankName = bankChoosed.name
                sendLoanApplicationRequest.bankAccountNumber = et_account_number?.text.toString()
                sendLoanApplicationRequest.bankAccountHolder = et_in_the_name?.text.toString()
                sendLoanApplicationRequest.bankAccountImage = image

                if (sendLoanApplicationRequest.isValidBank()) {
                    val intent = Intent(this, CompleteDocumentActivity::class.java)
                    intent.putExtra(COMPLETE_DATA, sendLoanApplicationRequest)
                    intent.putExtra(PaymentSimulationActivity.CREDIT, simulation)
                    intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
                    intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
                    startActivityForResult(intent, 1234)
                } else {
                    Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
            }
        }

        container_upload?.setOnClickListener {
            openImagePicker(1172)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1234) {
            val dataIntent = data?.extras
            if (dataIntent?.getBoolean(HOME, false) == true) {
                val intent = intent
                intent.putExtra(HOME, true)
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else if (dataIntent?.getBoolean(MESSAGE, false) == true) {
                val intent = intent
                intent.putExtra(MESSAGE, true)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        } else if (requestCode == 1172) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data?.getParcelableExtra<Uri>("path")

                uri?.path?.let {
                    image = it
                    loadImage(this, it, iv_preview)
                    tv_filename.text = getString(R.string.change_image)
                    sendLoanApplicationRequest.typeBankAccountImage = "file"
                }
            }
        }
    }

    private fun initBank() {
        listBankAdapter.add(PickerItem(id = "bca", name = "BCA"))
        listBankAdapter.add(PickerItem(id = "mandiri", name = "MANDIRI"))
        listBankAdapter.add(PickerItem(id = "cimb_niaga", name = "CIMB"))
    }

    private fun openImagePicker(request: Int) {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions(request)
                    } else {
                        // TODO - handle permission denied case
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    private fun showImagePickerOptions(request: Int) {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent(request)
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent(request)
            }

        })
    }

    private fun launchCameraIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, request)
    }

    private fun launchGalleryIntent(request: Int) {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, request)
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout(isBlocked: Boolean = false) {
        if (!isBlocked) {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            hideKeyboard(this)
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
