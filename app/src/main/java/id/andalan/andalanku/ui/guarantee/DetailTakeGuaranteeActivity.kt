package id.andalan.andalanku.ui.guarantee

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.CollateralRequest
import id.andalan.andalanku.ui.adapters.AttachmentComplainAdapter
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.activity_detail_take_guarantee.*
import kotlinx.android.synthetic.main.toolbar_login.*

class DetailTakeGuaranteeActivity : BaseApp() {
    companion object {
        val COLLATERAL_REQUEST_DETAIL = "collateral_request_detail"
    }
    private lateinit var attachmentComplainAdapter: AttachmentComplainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_take_guarantee)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.detail_submission_guarantee)

        val collateralRequest: CollateralRequest? = intent?.extras?.getParcelable(COLLATERAL_REQUEST_DETAIL)
        collateralRequest?.let {
            tv_submission_number?.text = it.ticketNumber
            tv_police_number?.text = it.agreementNumber
            tv_date_sent?.text = it.getRequestDateUI(this.getMonths())
            tv_status_submission?.text = it.status
            tv_date_take_guarantee?.text = it.getTakingDateUI(this.getMonths())
            hint_pesan?.visibility = if (it.readyDate.isNullOrBlank()) View.GONE else View.VISIBLE
            tv_message?.visibility = if (it.readyDate.isNullOrBlank()) View.GONE else View.VISIBLE
            tv_message?.text = "Barang Jaminan dapat diambil pada tanggal " + it.getReadyDateUI(this.getMonths())
        }

        attachmentComplainAdapter = AttachmentComplainAdapter(object: AttachmentComplainAdapter.OnClickItem{
            override fun onClickImage(urlImage: String) {
            }
        })

        rv_attachment.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_attachment.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.HORIZONTAL,
                false
            )
        rv_attachment.adapter = attachmentComplainAdapter
    }
}
