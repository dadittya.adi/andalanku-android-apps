package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.ui.base.BaseView

interface DetailKontrakView: BaseView {
    fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse)
}