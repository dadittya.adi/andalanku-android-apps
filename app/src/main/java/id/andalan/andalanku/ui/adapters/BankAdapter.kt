package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Bank
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_bank.view.*

class BankAdapter(val onItemClickListener: OnClickItem): BaseAdapter<Bank, BankAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BankAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_bank
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Bank>(itemView) {
        override fun bind(data: Bank) {
            itemView.setOnClickListener {
                onItemClickListener.onClickBank(data)
            }
            itemView.tv_bank_name?.text = data.bankName
        }

    }

    interface OnClickItem {
        fun onClickBank(bank: Bank)
    }
}