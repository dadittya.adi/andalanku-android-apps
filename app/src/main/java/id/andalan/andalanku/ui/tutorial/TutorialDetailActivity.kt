package id.andalan.andalanku.ui.tutorial

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.FaqParent
import id.andalan.andalanku.model.ui.TutorialParent
import id.andalan.andalanku.ui.adapters.FaqAnswerAdapter
import id.andalan.andalanku.ui.adapters.TutorialAnswerAdapter
import kotlinx.android.synthetic.main.activity_faq_detail.*
import kotlinx.android.synthetic.main.toolbar_login.*

class TutorialDetailActivity : BaseApp() {
    companion object {
        val CONTENT_TUTORIAL = "content_tutorial"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tutorial_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.tutorial)

        val content: TutorialParent? = intent?.extras?.getParcelable(CONTENT_TUTORIAL)

        if (content == null) {
            finish()
        }
        val adapter = TutorialAnswerAdapter()
        adapter.add(content?.tutorialAnswerList?: arrayListOf())
        tv_title_text?.text = content?.question
        rv_content.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_content.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_content.adapter = adapter

    }
}
