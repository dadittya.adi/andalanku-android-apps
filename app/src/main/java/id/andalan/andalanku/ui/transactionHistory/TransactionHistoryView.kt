package id.andalan.andalanku.ui.transactionHistory

import id.andalan.andalanku.model.response.ComplainListResponse
import id.andalan.andalanku.model.response.InstallmentPaymentHistoryListResponse
import id.andalan.andalanku.ui.base.BaseView

interface TransactionHistoryView: BaseView {
    fun getListResponse(installmentPaymentHistoryListResponse: InstallmentPaymentHistoryListResponse)
}