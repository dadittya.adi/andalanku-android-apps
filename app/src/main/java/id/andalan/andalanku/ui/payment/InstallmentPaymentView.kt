package id.andalan.andalanku.ui.payment

import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface InstallmentPaymentView: BaseView {
    fun onGetContractNumber(list: MutableList<PickerItem>)
    fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse)
}