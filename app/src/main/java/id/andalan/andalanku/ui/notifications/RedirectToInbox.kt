package id.andalan.andalanku.ui.notifications

import android.content.Intent
import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.ui.home.MainHomeActivity

class RedirectToInbox : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, MainHomeActivity::class.java)
        intent.putExtra(MainHomeActivity.EVENT_SENDER, "notification")
        startActivity(intent)
        finish()
    }
}