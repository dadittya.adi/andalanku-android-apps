package id.andalan.andalanku.ui.contract


import android.Manifest
import android.app.DownloadManager
import android.content.*
import android.net.Uri
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.fragment.app.Fragment
import androidx.core.content.FileProvider
import android.util.Log
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.downloader.*
import id.andalan.andalanku.BuildConfig

import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.AgreementDocumentResponse
import id.andalan.andalanku.model.response.DetailInsuranceResponse
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.FileUtils
import kotlinx.android.synthetic.main.fragment_polis_asuransi.*
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import java.io.File

class AgreementContractFragment : androidx.fragment.app.Fragment(), AgreementDocumentView,
    EasyPermissions.PermissionCallbacks {

    private var link = ""
    private var path = ""
    private val RC_STORAGE = 123
    private val STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE

    private var downloadManager: DownloadManager? = null
    private var refid: Long = 0
    private lateinit var onComplete: BroadcastReceiver
    private var filename = ""
    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_agreement_document, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        alertDialogProgress = AlertDialogCustom(context!!, null)
        alertDialogProgress.setView(LayoutInflater.from(context!!).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        context?.let {
            PRDownloader.initialize(it)
            path = FileUtils.getRootDirPath()
            downloadManager = it.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            onComplete = object: BroadcastReceiver() {
                override fun onReceive(p0: Context?, p1: Intent?) {
                    Toast.makeText(context, getString(R.string.success_download_contract_agreement), Toast.LENGTH_LONG).show()
//                    openFile(path, filename)
                }
            }
            it.registerReceiver(onComplete,
                IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
            )
        }

        btn_download?.setOnClickListener {
            (activity as ContractDetailActivity).agreementDocumentResponse?.let {
                getAgreementDocument(it)
            }
            if (hasStoragePermission()) {
                doDownload()
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.izinkan_aplikasi_mendownload_file),
                    RC_STORAGE,
                    STORAGE)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.unregisterReceiver(onComplete)
    }

    private fun hasStoragePermission (): Boolean {
        context?.let {
            return EasyPermissions.hasPermissions(it, STORAGE)
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        @NonNull permissions: Array<String>,
        @NonNull grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // EasyPermissions handles the request result.
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        AppSettingsDialog.Builder(this).build().show()
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {
        if (RC_STORAGE == requestCode) {
            doDownload()
        }
    }

    private fun doDownload() {
        if (link.isNotBlank()) {
            filename = "perjanjian-kontrak-${(activity as ContractDetailActivity).choosedPicker.name}.pdf"

            PRDownloader.download(link, path, filename)
                .build()
                .setOnProgressListener(object: OnProgressListener {
                    override fun onProgress(progress: Progress?) {
                        alertDialogProgress.show()
                    }

                })
                .start(object: OnDownloadListener{
                    override fun onDownloadComplete() {
                        alertDialogProgress.dismiss()
                        Toast.makeText(context, getString(R.string.success_download_contract_agreement), Toast.LENGTH_LONG).show()
                        openFile(path, filename)
                    }
                    override fun onError(error: Error?) {
                        if (error?.isConnectionError == true) {
                            Toast.makeText(context, getString(R.string.failed_download_connection_error), Toast.LENGTH_LONG).show()
                        } else if (error?.isServerError == true) {
                            Toast.makeText(context, getString(R.string.failed_download_server_error), Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(context, getString(R.string.failed_download), Toast.LENGTH_LONG).show()
                        }
                    }
                })
        } else {
            e("tag", "error_polis")
            Toast.makeText(context, getString(R.string.failed_download_server_error), Toast.LENGTH_LONG).show()
        }
    }

    override fun getAgreementDocument(agreementDocumentResponse: AgreementDocumentResponse) {
//        if (detailInsuranceResponse.status == ConfigVar.SUCCESS) {
            link = agreementDocumentResponse.agreementDocument?:""
//        } else {
//            Toast.makeText(context, detailInsuranceResponse.message, Toast.LENGTH_LONG).show()
//            btn_download.isEnabled = false
//        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    private fun openFile(path: String, filename: String) {
        context?.let {
            val file = File("$path/$filename")
            val target = Intent(Intent.ACTION_VIEW)
            val uri = FileProvider.getUriForFile(it,
                BuildConfig.APPLICATION_ID + ".provider",
                file)
            target.setDataAndType(uri, "application/pdf")
            target.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            val intent = Intent.createChooser(target, "Buka Kartu Piutang")
            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                // Instruct the user to install a PDF reader here, or something
            }
        }
    }

}
