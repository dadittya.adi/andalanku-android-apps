package id.andalan.andalanku.ui.onboarding

import android.content.Intent
import android.os.Bundle
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.synnapps.carouselview.ImageListener
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.register.RegisterActivity
import kotlinx.android.synthetic.main.activity_onboard_page1.*


class OnboardPage1 : AppCompatActivity() {

    /* Start Variabel Array carouselView */
    val sampleImages = intArrayOf(
            R.drawable.onboard_cair_cepat,
            R.drawable.onboard_rate,
            R.drawable.onboard_pencairan,
            R.drawable.onboard_ojk
    )

    var imagePosition: Int = 0
    /* End Variabel Array carouselView */

    /* Start Variabel Listener carouselView */
    val imagesListener = ImageListener { position, imageViewx ->
        imageViewx.setImageResource(sampleImages[position])
    }
    /* End Variabel Listener carouselView */

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboard_page1)

        /* Start Panggil widget carouselView */
        carouselView.pageCount = sampleImages.size
        carouselView.setImageListener(imagesListener)

        btn_onboarding_login.visibility = View.GONE
        btn_onboarding_register.visibility = View.GONE

        carouselView.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
               if (position == 3) {
                   btn_onboarding_login.visibility = View.VISIBLE
                   btn_onboarding_register.visibility = View.VISIBLE
               }
                else {
                   btn_onboarding_login.visibility = View.GONE
                   btn_onboarding_register.visibility = View.GONE
               }
            }

            override fun onPageSelected(position: Int) {

            }

            override fun onPageScrollStateChanged(state: Int) {}
        })
        /* End Panggil widget carouselView */

        btn_onboarding_login.setOnClickListener {
            finish()
            val intentLogin = Intent(this@OnboardPage1, LoginActivity::class.java)
            startActivity(intentLogin)
        }

        btn_onboarding_register.setOnClickListener {
            finish()
            val intentRegister = Intent(this@OnboardPage1, RegisterActivity::class.java)
            startActivity(intentRegister)
            intentRegister.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
    }

}