package id.andalan.andalanku.ui.login

import id.andalan.andalanku.model.response.LoginResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.ui.base.BaseView

interface LoginView: BaseView {
    fun onGetResponse(loginResponse: LoginResponse)
    fun getProfileInformation(personalInformationResponse: PersonalInformationResponse)
}