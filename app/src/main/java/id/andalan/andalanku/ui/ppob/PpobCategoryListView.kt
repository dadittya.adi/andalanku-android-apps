package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.PpobGroupResponse
import id.andalan.andalanku.ui.base.BaseView

interface PpobCategoryListView: BaseView {
    fun onGetLastPurchaseResponse(ppobGroupResponse: PpobGroupResponse)
}