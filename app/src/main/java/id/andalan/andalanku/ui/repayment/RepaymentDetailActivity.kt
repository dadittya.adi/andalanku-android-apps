package id.andalan.andalanku.ui.repayment

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.PrepaymentRequest
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import java.util.*
import kotlinx.android.synthetic.main.activity_repayment_detail.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import javax.inject.Inject
import android.app.DatePickerDialog
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.model.response.PrePaymentSimulationResponse
import id.andalan.andalanku.ui.adapters.ConditionAdapter
import id.andalan.andalanku.utils.*
import kotlinx.android.synthetic.main.activity_repayment_detail.sliding_layout
import kotlinx.android.synthetic.main.activity_used_car.*
import kotlinx.android.synthetic.main.layout_pelunasan_maju.*
import kotlinx.android.synthetic.main.layout_success.view.*
import org.greenrobot.eventbus.EventBus
import java.text.SimpleDateFormat


class RepaymentDetailActivity : BaseApp(), RepaymentDetailView {
    companion object {
        val AGREEMENT_NUMBER = "aggreement_number"
        val LIST_AGREEMENT = "list_aggreement"
    }
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: RepaymentDetailPresenter
    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()
    private val myCalendar = Calendar.getInstance()
    private var choosedDate = ""

    private lateinit var conditionAdapter: ConditionAdapter

    private lateinit var alertDialogCustom: AlertDialogCustom
    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repayment_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.simulasi_pelunasan_maju)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        if (!intent.hasExtra(AGREEMENT_NUMBER)) {
            finish()
        }

        val agreementNumber = intent.getStringExtra(AGREEMENT_NUMBER)
        val parcel = intent?.extras?.getParcelableArray(LIST_AGREEMENT)
        val listAgreement: MutableList<PickerItem> = arrayListOf()
        choosedPicker.name = agreementNumber

        parcel?.forEach {
            listAgreement.add(it as PickerItem)
        }

        deps.inject(this)
        presenter.view = this

        presenter.getDetailFinancial(agreementNumber)

        btn_sisa_pinjaman.text = agreementNumber
        btn_sisa_pinjaman.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }

        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        conditionAdapter = ConditionAdapter()
        conditionAdapter.add("Perhitungan Simulasi Pelunasan Maju Per: Sabtu, 16 Maret 2019")
        conditionAdapter.add("Biaya-biaya yang muncul akan berubah setiap harinya")

        rv_term?.adapter = conditionAdapter
        rv_term?.isNestedScrollingEnabled = true
        rv_term?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_term?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                btn_sisa_pinjaman?.text = pickerItem.name
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)
                btn_sisa_pinjaman.text = choosedPicker.name
                presenter.getDetailFinancial(choosedPicker.name?:"")
                resetView()
                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        pickerAdapter.add(listAgreement)
        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter

        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()
        }

        val datePickerDialog = DatePickerDialog(this, date, myCalendar.get(Calendar.YEAR),
            myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH))
        datePickerDialog.datePicker.minDate = System.currentTimeMillis()

        et_date_of_birth.setOnClickListener {
            datePickerDialog.show()
        }

        et_date_of_birth.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (btn_send.visibility == View.GONE) {
                    presenter.getDetailPrepaymentSimulation(choosedPicker.name?:"", choosedDate, false)
                }
            }
        })

        btn_send.setOnClickListener {
            if (cb_term_condition?.isChecked == true) {
                if (choosedDate.isNotEmpty()) {
                    //et_date_of_birth.isEnabled = false

                    presenter.getDetailPrepaymentSimulation(choosedPicker.name?:"", choosedDate, false)
                } else {
                    Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.info_check_term_condition), Toast.LENGTH_LONG).show()
            }
        }

        btn_submission.setOnClickListener {
            if (cb_term_condition_2?.isChecked == true) {
                presenter.sendRequestPrepayment(choosedPicker.name?:"", choosedDate)
            } else {
                Toast.makeText(this, getString(R.string.info_check_term_condition), Toast.LENGTH_LONG).show()
            }
        }

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            finish()
        }
    }

    private fun updateLabel() {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        choosedDate = sdf.format(myCalendar.time)
        et_date_of_birth.setText(sdf.format(myCalendar.time).stringToDate("yyyy-MM-dd").dateToString(this.getMonths()))
    }

    override fun getResponsePrepayment(prepaymentRequest: PrepaymentRequest) {
        if (prepaymentRequest.status == ConfigVar.SUCCESS) {
            alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.prepayment_request_success)
            alertDialogCustom.show()
        } else {
            if (prepaymentRequest.message == "already submit") {
                alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.prepayment_request_double)
            }
            else {
                alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.prepayment_request_failed)
            }
            alertDialogCustom.show()
            //Toast.makeText(this, prepaymentRequest.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun getResponsePrepaymentSimulation(prePaymentSimulationResponse: PrePaymentSimulationResponse, isFirst: Boolean) {
        if (prePaymentSimulationResponse.status == ConfigVar.SUCCESS) {
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = id.andalan.andalanku.model.request.EventBus.LOAD_INBOX))
            if (!isFirst) {
                conditionAdapter = ConditionAdapter()
                conditionAdapter.add("Perhitungan Simulasi Pelunasan Maju Per: " + et_date_of_birth.text)
                conditionAdapter.add("Biaya-biaya yang muncul akan berubah setiap harinya")

                rv_term?.adapter = conditionAdapter
                rv_term?.isNestedScrollingEnabled = true
                rv_term?.layoutManager =
                    androidx.recyclerview.widget.LinearLayoutManager(
                        this,
                        androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                        false
                    )
                rv_term?.itemAnimator =
                    androidx.recyclerview.widget.DefaultItemAnimator()

                btn_send?.visibility = View.GONE
                info_text?.visibility = View.GONE
                cb_term_condition?.visibility = View.GONE
                til_container_term?.visibility = View.GONE
                container_button?.visibility = View.VISIBLE
                container_pelunasan_naju?.visibility = View.VISIBLE

                tv_total_pelunasan?.text = (prePaymentSimulationResponse.totalPelunasan?:"0").getNumberOnly().convertToRpFormat()
            }
            tv_value_sisa_pinjaman?.text = (prePaymentSimulationResponse.sisaPinjaman?:"0").getNumberOnly().convertToRpFormat()
        } else {
            Toast.makeText(this, prePaymentSimulationResponse.message, Toast.LENGTH_LONG).show()
        }
    }

    private fun resetView() {
        et_date_of_birth.isEnabled = true
        btn_send?.visibility = View.VISIBLE
        info_text?.visibility = View.VISIBLE
        cb_term_condition?.visibility = View.VISIBLE
        til_container_term?.visibility = View.VISIBLE
        container_button?.visibility = View.GONE
        container_pelunasan_naju?.visibility = View.GONE
    }

    override fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse) {
        tv_value_jatuh_kontrak?.text = detailFinanceResponse.getMaturityDateUI(this.getMonths())
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {

    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, "Gagal mendapatkan data", Toast.LENGTH_LONG).show()
    }
}
