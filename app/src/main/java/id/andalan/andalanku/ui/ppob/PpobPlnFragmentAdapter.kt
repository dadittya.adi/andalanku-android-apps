package id.andalan.andalanku.ui.ppob

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.andalan.andalanku.ui.account.BlankFragment
import id.andalan.andalanku.ui.account.MyAccountFragment
import id.andalan.andalanku.ui.cash_value.CashValueComingSoonFragment
import id.andalan.andalanku.ui.contract.ContractFragment
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment
import id.andalan.andalanku.ui.help.HelpFragment
import id.andalan.andalanku.ui.inbox.DetailMessageFragment
import id.andalan.andalanku.ui.inbox.InboxFragment
import id.andalan.andalanku.ui.installments.InstallmentScheduleFragment
import id.andalan.andalanku.ui.payment.InstallmentPaymentFragment
import id.andalan.andalanku.ui.plafond.PlafondFragment
import id.andalan.andalanku.ui.repayment.RepaymentSimulationFragment
import java.util.*

class PpobPlnFragmentAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return getFragments()[position]!!
    }

    override fun getCount(): Int {
        return getFragments().size
    }

    private fun getFragments(): Hashtable<Int, androidx.fragment.app.Fragment> {
        val ht = Hashtable<Int, androidx.fragment.app.Fragment>()
        ht[0] = PpobPlnPrepaidFragment()
        ht[1] = PpobPlnPostpaidFragment()
        return ht
    }
}
