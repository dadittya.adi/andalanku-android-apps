package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.AgreementDocumentResponse
import id.andalan.andalanku.model.response.DetailInsuranceResponse
import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.ui.base.BaseView

interface AgreementDocumentView: BaseView {
    fun getAgreementDocument(agreementDocumentResponse: AgreementDocumentResponse)
}