package id.andalan.andalanku.ui.otp

import id.andalan.andalanku.model.response.OtpVerificationResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class OtpVerificationPresenter @Inject constructor(private val service: Services): BasePresenter<OtpVerificationView>() {
    override fun onCreate() {
    }

    fun doVerificationOtp(otp: String) {
        view.showWait()
        val subscription = service.doVerificationOtp(otp).subscribe(object : Subscriber<OtpVerificationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(otpVerificationResponse: OtpVerificationResponse) {
                view.onGetResponse(otpVerificationResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getPersonalInfo() {
        view.showWait()
        val subscription = service.getPersonalInfo().subscribe(object : Subscriber<PersonalInformationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(personalInformationResponse: PersonalInformationResponse) {
                view.getProfileInformation(personalInformationResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }
}