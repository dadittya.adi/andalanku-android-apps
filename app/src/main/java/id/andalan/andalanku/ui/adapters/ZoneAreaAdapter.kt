package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Premi
import id.andalan.andalanku.model.ui.ZoneArea
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*

class ZoneAreaAdapter(val onItemClickListener: OnClickItem): BaseAdapter<ZoneArea, ZoneAreaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_default_text
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<ZoneArea>(itemView) {
        override fun bind(data: ZoneArea) {
            itemView.setOnClickListener {
                onItemClickListener.onClickPromo(data)
            }
            itemView.tv_name.text = data.name
        }
    }

    interface OnClickItem {
        fun onClickPromo(zoneArea: ZoneArea)
    }
}