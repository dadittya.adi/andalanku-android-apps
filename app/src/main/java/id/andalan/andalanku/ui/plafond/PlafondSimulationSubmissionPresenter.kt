package id.andalan.andalanku.ui.plafond

import id.andalan.andalanku.model.request.PlafondSimulationRequest
import id.andalan.andalanku.model.response.OccupationsResponse
import id.andalan.andalanku.model.response.PlafondSimulationResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PlafondSimulationSubmissionPresenter @Inject constructor(private val service: Services): BasePresenter<PlafondSubmissionSimulationView>() {
    override fun onCreate() {
        getOccupationList()
    }

    fun sendPlafondSimulation(simulationRequest: PlafondSimulationRequest) {
        view.showWait()
        val subscription = service.sendPlafondSimulation(simulationRequest)
            .subscribe(object : Subscriber<PlafondSimulationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(plafondSimulationResponse: PlafondSimulationResponse) {
                    view.onGetSimulationPlafond(plafondSimulationResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun getOccupationList() {
        view.showWait()
        val subscription = service.getOccupation()
            .subscribe(object : Subscriber<OccupationsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(occupationsResponse: OccupationsResponse) {
                    view.onGetOccupation(occupationsResponse.occupationList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }
}