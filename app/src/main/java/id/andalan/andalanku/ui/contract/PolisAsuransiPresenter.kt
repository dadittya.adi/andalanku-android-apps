package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailInsuranceResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PolisAsuransiPresenter @Inject constructor(private val service: Services): BasePresenter<PolisAsuransiView>() {
    override fun onCreate() {
    }

    private fun getDetailPaymentHistory(agreementNumber: String) {
        view.showWait()
        val subscription = service.getDetailInsurance(agreementNumber).subscribe(object : Subscriber<DetailInsuranceResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailInsuranceResponse: DetailInsuranceResponse) {
                view.getDetailPolis(detailInsuranceResponse)
            }
        })
        subscriptions.add(subscription)
    }
}