package id.andalan.andalanku.ui.newcar

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CarBrandsResponse
import id.andalan.andalanku.model.ui.Brands
import id.andalan.andalanku.model.ui.CarBrand
import id.andalan.andalanku.model.ui.CreditSimulationO
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.BrandsAdapter
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import kotlinx.android.synthetic.main.activity_payment_new_car.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class PaymentNewCarActivity : BaseApp(), PaymentNewCarView {
    companion object {
        val REQUEST_PAYMENT_SIMULATION = 941
        val RESPONSE_PAYMENT_SIMULATION = 942
    }

    private val TAG = PaymentNewCarActivity::class.java.simpleName

    private lateinit var brandsAdapter: BrandsAdapter

    @Inject
    lateinit var presenter: PaymentNewCarPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_new_car)
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.new_cars_payment)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        brandsAdapter = BrandsAdapter(object: BrandsAdapter.OnClickItem{
            override fun onClickImage(brands: Brands, carBrand: CarBrand) {
                val intent = Intent(this@PaymentNewCarActivity, CarChooserActivity::class.java)
                intent.putExtra(CarChooserActivity.BRANDS, brands)
                intent.putExtra(CarChooserActivity.CARBRANDS, carBrand)
                startActivityForResult(intent, CarChooserActivity.REQUEST_BRAND)
            }
        })

        val gridLayoutManager =
            androidx.recyclerview.widget.GridLayoutManager(this, 2)
        rv_merk_mobil?.layoutManager = gridLayoutManager
        rv_merk_mobil?.adapter = brandsAdapter
        rv_merk_mobil?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
    }

    override fun onGetCarBrands(carBrandsResponse: CarBrandsResponse) {
        if (carBrandsResponse.status == ConfigVar.SUCCESS) {
            carBrandsResponse.CarBrandList?.let {
                brandsAdapter.add(it)
            }
        } else {
            Toast.makeText(this@PaymentNewCarActivity, carBrandsResponse.status, Toast.LENGTH_LONG).show()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CarChooserActivity.REQUEST_BRAND && resultCode == CarChooserActivity.RESPONSE_BRAND) {
            val isApply = data?.extras?.getBoolean(PaymentSimulationActivity.APPLY, false)
            if (isApply == true) {
                val creditSimulation: CreditSimulationO? = data.extras?.getParcelable(PaymentSimulationActivity.CREDIT)
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.APPLY, true)
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                val collateral: Collateral? =  data.extras?.getParcelable(CompleteDocumentActivity.COLLATERAL)
                intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
                val creditSimulationRequest: CreditSimulationRequest? = data.extras?.getParcelable(CompleteDocumentActivity.CREDITRESQUEST)
                intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
                setResult(RESPONSE_PAYMENT_SIMULATION, intent)

                finish()
            }
        }
    }

    private fun reset() {

    }
}
