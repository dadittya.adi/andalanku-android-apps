package id.andalan.andalanku.ui.help

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.ui.about_us.AboutUsActivity
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.faq.FaqActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import kotlinx.android.synthetic.main.fragment_help.*
import kotlinx.android.synthetic.main.toolbar_login.*

class HelpFragment : androidx.fragment.app.Fragment() {

    private lateinit var menuMidAdapter: MenuAdapterProfile

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_help, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.help)

        menuMidAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.faq)) {
                    startActivity(Intent(context, FaqActivity::class.java))
                } else if (menuProfile.name == getString(R.string.about_us)) {
                    startActivity(Intent(context, AboutUsActivity::class.java))
                }
            }
        })

        rv_menu?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu?.adapter = menuMidAdapter
        rv_menu?.isNestedScrollingEnabled = false
        menuMidAdapter.add(MenuProfile(name = getString(R.string.faq)))
        menuMidAdapter.add(MenuProfile(name = getString(R.string.about_us)))
    }
}
