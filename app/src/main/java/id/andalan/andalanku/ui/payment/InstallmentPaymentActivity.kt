package id.andalan.andalanku.ui.payment

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast

import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.adapters.PickerAdapter
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import kotlinx.android.synthetic.main.fragment_installment_payment.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class InstallmentPaymentActivity : BaseApp(), InstallmentPaymentView {
    private val TAG = InstallmentPaymentActivity::class.java.simpleName

    @Inject
    lateinit var presenter: InstallmentPaymentPresenter
    private lateinit var agreementNumber: String
    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_installment_payment)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tv_title?.text = getString(R.string.pembayaran_angsuran)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }
        deps.inject(this)
        presenter.view = this

        presenter.onCreate()

        //toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
        et_choosed_contract_number.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                et_choosed_contract_number?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)

                agreementNumber = choosedPicker.name?:""

                // presenter.getPaymentDetail(agreementNumber)
                val intent = Intent(this@InstallmentPaymentActivity, InstallmentPaymentDetailActivity::class.java)
                //val intent = Intent(this, InstallmentPaymentDetailActivity::class.java)
                intent.putExtra(InstallmentPaymentDetailActivity.AGREEMENT_NUMBER, agreementNumber)
                startActivity(intent)

                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter
    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerAdapter.add(list)
            container_empty_state?.visibility = View.GONE
        } else {
            container_empty_state?.visibility = View.VISIBLE
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state?.visibility = View.VISIBLE
        Toast.makeText(this, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }

    override fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse) {
        if (paymentInquiryResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this, InstallmentPaymentDetailActivity::class.java)
                intent.putExtra(InstallmentPaymentDetailActivity.AGREEMENT_NUMBER, agreementNumber)
                startActivity(intent)
        }
        else {

            //alertWarningCustom.getView().tv_t?.text = "Peringatan"
            alertWarningCustom.getView().tv_message?.text = paymentInquiryResponse.message
            alertWarningCustom.show()
        }
    }
}
