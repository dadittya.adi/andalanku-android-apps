package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.response.CategoryComplainResponse
import id.andalan.andalanku.model.response.InputComplainResponse
import id.andalan.andalanku.model.response.InputMessageResponse
import id.andalan.andalanku.ui.base.BaseView

interface MessageReplyView: BaseView {
    fun onSuccessSendMessage(inputMessageResponse: InputMessageResponse)
}