package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.ui.base.BaseView

interface PpobPaymentView: BaseView {
    fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse)
}