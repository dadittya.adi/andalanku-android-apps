package id.andalan.andalanku.ui.transactionHistory

import id.andalan.andalanku.model.response.PpobTransactionDetailResponse
import id.andalan.andalanku.ui.base.BaseView

interface PpobTransactionDetailView: BaseView {
    fun onGetDetailResponse(ppobTransactionDetailResponse: PpobTransactionDetailResponse)
}