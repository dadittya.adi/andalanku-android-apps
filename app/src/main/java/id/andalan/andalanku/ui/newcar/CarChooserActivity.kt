package id.andalan.andalanku.ui.newcar

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CarTypesResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.CarsAdapter
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity.Companion.COLLATERAL
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.fragment_cars_chooser.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class CarChooserActivity : BaseApp(), CarChooserView {
    companion object {
        val BRANDS = "brands"
        val CARBRANDS = "carbrands"
        val REQUEST_BRAND = 921
        val RESPONSE_BRAND = 922
    }

    private val TAG = CarChooserActivity::class.java.simpleName
    private lateinit var carsAdapter: CarsAdapter

    @Inject
    lateinit var presenter: CarChooserPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_car_chooser)

        deps.inject(this)
        presenter.view = this

        val carBrand: CarBrand? = intent?.extras?.getParcelable(CARBRANDS)
        var brand = Brands()
        carBrand?.let {
            brand = carBrand.getBrands()
        }
        if (brand.isEmpty()) {
            finish()
        }

        brand.id?.let {
            presenter.getTypesCar(it)
        }

        loadImage(this, brand.urlImage?:"", iv_brands)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.choose_your_favorite_car)

        carsAdapter = CarsAdapter(object: CarsAdapter.OnClickItem {
            override fun onClickImage(carType: CarType, carBrand: CarBrand) {
                val intent = Intent(this@CarChooserActivity, CarsDetailActivity::class.java)
                intent.putExtra(CarsDetailActivity.CARS, carType)
                intent.putExtra(CARBRANDS, carBrand)
                startActivityForResult(intent, CarsDetailActivity.REQUEST_CAR)
            }
        }, carBrand!!)

        rv_cars?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_cars?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_cars?.adapter = carsAdapter
    }

    override fun onGetTypeCar(carTypesResponse: CarTypesResponse) {
        if (carTypesResponse.status == ConfigVar.SUCCESS) {
            carTypesResponse.carTypeList?.let {
                carsAdapter.add(it)
                tv_totals_car.text = getString(R.string.mobil_pilihan, it.size.toString())
            }
        } else {
            Toast.makeText(this@CarChooserActivity, carTypesResponse.status, Toast.LENGTH_LONG).show()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_LONG).show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CarsDetailActivity.REQUEST_CAR && resultCode == CarsDetailActivity.RESPONSE_CAR) {
            val isResimulate = data?.extras?.getBoolean(PaymentSimulationActivity.RESIMULATE, false)
            if (isResimulate == true) {
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.RESIMULATE, true)
                setResult(RESPONSE_BRAND, intent)
                finish()
            }
            val isApply = data?.extras?.getBoolean(PaymentSimulationActivity.APPLY, false)
            if (isApply == true) {
                val creditSimulation: CreditSimulationO? = data.extras?.getParcelable(PaymentSimulationActivity.CREDIT)
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.APPLY, true)
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                val collateral: Collateral? =  data.extras?.getParcelable(COLLATERAL)
                intent.putExtra(COLLATERAL, collateral)
                val creditSimulationRequest: CreditSimulationRequest? = data.extras?.getParcelable(CompleteDocumentActivity.CREDITRESQUEST)
                intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
                setResult(RESPONSE_BRAND, intent)

                finish()
            }
        }
    }
}
