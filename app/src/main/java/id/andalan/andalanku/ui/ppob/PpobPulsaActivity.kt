package id.andalan.andalanku.ui.ppob

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.tabs.TabLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PpobPulsaDataAdapter
import id.andalan.andalanku.ui.viewmodel.ViewModelPpob
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import kotlinx.android.synthetic.main.activity_ppob_pulsa.*
import kotlinx.android.synthetic.main.fragment_ppob_pulsa_data.*
import kotlinx.android.synthetic.main.fragment_ppob_pulsa_pulsa.*
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobPulsaActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
        val REQUEST_PICK_CONTACT = 100
    }

    var biller:String = ""
    var accountNumber = ""
    var category = ""
    var productLogo = ""

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom
    private lateinit var ppobPulsaFragmentAdapter: PpobPulsaFragmentAdapter
    lateinit var ppobPulsaDataAdapter: PpobPulsaDataAdapter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter

    lateinit var model: ViewModelPpob

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_pulsa)

        deps.inject(this)
        model = ViewModelProviders.of(this).get(ViewModelPpob::class.java)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tv_title?.text = getString(R.string.isi_ulang_ponsel)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        category = intent.getStringExtra(CATEGORY)

        et_ppob_pulsa_phone_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val phoneNumber = et_ppob_pulsa_phone_number.text.toString()
                detectOperator(phoneNumber)
            }
            false
        }

        //ppobPulsaFragmentAdapter = PpobPulsaFragmentAdapter(supportFragmentManager)
        ppobPulsaFragmentAdapter = PpobPulsaFragmentAdapter(supportFragmentManager)
        container.adapter = ppobPulsaFragmentAdapter

        if (category == "data") {
            container.setCurrentItem(1, false)
        }

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })

        container.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                container.setCurrentItem(position, true)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

        ic_ppob_pulsa_phonebook.setOnClickListener {
            loadContacts()
        }

        ic_ppob_pulsa_clear_button.setOnClickListener {
            et_ppob_pulsa_phone_number.setText("")
            accountNumber = ""
            img_ppob_pulsa_operator_logo.visibility = View.GONE

            rv_ppob_pulsa_pulsa.apply {
                layoutManager = GridLayoutManager(context, 2)
                adapter = null
            }

            rv_ppob_pulsa_data.apply {
                layoutManager = GridLayoutManager(context, 1)
                adapter = null
            }
        }

        label_pascabayar.setOnClickListener {
            val intent = Intent(this, PpobPostpaidActivity::class.java)
            startActivity(intent)
        }

        ppobPulsaDataAdapter = PpobPulsaDataAdapter(object : PpobPulsaDataAdapter.OnClickItem {
            override fun onClickProduct(ppobProduct: PpobProduct) {
                val accountNumber = accountNumber

                if (accountNumber == "") {
                    alertWarningCustom.getView().tv_message?.text = "Nomor Ponsel Belum Diisi"
                    alertWarningCustom.show()
                }
                else {
                    productLogo = ppobProduct.logo.toString()
                    ppobProduct.code?.let {
                        presenter.ppobinquiry(
                            accountNumber, it,"top_up",
                            "",""
                        )
                    }
                }
            }

        })
    }

    private fun detectOperator(phoneNumber: String) {

        val standardPhoneNumber = phoneNumber.replace("+62", "0")
        et_ppob_pulsa_phone_number.setText(standardPhoneNumber)

        if (standardPhoneNumber.length >= 10) {
            val prefix = standardPhoneNumber.take(4)


            if ((prefix == "0811") or
                (prefix == "0812") or
                (prefix == "0813") or
                (prefix == "0821") or
                (prefix == "0822") or
                (prefix == "0823") or
                (prefix == "0851") or
                (prefix == "0852") or
                (prefix == "0853")
            ) {

                biller = "Telkomsel"

                img_ppob_pulsa_operator_logo.setImageResource(R.drawable.telkomsel_logo)
                img_ppob_pulsa_operator_logo.visibility = View.VISIBLE
            } else if ((prefix == "0814") or
                (prefix == "0815") or
                (prefix == "0816") or
                (prefix == "0855") or
                (prefix == "0856") or
                (prefix == "0857") or
                (prefix == "0858")
            ) {

                biller = "Indosat"

                img_ppob_pulsa_operator_logo.setImageResource(R.drawable.indosat_logo)
                img_ppob_pulsa_operator_logo.visibility = View.VISIBLE

            } else if ((prefix == "0817") or
                (prefix == "0818") or
                (prefix == "0819") or
                (prefix == "0859") or
                (prefix == "0877") or
                (prefix == "0878")
            ) {

                biller = "XL"

                img_ppob_pulsa_operator_logo.setImageResource(R.drawable.xl_logo)
                img_ppob_pulsa_operator_logo.visibility = View.VISIBLE
            } else if ((prefix == "0831") or
                (prefix == "0832") or
                (prefix == "0833") or
                (prefix == "0838")
            ) {

                biller = "Axis"

                img_ppob_pulsa_operator_logo.setImageResource(R.drawable.axis_logo)
                img_ppob_pulsa_operator_logo.visibility = View.VISIBLE
            } else if ((prefix == "0895") or
                (prefix == "0896") or
                (prefix == "0897") or
                (prefix == "0898") or
                (prefix == "0899")
            ) {

                biller = "Three"
                img_ppob_pulsa_operator_logo.setImageResource(R.drawable.tri_logo)
                img_ppob_pulsa_operator_logo.visibility = View.VISIBLE
            } else if ((prefix == "0881") or
                (prefix == "0882") or
                (prefix == "0883") or
                (prefix == "0884") or
                (prefix == "0885") or
                (prefix == "0886") or
                (prefix == "0887") or
                (prefix == "0888") or
                (prefix == "0889")
            ) {

                biller = "Smartfren"

                img_ppob_pulsa_operator_logo.setImageResource(R.drawable.smartfren_logo)
                img_ppob_pulsa_operator_logo.visibility = View.VISIBLE
            } else {
                img_ppob_pulsa_operator_logo.visibility = View.GONE
                biller = ""
                alertWarningCustom.getView().tv_message?.text = "Nomor Handphone Tidak Dikenali"
                alertWarningCustom.show()
            }

            if (biller != "") {
                presenter.getPpobProductList("Pulsa", biller)
            }

            accountNumber = phoneNumber
        }
        else {
            img_ppob_pulsa_operator_logo.visibility = View.GONE
            biller = ""
            alertWarningCustom.getView().tv_message?.text = "Nomor Handphone Minimal 10 digit"
            alertWarningCustom.show()
        }
    }
    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        model.currentPpobProduct.value = ppobProductListResponse
        presenter.getPpobProductList2("Paket Data", biller)
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {
        model.currentPpobProduct2.value = ppobProductListResponse

        ppobPulsaDataAdapter.removeAll()
        ppobProductListResponse.ppobProduct?.let { ppobPulsaDataAdapter.add(it) }

        rv_ppob_pulsa_data.apply {
            layoutManager = GridLayoutManager(context, 1)
            adapter = ppobPulsaDataAdapter
        }
    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {
        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER,
                ppobInquiryResponse.accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, ppobInquiryResponse.productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, ppobInquiryResponse.productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, ppobInquiryResponse.paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, ppobInquiryResponse.amount)
            removeWait()
            startActivity(intent)
        }
        else {
            removeWait()
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }

    private fun loadContacts() {
        var builder = StringBuilder()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSIONS_REQUEST_READ_CONTACTS
            )
            //callback onRequestPermissionsResult
        } else {
            val intent =
                Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
            startActivityForResult(intent, REQUEST_PICK_CONTACT)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts()
            } else {
                //  toast("Permission must be granted in order to display contacts information")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_PICK_CONTACT && data?.data != null) {
            val contactUri = data.data
            val cursor = contentResolver.query(contactUri!!, null, null, null, null)
            if (cursor!!.moveToFirst()) {
                var phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

                phoneNumber = phoneNumber.replace("+62", "0")
                val re = Regex("[^0-9]")
                phoneNumber = phoneNumber.let { re.replace(it, "") }
                et_ppob_pulsa_phone_number.setText(phoneNumber)
                detectOperator(phoneNumber)
            }

        }

    }
}