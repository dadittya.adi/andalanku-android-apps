package id.andalan.andalanku.ui.ppob

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.ArrayStaticAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailBillAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailCustomersAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailProductAdapter
import id.andalan.andalanku.ui.viewmodel.ViewModelPpob
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.activity_ppob_bpjs.*
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.ic_close
import kotlinx.android.synthetic.main.layout_bill_detail.img_cd_ppob_payment_review_product_logo
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import javax.inject.Inject


class PpobBpjsActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
        val REQUEST_PICK_CONTACT = 100
    }

    var biller:String = ""
    var accountNumber = ""
    var logo: String = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    private var periodeList: MutableList<String> =  arrayListOf<String>("1 Bulan","2 Bulan","3 Bulan", "4 Bulan", "5 Bulan", "6 Bulan", "7 Bulan","8 Bulan","9 Bulan", "10 Bulan", "11 Bulan", "12 Bulan")
    private var choosedPeriode: String = ""
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom

    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter
    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter
    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter
    lateinit var periodeAdapter: ArrayStaticAdapter

    lateinit var model: ViewModelPpob

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_bpjs)

        deps.inject(this)
        model = ViewModelProviders.of(this).get(ViewModelPpob::class.java)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tv_title?.text = getString(R.string.bpjs_kesehatan)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        ic_ppob_postpaid_clear_button.setOnClickListener {
            et_ppob_bpjs_account_number.setText("")
            accountNumber = ""
        }

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        btn_ppob_postpaid_show_bill.setOnClickListener {
            if (et_ppob_bpjs_account_number.text.toString() != "") {
                val accountNumber = et_ppob_bpjs_account_number.text.toString()
                presenter.ppobinquiry(accountNumber, "BPJSKES", "Tagihan", "", choosedPeriode)
            }
            else {
                biller = ""
                alertWarningCustom.getView().tv_message?.text = "Nomor ID Pelanggan Belum Diisi"
                alertWarningCustom.show()
            }
        }

        btn_bill_detail_payment.setOnClickListener {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, logo)
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }

        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

        periodeAdapter = ArrayStaticAdapter(object: ArrayStaticAdapter.OnClickItem{
            override fun onClick(periode: String) {
                tv_ppob_bpjs_periode?.text = periode
                choosedPeriode = periode
                val re = Regex("[^0-9]")
                choosedPeriode = periode.let { re.replace(it, "") }
                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        periodeAdapter.add(periodeList)

        ic_ppob_bpjs_id_down.setOnClickListener {
            loadPeriodList()
        }

    }

    fun loadPeriodList() {
        tv_bill_detail_tv_title_text?.text = getString(R.string.choose_periode)
        container_bill_detail_bills.visibility = View.GONE
        container_bill_detail_input.visibility = View.GONE
        container_static_option.visibility = View.VISIBLE
        rv_ppob_bill_detail_option_item.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_ppob_bill_detail_option_item.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_ppob_bill_detail_option_item.adapter = periodeAdapter

        print(periodeAdapter)
        rv_ppob_bill_detail_option_item.visibility = View.VISIBLE
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {
            container_bill_detail_bills.visibility = View.VISIBLE
            container_static_option.visibility = View.GONE
            logo = "bpjs"
            productName = ppobInquiryResponse.productName.toString()
            accountNumber = ppobInquiryResponse.accountNumber.toString()
            tv_bill_detail_tv_title_text?.text = getString(R.string.bpjs_kesehatan)
            tv_bill_detail_header_name?.text = "${preferencesUtil.getUser()?.customerName},"

            rv_ppob_bill_detail_customers.apply {
                layoutManager = GridLayoutManager(context, 1)
                ppobBillDetailCustomersAdapter.removeAll()
                ppobInquiryResponse.listBillCustomerDetails?.let {
                    ppobBillDetailCustomersAdapter.add(
                        it
                    )
                }
                adapter = ppobBillDetailCustomersAdapter
            }

            rv_ppob_bill_detail_product.apply {
                layoutManager = GridLayoutManager(context, 1)
                ppobBillDetailProductAdapter.removeAll()
                ppobInquiryResponse.listBillProductDetails?.let {
                    ppobBillDetailProductAdapter.add(
                        it
                    )
                }
                adapter = ppobBillDetailProductAdapter
            }

            rv_ppob_bill_detail_bills.apply {
                layoutManager = GridLayoutManager(context, 1)
                ppobBillDetailBillAdapter.removeAll()
                ppobInquiryResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                adapter = ppobBillDetailBillAdapter
            }

            tv_bill_detail_amount_total?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()
            tv_bill_detail_amount_total_payment?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()

            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            totalAmount = ppobInquiryResponse.amount.toString()

            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }


}