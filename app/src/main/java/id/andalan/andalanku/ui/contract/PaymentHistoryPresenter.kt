package id.andalan.andalanku.ui.contract

import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PaymentHistoryPresenter @Inject constructor(private val service: Services): BasePresenter<PaymentHistoryView>() {
    override fun onCreate() {
    }

    private fun getDetailPaymentHistory(agreementNumber: String) {
        view.showWait()
        val subscription = service.getDetailPaymentHistory(agreementNumber).subscribe(object : Subscriber<DetailPaymentHistoryResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailPaymentHistoryResponse: DetailPaymentHistoryResponse) {
                view.getPaymentHistory(detailPaymentHistoryResponse)
            }
        })
        subscriptions.add(subscription)
    }
}