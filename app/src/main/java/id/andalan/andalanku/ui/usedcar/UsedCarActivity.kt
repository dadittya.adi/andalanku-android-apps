package id.andalan.andalanku.ui.usedcar

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.e
import android.view.View
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.CarsTypeAdapter
import id.andalan.andalanku.ui.adapters.PremiAdapter
import id.andalan.andalanku.ui.adapters.TenorAdapter
import id.andalan.andalanku.ui.adapters.ZoneAreaAdapter
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.ui.newcar.CarsDetailPresenter
import id.andalan.andalanku.ui.newcar.CarsDetailView
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import id.andalan.andalanku.utils.*
import id.andalan.andalanku.utils.ekstensions.setHeight
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_used_car.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import java.util.*
import javax.inject.Inject
import android.app.Activity
//import android.support.design.widget.TextInputLayout
import android.view.LayoutInflater
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class UsedCarActivity : BaseApp(), CarsDetailView {
    companion object {
        val REQUEST_USED_CAR = 951
        val RESPONSE_USED_CAR = 952
    }
    private val TAG = UsedCarActivity::class.java.simpleName

    private lateinit var carsTypeAdapter: CarsTypeAdapter

    private lateinit var tenorAdapter: TenorAdapter
    private lateinit var premiAdapter: PremiAdapter
    private lateinit var zoneAreaAdapter: ZoneAreaAdapter

    private var choosedTenor: Tenor? = null
    private var choosedPremi: Premi? = null
    private var choosedZone: ZoneArea? = null
    private var choosedUsedCar: UsedCar? = null

    private lateinit var creditSimulationRequest: CreditSimulationRequest
    private lateinit var collateral: Collateral
    private var isDpEdited = false
    private var isDpPriceEdited = false

    private val listErrorForm: MutableList<TILExtension> = arrayListOf()

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: CarsDetailPresenter

    private lateinit var alertDialogProgress: AlertDialogCustom


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_used_car)
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.cars_used_payment)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        deps.inject(this)
        presenter.view = this

        carsTypeAdapter = CarsTypeAdapter(object: CarsTypeAdapter.OnClickItem{
            override fun onClickType(cars: UsedCar) {
                et_type?.setText(cars.AssetName)
                choosedUsedCar = cars
//                et_price_car?.setText(choosedUsedCar?.Price?.toLong()?.convertToRpFormat())
                hideSlideLayout()
            }
        })
        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }
        val height = resources.getDimension(R.dimen.height_drag_view_content_400)
        container_content_drag_view.setHeight(height.toInt())

        et_year_car?.setText(Date().dateToString("yyyy"))

        et_drag_view?.setOnEditorActionListener { _, i, _ ->
            return@setOnEditorActionListener if (i == EditorInfo.IME_ACTION_SEARCH) {
                presenter.getUsedCarList(et_year_car.text.toString(), et_drag_view.text.toString())
                true
            } else {
                false
            }
        }

        et_type?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.what_is_your_fav_car)
            et_drag_view.hint = getString(R.string.write_search)
            et_drag_view.visibility = View.VISIBLE
            rv_drag_view.adapter = carsTypeAdapter
            showSlideLayout()
        }

        et_tenor?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_tenor)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = tenorAdapter
            showSlideLayout()
        }

        et_premi?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_insurance)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = premiAdapter
            showSlideLayout()
        }

        et_zone?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.choose_area_zone)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = zoneAreaAdapter
            showSlideLayout()
        }

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        tenorAdapter = TenorAdapter(object: TenorAdapter.OnClickItem{
            override fun onClickPromo(tenor: Tenor) {
                choosedTenor = tenor
                et_tenor?.setText(tenor.getYear(getString(R.string.year)))
                hideSlideLayout()
            }
        })

        premiAdapter = PremiAdapter(object: PremiAdapter.OnClickItem{
            override fun onClickPromo(premi: Premi) {
                choosedPremi = premi
                et_premi?.setText(premi.name)
                hideSlideLayout()
            }
        })

        zoneAreaAdapter = ZoneAreaAdapter(object: ZoneAreaAdapter.OnClickItem{
            override fun onClickPromo(zoneArea: ZoneArea) {
                choosedZone = zoneArea
                et_zone?.setText(zoneArea.name)
                hideSlideLayout()
            }

        })

        et_percent?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if (et_price_car.text.isNotEmpty()) {
                    if (isDpEdited && !isDpPriceEdited) {
                        val numString = s.toString()

                        if (numString.isNotEmpty()) {
                            val dp = numString.toDouble()
                            if (dp <= 100) {
                                val num = et_price_car.text.toString().getNumberOnly()
                                val dpPrice = num.getValue(dp)
                                et_dp_price.setText(dpPrice.convertToRpFormat())
                            } else {
                                et_percent.setText(100.toString())
                            }
                        } else {
                            et_dp_price.setText("")
                        }
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        et_percent.setOnFocusChangeListener { _, hasFocus ->
            isDpEdited = hasFocus
        }

        et_price_car.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    e(TAG, price.toString())
                    if (et_price_car.text.toString() != price.convertToRpFormat()) et_price_car.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_price_car.setSelection(text.length - 2)
                }
            }

        })

        et_dp_price.setOnFocusChangeListener { _, hasFocus ->
            isDpPriceEdited = hasFocus
        }

        et_dp_price.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(s: Editable?) {
                if (!isDpEdited && isDpPriceEdited) {
                    val numString = s.toString()
                    val otr = et_price_car.text.toString()
                    if (numString.isNotEmpty()) {
                        val pricee = numString.getNumberOnly()
                        if (pricee <= (choosedUsedCar?.Price?:"0").getNumberOnly()) {
                            if (et_dp_price.text.toString() != pricee.convertToRpFormat()) et_dp_price.setText(pricee.convertToRpFormat())
                            val text = pricee.convertToRpFormat()
                            et_dp_price.setSelection(text.length - 2)
                            et_percent.setText(pricee.getPercentage((otr).getNumberOnly()))
                        } else {
                            if (et_dp_price.text.toString() != (otr).getNumberOnly().convertToRpFormat())
                                et_dp_price.setText((otr).getNumberOnly().convertToRpFormat())
                            val text = (otr).getNumberOnly().convertToRpFormat()
                            et_dp_price.setSelection(text.length - 2)
                            et_percent.setText((otr).getNumberOnly().getPercentage((otr).getNumberOnly()))
                        }
                    }
                }
            }
        })


        btn_calc_it.setOnClickListener {
            if (choosedUsedCar == null) {
                showError()

                return@setOnClickListener
            }
            val dpText = et_percent.text.toString()
            val dpPrice = dpText.getNumberOnly()
            val otrText = et_price_car.text.toString()
            val otrPrice = otrText.getNumberOnly()
            val tenor = choosedTenor?.id?:0
            val premi = choosedPremi?.type?:""
            val zone = choosedZone?.id?:0
            creditSimulationRequest = CreditSimulationRequest(
                type = ConfigVar.USED_CARS,
                otr = otrPrice.toString(),
                dp = dpPrice.toString(),
                tenor = tenor.toString(),
                asuransi = premi,
                wilayah = zone.toString()
            )
            choosedUsedCar?.let {
                creditSimulationRequest.usedCar = it
            }
            collateral = Collateral(
                collateralType = ConfigVar.BPKB,
                collateral = et_type.text.toString(),
                collateralYear = et_year_car.text.toString(),
                collateralOwner = preferencesUtil.getUser()?.customerName,
                collateralPaperNumber = "-",
                assetCode = choosedUsedCar?.AssetCode?:""
            )
            if (creditSimulationRequest.isValid()) {
                toggleError(list = listErrorForm, state = false)
                presenter.getCreditSimulation(creditSimulationRequest)
            } else {
                showError()
                Toast.makeText(this@UsedCarActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }

        initFakeData()
    }

    private fun initFakeData() {
        tenorAdapter.add(Tenor(id = 3))
        tenorAdapter.add(Tenor(id = 6))
        tenorAdapter.add(Tenor(id = 12))
        tenorAdapter.add(Tenor(id = 18))
        tenorAdapter.add(Tenor(id = 24))
        tenorAdapter.add(Tenor(id = 30))
        tenorAdapter.add(Tenor(id = 36))
        tenorAdapter.add(Tenor(id = 48))

        premiAdapter.add(Premi(id = 1, type = getString(R.string.tlo), name = getString(R.string.tlo)))
        premiAdapter.add(Premi(id = 2, type = getString(R.string.ark), name = getString(R.string.all_risk)))

        zoneAreaAdapter.add(ZoneArea(id = 1, name = getString(R.string.zone_1)))
        zoneAreaAdapter.add(ZoneArea(id = 2, name = getString(R.string.zone_2)))
        zoneAreaAdapter.add(ZoneArea(id = 3, name = getString(R.string.zone_3)))
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        clearAllFocus()
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
    }

    private fun clearAllFocus() {
        hideKeyboard(this@UsedCarActivity)
    }

    override fun onGetUsedCarsList(usedCarListResponse: UsedCarListResponse) {
        if (usedCarListResponse.status == ConfigVar.SUCCESS) {
            usedCarListResponse.carPriceList?.let {
                if (it.size > 0) {
                    carsTypeAdapter.add(it)
                } else {
                    Toast.makeText(this, "Daftar mobil Kosong", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            Toast.makeText(this, "Gagal Mendapatkan Daftar Mobil", Toast.LENGTH_SHORT).show()
        }
        hideKeyboard(this)
    }

    override fun onGetCreditSimulation(creditSimulationResponse: CreditSimulationResponse) {
        if (creditSimulationResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this@UsedCarActivity, PaymentSimulationActivity::class.java)
            intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulationResponse)
            intent.putExtra(PaymentSimulationActivity.IS_USED_CAR, true)
            intent.putExtra(PaymentSimulationActivity.YEAR_CAR, et_year_car.text.toString())
            intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
            intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
            startActivityForResult(intent, PaymentSimulationActivity.REQUEST_PAYMENT)
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }
                override fun onClickOk() {
                }
            }, this, getString(R.string.cars_used_payment), creditSimulationResponse.message?:"")
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            super.onBackPressed()
        }
    }
    override fun onGetCarTrim(carTrimsResponse: CarTrimsResponse) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PaymentSimulationActivity.REQUEST_PAYMENT && resultCode == PaymentSimulationActivity.RESPONSE_PAYMENT) {
            val isResimulate = data?.extras?.getBoolean(PaymentSimulationActivity.RESIMULATE, false)
            if (isResimulate == true) {
            }
            val isApply = data?.extras?.getBoolean(PaymentSimulationActivity.APPLY, false)

            if (isApply == true) {
                val creditSimulation: CreditSimulationO? = data.extras?.getParcelable(PaymentSimulationActivity.CREDIT)
                val intent = Intent()
                intent.putExtra(PaymentSimulationActivity.APPLY, true)
                intent.putExtra(PaymentSimulationActivity.CREDIT, creditSimulation)
                intent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
                intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, creditSimulationRequest)
                setResult(RESPONSE_USED_CAR, intent)
                finish()
            }
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun showError() {
        listErrorForm.clear()
        if (et_type?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_type_car, "Form unit masih kosong"))
        if (et_price_car?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_price_car, "Form harga masih kosong"))
        if (et_percent?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_dp_price, "Form uang muka masih kosong"))
        if (et_dp_price?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_dp_price, "Form uang muka masih kosong"))
        if (et_tenor?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_tenor, "Form tenor masih kosong"))
        if (et_premi?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_insurance_type, "Form tipe asuransi masih kosong"))
        if (et_zone?.text.isNullOrBlank()) listErrorForm.add(TILExtension(til_zone_type, "Form zona masih kosong"))

        toggleError(list = listErrorForm, state = true)
        hideError()
    }

    private fun toggleError(list: MutableList<TILExtension>, state: Boolean) {
        if (state) {
            list.forEach {
                it.til.error = it.error
                it.til.isErrorEnabled = true
            }
        } else {
            list.forEach {
                it.til.error = ""
                it.til.isErrorEnabled = false
            }
        }
    }

    private fun hideError() {
        GlobalScope.launch(Dispatchers.Main) {
            delay(2000)
            toggleError(listErrorForm, false)
        }
    }
}
