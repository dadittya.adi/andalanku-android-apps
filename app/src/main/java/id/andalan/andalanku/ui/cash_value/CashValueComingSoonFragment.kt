package id.andalan.andalanku.ui.cash_value

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.adapters.MenuAdapterHome
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import kotlinx.android.synthetic.main.toolbar_login.*

class CashValueComingSoonFragment : androidx.fragment.app.Fragment() {
    private lateinit var menuAdapterHome: MenuAdapterHome
    private lateinit var pickerContractAdapter: PickerAdapter
    private var choosedContract: PickerItem? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.activity_cash_value_coming_soon, container, false)
        return inflater.inflate(R.layout.activity_cash_value_coming_soon, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = "Cash Value"
        toolbar_login.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
    }

}