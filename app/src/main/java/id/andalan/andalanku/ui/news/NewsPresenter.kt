package id.andalan.andalanku.ui.news

import android.util.Log.e
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.NewsResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class NewsPresenter @Inject constructor(private val service: Services): BasePresenter<NewsView>() {
    override fun onCreate() {
        getListNews(1, "")
    }

    fun getListNews(page: Int, keyword: String) {
        view.showWait()
        val subscription = service.getNews(page, keyword).subscribe(object : Subscriber<NewsResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(newsResponse: NewsResponse) {
                if (newsResponse.status == ConfigVar.SUCCESS) {
                    view.onGetListNews(newsResponse.newsList?: arrayListOf())
                } else {
                    view.onFailure(newsResponse.status?:"")
                }
            }
        })
        subscriptions.add(subscription)
    }
}