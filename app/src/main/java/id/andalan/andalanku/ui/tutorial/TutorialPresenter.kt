package id.andalan.andalanku.ui.tutorial

import id.andalan.andalanku.model.response.FaqResponse
import id.andalan.andalanku.model.response.TutorialResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class TutorialPresenter @Inject constructor(private val service: Services): BasePresenter<TutorialView>() {
    override fun onCreate() {
        getListFaq()
    }

    private fun getListFaq() {
        view.showWait()
        val subscription = service.getListTutorial()
            .subscribe(object : Subscriber<TutorialResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(tutorialResponse: TutorialResponse) {
                    view.getTutorialist(tutorialResponse)
                }
            })
        subscriptions.add(subscription)
    }
}