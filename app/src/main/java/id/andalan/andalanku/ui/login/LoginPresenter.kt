package id.andalan.andalanku.ui.login

import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.model.response.LoginResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import id.andalan.andalanku.ui.pin.CreatePinView
import rx.Subscriber
import javax.inject.Inject

class LoginPresenter @Inject constructor(private val service: Services): BasePresenter<LoginView>() {
    override fun onCreate() {
    }

    fun doLogin(email: String, password: String) {
        view.showWait()
        val subscription = service.doLogin(email, password).subscribe(object : Subscriber<LoginResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(loginResponse: LoginResponse) {
                view.onGetResponse(loginResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getPersonalInfo() {
        view.showWait()
        val subscription = service.getPersonalInfo().subscribe(object : Subscriber<PersonalInformationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(personalInformationResponse: PersonalInformationResponse) {
                view.getProfileInformation(personalInformationResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }
}