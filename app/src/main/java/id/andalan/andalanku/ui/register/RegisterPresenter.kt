package id.andalan.andalanku.ui.register

import id.andalan.andalanku.model.request.RegisterRequest
import id.andalan.andalanku.model.response.RegisterResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class RegisterPresenter @Inject constructor(private val service: Services): BasePresenter<RegisterView>() {
    override fun onCreate() {
    }

    fun doRegister(registerRequest: RegisterRequest) {
        view.showWait()
        val subscription = service.doRegister(registerRequest).subscribe(object : Subscriber<RegisterResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(registerResponse: RegisterResponse) {
                view.onGetResponse(registerResponse)
            }
        })
        subscriptions.add(subscription)
    }
}