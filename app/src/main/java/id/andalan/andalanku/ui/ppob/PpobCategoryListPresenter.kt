package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.PpobGroupResponse
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PpobCategoryListPresenter @Inject constructor(private val service: Services): BasePresenter<PpobCategoryListView>(){
    override fun onCreate() {
        TODO("Not yet implemented")
    }

    fun getLastPurchase() {
        view.showWait()
        val subscription = service.PpobGroup().subscribe(object : Subscriber<PpobGroupResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")

            }

            override fun onNext(ppobGroupResponse: PpobGroupResponse) {
                view.removeWait()
                view.onGetLastPurchaseResponse(ppobGroupResponse)
            }
        })
        subscriptions.add(subscription)
    }

}