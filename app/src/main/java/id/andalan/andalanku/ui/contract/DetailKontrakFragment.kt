package id.andalan.andalanku.ui.contract

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer

import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.DetailAndalanFinancialResponse
import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.fragment_detail_kontrak.*

class   DetailKontrakFragment : androidx.fragment.app.Fragment(), DetailKontrakView {
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_kontrak, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //et_harga_otr.setText("Foooooo")
        activity?.let {
            val observerResponse = Observer<DetailAndalanFinancialResponse> {
                // Update the UI, in this case, a TextView.
                it?.andalanDetailFinancial?.let {list ->
                    if (list.isNotEmpty()) {
                        val detailFinanceResponse = list[0]
                        onGetDetailContract(detailFinanceResponse)
                    }
                }
            }
            (activity as ContractDetailActivity).model.currentAndalanFinancialResponse.observe(this, observerResponse)
        }
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            activity?.let {
                onGetDetailContract((activity as ContractDetailActivity).detailFinanceResponse?:DetailFinanceResponse())
            }
        }
    }

    override fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse) {
        et_harga_otr?.setText("FOOOOBARRRR")
        val otr = (if (detailFinanceResponse.otr.isNullOrEmpty()) "0" else detailFinanceResponse.otr).toLong().convertToRpFormat()
        et_harga_otr?.setText(otr)
        val downPayment = (if (detailFinanceResponse.downPayment.isNullOrEmpty()) "0" else detailFinanceResponse.downPayment).toLong().convertToRpFormat()
        et_uang_muka?.setText(downPayment)
        val insuranceCapitalized = (if (detailFinanceResponse.insuranceCapitalized.isNullOrEmpty()) "0" else detailFinanceResponse.insuranceCapitalized).toLong().convertToRpFormat()
        et_asuransi?.setText(insuranceCapitalized)
        val netFinance = (if (detailFinanceResponse.netFinance.isNullOrEmpty()) "0" else detailFinanceResponse.netFinance).toLong().convertToRpFormat()
        et_nilai_pembiayaan?.setText(netFinance)
        context?.let {
            et_tanggal_kontrak?.setText(detailFinanceResponse.getAgreementDateUI(it.getMonths()))
            et_tanggal_berlaku?.setText(detailFinanceResponse.getEffectiveDateUI(it.getMonths()))
            //et_jatuh_tempo?.setText(detailFinanceResponse.getMaturityDateUI(it.getMonths()))
            et_jatuh_tempo?.setText(detailFinanceResponse.getNextDueDateUI(it.getMonths()))
        }
        et_jangka_waktu_tenor?.setText(detailFinanceResponse.tenor + " bulan")
        val installmentAmount = (if (detailFinanceResponse.installmentAmount.isNullOrEmpty()) "0" else detailFinanceResponse.installmentAmount).toLong().convertToRpFormat()
        et_jumlah_angsuran?.setText(installmentAmount)
    }

    fun fillData(content: String) {
        et_harga_otr?.setText(content)
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
