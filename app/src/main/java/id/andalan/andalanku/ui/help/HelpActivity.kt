package id.andalan.andalanku.ui.help

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.faq.FaqActivity
import id.andalan.andalanku.ui.about_us.AboutUsActivity
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import kotlinx.android.synthetic.main.activity_help.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class HelpActivity : BaseApp() {

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var menuMidAdapter: MenuAdapterProfile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.help)

        menuMidAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.faq)) {
                    startActivity(Intent(this@HelpActivity, FaqActivity::class.java))
                } else if (menuProfile.name == getString(R.string.about_us)) {
                    startActivity(Intent(this@HelpActivity, AboutUsActivity::class.java))
                }
            }
        })

        rv_menu?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu?.adapter = menuMidAdapter
        rv_menu?.isNestedScrollingEnabled = false
        menuMidAdapter.add(MenuProfile(name = getString(R.string.faq)))
        menuMidAdapter.add(MenuProfile(name = getString(R.string.about_us)))
    }
}
