package id.andalan.andalanku.ui.password

import id.andalan.andalanku.model.response.ChangePasswordResponse
import id.andalan.andalanku.ui.base.BaseView

interface UpdatePasswordView: BaseView {
    fun onChangePassword(changePasswordResponse: ChangePasswordResponse)
}