package id.andalan.andalanku.ui.transactionHistory

import android.os.Bundle
import android.view.LayoutInflater
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class InstallmentTransactionDetailActivity : BaseApp() {

    companion object {
        val AGREEMENT_NUMBER = "agreement_number"
    }

    private lateinit var alertDialogProgress: AlertDialogCustom
    @Inject
    //lateinit var presenter: InstallmentPaymentDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installment_transaction_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Detail Transaksi"

//        if (!intent.hasExtra(AGREEMENT_NUMBER)) {
//            finish()
//        }

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(  false)
        alertDialogProgress.dismiss()

        //agreementNumber = intent.getStringExtra(AGREEMENT_NUMBER)
        //deps.inject(this)
        //presenter.view = this
        //presenter.getPaymentDetail(agreementNumber)

    }

    /*override fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse) {
        if (paymentInquiryResponse.status == ConfigVar.SUCCESS) {
            val periode = paymentInquiryResponse.installmentSequence.toString() + " "  + paymentInquiryResponse.periode
            et_contract.setText(paymentInquiryResponse.agreementNumber)
            et_customer_name.setText(paymentInquiryResponse.name)
            et_angsuran_ke.setText(periode)
            et_jatuh_tempo.setText(paymentInquiryResponse.dueDate)
            //et_merk_type.setText(paymentInquiryResponse.carType)
            et_nopol.setText(paymentInquiryResponse.policeNumber)
            et_jml_angsuran.setText(paymentInquiryResponse?.amount?.toLong()?.convertToRpFormat())
            et_pinalty.text = paymentInquiryResponse?.penalty?.toLong()?.convertToRpFormat()
            et_biaya_admin.text = paymentInquiryResponse?.adminFee?.toLong()?.convertToRpFormat()
            et_total_tagihan.text = paymentInquiryResponse?.totalAmount?.toLong()?.convertToRpFormat()

            paymentUrl = "https://devapi.andalanku.id/carspay?transaction_code=" + paymentInquiryResponse.transactionCode
        }
        else {

        }
    }*/
}