package id.andalan.andalanku.ui.bill

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.activity_installment_billdetails.*
import kotlinx.android.synthetic.main.toolbar_login.*

class InstallmentBilldetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installment_billdetails)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Detail Tagihan"

        btn_bayar.setOnClickListener {
            // TODO - Bayar Tagihan
        }
    }
}