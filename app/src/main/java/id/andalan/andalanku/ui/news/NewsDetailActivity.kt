package id.andalan.andalanku.ui.news

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
//import android.text.format.DateUtils
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.News
import id.andalan.andalanku.preferences.PreferencesUtil
//import id.andalan.andalanku.preferences.PreferencesUtil
//import id.andalan.andalanku.ui.home.HomeFragment
import id.andalan.andalanku.utils.convertStringToHTML
//import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.ekstensions.loadImage
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.activity_detail_promo.*
import kotlinx.android.synthetic.main.activity_news_detail.*
import kotlinx.android.synthetic.main.activity_news_detail.iv_promo
import kotlinx.android.synthetic.main.activity_news_detail.tv_content
import kotlinx.android.synthetic.main.footer_share.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import javax.inject.Inject

//import javax.inject.Inject

class NewsDetailActivity : BaseApp() {
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    companion object {
        var NEWS = "news"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.detail_news)
        deps.inject(this)

        val news: News? = intent?.extras?.getParcelable(NEWS)

        if (news?.isEmpty() == false) {
            loadImage(this, news.image?:"", iv_promo)
            tv_title_news.text = news.title
            tv_date.text = news.getDateUI(this.getMonths())
            //tv_content.text = news.content?.convertStringToHTML()
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv_content.text = Html.fromHtml(news.content, Html.FROM_HTML_MODE_COMPACT)
            } else {
                tv_content.text = Html.fromHtml(news.content)
            }
        }

        btn_share.setOnClickListener{
            val shareIntent = Intent(Intent.ACTION_SEND)
            val webBasedUrl = preferencesUtil.getWebUrl()
            shareIntent.type = "text/plain"

            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bagikan Nerita Ini")

            val appUrl =
                "$webBasedUrl/news/${news?.slug}"

            shareIntent.putExtra(Intent.EXTRA_TEXT, appUrl)

            startActivity(Intent.createChooser(shareIntent, "Share via"))
        }
    }
}
