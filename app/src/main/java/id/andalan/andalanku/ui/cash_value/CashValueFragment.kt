package id.andalan.andalanku.ui.cash_value


import android.app.Activity
import androidx.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager

import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.CashValueResponse
import id.andalan.andalanku.model.ui.CashValue
import id.andalan.andalanku.model.ui.Menu
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.adapters.MenuAdapterHome
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.contract.ContractDetailActivity
import id.andalan.andalanku.ui.dana.SimulationDanaActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.login.LoginActivity
import id.andalan.andalanku.ui.newcar.PaymentNewCarActivity
import id.andalan.andalanku.ui.usedcar.UsedCarActivity
import id.andalan.andalanku.utils.convertToRpFormat
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.fragment_cash_value.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*

class CashValueFragment : androidx.fragment.app.Fragment() {
    private lateinit var menuAdapterHome: MenuAdapterHome
    private lateinit var pickerContractAdapter: PickerAdapter
    private var choosedContract: PickerItem? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_cash_value, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = "Cash Value"
        toolbar_login.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
        btn_refresh_cash.setOnClickListener {
            (activity as MainHomeActivity).presenter.getCashValue()
        }

        btn_detail_contract.setOnClickListener {
            if (((activity as MainHomeActivity).preferencesUtil.getUser()?.idCustomer?:0) == 0) {
                startActivityForResult(Intent(context, LoginActivity::class.java), LoginActivity.REQUEST_LOGIN)
            } else {
                val intent = Intent(context, ContractDetailActivity::class.java)
                intent.putExtra(ContractDetailActivity.AGREEMENT_NUMBER, choosedContract?.name?:"")
                intent.putExtra(ContractDetailActivity.LIST_AGREEMENT, pickerContractAdapter.getData().toTypedArray())
                startActivity(intent)
            }
        }

        menuAdapterHome = MenuAdapterHome(object: MenuAdapterHome.onClickItem{
            override fun onClickMenu(menu: Menu) {
                when {
                    menu.name == getString(R.string.new_cars) -> startActivity(Intent(context, PaymentNewCarActivity::class.java))
                    menu.name == getString(R.string.used_car) -> startActivity(Intent(context, UsedCarActivity::class.java))
                    menu.name == getString(R.string.wallet_andalanku) -> startActivity(Intent(context, SimulationDanaActivity::class.java))
                }
            }
        })

        pickerContractAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                et_choosed_contract_number?.setText(pickerItem.name)
                choosedContract = pickerItem
                val cashValue = (pickerItem.realObject as CashValue)
                tv_plafond_value?.text = cashValue.CashValue?.toLong()?.convertToRpFormat()
                btn_detail_contract?.visibility = View.VISIBLE
                hideSlideLayout()
            }
        }, "cash_value")
        et_choosed_contract_number?.setOnClickListener {
            tv_title_drag_view.text = "Pilih Nomer Kontrak"
            et_drag_view.visibility = View.GONE
            showSlideLayout()
        }

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        menuAdapterHome.add(
            Menu(
                name = getString(R.string.new_cars),
                image = R.drawable.ic_new_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.used_car),
                image = R.drawable.ic_used_cars
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.wallet_andalanku),
                image = R.drawable.ic_dana_andalanku
            )
        )
        menuAdapterHome.add(
            Menu(
                name = getString(R.string.e_commerce),
                image = R.drawable.ic_ecommerce
            )
        )

        val layoutManager =
            androidx.recyclerview.widget.GridLayoutManager(context, 3)
        rv_menu?.layoutManager = layoutManager //as RecyclerView.LayoutManager
        rv_menu?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu?.adapter = menuAdapterHome
        rv_menu?.isNestedScrollingEnabled = true

        rv_drag_view?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_drag_view?.adapter = pickerContractAdapter
        rv_drag_view?.isNestedScrollingEnabled = true

        val observerCashValueResponse = Observer<CashValueResponse> {
            it?.let {
                pickerContractAdapter.removeAll()
                val listPicker = arrayListOf<PickerItem>()
                it.cashValueList?.forEach {
                    val pickerItem = PickerItem(id = it.AgreementNo, name = it.AgreementNo)
                    pickerItem.realObject = it
                    listPicker.add(pickerItem)
                }
                if (listPicker.size > 0) {
                    choosedContract = listPicker[0]
                    choosedContract?.let {
                        et_choosed_contract_number?.setText(it.name)
                        val cashValue = (it.realObject as CashValue)
                        tv_plafond_value?.text = cashValue.CashValue?.toLong()?.convertToRpFormat()
                        btn_detail_contract?.visibility = View.VISIBLE
                    }
                }
                pickerContractAdapter.add(listPicker)
            }
        }
        (activity as MainHomeActivity).model.currentCashValue.observe(this, observerCashValueResponse)
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        activity?.let {
            hideKeyboard(it)
        }
    }

    private fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
