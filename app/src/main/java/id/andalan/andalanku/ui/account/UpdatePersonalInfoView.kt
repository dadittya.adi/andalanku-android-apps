package id.andalan.andalanku.ui.account

import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.UpdateAddressResponse
import id.andalan.andalanku.model.response.UpdateProfileResponse
import id.andalan.andalanku.model.ui.AddressName
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.ui.base.BaseView

interface UpdatePersonalInfoView: BaseView {
    fun onGetProvince(mutableList: MutableList<Location>)
    fun onGetCity(mutableList: MutableList<Location>)
    fun onGetSubdistrict(mutableList: MutableList<Location>)
    fun onGetVillage(mutableList: MutableList<Location>)
    fun getResponseUpdatePersonalInfo(updateAddressResponse: UpdateAddressResponse)
    fun successUpdateAddress(updateAddressResponse: UpdateAddressResponse)
    fun getResponseUpdateAllAddress(updateAllAddressResponse: UpdateAddressResponse)
    fun onGetZipCode(zipCode: String)
    //fun getProfileInformation(personalInformationResponse: PersonalInformationResponse)
    //fun onGetPersonalInformation12(personalInformationResponse: PersonalInformationResponse)
    //fun foo()
}