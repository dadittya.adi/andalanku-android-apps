package id.andalan.andalanku.ui.home

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import id.andalan.andalanku.ui.account.BlankFragment
import id.andalan.andalanku.ui.account.MyAccountFragment
import id.andalan.andalanku.ui.cash_value.CashValueComingSoonFragment
import id.andalan.andalanku.ui.contract.ContractFragment
import id.andalan.andalanku.ui.loan_application.CompleteDataFragment
import id.andalan.andalanku.ui.help.HelpFragment
import id.andalan.andalanku.ui.inbox.DetailMessageFragment
import id.andalan.andalanku.ui.inbox.InboxFragment
import id.andalan.andalanku.ui.installments.InstallmentScheduleFragment
import id.andalan.andalanku.ui.payment.InstallmentPaymentFragment
import id.andalan.andalanku.ui.plafond.PlafondFragment
import id.andalan.andalanku.ui.repayment.RepaymentSimulationFragment
import id.andalan.andalanku.ui.transactionHistory.TransactionHistoryFragment
import java.util.*

class MainViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): androidx.fragment.app.Fragment {
        return getFragments()[position]!!
    }

    override fun getCount(): Int {
        return getFragments().size
    }

    private fun getFragments(): Hashtable<Int, androidx.fragment.app.Fragment> {
        val ht = Hashtable<Int, androidx.fragment.app.Fragment>()
        ht[0] = HomeFragment()
        ht[1] = TransactionHistoryFragment()
        ht[2] = InboxFragment()
        ht[3] = MyAccountFragment()
        ht[4] = BlankFragment()
        ht[5] = PlafondFragment()
        ht[6] = InstallmentPaymentFragment()
        ht[7] = CompleteDataFragment()
        ht[8] = InstallmentScheduleFragment()
        ht[9] = RepaymentSimulationFragment()
        ht[10] = ContractFragment()
        ht[11] = DetailMessageFragment()
        ht[12] = CashValueComingSoonFragment()
        return ht
    }
}
