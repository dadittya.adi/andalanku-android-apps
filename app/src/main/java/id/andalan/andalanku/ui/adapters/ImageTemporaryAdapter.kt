package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.GlideApp
import kotlinx.android.synthetic.main.form_upload_image.view.*

class ImageTemporaryAdapter(val onItemClickListener: OnClickItem): BaseAdapter<String, ImageTemporaryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageTemporaryAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.form_upload_image
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<String>(itemView) {
        override fun bind(data: String) {
            itemView.btn_delete.visibility = View.VISIBLE
            itemView.btn_delete.setOnClickListener {
                onItemClickListener.onClickDelete(data)
            }
            val filename = data.substring(data.lastIndexOf("/") + 1)
            itemView.tv_filename.text = filename
            GlideApp.with(itemView.context).load(data)
                .into(itemView.iv_preview)
        }

    }

    interface OnClickItem {
        fun onClickDelete(image: String)
    }
}