@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PpobBillCustomerDetails
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_ppob_bill_detail.view.*


class PpobBillDetailCustomersAdapter : BaseAdapter<PpobBillCustomerDetails, PpobBillDetailCustomersAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_bill_detail
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobBillCustomerDetails>(itemView) {
        override fun bind(ppobBillCustomerDetails: PpobBillCustomerDetails) {
            val data = ppobBillCustomerDetails.getCustomerDetails()

            if (data.key == "Waktu & Tanggal Pembayaran") {
                itemView.tv_ppob_bill_detail_key.text = "Tanggal"
            }
            else {
                itemView.tv_ppob_bill_detail_key.text = data.key
            }
            itemView.tv_ppob_bill_detail_value.text = data.value

        }

    }
}