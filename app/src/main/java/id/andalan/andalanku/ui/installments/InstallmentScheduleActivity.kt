package id.andalan.andalanku.ui.installments

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.DetailFinanceResponse
import id.andalan.andalanku.model.response.DetailPaymentHistoryResponse
import id.andalan.andalanku.model.response.PrePaymentSimulationResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.contract.ContractDetailActivity
import id.andalan.andalanku.ui.payment.InstallmentPaymentView
import kotlinx.android.synthetic.main.fragment_contract.*
import kotlinx.android.synthetic.main.fragment_contract.et_choosed_contract_number
import kotlinx.android.synthetic.main.fragment_contract.sliding_layout
import kotlinx.android.synthetic.main.fragment_installment_schedule.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import javax.inject.Inject

class InstallmentScheduleActivity: BaseApp(), InstallmentsScheduleView {
    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: InstallmentsSchedulePresenter

    private lateinit var pickerAdapter: PickerAdapter
    private var choosedPicker = PickerItem()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.fragment_installment_schedule)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.jadwal_angsuran)

        deps.inject(this)
        presenter.view = this
        presenter.getAggreementNumber()

        et_choosed_contract_number.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                et_choosed_contract_number?.setText(pickerItem.name)
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)

                val intent = Intent(this@InstallmentScheduleActivity, InstallmentScheduleDetailActivity::class.java)
                intent.putExtra(InstallmentScheduleDetailActivity.AGREEMENT_NUMBER, choosedPicker.name)
                intent.putExtra(InstallmentScheduleDetailActivity.LIST_AGREEMENT, pickerAdapter.getData().toTypedArray())
                startActivity(intent)

                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        rv_item?.layoutManager =
            androidx.recyclerview .widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter

    }

    override fun getResponseInstallment(detailPaymentHistoryResponse: DetailPaymentHistoryResponse) {

    }

    override fun onGetDetailContract(detailFinanceResponse: DetailFinanceResponse) {

    }

    override fun getResponsePrepaymentSimulation(prePaymentSimulationResponse: PrePaymentSimulationResponse) {

    }

    override fun onGetContractNumber(list: MutableList<PickerItem>) {
        if (list.size > 0) {
            pickerAdapter.removeAll()
            pickerAdapter.add(list)
            container_empty_state?.visibility = View.GONE
        } else {
            container_empty_state?.visibility = View.VISIBLE
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        container_empty_state?.visibility = View.VISIBLE
        Toast.makeText(this, appErrorMessage?:"", Toast.LENGTH_SHORT).show()
    }
}