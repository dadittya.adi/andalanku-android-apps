package id.andalan.andalanku.ui.inbox

import androidx.lifecycle.Observer
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import id.andalan.andalanku.R
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_INBOX
import id.andalan.andalanku.model.response.InboxResponse
import id.andalan.andalanku.model.ui.Inbox
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.InboxAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.utils.EndlessRecyclerOnScrollListener
import kotlinx.android.synthetic.main.fragment_inbox.*
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import id.andalan.andalanku.model.request.*

class InboxFragment : androidx.fragment.app.Fragment(), InboxView {
    lateinit var preferencesUtil: PreferencesUtil
    lateinit var presenter: InboxPresenter
    private lateinit var inboxAdapter: InboxAdapter
    private lateinit var endlessRecyclerOnScrollListener: EndlessRecyclerOnScrollListener
    private var page = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inbox, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.inbox)

        presenter = InboxPresenter((activity as MainHomeActivity).services)
        presenter.view = this
        inboxAdapter = InboxAdapter(object: InboxAdapter.OnClick{
            override fun onClickItem(inbox: Inbox) {
                (activity as MainHomeActivity).selectedMessage = inbox
                EventBus.getDefault().post(EventBus(type = LOAD_INBOX))
                (activity as MainHomeActivity).moveViewNoAnim(11)
            }
        })

        val observerInboxResponse = Observer<MutableList<Inbox>> {
            it?.let {
                swipe_layout?.isRefreshing = false
                if (page == 1) {
                    endlessRecyclerOnScrollListener.setCurrentPage(1)
                }
                inboxAdapter.removeAll()
                inboxAdapter.add(it)
            }
        }
        (activity as MainHomeActivity).model.currentInboxList.observe(this, observerInboxResponse)

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        endlessRecyclerOnScrollListener = object: EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                page += 1
                (activity as MainHomeActivity).presenter.getListInbox((currentPage + 1).toString())
            }
        }

        rv_list_message?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_list_message?.layoutManager = layoutManager
        rv_list_message?.adapter = inboxAdapter
        rv_list_message?.addOnScrollListener(endlessRecyclerOnScrollListener)

        swipe_layout?.setOnRefreshListener {
            page = 1
            (activity as MainHomeActivity).presenter.getListInbox((1).toString())
        }

        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }
    }

    override fun onGetResponse(inboxResponse: InboxResponse) {
        inboxAdapter.add(inboxResponse.listInbox?: arrayListOf())
    }

    override fun showWait() {
        progress_inbox?.visibility = View.VISIBLE
    }

    override fun removeWait() {
        progress_inbox?.visibility = View.GONE
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    override fun onResume() {
        super.onResume()

        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.inbox)

        presenter = InboxPresenter((activity as MainHomeActivity).services)
        presenter.view = this
        inboxAdapter = InboxAdapter(object: InboxAdapter.OnClick{
            override fun onClickItem(inbox: Inbox) {
                (activity as MainHomeActivity).selectedMessage = inbox
                EventBus.getDefault().post(EventBus(type = LOAD_INBOX))
                (activity as MainHomeActivity).moveViewNoAnim(11)
            }
        })

        val observerInboxResponse = Observer<MutableList<Inbox>> {
            it?.let {
                swipe_layout?.isRefreshing = false
                if (page == 1) {
                    endlessRecyclerOnScrollListener.setCurrentPage(1)
                }
                inboxAdapter.removeAll()
                inboxAdapter.add(it)
            }
        }
        (activity as MainHomeActivity).model.currentInboxList.observe(this, observerInboxResponse)

        val layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
            false
        )
        endlessRecyclerOnScrollListener = object: EndlessRecyclerOnScrollListener(layoutManager) {
            override fun onLoadMore(currentPage: Int) {
                page += 1
                (activity as MainHomeActivity).presenter.getListInbox((currentPage + 1).toString())
            }
        }

        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(0) }

        rv_list_message?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_list_message?.layoutManager = layoutManager
        rv_list_message?.adapter = inboxAdapter
        rv_list_message?.addOnScrollListener(endlessRecyclerOnScrollListener)
    }
}
