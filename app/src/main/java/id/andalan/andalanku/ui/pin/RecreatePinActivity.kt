package id.andalan.andalanku.ui.pin

import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.activity_recreate_pin.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class RecreatePinActivity : BaseApp(), RecreatePinView {
    private val TAG = RecreatePinActivity::class.java.simpleName

    @Inject
    lateinit var presenter: RecreatePinPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var alertDialogCustom: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recreate_pin)

        deps.inject(this)
        presenter.view = this

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.security_account)

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.success_message_change_pin)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            finish()
        }

        btn_send?.setOnClickListener {
            if (cb_term_condition.isChecked) {
                presenter.doVerificationInput(et_password?.text.toString(), et_pin.text.toString(), et_pin_confirm.text.toString())
            } else {
                Toast.makeText(this@RecreatePinActivity, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onGetResponse(createPinResponse: CreatePinResponse) {
        if (createPinResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.show()
        } else {
            Toast.makeText(this@RecreatePinActivity, createPinResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun callbackVerificationInput(code: Int) {
        when (code) {
            1 -> {
                Toast.makeText(this@RecreatePinActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
            2 -> {
                Toast.makeText(this@RecreatePinActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
            3 -> {
                Toast.makeText(this@RecreatePinActivity, getString(R.string.new_pin_not_equal), Toast.LENGTH_SHORT).show()
            }
            else -> {
                Toast.makeText(this@RecreatePinActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
