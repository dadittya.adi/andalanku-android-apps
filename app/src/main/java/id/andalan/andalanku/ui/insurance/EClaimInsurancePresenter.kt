package id.andalan.andalanku.ui.insurance

import id.andalan.andalanku.model.response.ClaimInsuranceListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class EClaimInsurancePresenter @Inject constructor(private val service: Services): BasePresenter<EClaimInsuranceView>() {
    override fun onCreate() {
        getListClaimInsurance()
    }

    fun getListClaimInsurance(status: String = "") {
        view.showWait()
        val subscription = service.getClaimInsuranceList(status).subscribe(object : Subscriber<ClaimInsuranceListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(claimInsuranceListResponse: ClaimInsuranceListResponse) {
                view.onGetEClaimResponse(claimInsuranceListResponse)
            }
        })
        subscriptions.add(subscription)
    }
}