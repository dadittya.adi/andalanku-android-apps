package id.andalan.andalanku.ui.contract

import android.util.Log.e
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class ContractPresenter @Inject constructor(private val service: Services): BasePresenter<ContractView>() {
    override fun onCreate() {
    }

    fun getAndalanFinancial(agreementNumber: String) {
        view.showWait()
        val subscription = service.getAndalanFinancial(agreementNumber).subscribe(object : Subscriber<DetailAndalanFinancialResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(detailAndalanFinancialResponse: DetailAndalanFinancialResponse) {
                view.getAndalanFinencial(detailAndalanFinancialResponse)
            }
        })
        subscriptions.add(subscription)
    }
}