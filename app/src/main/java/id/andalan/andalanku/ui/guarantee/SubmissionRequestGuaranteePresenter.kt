package id.andalan.andalanku.ui.guarantee

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.AgreementListResponse
import id.andalan.andalanku.model.response.CollateralRequestResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class SubmissionRequestGuaranteePresenter @Inject constructor(private val service: Services): BasePresenter<SubmissionRequestGuaranteeView>() {
    override fun onCreate() {
        getAggreementNumber()
    }

    fun getAggreementNumber() {
        view.showWait()
        val subscription = service.getAgreementList().subscribe(object : Subscriber<AgreementListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(agreementListResponse: AgreementListResponse) {
                if (agreementListResponse.status == ConfigVar.SUCCESS) {
                    val list: MutableList<PickerItem> = arrayListOf()
                    agreementListResponse.agreementList?.let {
                        it.forEach {item ->
                            list.add(PickerItem(name = item.agreementNo, contract_status = item.contract_status, isChoosed = false))
                        }
                    }
                    view.onGetContractNumber(list)
                } else {
                    view.onFailure("Gagal Mendapatkan Nomer Kontrak")
                }
            }
        })
        subscriptions.add(subscription)
    }

    fun sendCollateral(agreementNumber: String = "", takingDate: String = "") {
        view.showWait()
        val subscription = service.sendCollateralRequest(agreementNumber = agreementNumber, takingDate = takingDate)
            .subscribe(object : Subscriber<CollateralRequestResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(collateralRequestResponse: CollateralRequestResponse) {
                    view.onGetResponseSubmission(collateralRequestResponse)
                }
            })
        subscriptions.add(subscription)
    }
}