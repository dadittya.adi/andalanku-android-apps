@file:Suppress("NAME_SHADOWING")

package id.andalan.andalanku.ui.loan_application

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer

import id.andalan.andalanku.R
import id.andalan.andalanku.model.request.SendLoanApplicationRequest
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.OccupationAdapter
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.payment.PaymentSimulationActivity
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.fragment_complete_data.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import java.text.SimpleDateFormat
import java.util.*
import android.app.TimePickerDialog
import android.util.Log.e
import android.view.inputmethod.InputMethodManager
import android.widget.RadioButton
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.response.BranchListResponse
import id.andalan.andalanku.model.response.PlafondApplicationResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.ui.account.MyAccountFragment
import id.andalan.andalanku.ui.account.UpdatePersonalInfoActivity
import id.andalan.andalanku.ui.adapters.BranchAdapter
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.address.UpdateAddressAccordingDomicileActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingKtpActivity
import id.andalan.andalanku.ui.address.UpdateAddressAccordingWorkPlaceActivity
import id.andalan.andalanku.ui.agent.AgentRegisterActivity
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity.Companion.HOME
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity.Companion.MESSAGE
import id.andalan.andalanku.utils.*
import kotlinx.android.synthetic.main.fragment_complete_data.btn_next
import kotlinx.android.synthetic.main.fragment_complete_data.cb_term_condition
import kotlinx.android.synthetic.main.fragment_complete_data.et_additional_salary
import kotlinx.android.synthetic.main.fragment_complete_data.et_additionl_job
import kotlinx.android.synthetic.main.fragment_complete_data.et_choose_branch
import kotlinx.android.synthetic.main.fragment_complete_data.et_complate_name
import kotlinx.android.synthetic.main.fragment_complete_data.et_date
import kotlinx.android.synthetic.main.fragment_complete_data.et_date_of_birth
import kotlinx.android.synthetic.main.fragment_complete_data.et_job
import kotlinx.android.synthetic.main.fragment_complete_data.et_ktp_number
import kotlinx.android.synthetic.main.fragment_complete_data.et_monthly_income
import kotlinx.android.synthetic.main.fragment_complete_data.et_mother_name
import kotlinx.android.synthetic.main.fragment_complete_data.et_phone_number
import kotlinx.android.synthetic.main.fragment_complete_data.et_place_of_birth
import kotlinx.android.synthetic.main.fragment_complete_data.et_telephone_number
import kotlinx.android.synthetic.main.fragment_complete_data.et_time
import kotlinx.android.synthetic.main.fragment_complete_data.et_label_home_phone
import kotlinx.android.synthetic.main.fragment_complete_data.sliding_layout
import kotlinx.android.synthetic.main.layout_confirmation.view.*
import kotlinx.android.synthetic.main.layout_confirmation.view.btn_ok
import kotlinx.android.synthetic.main.layout_success.view.tv_intro_text as tvIntroSuccessLayout


class CompleteDataFragment : androidx.fragment.app.Fragment(), CompleteDataView {
    override fun onResponseApplicationPlafond(plafondApplicationResponse: PlafondApplicationResponse) {
    }

    companion object {
        val COMPLETE_DATA = "complete_data"
        val KTP_REQ = 11223
        val KTP_DATA = "ktp_address"
        val ISREFERENSI = "is_referensi"
        val DOMICILE_REQ = 11224
        val DOMICILE_DATA = "domicile_address"
        val OFFICE_REQ = 11225
        val OFFICE_DATA = "office_address"
        val IS_COMPLETE_DATA_FORM = "is_complete"
    }
    lateinit var preferencesUtil: PreferencesUtil
    lateinit var presenter: CompleteDataPresenter
    private lateinit var menuBottomAdapter: MenuAdapterProfile
    private var address: UpdateAddressRequest? = null
    private var addressDomicile: UpdateAddressRequest? = null
    private var addressOffice: UpdateAddressRequest? = null

    lateinit var occupationAdapter: OccupationAdapter
    lateinit var branchAdapter: BranchAdapter

    private var occupationChoosed: Occupation? = null
    private var branchChoosed: Branch? = null
    private var dateChoosed: String = ""
    private var dateOfBirthChoosed: String = ""
    private val dataCalendar = Calendar.getInstance()
    private val dobCalendar = Calendar.getInstance()
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertDialogConfirm: AlertDialogCustom
    private lateinit var alertDialogSuccess: AlertDialogCustom

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_complete_data, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.complete_data_title)

        context?.let {
            alertDialogProgress = AlertDialogCustom(it, null)
            alertDialogProgress.setView(LayoutInflater.from(it).inflate(R.layout.layout_progress_loading, null, false))
            alertDialogProgress.create()
            alertDialogProgress.setCancelable(true)
        }

        toolbar_login.setNavigationOnClickListener {
            hideSlideLayout()
            (activity as MainHomeActivity).moveViewNoAnim(0)
        }

        preferencesUtil = (activity as MainHomeActivity).preferencesUtil
        presenter = CompleteDataPresenter((activity as MainHomeActivity).services)
        presenter.view = this

        alertDialogConfirm = AlertDialogCustom(context!!, null)
        alertDialogConfirm.setView(LayoutInflater.from(context).inflate(R.layout.layout_confirmation, null, false))
        alertDialogConfirm.create()
        alertDialogConfirm.getView().btn_ok.setOnClickListener {
            alertDialogConfirm.dismiss()
            val intent = Intent(context, AgentRegisterActivity::class.java)
            intent.putExtra(UpdatePersonalInfoActivity.IS_ACCOUNT_SETTING, true)
            startActivityForResult(intent, MyAccountFragment.REQ_CODE_UPDATED_INFO)
        }
        alertDialogConfirm.getView().btn_cancel?.setOnClickListener {
            alertDialogConfirm.dismiss()
        }

        alertDialogSuccess = AlertDialogCustom(context!!, null)
        alertDialogSuccess.setView(
            LayoutInflater.from(context).inflate(R.layout.layout_success, null, false)
        )
        alertDialogSuccess.create()
        alertDialogSuccess.getView().btn_ok?.setOnClickListener {
            alertDialogSuccess.dismiss()
        }

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }

        occupationAdapter = OccupationAdapter(object: OccupationAdapter.OnClickItem{
            override fun onClick(occupation: Occupation) {
                occupationChoosed = occupation
                et_job.setText(occupation.name)
                hideSlideLayout()
            }
        })

        menuBottomAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.alamat_sesuai_ktp)) {
                    val intent = Intent(context, UpdateAddressAccordingKtpActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(ISREFERENSI, (activity as MainHomeActivity).isReferensikan)
                    startActivityForResult(intent, KTP_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_domisili)) {
                    val intent = Intent(context, UpdateAddressAccordingDomicileActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(KTP_DATA, address)
                    intent.putExtra(ISREFERENSI, (activity as MainHomeActivity).isReferensikan)
                    intent.putExtra(DOMICILE_DATA, addressDomicile)
                    startActivityForResult(intent, DOMICILE_REQ)
                } else if (menuProfile.name == getString(R.string.alamat_kantor)) {
                    val intent = Intent(context, UpdateAddressAccordingWorkPlaceActivity::class.java)
                    intent.putExtra(IS_COMPLETE_DATA_FORM, true)
                    intent.putExtra(OFFICE_DATA, addressOffice)
                    intent.putExtra(ISREFERENSI, (activity as MainHomeActivity).isReferensikan)
                    startActivityForResult(intent, OFFICE_REQ)
                }
            }
        })

        branchAdapter = BranchAdapter(object: BranchAdapter.OnClickItem{
            override fun onClick(data: Branch) {
                branchChoosed = data
                et_choose_branch?.setText(branchChoosed?.address)
                hideSlideLayout()
            }

        })

        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        val isBlockedSlideUp = false

        et_job?.setOnClickListener {
            if (occupationAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.job)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = occupationAdapter
            val isReferenceOther = cb_referensikan?.isChecked == true
            showSlideLayout(isBlockedSlideUp && !isReferenceOther)
        }

        et_choose_branch?.setOnClickListener {
            if (branchAdapter.getData().isEmpty()) { presenter.getListBranch(addressDomicile?.regencyId?:"") }
            tv_title_drag_view.text = "Pilih Cabang"
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = branchAdapter
            showSlideLayout()
        }

        et_monthly_income?.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    if (et_monthly_income.text.toString() != price.convertToRpFormat()) et_monthly_income.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_monthly_income.setSelection(text.length - 2)
                }
            }
        })

        et_additional_salary?.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    if (et_additional_salary.text.toString() != price.convertToRpFormat()) et_additional_salary.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_additional_salary.setSelection(text.length - 2)
                }
            }
        })
        val personalInformationResponse = preferencesUtil.getProfile()
        btn_next?.setOnClickListener {
            val placeOfBirth = et_place_of_birth.text.toString()
            val selectedGender = rg_container?.checkedRadioButtonId
            val selectedGenderRadio: RadioButton? = getView()?.findViewById(selectedGender?:0)
            val gender = selectedGenderRadio?.tag.toString()
            val dateOfBirth = dateOfBirthChoosed
            val idNumber = et_ktp_number.text.toString()
            val motherName = et_mother_name.text.toString()
            val homePhoneNumber = et_telephone_number.text.toString()
            val phoneNumber = et_phone_number.text.toString()
            val areaCode = et_label_home_phone.text.toString()
            val monthlyIncome = et_monthly_income.text.toString().getNumberOnly()
            val additionalIncome = et_additional_salary.text.toString().getNumberOnly()
            val sideJob = et_additionl_job.text.toString()

            val sendLoanApplicationRequest = SendLoanApplicationRequest(
                customerName = et_complate_name?.text.toString(),
                email = et_email?.text.toString(),
                gender = gender,
                placeOfBirth = placeOfBirth,
                dateOfBirth = dateOfBirth,
                idNumber = idNumber,
                motherName = motherName,
                areaCode = areaCode,
                homePhoneNumber = homePhoneNumber,
                phoneNumber = phoneNumber,
                occupationId = occupationChoosed?.id,
                monthlyIncome = monthlyIncome.toString(),
                additionalIncome = additionalIncome.toString(),
                sideJob = sideJob,
                provinceId = address?.provinceId,
                cityId = address?.regencyId,
                districtId = address?.districtId,
                villageId = address?.villageId,
                address = address?.address,
                rt = address?.rt,
                rw = address?.rw,
                zipCode = address?.zipCode,

                provinceIdDomicile = addressDomicile?.provinceId,
                cityIdDomicile = addressDomicile?.regencyId,
                districtIdDomicile = addressDomicile?.districtId,
                villageIdDomicile = addressDomicile?.villageId,
                addressDomicile = addressDomicile?.address,
                rtDomicile = addressDomicile?.rt,
                rwDomicile = addressDomicile?.rw,
                zipCodeDomicile = addressDomicile?.zipCode,

                provinceIdOffice = addressOffice?.provinceId,
                cityIdOffice = addressOffice?.regencyId,
                districtIdOffice = addressOffice?.districtId,
                villageIdOffice = addressOffice?.villageId,
                addressOffice = addressOffice?.address,
                rtOffice = addressOffice?.rt,
                rwOffice = addressOffice?.rw,
                zipCodeOffice = addressOffice?.zipCode,

                branchId = branchChoosed?.branchID,
                surveyDate = dateChoosed,
                surveyClock = et_time.text.toString(),
                loanApplicationReferal = if (cb_referensikan?.isChecked == true) "Y" else "N"
            )

            if (cb_term_condition.isChecked) {
                if (sendLoanApplicationRequest.isValidFormCompleteData()) {
                    if (cb_referensikan?.isChecked == false) {
                        val intent = Intent(context, CompleteDocumentActivity::class.java)
                        intent.putExtra(CompleteDocumentActivity.CREDIT, (activity as MainHomeActivity).simulation)
                        intent.putExtra(CompleteDocumentActivity.COLLATERAL, (activity as MainHomeActivity).collateral)
                        intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, (activity as MainHomeActivity).creditSimulationRequest)
                        val dpAmount = (activity as MainHomeActivity).simulation?.dp?.getNumberOnly()
                        val plafond = (activity as MainHomeActivity).simulation?.totalHutang?.getNumberOnly()?.minus(dpAmount?:0)
                        val recievedFunds = (activity as MainHomeActivity).simulation?.danaDiterima?.getNumberOnly()
                        val installment = (activity as MainHomeActivity).simulation?.angsuran?.getNumberOnly()
                        sendLoanApplicationRequest.dpAmount = dpAmount.toString()
                        sendLoanApplicationRequest.plafond = plafond.toString()
                        sendLoanApplicationRequest.recievedFunds = recievedFunds.toString()
                        sendLoanApplicationRequest.installment = installment.toString()
                        sendLoanApplicationRequest.assetCode = (activity as MainHomeActivity).collateral?.assetCode?:""

                        intent.putExtra(COMPLETE_DATA, sendLoanApplicationRequest)
                        startActivityForResult(intent, 1234)
                    } else {
                        if (personalInformationResponse?.bankAccountNumber.isNullOrEmpty()) {
                            val intent = Intent(context, AccountDocumentActivity::class.java)
                            intent.putExtra(CompleteDocumentActivity.CREDIT, (activity as MainHomeActivity).simulation)
                            intent.putExtra(CompleteDocumentActivity.COLLATERAL, (activity as MainHomeActivity).collateral)
                            intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, (activity as MainHomeActivity).creditSimulationRequest)
                            val dpAmount = (activity as MainHomeActivity).simulation?.dp?.getNumberOnly()
                            val plafond = (activity as MainHomeActivity).simulation?.totalHutang?.getNumberOnly()?.minus(dpAmount?:0)
                            val recievedFunds = (activity as MainHomeActivity).simulation?.danaDiterima?.getNumberOnly()
                            val installment = (activity as MainHomeActivity).simulation?.angsuran?.getNumberOnly()
                            sendLoanApplicationRequest.dpAmount = dpAmount.toString()
                            sendLoanApplicationRequest.plafond = plafond.toString()
                            sendLoanApplicationRequest.recievedFunds = recievedFunds.toString()
                            sendLoanApplicationRequest.installment = installment.toString()
                            sendLoanApplicationRequest.assetCode = (activity as MainHomeActivity).collateral?.assetCode?:""

                            intent.putExtra(COMPLETE_DATA, sendLoanApplicationRequest)
                            startActivityForResult(intent, 1234)
                        }
                        else {
                            val intent = Intent(context, CompleteDocumentActivity::class.java)
                            intent.putExtra(CompleteDocumentActivity.CREDIT, (activity as MainHomeActivity).simulation)
                            intent.putExtra(CompleteDocumentActivity.COLLATERAL, (activity as MainHomeActivity).collateral)
                            intent.putExtra(CompleteDocumentActivity.CREDITRESQUEST, (activity as MainHomeActivity).creditSimulationRequest)
                            val dpAmount = (activity as MainHomeActivity).simulation?.dp?.getNumberOnly()
                            val plafond = (activity as MainHomeActivity).simulation?.totalHutang?.getNumberOnly()?.minus(dpAmount?:0)
                            val recievedFunds = (activity as MainHomeActivity).simulation?.danaDiterima?.getNumberOnly()
                            val installment = (activity as MainHomeActivity).simulation?.angsuran?.getNumberOnly()
                            sendLoanApplicationRequest.dpAmount = dpAmount.toString()
                            sendLoanApplicationRequest.plafond = plafond.toString()
                            sendLoanApplicationRequest.recievedFunds = recievedFunds.toString()
                            sendLoanApplicationRequest.installment = installment.toString()
                            sendLoanApplicationRequest.assetCode = (activity as MainHomeActivity).collateral?.assetCode?:""

                            intent.putExtra(COMPLETE_DATA, sendLoanApplicationRequest)
                            startActivityForResult(intent, 1234)
                        }
                    }
                } else {
                    Toast.makeText(context, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(context, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
            }

        }

        cb_referensikan?.setOnCheckedChangeListener { _, b ->
            if (!b) {
                (activity as MainHomeActivity).isReferensikan = false
                loadPersonalInfo()
                val personalInformationResponse = preferencesUtil.getProfile()
                focusableForm(personalInformationResponse?.andalanCustomerId?.isEmpty() == true )
            } else {
                val agentCode = preferencesUtil.getAgentCode()?.agentCode
                if (agentCode == "") {
                    cb_referensikan.isChecked = false
                    alertDialogConfirm.getView().tv_intro_text?.text =
                        "Anda Harus Terdaftar Sebagai Agen Terlebih Dahulu \n Daftar Sekarang?"
                    alertDialogConfirm.show()
                }
                else {
                    if (agentCode == "000") {
                        cb_referensikan.isChecked = false
                       alertDialogSuccess.getView().tvIntroSuccessLayout?.text =
                           "Proses Pendaftaran Agent Anda Sedang Diproses"
                        alertDialogSuccess.show()
                    }
                    else {
                        (activity as MainHomeActivity).isReferensikan = true
                        focusableForm(true)
                        resetPersonalInfo()
                    }
                }
            }
        }


        focusableForm(personalInformationResponse?.andalanCustomerId?.isEmpty() == true )

        rv_list_alamat?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_list_alamat?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_list_alamat?.adapter = menuBottomAdapter
        rv_list_alamat?.isNestedScrollingEnabled = false

        initMenu()
        openDatePicker()
        loadPersonalInfo()

        val observerResponse = Observer<Boolean> {
            if (it == true) {
                doResetForm()
                (activity as MainHomeActivity).model.isNeedReset.value = false
            }
        }
        (activity as MainHomeActivity).model.isNeedReset.observe(this, observerResponse)
    }

    private fun initMenu() {
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_sesuai_ktp)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_domisili)))
        menuBottomAdapter.add(MenuProfile(name = getString(R.string.alamat_kantor)))
    }

    private fun loadPersonalInfo() {
        presenter.getOccupationList()
        val dataPersonal = (activity as MainHomeActivity).preferencesUtil.getProfile()

        et_complate_name?.setText(dataPersonal?.customerName)
        et_email?.setText(dataPersonal?.email)
        et_place_of_birth?.setText(dataPersonal?.placeOfBirth)
        context?.let {
            et_date_of_birth?.setText(dataPersonal?.getDOBUI(it.getMonths()))
        }
        dateOfBirthChoosed = dataPersonal?.dateOfBirth?:""
        et_ktp_number?.setText(dataPersonal?.idNumber)
        et_mother_name?.setText(dataPersonal?.motherName)
        et_telephone_number?.setText(dataPersonal?.homePhoneNumber)
        et_label_home_phone?.setText(dataPersonal?.areaCode)
        et_phone_number?.setText(dataPersonal?.phoneNumber)
        if (dataPersonal?.gender == "M" || dataPersonal?.gender == "L") {
            rg_container.check(radio_male.id)
        } else if (dataPersonal?.gender === "P" || dataPersonal?.gender === "F") {
            rg_container.check(radio_female.id)
        }
        address = dataPersonal?.getAddressLegal()
        addressDomicile = dataPersonal?.getAddressDomicile()
        addressOffice = dataPersonal?.getAddressOffice()
        et_job?.setText(dataPersonal?.occupation)
        val salary = (dataPersonal?.monthlyIncome?.getNumberOnly()?:0).convertToRpFormat()
        et_monthly_income?.setText(salary)
        val additionalSalary = (dataPersonal?.additionalIncome?.getNumberOnly()?:0).convertToRpFormat()
        et_additional_salary?.setText(additionalSalary)
        et_additionl_job?.setText(dataPersonal?.sideJob)

        occupationChoosed = Occupation(name = dataPersonal?.occupation)

//        if (dataPersonal?.andalanCustomerId.isNullOrBlank()) {
//            focusableForm(true)
//        } else {
//            focusableForm(false)
//        }
    }

    private fun focusableForm(state: Boolean) {
        et_complate_name?.isFocusable = state
        et_complate_name?.isFocusableInTouchMode = state
        et_place_of_birth?.isFocusable = state
        et_place_of_birth?.isFocusableInTouchMode = state
        et_mother_name?.isFocusable = state
        et_mother_name?.isFocusableInTouchMode = state
    }

    private fun resetPersonalInfo() {
        address = null
        addressDomicile = null
        addressOffice = null
        et_complate_name?.setText("")
        et_email?.setText("")
        et_place_of_birth?.setText("")
        et_date_of_birth?.setText("")
        dateOfBirthChoosed = ""
        et_ktp_number?.setText("")
        et_mother_name?.setText("")
        et_telephone_number?.setText("")
        et_label_home_phone?.setText("")
        et_phone_number?.setText("")
        et_job?.setText("")
        et_monthly_income?.setText("")
        et_additional_salary?.setText("")
        et_additionl_job?.setText("")
        occupationChoosed = null

//        focusableForm(true)
    }

    private fun openDatePicker() {
        val date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            dataCalendar.set(Calendar.YEAR, year)
            dataCalendar.set(Calendar.MONTH, monthOfYear)
            dataCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val textFormat = "dd/MM/yyyy"
            val sdfText = SimpleDateFormat(textFormat, Locale.US)
            val dataFormat = "yyyy-MM-dd"
            val sdfData = SimpleDateFormat(dataFormat, Locale.US)
            dateChoosed = sdfData.format(dataCalendar.time)

            context?.let {
                et_date?.setText(sdfText.format(dataCalendar.time).stringToDate("dd/MM/yyyy").dateToString(it.getMonths()))
            }
        }

        val dateOfBirth = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            dobCalendar.set(Calendar.YEAR, year)
            dobCalendar.set(Calendar.MONTH, monthOfYear)
            dobCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            val textFormat = "dd/MM/yyyy"
            val sdfText = SimpleDateFormat(textFormat, Locale.US)
            val dataFormat = "yyyy-MM-dd"
            val sdfData = SimpleDateFormat(dataFormat, Locale.US)
            dateOfBirthChoosed = sdfData.format(dobCalendar.time)

            context?.let {
                et_date_of_birth.setText(sdfText.format(dobCalendar.time).stringToDate("dd/MM/yyyy").dateToString(it.getMonths()))
            }
        }

        et_date.setOnClickListener {
            val datePicker = DatePickerDialog(
                context!!, date, dataCalendar
                    .get(Calendar.YEAR), dataCalendar.get(Calendar.MONTH),
                dataCalendar.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.datePicker.minDate = Date().time
            datePicker.show()
        }

        et_date_of_birth.setOnClickListener {
            val personalInformationResponse = preferencesUtil.getProfile()
            if (cb_referensikan?.isChecked == true || personalInformationResponse?.andalanCustomerId?.isEmpty() == true) {
                DatePickerDialog(
                    context!!, dateOfBirth, dobCalendar
                        .get(Calendar.YEAR) - 10, dobCalendar.get(Calendar.MONTH),
                    dobCalendar.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }

        et_time.setOnClickListener {
            openTimePicker()
        }
    }

    private fun openTimePicker() {
        val hour = dataCalendar.get(Calendar.HOUR_OF_DAY)
        val minute = dataCalendar.get(Calendar.MINUTE)
        val mTimePicker: TimePickerDialog
        mTimePicker = TimePickerDialog(context,
            TimePickerDialog.OnTimeSetListener { _, selectedHour, selectedMinute ->
                et_time.setText(
                    String.format("%02d:%02d", selectedHour, selectedMinute)
                )
            }, hour, minute, true
        )
        mTimePicker.setTitle("Select Time")
        mTimePicker.show()
    }

    override fun onGetBranch(branchListResponse: BranchListResponse) {
        branchAdapter.add(branchListResponse.branchList?: arrayListOf())
    }

    override fun onGetOccupation(list: MutableList<Occupation>) {
        occupationAdapter.add(list)
        if (occupationChoosed != null || occupationChoosed?.name?.isNotBlank() == true) {
            val item = list.find { it.name == occupationChoosed?.name }
            item?.let {
                occupationChoosed = it
            }
        }
    }

    override fun onGetCity(mutableList: MutableList<Location>) {
    }

    override fun onGetSubdistrict(mutableList: MutableList<Location>) {
    }

    override fun onGetVillage(mutableList: MutableList<Location>) {
    }

    override fun onGetProvince(mutableList: MutableList<Location>) {
    }

    override fun showWait() {
        if (::alertDialogProgress.isInitialized) {
            alertDialogProgress.show()
        }
    }

    override fun removeWait() {
        if (::alertDialogProgress.isInitialized) {
            alertDialogProgress.dismiss()
        }
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout(isBlocked: Boolean = false) {
        if (!isBlocked) {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
            hideKeyboard(activity as MainHomeActivity)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1234) {
            val dataIntent = data?.extras
            if (dataIntent?.getBoolean(HOME, false) == true) {
                (activity as MainHomeActivity).moveViewNoAnim(0)
            } else if (dataIntent?.getBoolean(MESSAGE, false) == true) {
                (activity as MainHomeActivity).moveViewNoAnim(2)
            }
        } else if (requestCode == KTP_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            address = intentData?.getParcelable(KTP_DATA)
            e("tess", address.toString())
        } else if (requestCode == DOMICILE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressDomicile = intentData?.getParcelable(DOMICILE_DATA)
        } else if (requestCode == OFFICE_REQ && resultCode == Activity.RESULT_OK) {
            val intentData = data?.extras
            addressOffice = intentData?.getParcelable(OFFICE_DATA)
        }
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun doResetForm() {
        branchChoosed = null
        dateChoosed = ""
        et_time?.setText("")
        et_date?.setText("")
        et_choose_branch?.setText("")
        cb_term_condition.isChecked = false
        cb_referensikan?.isChecked = false
        loadPersonalInfo()
    }

}
