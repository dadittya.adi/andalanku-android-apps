package id.andalan.andalanku.ui.password

import id.andalan.andalanku.model.response.ResetPasswordResponse
import id.andalan.andalanku.ui.base.BaseView

interface ForgotPasswordView: BaseView {
    fun getResponse(resetPasswordResponse: ResetPasswordResponse)
}