package id.andalan.andalanku.ui.loan_application

import id.andalan.andalanku.model.response.LoanApplicationResponse
import id.andalan.andalanku.ui.base.BaseView

interface CompleteDocumentView: BaseView {
    fun onResponseSend(sendLoanApplicationResponse: LoanApplicationResponse)
}