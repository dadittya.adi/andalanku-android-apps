package id.andalan.andalanku.ui.adapters

import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Tenor
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*

class TenorAdapter(val onItemClickListener: OnClickItem): BaseAdapter<Tenor, TenorAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_default_text
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Tenor>(itemView) {
        override fun bind(data: Tenor) {
            itemView.setOnClickListener {
                onItemClickListener.onClickPromo(data)
            }
            itemView.tv_name.text = data.getYear(itemView.context.getString(R.string.year))
        }

    }

    interface OnClickItem {
        fun onClickPromo(tenor: Tenor)
    }
}