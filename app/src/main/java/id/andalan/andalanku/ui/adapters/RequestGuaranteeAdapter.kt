package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.CollateralRequest
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_request_guarantee.view.*

class RequestGuaranteeAdapter(val onItemClickListener: OnClickItem): BaseAdapter<CollateralRequest, RequestGuaranteeAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_request_guarantee
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<CollateralRequest>(itemView) {
        override fun bind(data: CollateralRequest) {
            itemView.setOnClickListener {
                onItemClickListener.onClickRequest(data)
            }

            itemView.tv_contract_number.text = data.ticketNumber
//            itemView.tv_id_request.text = data.id
            itemView.tv_date.text = data.getRequestDateUI(itemView.context.getMonths())
            itemView.tv_police_number.text = data.agreementNumber
            itemView.tv_state.text = data.status
        }

    }

    interface OnClickItem {
        fun onClickRequest(data: CollateralRequest)
    }
}