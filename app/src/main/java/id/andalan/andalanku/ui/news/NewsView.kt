package id.andalan.andalanku.ui.news

import id.andalan.andalanku.model.ui.News
import id.andalan.andalanku.ui.base.BaseView

interface NewsView: BaseView {
    fun onGetListNews(listNews: MutableList<News>)
}