@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.PpobBillBillDetails
import id.andalan.andalanku.model.ui.PpobBillCustomerDetails
import id.andalan.andalanku.model.ui.PpobBillProductDetails
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.item_ppob_bill_detail.view.*


class PpobBillDetailBillAdapter : BaseAdapter<PpobBillBillDetails, PpobBillDetailBillAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_bill_detail
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobBillBillDetails>(itemView) {
        override fun bind(ppobBillBillDetails: PpobBillBillDetails) {
            val data = ppobBillBillDetails.getBillDetails()

            itemView.tv_ppob_bill_detail_key.text = data.key

            val amount = data.value?.toLongOrNull()

            if (amount != null) {
                if (amount > 0) {
                    itemView.tv_ppob_bill_detail_value.text = amount.convertToRpFormat()
                }
                else {
                    itemView.tv_ppob_bill_detail_value.text = "FREE"
                }

            }
            else {
                itemView.tv_ppob_bill_detail_value.text = data.key
            }

        }

    }
}