package id.andalan.andalanku.ui.loan_application

import id.andalan.andalanku.model.response.BranchListResponse
import id.andalan.andalanku.model.response.PlafondApplicationResponse
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.model.ui.Occupation
import id.andalan.andalanku.ui.base.BaseView

interface CompleteDataView: BaseView {
    fun onGetProvince(mutableList: MutableList<Location>)
    fun onGetCity(mutableList: MutableList<Location>)
    fun onGetSubdistrict(mutableList: MutableList<Location>)
    fun onGetVillage(mutableList: MutableList<Location>)
    fun onGetOccupation(list: MutableList<Occupation>)
    fun onGetBranch(branchListResponse: BranchListResponse)
    fun onResponseApplicationPlafond(plafondApplicationResponse: PlafondApplicationResponse)
}