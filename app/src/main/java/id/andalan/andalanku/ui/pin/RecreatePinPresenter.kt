package id.andalan.andalanku.ui.pin

import id.andalan.andalanku.model.response.CreatePinResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class RecreatePinPresenter @Inject constructor(private val service: Services): BasePresenter<RecreatePinView>() {
    override fun onCreate() {
    }

    fun doVerificationInput(password: String, newPin: String, newPinConfirm: String) {
        when {
            password.isBlank() -> {
                view.callbackVerificationInput(1)
                return
            }
            newPin.isBlank() -> {
                view.callbackVerificationInput(2)
                return
            }
            newPin != newPinConfirm -> {
                view.callbackVerificationInput(3)
                return
            }
            else -> doResetPinWithPassword(password, newPin)
        }
    }

    private fun doResetPinWithPassword(password: String, newPin: String) {
        view.showWait()
        val subscription = service.doResetPinWithPassword(password, newPin)
            .subscribe(object : Subscriber<CreatePinResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(createPinResponse: CreatePinResponse) {
                    view.onGetResponse(createPinResponse)
                }
            })
        subscriptions.add(subscription)
    }
}