package id.andalan.andalanku.ui.ppob

import android.content.Intent
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.activity_ppob_general.*
import kotlinx.android.synthetic.main.activity_ppob_general.rv_ppob_product_list
import kotlinx.android.synthetic.main.activity_ppob_general.sliding_layout
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.ic_close
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobGeneralActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val TITLE = "title"
        val ACC_NUMBER_LABEL = "acc_number_label"
    }

    var biller:String = ""
    var accountNumber = ""
    var logo: String = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    var category: String = ""
    var title: String = ""
    var accNumberLabel: String = ""
    var minLength: Int = 0

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom


    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter

    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter
    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter
    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter
    lateinit var ppobProductListAdapter: PpobProductListAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_general)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        category = intent?.extras?.getString(PpobGeneralActivity.CATEGORY).toString()
        title = intent?.extras?.getString(PpobGeneralActivity.TITLE).toString()
        accNumberLabel = intent?.extras?.getString(PpobGeneralActivity.ACC_NUMBER_LABEL).toString()

        if (accNumberLabel != "") {
            label_ppob_bill_detail_account_number_label.text = accNumberLabel
        }

        tv_title?.text = title

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }


        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        btn_bill_detail_show_bill.setOnClickListener {

            val accountNumber = et_ppob_bill_detail_account_number.text.toString()
            if (validateNumber(accountNumber)) {
                presenter.ppobinquiry(accountNumber, productCode, "Tagihan", "", "")
            }
            else {
                biller = ""
                alertWarningCustom.getView().tv_message?.text = accNumberLabel + " Belum Lengkap"
                alertWarningCustom.show()
            }
        }

        ic_ppob_bill_detail_clear_button.setOnClickListener {
            et_ppob_bill_detail_account_number.setText("")
        }
        btn_bill_detail_payment.setOnClickListener {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, logo)
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }

        presenter.getPpobProductList(category, "", "")
        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

        if (category == "emoney"
            || category == "streaming 1"
            || category == "streaming 1"
            || category == "voucher digital"
            || category == "voucher game") {
            ppobProductListAdapter =
                PpobProductListAdapter(object : PpobProductListAdapter.OnClickItem {
                    override fun onClickMenu(ppobProduct: PpobProduct) {
                        val intent = Intent(this@PpobGeneralActivity, PpobGeneralSubProductListActivity::class.java)
                        intent.putExtra("category", category)
                        intent.putExtra("biller", ppobProduct.biller)
                        intent.putExtra("title", ppobProduct.name)
                        intent.putExtra("acc_number_label", "")
                        startActivity(intent)
                    }
                })

            et_ppob_pdam_biller_search.visibility = View.GONE
        }
        else {
            ppobProductListAdapter =
                PpobProductListAdapter(object : PpobProductListAdapter.OnClickItem {
                    override fun onClickMenu(ppobProduct: PpobProduct) {
                        productCode = ppobProduct.code.toString()
                        productName = ppobProduct.name.toString()
                        if (ppobProduct.logo != "") {
                            logo = ppobProduct.logo.toString()
                        }

                        et_ppob_bill_detail_account_number.setText("")
                        tv_bill_detail_tv_title_text.text = productName
                        if (accNumberLabel != "") {
                            label_ppob_bill_detail_account_number_label.text =
                                accNumberLabel
                        }
                        else {
                            label_ppob_bill_detail_account_number_label.text =
                                getString(R.string.account_number)
                        }
                        container_bill_detail_bills.visibility = View.GONE
                        container_bill_detail_input.visibility = View.VISIBLE

                        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
                    }
                })
        }

        et_ppob_bill_detail_account_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val accountNumber = et_ppob_bill_detail_account_number.text.toString()
                if (validateNumber(accountNumber)) {
                    presenter.ppobinquiry(accountNumber, productCode, "Tagihan", "", "")
                }
                else {
                    biller = ""
                    alertWarningCustom.getView().tv_message?.text = accNumberLabel + " Belum Lengkap"
                    alertWarningCustom.show()
                }
            }
            false
        }

        et_ppob_pdam_biller_search.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val keyword = et_ppob_pdam_biller_search.text.toString()
                presenter.getPpobProductList(
                    category, "", keyword)
            }
            false
        }

        if (category == "pdam") {
            et_ppob_bill_detail_account_number.filters += InputFilter.LengthFilter(20)
            minLength = 4
        }
        else if (category == "Internet dan TV Kabel") {
            et_ppob_bill_detail_account_number.filters += InputFilter.LengthFilter(15)
            minLength = 4
        }
        else if (category == "Pembayaran Angsuran") {
            et_ppob_bill_detail_account_number.filters += InputFilter.LengthFilter(20)
            minLength = 4
        }
        else if (category == "Kartu Kredit") {
            et_ppob_bill_detail_account_number.filters += InputFilter.LengthFilter(19)
            minLength = 4
        }


    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        if (ppobProductListResponse.status == ConfigVar.SUCCESS) {
            if (ppobProductListResponse.ppobProduct.isNullOrEmpty()) {
                //empty response
            } else {
                ppobProductListAdapter.removeAll()
                ppobProductListAdapter.add(ppobProductListResponse.ppobProduct)

                initProductItem()
            }
        }
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {
            container_bill_detail_bills.visibility = View.VISIBLE
            container_bill_detail_input.visibility = View.GONE

            accountNumber = ppobInquiryResponse.accountNumber.toString()

            if (title != "") {
                if (title == "Internet dan TV Kabel") {
                    tv_bill_detail_tv_title_text?.text = "Detail Tagihan Internet / TV"
                }
                else {
                    tv_bill_detail_tv_title_text?.text = "Detail Tagihan " + title
                }
            }
            else
            {
                tv_bill_detail_tv_title_text?.text = "Detail Tagihan"
            }

            tv_bill_detail_header_name?.text = "${preferencesUtil.getUser()?.customerName},"

            if (ppobInquiryResponse.listBillCustomerDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_customers.visibility = View.VISIBLE
                guideline3.visibility = View.VISIBLE
                rv_ppob_bill_detail_customers.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailCustomersAdapter.removeAll()
                    ppobInquiryResponse.listBillCustomerDetails?.let {
                        ppobBillDetailCustomersAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailCustomersAdapter
                }
            }
            else {
                rv_ppob_bill_detail_customers.visibility = View.GONE
                guideline3.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillProductDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_product.visibility = View.VISIBLE
                guideline4.visibility = View.VISIBLE

                rv_ppob_bill_detail_product.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailProductAdapter.removeAll()
                    ppobInquiryResponse.listBillProductDetails?.let {
                        ppobBillDetailProductAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailProductAdapter
                }
            }
            else {
                rv_ppob_bill_detail_product.visibility = View.GONE
                guideline4.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillBillDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_bills.visibility = View.VISIBLE
                guideline6.visibility = View.VISIBLE

                rv_ppob_bill_detail_bills.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailBillAdapter.removeAll()
                    ppobInquiryResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                    adapter = ppobBillDetailBillAdapter
                }
            }
            else {
                rv_ppob_bill_detail_bills.visibility = View.GONE
                guideline6.visibility = View.GONE
            }

            tv_bill_detail_amount_total?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()
            tv_bill_detail_amount_total_payment?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()

            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            totalAmount = ppobInquiryResponse.amount.toString()

            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun showWait() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }

    fun initProductItem() {
        rv_ppob_product_list.apply {
            layoutManager = GridLayoutManager(this@PpobGeneralActivity, 1)
            adapter = ppobProductListAdapter
        }
    }

    private fun validateNumber(number: String) : Boolean {
        if ((number == "") or (number.length < minLength)) {
            return false
        }

        return true
    }

}