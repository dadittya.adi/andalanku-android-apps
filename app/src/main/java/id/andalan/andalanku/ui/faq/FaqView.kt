package id.andalan.andalanku.ui.faq

import id.andalan.andalanku.model.response.FaqResponse
import id.andalan.andalanku.ui.base.BaseView

interface FaqView: BaseView {
    fun getFaqList(faqResponse: FaqResponse)
}