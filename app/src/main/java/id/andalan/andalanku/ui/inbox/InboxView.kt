package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.response.InboxResponse
import id.andalan.andalanku.ui.base.BaseView

interface InboxView: BaseView {
    fun onGetResponse(inboxResponse: InboxResponse)
}