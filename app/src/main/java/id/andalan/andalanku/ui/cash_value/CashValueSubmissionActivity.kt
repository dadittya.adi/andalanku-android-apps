package id.andalan.andalanku.ui.cash_value

import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R

class CashValueSubmissionActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cash_value_submission)
    }
}
