package id.andalan.andalanku.ui.newcar

import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class CarsDetailPresenter @Inject constructor(private val service: Services): BasePresenter<CarsDetailView>() {
    override fun onCreate() {
    }

    fun getCreditSimulation(creditSimulationRequest: CreditSimulationRequest) {
        view.showWait()
        val subscription = service.getCreditSimulation(creditSimulationRequest.type?:"",
            creditSimulationRequest.otr?:"",
            creditSimulationRequest.dp?:"",
            creditSimulationRequest.tenor?:"",
            creditSimulationRequest.asuransi?:"",
            creditSimulationRequest.wilayah?:"",
            creditSimulationRequest.car_price?:"")
            .subscribe(object : Subscriber<CreditSimulationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(creditSimulationResponse: CreditSimulationResponse) {
                    view.onGetCreditSimulation(creditSimulationResponse)
                }
        })
        subscriptions.add(subscription)
    }

    fun getCarTrim(id: String) {
        view.showWait()
        val subscription = service.getTrimCar(id)
            .subscribe(object : Subscriber<CarTrimsResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(carTrimsResponse: CarTrimsResponse) {
                    view.onGetCarTrim(carTrimsResponse)
                }
            })
        subscriptions.add(subscription)
    }

    fun getUsedCarList(year: String, keyword: String) {
        view.showWait()
        val subscription = service.getUsedCarList(year = year, keyword = keyword)
            .subscribe(object : Subscriber<UsedCarListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(usedCarListResponse: UsedCarListResponse) {
                    view.onGetUsedCarsList(usedCarListResponse)
                }
            })
        subscriptions.add(subscription)
    }
}