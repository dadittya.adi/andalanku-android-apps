package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Faq
import id.andalan.andalanku.model.ui.Tutorial
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_faq.view.*
import kotlinx.android.synthetic.main.item_tutorial.view.tv_name
import kotlinx.android.synthetic.main.item_tutorial.view.tv_number
import kotlinx.android.synthetic.main.item_tutorial.view.*
import java.lang.NullPointerException

class TutorialAnswerAdapter : BaseAdapter<Tutorial, TutorialAnswerAdapter.ViewHolder>() {
    var i = 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_tutorial
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Tutorial>(itemView) {
        override fun bind(data: Tutorial) {
            itemView.tv_number.text = "$i."
            itemView.tv_name.text = data.description
            if (data.image?.isNotEmpty() == true){
                try {
                    Glide
                        .with(itemView.context)
                        .load(data.image)
                        .into(itemView.iv_tutorial)
                } catch (error: NullPointerException) {}
            }
            i++
        }

    }
}