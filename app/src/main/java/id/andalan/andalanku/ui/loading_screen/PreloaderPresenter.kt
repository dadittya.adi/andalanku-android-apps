package id.andalan.andalanku.ui.loading_screen

import id.andalan.andalanku.model.response.TokenResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class PreloaderPresenter @Inject constructor(private val service: Services): BasePresenter<PreloaderView>() {
    override fun onCreate() {
        getTokenNumber()
    }

    private fun getTokenNumber() {
        view.showWait()
        val subscription = service.getTokenNumber().subscribe(object : Subscriber<TokenResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(tokenResponse: TokenResponse) {
                view.onGetToken(tokenResponse)
            }
        })
        subscriptions.add(subscription)
    }


}