package id.andalan.andalanku.ui.base

import androidx.recyclerview.widget.RecyclerView
import android.view.View

abstract class BaseViewHolder<in T>(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {
    abstract fun bind(data: T)
}