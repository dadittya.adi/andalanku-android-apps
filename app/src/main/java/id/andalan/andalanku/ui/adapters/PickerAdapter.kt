package id.andalan.andalanku.ui.adapters

import android.graphics.Color
import android.text.style.BackgroundColorSpan
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.CashValue
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.convertStringToHTML
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.item_picker.view.*

class PickerAdapter(val onItemClickListener: OnClickItem, val type:String = "default"): BaseAdapter<PickerItem, PickerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_picker
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PickerItem>(itemView) {
        override fun bind(data: PickerItem) {
            if (type == "cash_value") {
                val cash = data.realObject as CashValue
                val params = itemView.container.layoutParams
                params.height = 100
                itemView.container.layoutParams = params
                itemView.tv_item_value.visibility = View.VISIBLE
                itemView.tv_item_value.text = "<font color=\"#017cc2\">value: </font>${cash.CashValue?.toLong()?.convertToRpFormat()}".convertStringToHTML()
            }
            itemView.setOnClickListener {
                onItemClickListener.onClickPartner(data)
            }
            itemView.ic_done.visibility = if (data.isChoosed == true) View.VISIBLE else View.GONE
            itemView.tv_item_title.text = data.name

            if (data.contract_status != "") {
                if ((data.contract_status == "RRD") or (data.contract_status == "LIV")) {
                    itemView.tv_item_status.visibility = View.VISIBLE
                    itemView.tv_item_status.text = "Active"
                }
                else if ((data.contract_status == "WO") or (data.contract_status == "EXP")){
                    itemView.tv_item_status.visibility = View.VISIBLE
                    itemView.tv_item_status.text = "Non Active"
                    itemView.tv_item_status.setBackgroundColor(Color.GRAY)
                }
            }


        }

    }

    interface OnClickItem {
        fun onClickPartner(pickerItem: PickerItem)
    }
}