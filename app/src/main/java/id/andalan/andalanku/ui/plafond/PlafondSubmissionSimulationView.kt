package id.andalan.andalanku.ui.plafond

import id.andalan.andalanku.model.response.PlafondSimulationResponse
import id.andalan.andalanku.model.ui.Occupation
import id.andalan.andalanku.ui.base.BaseView

interface PlafondSubmissionSimulationView: BaseView {
    fun onGetSimulationPlafond(plafondSimulationResponse: PlafondSimulationResponse)
    fun onGetOccupation(list: MutableList<Occupation>)
}