package id.andalan.andalanku.ui.security

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.MenuProfile
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.password.UpdatePasswordActivity
import id.andalan.andalanku.ui.touch.TouchIntegrationActivity
import kotlinx.android.synthetic.main.activity_security_setting.*
import kotlinx.android.synthetic.main.toolbar_login.*

class SecuritySettingActivity : BaseApp() {

    private lateinit var menuMidAdapter: MenuAdapterProfile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_security_setting)
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.security_setting)

        menuMidAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == getString(R.string.kata_sandi)) {
                    startActivity(Intent(this@SecuritySettingActivity, UpdatePasswordActivity::class.java))
                } else if (menuProfile.name == getString(R.string.touch_id)) {
                    startActivity(Intent(this@SecuritySettingActivity, TouchIntegrationActivity::class.java))
                }
            }
        })

        rv_menu?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu?.adapter = menuMidAdapter
        rv_menu?.isNestedScrollingEnabled = false
        menuMidAdapter.add(MenuProfile(name = getString(R.string.kata_sandi)))
    }
}
