package id.andalan.andalanku.ui.ppob

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.tabs.TabLayout
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PpobBillDetailBillAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailCustomersAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailProductAdapter
import id.andalan.andalanku.ui.viewmodel.ViewModelPpob
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.activity_main_home.*
import kotlinx.android.synthetic.main.activity_ppob_ponsel_pascabayar.*
import kotlinx.android.synthetic.main.activity_ppob_pulsa.*
import kotlinx.android.synthetic.main.activity_ppob_pulsa.container
import kotlinx.android.synthetic.main.activity_ppob_pulsa.tabs
import kotlinx.android.synthetic.main.fragment_ppob_pln_prepaid.*
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobPlnActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
        val REQUEST_PICK_CONTACT = 100
    }

    var biller:String = ""
    var accountNumber = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    var category: String = ""

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom
    private lateinit var ppobPlnFragmentAdapter: PpobPlnFragmentAdapter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter

    lateinit var model: ViewModelPpob
    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter
    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter
    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_pln)

        deps.inject(this)
        model = ViewModelProviders.of(this).get(ViewModelPpob::class.java)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tv_title?.text = getString(R.string.listrik_pln)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        category = intent.getStringExtra(PpobPlnActivity.CATEGORY)

        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        ppobPlnFragmentAdapter = PpobPlnFragmentAdapter(supportFragmentManager)
        container.adapter = ppobPlnFragmentAdapter

        if (category == "pascabayar") {
            container.setCurrentItem(1, false)
        }

        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }

        })

        container.addOnPageChangeListener(object :
            androidx.viewpager.widget.ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                container.setCurrentItem(position, false)
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

        presenter.getPpobProductList("Listrik", "PLN Prepaid")

        btn_bill_detail_payment.setOnClickListener {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, "pln")
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }
    }

    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        model.currentPpobProduct.value = ppobProductListResponse
        //presenter.getPpobProductList2("Paket Data", biller)
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {
        model.currentPpobProduct2.value = ppobProductListResponse
    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {
        hideKeyboard(this)

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {

            container_bill_detail_input.visibility = View.GONE
            btn_bill_detail_show_bill.visibility = View.GONE

            tv_bill_detail_header_name?.text = "${preferencesUtil.getUser()?.customerName},"

            rv_ppob_bill_detail_customers.apply {
                layoutManager = GridLayoutManager(context, 1)
                ppobBillDetailCustomersAdapter.removeAll()
                ppobInquiryResponse.listBillCustomerDetails?.let {
                    ppobBillDetailCustomersAdapter.add(
                        it
                    )
                }
                adapter = ppobBillDetailCustomersAdapter
            }

            rv_ppob_bill_detail_product.apply {
                layoutManager = GridLayoutManager(context, 1)
                ppobBillDetailProductAdapter.removeAll()
                ppobInquiryResponse.listBillProductDetails?.let {
                    ppobBillDetailProductAdapter.add(
                        it
                    )
                }
                adapter = ppobBillDetailProductAdapter
            }

            rv_ppob_bill_detail_bills.apply {
                layoutManager = GridLayoutManager(context, 1)
                ppobBillDetailBillAdapter.removeAll()
                ppobInquiryResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                adapter = ppobBillDetailBillAdapter
            }

            tv_bill_detail_amount_total?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()
            tv_bill_detail_amount_total_payment?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()

            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            totalAmount = ppobInquiryResponse.amount.toString()
            productCode = ppobInquiryResponse.productCode.toString()
            productName = ppobInquiryResponse.productName.toString()

            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED


        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()

    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda Atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}