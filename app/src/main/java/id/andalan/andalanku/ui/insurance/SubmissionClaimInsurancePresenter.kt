package id.andalan.andalanku.ui.insurance

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.InsuranceClaimRequest
import id.andalan.andalanku.model.response.AgreementListResponse
import id.andalan.andalanku.model.response.InsuranceClaimResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class SubmissionClaimInsurancePresenter @Inject constructor(private val service: Services): BasePresenter<SubmissionClaimInsuranceView>() {
    override fun onCreate() {
        getAggreementNumber()
        onGetCategory()

        val listCaseEvent: MutableList<PickerItem> = arrayListOf()
        listCaseEvent.add(PickerItem(id = "ST", name = "Stolen"))
        listCaseEvent.add(PickerItem(id = "TLO", name = "Total Loss"))
        listCaseEvent.add(PickerItem(id = "A", name = "Accident"))
        view.onGetEventCase(listCaseEvent)
    }

    fun onGetCategory() {
        val listCategory: MutableList<PickerItem> = arrayListOf()
        listCategory.add(PickerItem(id = "ARK", name = "All Risk"))
        listCategory.add(PickerItem(id = "TLO", name = "TLO"))
        listCategory.add(PickerItem(id = "ARK&TLO", name = "All Risk & TLO"))
        view.onGetCategoryInsurance(listCategory)
    }

    private fun getAggreementNumber() {
        view.showWait()
        val subscription = service.getAgreementList().subscribe(object : Subscriber<AgreementListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(agreementListResponse: AgreementListResponse) {
                if (agreementListResponse.status == ConfigVar.SUCCESS) {
                    val list: MutableList<PickerItem> = arrayListOf()
                    agreementListResponse.agreementList?.let {
                        it.forEach {item ->
                            val pickerItem = PickerItem(name = item.agreementNo, contract_status = item.contract_status, isChoosed = false)
                            pickerItem.realObject = item
                            list.add(pickerItem)
                        }
                    }
                    view.onGetContractNumber(list)
                } else {
                    view.onFailure("Gagal Mendapatkan Nomer Kontrak")
                }
            }
        })
        subscriptions.add(subscription)
    }

    fun sendData(insuranceClaimRequest: InsuranceClaimRequest, media: List<String>) {
        view.showWait()
        val subscription = service.sendInsuranceClaim(insuranceClaimRequest, media).subscribe(object : Subscriber<InsuranceClaimResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(insuranceClaimResponse: InsuranceClaimResponse) {
                view.onGetResponseClaimInsurance(insuranceClaimResponse)
            }
        })
        subscriptions.add(subscription)
    }
}