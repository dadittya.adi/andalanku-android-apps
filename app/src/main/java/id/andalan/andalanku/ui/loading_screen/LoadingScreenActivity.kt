package id.andalan.andalanku.ui.loading_screen

import android.content.Intent
import android.os.Bundle
import android.util.Log.e
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.TokenResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.preloader.PreloaderActivity
import javax.inject.Inject

class LoadingScreenActivity : BaseApp(), PreloaderView {
    val TAG = PreloaderActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: PreloaderPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_loading_screen)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        e(TAG, preferencesUtil.getUser().toString())
        val user = preferencesUtil.getUser()

        if (user?.isVerify == true) {
            val intent = Intent(this@LoadingScreenActivity, MainHomeActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onGetToken(tokenResponse: TokenResponse) {
        if (tokenResponse.status == ConfigVar.SUCCESS) {
            preferencesUtil.putLoginDate()
            preferencesUtil.putAccessToken(tokenResponse.tokenNumber?:"")
            val intent = Intent(this@LoadingScreenActivity, MainHomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
        } else {
            finish()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
