package id.andalan.andalanku.ui.payment

import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.request.CreditSimulationRequest
import id.andalan.andalanku.model.response.CreditSimulationResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import kotlinx.android.synthetic.main.activity_payment_simulation.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class PaymentSimulationActivity : BaseApp() {
    private val TAG = PaymentSimulationActivity::class.java.simpleName

    companion object {
        val CREDIT = "credit"
        val IS_USED_CAR = "is_used_car"
        val RESIMULATE = "resimulate"
        val APPLY = "apply"
        val YEAR_CAR = "year"
        val REQUEST_PAYMENT = 9001
        val RESPONSE_PAYMENT = 9002
    }

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var paymentSimulationAdapter: PaymentSimulationAdapter
    lateinit var creditSimulationResponse: CreditSimulationResponse
    lateinit var collateral: Collateral
    lateinit var creditSimulationRequest: CreditSimulationRequest
    var isUsedCar = false
    var year = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_simulation)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.payment_simulation_title)

        creditSimulationResponse = intent?.extras?.getParcelable(CREDIT) as CreditSimulationResponse
        isUsedCar = intent?.extras?.getBoolean(IS_USED_CAR)?: false
        collateral = intent?.extras?.getParcelable(CompleteDocumentActivity.COLLATERAL)?: Collateral()
        creditSimulationRequest = intent?.extras?.getParcelable(CompleteDocumentActivity.CREDITRESQUEST)?: CreditSimulationRequest()

        year = intent?.extras?.getString(YEAR_CAR)?: ""

        paymentSimulationAdapter = PaymentSimulationAdapter(supportFragmentManager)
        container.adapter = paymentSimulationAdapter
        container.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(container))
    }

    fun moveView(index: Int) {
        container.currentItem = index
    }
    fun moveViewNoAnim(index: Int) {
        container.setCurrentItem(index, false)
    }

    fun finishActivity() {
        finish()
    }
}
