package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.ClaimInsurance
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_claim_insurance.view.*

class ClaimInsuranceAdapter(val onItemClickListener: OnClickItem): BaseAdapter<ClaimInsurance, ClaimInsuranceAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_claim_insurance
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<ClaimInsurance>(itemView) {
        override fun bind(data: ClaimInsurance) {
            itemView.setOnClickListener {
                onItemClickListener.onClickClaim(data)
            }

            itemView.tv_contract_number.text = data.agreementNumber
            itemView.tv_id_request.text = data.ticketNumber
            itemView.tv_date.text = data.getEventDateUI(itemView.context.getMonths())
            val item = ConfigVar.listCategory.find { data.claimType == it.id }
            itemView.tv_category_insurance.text = item?.name?:""
            itemView.tv_state.text = data.status
        }

    }

    interface OnClickItem {
        fun onClickClaim(claimInsurance: ClaimInsurance)
    }
}