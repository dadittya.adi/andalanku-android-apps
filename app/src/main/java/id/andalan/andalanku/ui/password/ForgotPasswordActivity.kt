package id.andalan.andalanku.ui.password

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.ResetPasswordResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.utils.AlertDialogCustom
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.layout_success.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class ForgotPasswordActivity : BaseApp(), ForgotPasswordView {
    companion object {
        val REQUEST_FORGOT = 611
        val RESPOSE_FORGOT = 612
    }

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: ForgotPasswordPresenter

    private lateinit var alertDialogCustom: AlertDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        setContentView(R.layout.activity_forgot_password)
        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.forgot_password_title)

        deps.inject(this)
        presenter.view = this

        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.GONE

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_success, null, false))
        alertDialogCustom.create()
        alertDialogCustom.getView().tv_title_text?.text = getString(R.string.information)
        alertDialogCustom.getView().btn_ok?.setOnClickListener {
            alertDialogCustom.dismiss()
            finish()
        }

        btn_send?.setOnClickListener {
            val email = et_email?.text.toString()
            if (email.isNotBlank()) {
                alertDialogCustom.getView().tv_intro_text?.text = getString(R.string.panduan_forgot_password, email)
                presenter.doResetPassword(email)
            } else {
                Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun getResponse(resetPasswordResponse: ResetPasswordResponse) {
        if (resetPasswordResponse.status == ConfigVar.SUCCESS) {
            alertDialogCustom.show()
        } else {
            Toast.makeText(this, resetPasswordResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showWait() {
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.VISIBLE
    }

    override fun removeWait() {
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.GONE
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
