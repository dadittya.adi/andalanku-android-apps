package id.andalan.andalanku.ui.faq

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.FaqParent
import id.andalan.andalanku.model.response.FaqResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.FaqAdapter
import kotlinx.android.synthetic.main.activity_faq.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class FaqActivity : BaseApp(), FaqView {
    private val TAG = FaqActivity::class.java.simpleName
    private lateinit var menuMidAdapter: FaqAdapter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: FaqPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_faq)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.faqs)

        menuMidAdapter = FaqAdapter(object: FaqAdapter.OnClickItem{
            override fun onClickMenu(faqParent: FaqParent) {
                val intent = Intent(this@FaqActivity, FaqDetailActivity::class.java)
                intent.putExtra(FaqDetailActivity.CONTENT_FAQS, faqParent)
                startActivity(intent)
            }
        })

        rv_menu?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu?.adapter = menuMidAdapter
        rv_menu?.isNestedScrollingEnabled = false
    }

    override fun getFaqList(faqResponse: FaqResponse) {
        if (faqResponse.status == ConfigVar.SUCCESS) {
            menuMidAdapter.add(faqResponse.faqParentList?: arrayListOf())
            menuMidAdapter.notifyDataSetChanged()
        }
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }
}
