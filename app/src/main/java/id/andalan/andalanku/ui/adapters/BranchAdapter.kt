package id.andalan.andalanku.ui.adapters

import android.util.Log
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Branch
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*
import kotlinx.android.synthetic.main.item_picker.view.*

class BranchAdapter(val onItemClickListener: BranchAdapter.OnClickItem): BaseAdapter<Branch, BranchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BranchAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_picker
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Branch>(itemView) {
        override fun bind(data: Branch) {
            itemView.tv_item_title.text = data.name

            itemView.setOnClickListener {
                onItemClickListener.onClick(data)
            }
        }

    }

    interface OnClickItem {
        fun onClick(data: Branch)
    }
}