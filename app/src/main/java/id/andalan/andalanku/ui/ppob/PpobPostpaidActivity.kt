package id.andalan.andalanku.ui.ppob

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PpobInquiryResponse
import id.andalan.andalanku.model.response.PpobProductListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PpobBillDetailBillAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailCustomersAdapter
import id.andalan.andalanku.ui.adapters.PpobBillDetailProductAdapter
import id.andalan.andalanku.ui.viewmodel.ViewModelPpob
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.activity_ppob_ponsel_pascabayar.*
import kotlinx.android.synthetic.main.activity_ppob_ponsel_pascabayar.sliding_layout
import kotlinx.android.synthetic.main.activity_ppob_pulsa.*
import kotlinx.android.synthetic.main.layout_bill_detail.*
import kotlinx.android.synthetic.main.layout_bill_detail.ic_close
import kotlinx.android.synthetic.main.layout_bill_detail.img_cd_ppob_payment_review_product_logo
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject


class PpobPostpaidActivity : BaseApp(), PpobView {

    companion object {
        val CATEGORY = "category"
        val PERMISSIONS_REQUEST_READ_CONTACTS = 100
        val REQUEST_PICK_CONTACT = 100
    }

    var biller:String = ""
    var accountNumber = ""
    var logo: String = ""
    var productCode: String = ""
    var paymentUrl: String = ""
    var productName: String = ""
    var totalAmount: String = "0"
    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom


    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: PpobPresenter

    lateinit var ppobBillDetailCustomersAdapter: PpobBillDetailCustomersAdapter

    lateinit var ppobBillDetailBillAdapter: PpobBillDetailBillAdapter

    lateinit var ppobBillDetailProductAdapter: PpobBillDetailProductAdapter

    lateinit var model: ViewModelPpob

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ppob_ponsel_pascabayar)

        deps.inject(this)
        model = ViewModelProviders.of(this).get(ViewModelPpob::class.java)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        tv_title?.text = getString(R.string.pascabayar)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(
            LayoutInflater.from(this).inflate(
                R.layout.layout_progress_loading,
                null,
                false
            )
        )
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
        }

        presenter.view = this

        et_ppob_postpaid_phone_number.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                val phoneNumber = et_ppob_postpaid_phone_number.text.toString()
                detectOperator(phoneNumber)
            }
            false
        }

        ic_ppob_postpaid_phonebook.setOnClickListener {
            loadContacts()
        }

        ic_ppob_postpaid_clear_button.setOnClickListener {
            et_ppob_postpaid_phone_number.setText("")
            accountNumber = ""
            img_ppob_postpaid_operator_logo.visibility = View.GONE
        }

        label_prabayar.setOnClickListener {
            val intent = Intent(this, PpobPulsaActivity::class.java)
            intent.putExtra(PpobPulsaActivity.CATEGORY, "pulsa")
            startActivity(intent)
        }

        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        btn_ppob_postpaid_show_bill.setOnClickListener {
            if (et_ppob_postpaid_phone_number.text.toString() != "") {
                val phoneNumber = et_ppob_postpaid_phone_number.text.toString()
                detectOperator(phoneNumber)
            }
            else {
                biller = ""
                alertWarningCustom.getView().tv_message?.text = "Nomor Handphone Belum Diisi"
                alertWarningCustom.show()
            }
        }

        btn_bill_detail_payment.setOnClickListener {
            val intent = Intent(this, PpobPaymentProcessActivity::class.java)

            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_IMAGE, logo)
            intent.putExtra(PpobPaymentProcessActivity.MONTH, "")
            intent.putExtra(PpobPaymentProcessActivity.ZONE_ID, "")
            intent.putExtra(
                PpobPaymentProcessActivity.ACCOUNT_NUMBER, accountNumber
            )
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_CODE, productCode)
            intent.putExtra(PpobPaymentProcessActivity.PRODUCT_NAME, productName)
            intent.putExtra(PpobPaymentProcessActivity.PAYMENT_URL, paymentUrl)
            intent.putExtra(PpobPaymentProcessActivity.TOTAL_AMOUNT, totalAmount)
            startActivity(intent)
        }

        ppobBillDetailProductAdapter = PpobBillDetailProductAdapter()
        ppobBillDetailCustomersAdapter = PpobBillDetailCustomersAdapter()
        ppobBillDetailBillAdapter = PpobBillDetailBillAdapter()

    }

    private fun detectOperator(phoneNumber: String) {
        val standardPhoneNumber = phoneNumber.replace("+62", "0")
        et_ppob_postpaid_phone_number.setText(standardPhoneNumber)

        if (standardPhoneNumber.length >= 10) {
            val prefix = phoneNumber.take(4)

            if ((prefix == "0811") or
                (prefix == "0812") or
                (prefix == "0813") or
                (prefix == "0821") or
                (prefix == "0822") or
                (prefix == "0823") or
                (prefix == "0851") or
                (prefix == "0852") or
                (prefix == "0853")) {

                biller = "Telkomsel Halo"

                img_ppob_postpaid_operator_logo.setImageResource(R.drawable.telkomsel_logo)
                img_ppob_postpaid_operator_logo.visibility = View.VISIBLE

                logo = "Telkomsel"
            }
            else if((prefix == "0814") or
                (prefix == "0815") or
                (prefix == "0816") or
                (prefix == "0855") or
                (prefix == "0856") or
                (prefix == "0857") or
                (prefix == "0858")) {

                biller = "Indosat"

                img_ppob_postpaid_operator_logo.setImageResource(R.drawable.indosat_logo)
                img_ppob_postpaid_operator_logo.visibility = View.VISIBLE

                logo = "Indosat"
            }

            else if((prefix == "0817") or
                (prefix == "0818") or
                (prefix == "0819") or
                (prefix == "0859") or
                (prefix == "0877") or
                (prefix == "0878")) {

                biller = "XL"

                img_ppob_postpaid_operator_logo.setImageResource(R.drawable.xl_logo)
                img_ppob_postpaid_operator_logo.visibility = View.VISIBLE

                logo = "XL"
            }

            else if((prefix == "0831") or
                (prefix == "0832") or
                (prefix == "0833") or
                (prefix == "0838")) {

                biller = "Axis"

                img_ppob_postpaid_operator_logo.setImageResource(R.drawable.axis_logo)
                img_ppob_postpaid_operator_logo.visibility = View.VISIBLE

                logo = "Axis"
            }

            else if((prefix == "0895") or
                (prefix == "0896") or
                (prefix == "0897") or
                (prefix == "0898") or
                (prefix == "0899")) {

                biller = "Three"
                img_ppob_postpaid_operator_logo.setImageResource(R.drawable.tri_logo)
                img_ppob_postpaid_operator_logo.visibility = View.VISIBLE

                logo = "Three"
            }

            else if((prefix == "0881") or
                (prefix == "0882") or
                (prefix == "0883") or
                (prefix == "0884") or
                (prefix == "0885") or
                (prefix == "0886") or
                (prefix == "0887") or
                (prefix == "0888") or
                (prefix == "0889")) {

                biller = "Smartfren"

                img_ppob_postpaid_operator_logo.setImageResource(R.drawable.smartfren_logo)
                img_ppob_postpaid_operator_logo.visibility = View.VISIBLE

                logo = "Smartfren"
            }

            else {
                img_ppob_postpaid_operator_logo.visibility = View.GONE
                biller = ""
                alertWarningCustom.getView().tv_message?.text = "Nomor Handphone Tidak Dikenali"
                alertWarningCustom.show()
            }

            if (biller != "") {
                presenter.getPpobProductList("Pascabayar", biller)
            }

            accountNumber = phoneNumber
        }
        else {
            img_ppob_postpaid_operator_logo.visibility = View.GONE
            biller = ""
            alertWarningCustom.getView().tv_message?.text = "Nomor Handphone Minimal 10 digit"
            alertWarningCustom.show()
        }
    }
    override fun onGetProductListResponse(ppobProductListResponse: PpobProductListResponse) {
        if (ppobProductListResponse.ppobProduct?.size ?: 0 > 0) {
            productCode = ppobProductListResponse.ppobProduct?.first()?.code.toString()
            productName = ppobProductListResponse.ppobProduct?.first()?.name.toString()
            if (logo == "") {
                logo = ppobProductListResponse.ppobProduct?.first()?.logo.toString()
            }

            if (logo != "") {
                if (logo == "Telkomsel") {
                    img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.telkomsel_logo)
                } else if (logo == "Indosat") {
                    img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.indosat_logo)
                } else if (logo == "XL") {
                    img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.xl_logo)
                } else if (logo == "Axis") {
                    img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.axis_logo)
                } else if (logo == "Three") {
                    img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.tri_logo)
                } else if (logo == "Smartfren") {
                    img_cd_ppob_payment_review_product_logo.setImageResource(R.drawable.smartfren_logo)
                } else {
                    loadImage(this, logo, img_cd_ppob_payment_review_product_logo)
                }
            }

            if (productCode != null) {
                presenter.ppobinquiry(accountNumber, productCode, "Tagihan", "", "")
            }
        }
        else {
            alertWarningCustom.getView().tv_message?.text = "Produk Tidak Tersedia"
            alertWarningCustom.show()
        }
    }

    override fun onGetProductListResponse2(ppobProductListResponse: PpobProductListResponse) {

    }

    override fun onPpobInquiryResponse(ppobInquiryResponse: PpobInquiryResponse) {

        if (ppobInquiryResponse.status == ConfigVar.SUCCESS) {

            container_bill_detail_input.visibility = View.GONE

            tv_bill_detail_header_name?.text = "${preferencesUtil.getUser()?.customerName},"

            if (ppobInquiryResponse.listBillCustomerDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_customers.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailCustomersAdapter.removeAll()
                    ppobInquiryResponse.listBillCustomerDetails?.let {
                        ppobBillDetailCustomersAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailCustomersAdapter
                }
            }
            else {
                rv_ppob_bill_detail_customers.visibility = View.GONE
                guideline3.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillProductDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_product.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailProductAdapter.removeAll()
                    ppobInquiryResponse.listBillProductDetails?.let {
                        ppobBillDetailProductAdapter.add(
                            it
                        )
                    }
                    adapter = ppobBillDetailProductAdapter
                }
            }
            else {
                rv_ppob_bill_detail_product.visibility = View.GONE
                guideline4.visibility = View.GONE
            }

            if (ppobInquiryResponse.listBillBillDetails?.size ?: 0 > 0) {
                rv_ppob_bill_detail_bills.apply {
                    layoutManager = GridLayoutManager(context, 1)
                    ppobBillDetailBillAdapter.removeAll()
                    ppobInquiryResponse.listBillBillDetails?.let { ppobBillDetailBillAdapter.add(it) }
                    adapter = ppobBillDetailBillAdapter
                }
            }
            else {
                rv_ppob_bill_detail_bills.visibility = View.GONE
                guideline6.visibility = View.GONE
            }

            tv_bill_detail_amount_total?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()
            tv_bill_detail_amount_total_payment?.text =
                ppobInquiryResponse.amount?.toLong()?.convertToRpFormat()

            paymentUrl = ppobInquiryResponse.paymentUrl.toString()
            totalAmount = ppobInquiryResponse.amount.toString()

            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }
        else {
            alertWarningCustom.getView().tv_message?.text = ppobInquiryResponse.message
            alertWarningCustom.show()
        }

        removeWait()
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().tv_message?.text = "Inquiry Gagal, Periksa Kembali Inputan Anda atau Coba Beberapa Saat Lagi"
        alertWarningCustom.show()
    }

    private fun loadContacts() {
        var builder = StringBuilder()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(
                Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_CONTACTS),
                PERMISSIONS_REQUEST_READ_CONTACTS
            )
            //callback onRequestPermissionsResult
        } else {
            val intent =
                Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI)
            startActivityForResult(intent, REQUEST_PICK_CONTACT)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                loadContacts()
            } else {
                //  toast("Permission must be granted in order to display contacts information")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_PICK_CONTACT && data?.data != null) {
            val contactUri = data.data
            val cursor = contentResolver.query(contactUri!!, null, null, null, null)
            if (cursor!!.moveToFirst()) {
                var phoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

                phoneNumber = phoneNumber.replace("+62", "0")
                val re = Regex("[^0-9]")
                phoneNumber = phoneNumber.let { re.replace(it, "") }
                et_ppob_postpaid_phone_number.setText(phoneNumber)
                detectOperator(phoneNumber)
            }

        }

    }

}