package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Inbox
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.getDays
import id.andalan.andalanku.utils.getMonths
import id.andalan.andalanku.utils.getMonthsShort
import kotlinx.android.synthetic.main.item_message_inbox.view.*

class InboxAdapter(val onItemClickListener: OnClick): BaseAdapter<Inbox, InboxAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_message_inbox
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Inbox>(itemView) {
        override fun bind(data: Inbox) {
            itemView.setOnClickListener {
                onItemClickListener.onClickItem(data)
            }
            var newTitle = data.title
            if (newTitle?.length!! > 22) {
                newTitle = data.title?.substring(0, 21) + "..."
            }
            itemView.tv_title?.text = newTitle
            itemView.tv_content?.text = data.message
            itemView.iv_thumbnail?.setImageResource(if (data.status == "1") R.drawable.ic_read_message else R.drawable.ic_unread_message)
            itemView.tv_date?.text = data.displayDate
        }
    }

    interface OnClick {
        fun onClickItem(inbox: Inbox)
    }
}