package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Brands
import id.andalan.andalanku.model.ui.CarBrand
import id.andalan.andalanku.model.ui.CarType
import id.andalan.andalanku.model.ui.Cars
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.ekstensions.loadImage
import kotlinx.android.synthetic.main.item_cars.view.*

class CarsAdapter(val onItemClickListener: OnClickItem, val carBrand: CarBrand): BaseAdapter<CarType, CarsAdapter.ViewHolder>() {
    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_cars
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<CarType>(itemView) {
        override fun bind(data: CarType) {
            itemView.setOnClickListener {
                onItemClickListener.onClickImage(data, carBrand)
            }
            try {
                data.images?.let {
                    loadImage(itemView.context, it[0], itemView.iv_car_image)
                }
            } catch (e: Exception) { }
            itemView.tv_brands?.text = carBrand.name
            itemView.tv_name.text = data.name
            itemView.tv_start_from_price.text = data.lowestPrice
        }

    }

    interface OnClickItem {
        fun onClickImage(carType: CarType, carBrand: CarBrand)
    }
}