package id.andalan.andalanku.ui.newcar

import id.andalan.andalanku.model.response.CarTypesResponse
import id.andalan.andalanku.ui.base.BaseView

interface CarChooserView: BaseView {
    fun onGetTypeCar(carTypesResponse: CarTypesResponse)
}