package id.andalan.andalanku.ui.adapters

import android.util.Log.e
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Menu
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_menu_home.view.*

class MenuAdapterHome(val onItemClickListener: onClickItem): BaseAdapter<Menu, MenuAdapterHome.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuAdapterHome.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_menu_home
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Menu>(itemView) {
        override fun bind(data: Menu) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            itemView.ic_logo.setImageResource(data.image)
            itemView.title_text.text = data.name
            e("tes", data.name)
        }

    }

    interface onClickItem {
        fun onClickMenu(menu: Menu)
    }
}