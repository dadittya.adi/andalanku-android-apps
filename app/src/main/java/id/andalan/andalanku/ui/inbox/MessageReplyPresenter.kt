package id.andalan.andalanku.ui.inbox

import id.andalan.andalanku.model.request.InputComplainRequest
import id.andalan.andalanku.model.request.InputMessageRequest
import id.andalan.andalanku.model.response.CategoryComplainResponse
import id.andalan.andalanku.model.response.InputComplainResponse
import id.andalan.andalanku.model.response.InputMessageResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import okhttp3.MultipartBody
import retrofit2.http.Multipart
import rx.Subscriber
import javax.inject.Inject

class MessageReplyPresenter @Inject constructor(private val service: Services): BasePresenter<MessageReplyView>() {

    fun sendMessage(request: InputMessageRequest, media: List<String>) {
        view.showWait()
        val subscription = service.sendMessage(request, media)
            .subscribe(object : Subscriber<InputMessageResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(inputMessageResponse: InputMessageResponse) {
                    view.onSuccessSendMessage(inputMessageResponse)
                }
            })
        subscriptions.add(subscription)
    }

    override fun onCreate() {

    }
}