package id.andalan.andalanku.ui.contract

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import android.view.LayoutInflater
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.PickerAdapter
import id.andalan.andalanku.ui.viewmodel.ViewModelAndalanDetail
import id.andalan.andalanku.utils.AlertDialogCustom
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.activity_contract_detail.*
import kotlinx.android.synthetic.main.layout_picker.*
import kotlinx.android.synthetic.main.toolbar_plafond.*
import javax.inject.Inject

class ContractDetailActivity : BaseApp(), ContractView {
    companion object {
        val AGREEMENT_NUMBER = "aggreement_number"
        val LIST_AGREEMENT = "list_aggreement"
    }

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var services: Services
    @Inject
    lateinit var presenter: ContractPresenter

    private lateinit var pickerAdapter: PickerAdapter
    var choosedPicker = PickerItem()
    private lateinit var contractDetailViewAdapter: ContractDetailViewAdapter

    var detailPaymentHistoryResponse: DetailPaymentHistoryResponse? = null
    var detailUnitResponse: DetailUnitResponse? = null
    var detailFinanceResponse: DetailFinanceResponse? = null
    var detailInsuranceResponse: DetailInsuranceResponse? = null
    var agreementDocumentResponse: AgreementDocumentResponse? = null

    private lateinit var alertDialogCustom: AlertDialogCustom

    lateinit var model: ViewModelAndalanDetail

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contract_detail)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.detail_dokumen_kontrak)

        alertDialogCustom = AlertDialogCustom(this, null)
        alertDialogCustom.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogCustom.create()
        alertDialogCustom.setCancelable(true)

        if (!intent.hasExtra(AGREEMENT_NUMBER)) {
            finish()
        }

        val agreementNumber = intent.getStringExtra(AGREEMENT_NUMBER)
        val parcel = intent?.extras?.getParcelableArray(LIST_AGREEMENT)
        val listAgreement: MutableList<PickerItem> = arrayListOf()
        choosedPicker.name = agreementNumber

        parcel?.forEach {
            listAgreement.add(it as PickerItem)
        }

        deps.inject(this)
        presenter.view = this
        model = ViewModelProviders.of(this).get(ViewModelAndalanDetail::class.java)

        tv_title_text?.text = getString(R.string.choose_contract_number)
        ic_close?.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
        }
        sliding_layout?.isTouchEnabled = false
        sliding_layout?.setFadeOnClickListener { sliding_layout.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED }

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                val index = pickerAdapter.getData().indexOf(choosedPicker)

                choosedPicker.isChoosed = false
                if (index > -1) pickerAdapter.update(index, choosedPicker)
                btn_sisa_pinjaman?.text = pickerItem.name
                pickerItem.isChoosed = true
                choosedPicker = pickerItem
                val indexChoosed = pickerAdapter.getData().indexOf(pickerItem)
                if (indexChoosed > -1) pickerAdapter.update(indexChoosed, choosedPicker)
                btn_sisa_pinjaman.text = choosedPicker.name
                loadData()
                sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
            }
        })

        pickerAdapter.add(listAgreement)
        rv_item?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_item?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_item?.adapter = pickerAdapter

        btn_sisa_pinjaman.text = agreementNumber
        btn_sisa_pinjaman.setOnClickListener {
            sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        }

        contractDetailViewAdapter = ContractDetailViewAdapter(supportFragmentManager)

        vp_content.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(vp_content))
        vp_content.adapter = contractDetailViewAdapter
        presenter.getAndalanFinancial(agreementNumber)

        loadData()
    }
    private fun loadData() {
        presenter.getAndalanFinancial(choosedPicker.name?:"")
        tv_value_sisa_pinjaman?.text = preferencesUtil.getProfile()?.customerName?:""
    }

    override fun getAndalanFinencial(detailAndalanFinancialResponse: DetailAndalanFinancialResponse) {
        model.currentAndalanFinancialResponse.value = detailAndalanFinancialResponse

//        detailAndalanFinancialResponse.andalanDetailUnit?.let {
//            if (it.isNotEmpty()) {
//                val detailUnitResponse = it[0]
//                this.detailUnitResponse = detailUnitResponse
//                val fa = vp_content.adapter as ContractDetailViewAdapter
//                val fragment = fa.getItem(0) as DetailUnitFragment
//                fragment.getDetailUnit(detailUnitResponse)
//            }
//        }

        detailAndalanFinancialResponse.andalanDetailFinancial?.let {
            if (it.isNotEmpty()) {
                val detailFinanceResponse = it[0]
                this.detailFinanceResponse = detailFinanceResponse
                tv_value_jatuh_kontrak?.text = detailFinanceResponse.getMaturityDateUI(this.getMonths())
            }
        }


        detailAndalanFinancialResponse.andalanDetailPaymentHistory?.let {
            if (it.isNotEmpty()) {
                val detailPaymentHistoryResponse = it[0]
                this.detailPaymentHistoryResponse = detailPaymentHistoryResponse
                val fa = vp_content.adapter as ContractDetailViewAdapter
                val fragment = fa.getItem(2) as PaymentHistoryFragment
                fragment.getPaymentHistory(detailPaymentHistoryResponse)
            }
        }

        detailAndalanFinancialResponse.andalanDetailInsurance?.let {
            if (it.isNotEmpty()) {
                val detailInsuranceResponse = it[0]
                this.detailInsuranceResponse = detailInsuranceResponse
                val fa = vp_content.adapter as ContractDetailViewAdapter
                val fragment = fa.getItem(3) as PolisAsuransiFragment
                fragment.getDetailPolis(detailInsuranceResponse)
            }
        }

        detailAndalanFinancialResponse.andalanAgreementDocumument?.let {
            if (it.isNotEmpty()) {
                val agreementDocumentResponse = it[0]
                this.agreementDocumentResponse = agreementDocumentResponse
                val fa = vp_content.adapter as ContractDetailViewAdapter
                val fragment = fa.getItem(4) as AgreementContractFragment
                fragment.getAgreementDocument(agreementDocumentResponse)
            }
        }
    }
    override fun showWait() {
        alertDialogCustom.show()
    }

    override fun removeWait() {
        alertDialogCustom.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }
}
