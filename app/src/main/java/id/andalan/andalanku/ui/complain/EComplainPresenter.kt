package id.andalan.andalanku.ui.complain

import id.andalan.andalanku.model.response.ComplainListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class EComplainPresenter @Inject constructor(private val service: Services): BasePresenter<EComplainView>() {
    override fun onCreate() {
        getListComplain()
    }

    fun getListComplain(status: String? = "") {
        view.showWait()
        val subscription = service.getComplainList(status)
            .subscribe(object : Subscriber<ComplainListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(complainListResponse: ComplainListResponse) {
                    view.getListResponse(complainListResponse)
                }
            })
        subscriptions.add(subscription)
    }
}