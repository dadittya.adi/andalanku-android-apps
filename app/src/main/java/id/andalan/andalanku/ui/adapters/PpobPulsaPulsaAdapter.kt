@file:Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
package id.andalan.andalanku.ui.adapters

//import android.util.Log.e
import android.graphics.Color
import androidx.core.content.ContextCompat
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory
import id.andalan.andalanku.model.ui.PartnerHome
import id.andalan.andalanku.model.ui.PpobProduct
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.item_ppob_pulsa_pulsa.view.*
import kotlinx.android.synthetic.main.item_trasaction.view.*

class PpobPulsaPulsaAdapter(val onItemClickListener: OnClickItem): BaseAdapter<PpobProduct, PpobPulsaPulsaAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_ppob_pulsa_pulsa
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<PpobProduct>(itemView) {
        override fun bind(ppobProduct: PpobProduct) {
            val data = ppobProduct.getPpobProduct()

            var productName = data.name
            val re = Regex("[^0-9]")
            productName = productName?.let { re.replace(it, "") }
            productName = productName?.dropLast(3) + "."
            val productPrice = "Harga ${data.amount?.toLong()?.convertToRpFormat()}"
            itemView.tv_ppob_pulsa_amount_header.text = productName.toString()
            itemView.tv_ppob_pulsa_amount_thousand.text = "000"
            itemView.tv_ppob_pulsa_price.text = productPrice

            itemView.setOnClickListener {
                onItemClickListener.onClickProduct(data)
            }
        }

    }

    interface OnClickItem {
        fun onClickProduct(ppobProduct: PpobProduct)
    }
}