package id.andalan.andalanku.ui.base

import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView

abstract class BaseAdapter<T, Holder: BaseViewHolder<T>> : androidx.recyclerview.widget.RecyclerView.Adapter<Holder>() {
    private var data: MutableList<T> = arrayListOf()

    abstract override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder
    abstract fun getItemResourceLayout(viewType: Int): Int

    fun getView(parent: ViewGroup?, viewType: Int): View = LayoutInflater.from(parent?.context).inflate(getItemResourceLayout(viewType), parent, false)

    fun getData(): List<T> = data

    fun add(item: T) {
        data.add(item)
        this.notifyDataSetChanged()
    }
    fun add(items: MutableList<T>) {
        data.addAll(items)
        this.notifyDataSetChanged()
    }
    fun removeAt(index: Int) {
        data.removeAt(index)
        this.notifyDataSetChanged()
    }
    fun removeAll() {
        data = ArrayList()
        this.notifyDataSetChanged()
    }
    fun update(index: Int, item: T) {
        data[index] = item
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: Holder, position: Int) = holder.bind(data[position])
}