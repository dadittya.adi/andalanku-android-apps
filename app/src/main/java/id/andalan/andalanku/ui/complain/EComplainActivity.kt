package id.andalan.andalanku.ui.complain

import android.content.Intent
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.util.Log.e
import android.view.View
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.ComplainListResponse
import id.andalan.andalanku.model.ui.Complain
import id.andalan.andalanku.model.ui.Complainservices
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.ComplainAdapter
import kotlinx.android.synthetic.main.activity_ecomplain.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class EComplainActivity : BaseApp(), EComplainView {
    companion object {
        val REQUEST_SEND_COMPLAIN = 113
        val RESPONSE_SEND_COMPLAIN = 213
    }
    private val TAG = EComplainActivity::class.java.simpleName

    private lateinit var complainAdapter: ComplainAdapter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: EComplainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ecomplain)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.e_complain_title)
        layout?.setBackgroundColor(ContextCompat.getColor(this, R.color.snow))
        btn_add?.visibility = View.VISIBLE
        tabs?.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabReselected(p0: TabLayout.Tab?) {
                e(TAG, "reselect ${p0?.position}")
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
                e(TAG, "un selected ${p0?.position}")
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                onTabsPosition(p0?.position?:0)
            }

        })
        btn_add?.setOnClickListener {
            startActivityForResult(Intent(this, SubmissionComplainActivity::class.java), REQUEST_SEND_COMPLAIN)
        }
        btn_complain_submission?.setOnClickListener {
            startActivityForResult(Intent(this, SubmissionComplainActivity::class.java), REQUEST_SEND_COMPLAIN)
        }

        complainAdapter = ComplainAdapter(object: ComplainAdapter.OnClickItem{
            override fun onClickComplain(complain: Complain) {
                val intent = Intent(this@EComplainActivity, DetailComplainActivity::class.java)
                intent.putExtra(DetailComplainActivity.COMPLAIN, complain)
                startActivity(intent)
            }
        })

        rv_complain?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_complain?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_complain?.adapter = complainAdapter

    }

    fun onTabsPosition (position: Int) {
        when (position) {
            0 -> {
                presenter.getListComplain()
            }
            1 -> {
                presenter.getListComplain(ConfigVar.SUBMITTED)
            }
            2 -> {
                presenter.getListComplain(ConfigVar.ONPROCESS)
            }
            3 -> {
                presenter.getListComplain(ConfigVar.SOLVED)
            }
        }
        complainAdapter.removeAll()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_SEND_COMPLAIN && resultCode == RESPONSE_SEND_COMPLAIN) {
            onTabsPosition(tabs.selectedTabPosition)
        }
    }

    override fun getListResponse(complainListResponse: ComplainListResponse) {
        if (complainListResponse.status == ConfigVar.SUCCESS) {
            complainListResponse.listComplain?.let {
                if (it.size > 0) {
                    complainAdapter.add(it)
                    complainAdapter.notifyDataSetChanged()
                    showEmptyState(false)
                } else {
                    showEmptyState(true)
                }
            }
        } else {
            showEmptyState(true)
        }
    }

    private fun showEmptyState(state: Boolean) {
        rv_complain?.visibility = if (state) View.GONE else View.VISIBLE
        container_empty_state?.visibility = if (state) View.VISIBLE else View.GONE
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this, appErrorMessage, Toast.LENGTH_SHORT).show()
    }
}
