@file:Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package id.andalan.andalanku.ui.dana

import android.app.Activity
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.View
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.utils.ekstensions.setHeight
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_choose_document_agunan.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import android.content.Intent
import android.util.Log.e
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.*
import id.andalan.andalanku.ui.loan_application.CompleteDocumentActivity
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.dateToString
import kotlinx.android.synthetic.main.layout_agunan_document_car_type.*
import kotlinx.android.synthetic.main.layout_agunan_document_sertipikat_type.*
import java.util.*
import javax.inject.Inject


class ChooseDocumentAgunanActivity : BaseApp(), ChooseDocumentAgunanView {

    companion object {
        val REQUEST_DANA_SIMULATION = 981
    }

    override fun onGetUsedCarsList(usedCarListResponse: UsedCarListResponse) {
        if (usedCarListResponse.status == ConfigVar.SUCCESS) {
            usedCarListResponse.carPriceList?.let {
                if (it.size > 0) {
                    carsTypeAdapter.add(it)
                } else {
                    Toast.makeText(this, "Daftar mobil Kosong", Toast.LENGTH_SHORT).show()
                }
            }
        } else {
            Toast.makeText(this, "Gagal Mendapatkan Daftar Mobil", Toast.LENGTH_SHORT).show()
        }
        hideKeyboard(this)
    }

    private val TAG = ChooseDocumentAgunanActivity::class.java.simpleName

    private lateinit var pickerAdapter: PickerAdapter
    private lateinit var pickerAdapterSertipikat: PickerAdapter
    private lateinit var pickerAdapterYear: PickerAdapter

    private lateinit var pickerAdapterBrand: PickerAdapter
    private lateinit var pickerAdapterType: PickerAdapter
    private lateinit var pickerAdapterModel: PickerAdapter
    private lateinit var pickerAdapterYearCar: PickerAdapter

//    private lateinit var brandsAdapter: BrandsAdapter
//    private lateinit var carsAdapter: CarsAdapter
//    private lateinit var carTrimAdapter: CarTrimAdapter

    private var choosedItem:PickerItem = PickerItem()
    private var choosedIndex = -1
    private var choosedSertipikat = PickerItem()
    private var choosedIndexSertipikat = -1
    private var choosedYear = PickerItem()
    private var choosedIndexYear = -1
    private var choosedYearCar = PickerItem()
    private var choosedIndexYearCar = -1

    private var choosedBrand = PickerItem()
    private var choosedIndexBrand = -1
    private var choosedType = PickerItem()
    private var choosedIndexType= -1
    private var choosedModel = PickerItem()
    private var choosedIndexModel = -1

    private var choosedUsedCar: UsedCar? = null

    //private var bpkb = Bpkb()
    //private var sertipikat = Sertipikat()
    private lateinit var collateral: Collateral

    private lateinit var carsTypeAdapter: CarsTypeAdapter

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: ChooseDocumentAgunanPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_document_agunan)

        if (intent.hasExtra(SimulationDanaActivity.CHOOSED_INDEX)) {
            choosedIndex = intent.extras?.getInt(SimulationDanaActivity.CHOOSED_INDEX) ?: 0
            if (choosedIndex > -1) setDocumentAgunan()
        }

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.agunan_data)

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }
        showSlideLayout()
        hideSlideLayout()

        et_year_car?.setText(Date().dateToString("yyyy"))
        et_built_year_sertipikat?.setText(Date().dateToString("yyyy"))

        carsTypeAdapter = CarsTypeAdapter(object: CarsTypeAdapter.OnClickItem{
            override fun onClickType(cars: UsedCar) {
                et_type?.setText(cars.AssetName)
                choosedUsedCar = cars
                et_price_car?.setText(choosedUsedCar?.Price?.toLong()?.convertToRpFormat())
                hideSlideLayout()
            }
        })

        pickerAdapter = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                if (choosedIndex > -1) pickerAdapter.update(choosedIndex, PickerItem(name = choosedItem.name, isChoosed = false))
                val index = pickerAdapter.getData().indexOf(pickerItem)
                choosedItem = pickerItem

                e(TAG, pickerItem.name)

                if (index > -1) {
                    choosedIndex = index
                    pickerAdapter.update(index, PickerItem(name = pickerItem.name, isChoosed = true))
                }
                hideSlideLayout()
                setDocumentAgunan()
            }
        })

        pickerAdapterSertipikat = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                if (choosedIndexSertipikat > -1) pickerAdapterSertipikat.update(choosedIndexSertipikat, PickerItem(name = choosedSertipikat.name, isChoosed = false))
                val index = pickerAdapterSertipikat.getData().indexOf(pickerItem)
                choosedSertipikat = pickerItem

                if (index > -1) {
                    et_sertipikat_type.setText(pickerItem.name)
                    choosedIndexSertipikat = index
                    pickerAdapterSertipikat.update(index, PickerItem(name = pickerItem.name, isChoosed = true))
                }
                hideSlideLayout()
            }
        })

        pickerAdapterYear = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                if (choosedIndexYear> -1) pickerAdapterYear.update(choosedIndexYear, PickerItem(name = choosedYear.name, isChoosed = false))
                val index = pickerAdapterYear.getData().indexOf(pickerItem)
                choosedYear = pickerItem

                if (index > -1) {
                    et_built_year_sertipikat.setText(pickerItem.name)
                    choosedIndexYear = index
                    pickerAdapterYear.update(index, PickerItem(name = pickerItem.name, isChoosed = true))
                }
                hideSlideLayout()
            }
        })

        pickerAdapterBrand = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                choosedBrand.isChoosed = false
                if (choosedIndexBrand> -1) pickerAdapterBrand.update(choosedIndexBrand, choosedBrand)
                val index = pickerAdapterBrand.getData().indexOf(pickerItem)
                choosedBrand = pickerItem
                choosedBrand.isChoosed = true

                if (index > -1) {
                    presenter.getTypesCar(choosedBrand.id?:"")
                    et_cars_brand.setText(pickerItem.name)
                    choosedIndexBrand = index
                    pickerAdapterBrand.update(index, choosedBrand)
                }
                hideSlideLayout()
            }
        })

        pickerAdapterType = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                choosedType.isChoosed = false
                if (choosedIndexType> -1) pickerAdapterType.update(choosedIndexType, choosedType)
                val index = pickerAdapterType.getData().indexOf(pickerItem)
                choosedType = pickerItem
                choosedType.isChoosed = true

                if (index > -1) {
                    presenter.getCarTrim(choosedType.id?:"")
                    et_cars_type.setText(pickerItem.name)
                    choosedIndexType = index
                    pickerAdapterType.update(index, choosedType)
                }
                hideSlideLayout()
            }
        })

        pickerAdapterYearCar = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                if (choosedIndexYearCar> -1) pickerAdapterYearCar.update(choosedIndexYearCar, PickerItem(name = choosedYearCar.name, isChoosed = false))
                val index = pickerAdapterYearCar.getData().indexOf(pickerItem)
                choosedYearCar = pickerItem

                if (index > -1) {
                    et_built_year.setText(pickerItem.name)
                    choosedIndexYearCar = index
                    pickerAdapterYearCar.update(index, PickerItem(name = pickerItem.name, isChoosed = true))
                }
                hideSlideLayout()
            }
        })

        pickerAdapterModel = PickerAdapter(object: PickerAdapter.OnClickItem{
            override fun onClickPartner(pickerItem: PickerItem) {
                choosedModel.isChoosed = false
                if (choosedIndexModel> -1) pickerAdapterModel.update(choosedIndexModel, choosedModel)
                val index = pickerAdapterModel.getData().indexOf(pickerItem)
                choosedModel = pickerItem
                choosedModel.isChoosed = true
                if (index > -1) {
                    et_model.setText(pickerItem.name)
                    choosedIndexModel = index
                    pickerAdapterModel.update(index, choosedModel)
                }
                hideSlideLayout()
            }
        })

        rv_drag_view?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        et_kebutuhan_dana?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.pilih_document_jaminan)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = pickerAdapter
            showSlideLayout()
        }

        et_type?.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.collateral_car)
            et_drag_view.hint = getString(R.string.write_search)
            et_drag_view.visibility = View.VISIBLE
            rv_drag_view.adapter = carsTypeAdapter
            showSlideLayout(resources.getDimension(R.dimen.height_drag_view_content_400).toInt())
        }

        et_drag_view?.setOnEditorActionListener { _, i, _ ->
            return@setOnEditorActionListener if (i == EditorInfo.IME_ACTION_SEARCH) {
                presenter.getUsedCarList(et_year_car.text.toString(), et_drag_view.text.toString())
                true
            } else {
                false
            }
        }

        btn_save.setOnClickListener {
            val returnIntent = Intent()
            if (choosedIndex == 0) {
                val bpkb = Bpkb(
                    owners = et_bpkb_owner.text.toString(),
                    carNumber = et_car_registration.text.toString(),
                    brand = choosedUsedCar?.AssetCode,
                    type = choosedUsedCar?.AssetName,
                    builtYear = choosedUsedCar?.ManufacturingYear,
                    model = choosedUsedCar?.AssetCode,
                    price = choosedUsedCar?.Price
                )
                collateral = Collateral(
                    collateralType = ConfigVar.BPKB,
                    collateral = "${bpkb.brand}/${bpkb.type}/${bpkb.model}",
                    collateralYear = "${bpkb.builtYear}",
                    collateralOwner = "${bpkb.owners}",
                    collateralPaperNumber = "${bpkb.carNumber}",
                    collateralPrice = "${bpkb.price}",
                    assetCode = choosedUsedCar?.AssetCode
                )
                e("TAG", bpkb.toString())
                if (bpkb.isValid()) {
                    returnIntent.putExtra(SimulationDanaActivity.BPKB, bpkb)
                    returnIntent.putExtra(SimulationDanaActivity.CHOOSED_INDEX, 0)
                    returnIntent.putExtra(SimulationDanaActivity.COLLATERAL, collateral)
                    setResult(SimulationDanaActivity.CODE_AGUNAN_TYPE_RESULT, returnIntent)
                    finish()
                } else {
                    Toast.makeText(this@ChooseDocumentAgunanActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
                }
            } else if (choosedIndex == 1) {
                val sertipikat = Sertipikat(
                    type = et_sertipikat_type.text.toString(),
                    sertipikatNumber = et_sertipikat_number.text.toString(),
                    largeArea = et_large_area.text.toString(),
                    ownersArea = et_sertipikat_owners.text.toString(),
                    builtYear = et_built_year_sertipikat.text.toString()
                )
                val number = et_sertipikat_number.text.toString()
                collateral = Collateral(
                    collateralType = ConfigVar.SERTIFIKAT,
                    collateral = "${sertipikat.type}/${sertipikat.largeArea}/${sertipikat.builtYear}",
                    collateralYear = "${sertipikat.builtYear}",
                    collateralOwner = "${sertipikat.ownersArea}",
                    collateralPaperNumber = number
                )
                if (sertipikat.isValid()) {
                    returnIntent.putExtra(SimulationDanaActivity.SERTIPIKAT, sertipikat)
                    returnIntent.putExtra(SimulationDanaActivity.CHOOSED_INDEX, 1)
                    returnIntent.putExtra(CompleteDocumentActivity.COLLATERAL, collateral)
                    setResult(SimulationDanaActivity.CODE_AGUNAN_TYPE_RESULT, returnIntent)
                    finish()
                } else {
                    Toast.makeText(this@ChooseDocumentAgunanActivity, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
                }
            }
        }

        et_sertipikat_type.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.pilih_jenis_sertifikat)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = pickerAdapterSertipikat
            showSlideLayout()
        }
//        et_built_year_sertipikat.setOnClickListener {
//            tv_title_drag_view.text = getString(R.string.pilih_tahun_pembuatan)
//            et_drag_view.visibility = View.GONE
//            rv_drag_view.adapter = pickerAdapterYear
//            showSlideLayout()
//        }
        et_cars_brand.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.pilih_merek_mobil)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = pickerAdapterBrand
            if (pickerAdapterBrand.getData().isEmpty()) presenter.getBrandsCar()
            showSlideLayout()
        }
        et_cars_type.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.pilih_type_mobil)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = pickerAdapterType
            showSlideLayout()
        }
        et_built_year.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.pilih_tahun_pembuatan)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = pickerAdapterYearCar
            showSlideLayout()
        }
        et_model.setOnClickListener {
            tv_title_drag_view.text = getString(R.string.piih_model_car)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = pickerAdapterModel
            showSlideLayout()
        }

        initDataPicker()
    }

    private fun setDocumentAgunan() {
        when (choosedIndex) {
            0 -> {
                et_kebutuhan_dana.setText(choosedItem.name)
                car_type_container.visibility = View.VISIBLE
                sertipikat_type_container.visibility = View.GONE
            }
            1 -> {
                choosedIndex = 0
                choosedItem = pickerAdapter.getData()[0]
                val list = pickerAdapter.getData().toMutableList()
                list[0].isChoosed = true
                list[1].isChoosed = false
                pickerAdapter.removeAll()
                pickerAdapter.add(list)
                Toast.makeText(this, "Maaf untuk dokumen jaminan ini sedang diperbaiki", Toast.LENGTH_SHORT).show()
//                et_kebutuhan_dana.setText(choosedItem.name)
//                car_type_container.visibility = View.GONE
//                sertipikat_type_container.visibility = View.VISIBLE
            }
            else -> {
            }
        }
        btn_save.visibility = View.VISIBLE
        btn_save_2.visibility = View.GONE
    }

    private fun initDataPicker() {
        pickerAdapter.add(PickerItem(name = getString(R.string.cars_bpkb), isChoosed = false))
        pickerAdapter.add(PickerItem(name = getString(R.string.home_sertipikat), isChoosed = false))

        pickerAdapterSertipikat.add(PickerItem(name = getString(R.string.hak_milik_sendiri), isChoosed = false))

        pickerAdapterYear.add(PickerItem(name = "2009", isChoosed = false))
        pickerAdapterYear.add(PickerItem(name = "2010", isChoosed = false))
        pickerAdapterYear.add(PickerItem(name = "2011", isChoosed = false))
        pickerAdapterYear.add(PickerItem(name = "2012", isChoosed = false))
        pickerAdapterYear.add(PickerItem(name = "2013", isChoosed = false))
        pickerAdapterYear.add(PickerItem(name = "2014", isChoosed = false))
        pickerAdapterYear.add(PickerItem(name = "2015", isChoosed = false))

        pickerAdapterYearCar.add(PickerItem(name = "2009", isChoosed = false))
        pickerAdapterYearCar.add(PickerItem(name = "2010", isChoosed = false))
        pickerAdapterYearCar.add(PickerItem(name = "2011", isChoosed = false))
        pickerAdapterYearCar.add(PickerItem(name = "2012", isChoosed = false))
        pickerAdapterYearCar.add(PickerItem(name = "2013", isChoosed = false))
        pickerAdapterYearCar.add(PickerItem(name = "2014", isChoosed = false))
        pickerAdapterYearCar.add(PickerItem(name = "2015", isChoosed = false))
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout(height: Int? = null) {
        container_content_drag_view.setHeight(height?:resources.getDimension(R.dimen.height_drag_view_content).toInt())
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    override fun onBackPressed() {
        if (sliding_layout?.panelState == SlidingUpPanelLayout.PanelState.EXPANDED) {
            hideSlideLayout()
        } else {
            super.onBackPressed()
        }
    }

    override fun onGetTypeCar(list: MutableList<PickerItem>) {
        pickerAdapterType.add(list)
    }

    override fun onGetCarTrim(list: MutableList<PickerItem>) {
        pickerAdapterModel.add(list)
    }

    override fun onGetCarBrands(list: MutableList<PickerItem>) {
        pickerAdapterBrand.add(list)
    }

    override fun showWait() {
    }

    override fun removeWait() {
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
