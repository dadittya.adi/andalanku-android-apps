package id.andalan.andalanku.ui.password

import id.andalan.andalanku.model.response.ResetPasswordResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class ForgotPasswordPresenter @Inject constructor(private val service: Services): BasePresenter<ForgotPasswordView>() {
    override fun onCreate() {
    }

    fun doResetPassword(email: String) {
        view.showWait()
        val subscription = service.doResetPassword(email).subscribe(object : Subscriber<ResetPasswordResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(resetPasswordResponse: ResetPasswordResponse) {
                view.getResponse(resetPasswordResponse)
            }
        })
        subscriptions.add(subscription)
    }
}