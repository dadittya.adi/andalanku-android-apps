package id.andalan.andalanku.ui.payment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.AlertWarningCustom
import kotlinx.android.synthetic.main.activity_installment_billdetails.*
import kotlinx.android.synthetic.main.activity_installment_billdetails.et_jatuh_tempo
import kotlinx.android.synthetic.main.layout_warning_alert.view.*
import kotlinx.android.synthetic.main.layout_warning_alert.view.title_text
import kotlinx.android.synthetic.main.toolbar_login.*
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.synthetic.main.layout_warning_alert.view.btn_ok

import javax.inject.Inject

class InstallmentPaymentDetailActivity : BaseApp(), InstallmentPaymentDetailView {

    companion object {
        val AGREEMENT_NUMBER = "agreement_number"
    }

    private lateinit var alertDialogProgress: AlertDialogCustom
    private lateinit var alertWarningCustom: AlertWarningCustom
    private var agreementNumber = ""
    private var paymentUrl = ""
    @Inject
    lateinit var presenter: InstallmentPaymentDetailPresenter

    private var totalAmount: Long = 0
    private var pinalty: Long = 0
    private var adminFee: Long = 0
    private var installment: Long = 0
    private var payPinalty = "Y"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_installment_billdetails)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Detail Tagihan"

        if (!intent.hasExtra(AGREEMENT_NUMBER)) {
            finish()
        }

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(false)
        alertDialogProgress.dismiss()

        alertWarningCustom = AlertWarningCustom(this, null)
        alertWarningCustom.setView()
        alertWarningCustom.create()
        alertWarningCustom.getView().btn_ok.setOnClickListener {
            alertWarningCustom.dissmis()
            finish()
        }

        agreementNumber = intent.getStringExtra(AGREEMENT_NUMBER)
        deps.inject(this)
        presenter.view = this
        presenter.getPaymentDetail(agreementNumber)

        btn_bayar.setOnClickListener {
            val intent = Intent(this, InstallmentPaymentProcessActivity::class.java)
            intent.putExtra(InstallmentPaymentProcessActivity.TRANSACTION_URL, paymentUrl)
            intent.putExtra(InstallmentPaymentProcessActivity.PAY_PINALTY, payPinalty)
            startActivity(intent)
        }

        cb_pinalty?.setOnCheckedChangeListener { _, b ->
            var totalAmount: Long = 0
            if (!b) {
                totalAmount = installment + adminFee
                payPinalty = "N"
            } else {
                totalAmount = installment + adminFee + pinalty
                payPinalty = "Y"
            }

            et_total_tagihan.text = totalAmount.convertToRpFormat()
        }
    }

    override fun onGetPaymentDetail(paymentInquiryResponse: PaymentInquiryResponse) {
        if (paymentInquiryResponse.status == ConfigVar.SUCCESS) {
            val periode = paymentInquiryResponse.installmentSequence.toString() + " (Periode "  + paymentInquiryResponse.periode + ")"
            et_contract.setText(paymentInquiryResponse.agreementNumber)
            et_customer_name.setText(paymentInquiryResponse.name)
            et_angsuran_ke.setText(periode)
            et_jatuh_tempo.setText(paymentInquiryResponse.dueDate)
            et_merk_type.setText(paymentInquiryResponse.assetDescription)
            et_nopol.setText(paymentInquiryResponse.policeNumber)
            et_jml_angsuran.text = paymentInquiryResponse.amount?.toLong()?.convertToRpFormat()
            et_pinalty.text = paymentInquiryResponse.penalty?.toLong()?.convertToRpFormat()
            et_biaya_admin.text = paymentInquiryResponse.adminFee?.toLong()?.convertToRpFormat()
            et_total_tagihan.text = paymentInquiryResponse.totalAmount?.toLong()?.convertToRpFormat()

            paymentUrl = paymentInquiryResponse.paymentUrl ?:""
            installment = paymentInquiryResponse.amount?.toLong() ?: 0
            adminFee = paymentInquiryResponse.adminFee?.toLong() ?: 0
            pinalty = paymentInquiryResponse.penalty?.toLong() ?: 0
        }
        else {
            alertWarningCustom.getView().title_text?.text = "Peringatan"
            alertWarningCustom.getView().tv_message?.text = paymentInquiryResponse.message

            alertWarningCustom.show()
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
        alertWarningCustom.getView().title_text?.text = "Peringatan"
        alertWarningCustom.getView().tv_message?.text = appErrorMessage

        alertWarningCustom.show()
    }
}