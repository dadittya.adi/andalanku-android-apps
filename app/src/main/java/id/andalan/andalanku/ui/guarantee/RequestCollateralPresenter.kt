package id.andalan.andalanku.ui.guarantee

import id.andalan.andalanku.model.response.CollateralListResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class RequestCollateralPresenter @Inject constructor(private val service: Services): BasePresenter<RequestCollateralView>() {
    override fun onCreate() {
        getListCollateral()
    }

    fun getListCollateral(status: String = "") {
        view.showWait()
        val subscription = service.getCollateralRequestList(status)
            .subscribe(object : Subscriber<CollateralListResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(collateralListResponse: CollateralListResponse) {
                    view.getRequestCollateralList(collateralListResponse)
                }
            })
        subscriptions.add(subscription)
    }
}