package id.andalan.andalanku.ui.inbox


import android.Manifest
import android.app.DownloadManager
import android.content.*
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Layout.JUSTIFICATION_MODE_INTER_WORD
import android.util.Log.e
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.google.gson.Gson
import id.andalan.andalanku.BuildConfig

import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.DetailInboxResponse
import id.andalan.andalanku.model.ui.*
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.about_us.AboutUsActivity
import id.andalan.andalanku.ui.adapters.MenuAdapterProfile
import id.andalan.andalanku.ui.complain.EComplainActivity
import id.andalan.andalanku.ui.guarantee.RequestTakeGuaranteeActivity
import id.andalan.andalanku.ui.home.MainHomeActivity
import id.andalan.andalanku.ui.insurance.EClaimInsuranceActivity
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.FileUtils
import id.andalan.andalanku.utils.convertStringToHTML
import id.andalan.andalanku.utils.getMonths
import kotlinx.android.synthetic.main.fragment_detail_message.*
import kotlinx.android.synthetic.main.layout_mid_detail_message.*
import kotlinx.android.synthetic.main.toolbar_login.*
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.lang.Exception
import android.content.Intent
import android.text.Html

class DetailMessageFragment : androidx.fragment.app.Fragment(), DetailMessageView {

    companion object {
        val REQUEST_SEND_COMPLAIN = 113
        val RESPONSE_SEND_COMPLAIN = 213
    }

    lateinit var preferencesUtil: PreferencesUtil
    lateinit var presenter: DetailMessagePesenter
    private lateinit var menuMidAdapter: MenuAdapterProfile
    private var link = ""
    private var path = ""
    private val RC_STORAGE = 123
    private val STORAGE = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var filename = ""
    private var inputDate = ""
    private var downloadManager: DownloadManager? = null
    private lateinit var onComplete: BroadcastReceiver

    private lateinit var alertDialogProgress: AlertDialogCustom

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_message, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            PRDownloader.initialize(it)
            path = FileUtils.getRootDirPath()
            downloadManager = it.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            onComplete = object: BroadcastReceiver() {
                override fun onReceive(p0: Context?, p1: Intent?) {
                    Toast.makeText(context, getString(R.string.success_download), Toast.LENGTH_LONG).show()
//                    openFile(path, filename)
                }
            }
            it.registerReceiver(onComplete,
                IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
            )
        }

        (activity as MainHomeActivity).setSupportActionBar(toolbar_login)
        (activity as MainHomeActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        (activity as MainHomeActivity).supportActionBar?.setDisplayShowHomeEnabled(true)
//        tv_title.text = getString(R.string.inbox)
        context?.let {
            alertDialogProgress = AlertDialogCustom(it, null)
            alertDialogProgress.setView(LayoutInflater.from(it).inflate(R.layout.layout_progress_loading, null, false))
            alertDialogProgress.create()
            alertDialogProgress.setCancelable(true)
        }

        presenter = DetailMessagePesenter((activity as MainHomeActivity).services)
        presenter.view = this

        presenter.getDetailMessage((activity as MainHomeActivity).selectedMessage.id?:"")

        menuMidAdapter = MenuAdapterProfile(object: MenuAdapterProfile.OnClickItem{
            override fun onClickMenu(menuProfile: MenuProfile) {
                if (menuProfile.name == context?.getString(R.string.contact_us)) {
                    startActivity(Intent(context, AboutUsActivity::class.java))
                }
                else if (menuProfile.name == context?.getString(R.string.detail_complain_message)) {
                    startActivity(Intent(context, EComplainActivity::class.java))
                }
                else if (menuProfile.name == context?.getString(R.string.detail_claim_insurance_submission)) {
                    startActivity(Intent(context, EClaimInsuranceActivity::class.java))
                }
                else if (menuProfile.name == context?.getString(R.string.detail_collateral_request)) {
                    startActivity(Intent(context, RequestTakeGuaranteeActivity::class.java))
                }
            }
        })

        rv_menu_bottom?.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                context,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_menu_bottom?.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()
        rv_menu_bottom?.adapter = menuMidAdapter
        rv_menu_bottom?.isNestedScrollingEnabled = false

        toolbar_login?.setNavigationOnClickListener { (activity as MainHomeActivity).moveViewNoAnim(2) }

        imageView?.setOnClickListener {
            (activity as MainHomeActivity).detailInboxResponse?.let {
                getFileAttachment(it)
            }
            if (hasStoragePermission()) {
                doDownload()
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.izinkan_aplikasi_mendownload_file),
                    RC_STORAGE,
                    STORAGE)
            }
        }

        context?.let {
            btn_reply?.setOnClickListener {
                startActivityForResult(
                    Intent(context, MessageReplyActivity::class.java).putExtra("title", tv_message_title?.text),
                    REQUEST_SEND_COMPLAIN
                )
            }
        }
    }

    override fun onGetDetailMessage(detailInboxResponse: DetailInboxResponse) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            tv_message_content?.justificationMode = JUSTIFICATION_MODE_INTER_WORD
        }
        if (detailInboxResponse.type == ConfigVar.ECOMPLAIN) {
            tv_title?.text = "Info Pengajuan Komplain"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.detail_complain_message)))
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailMessage = Gson().fromJson(detailInboxResponse.content, ComplainMessage::class.java)
                tv_message_content?.text = detailMessage.complainMessage
                tv_no_req?.text = detailMessage.ticketNumber
                tv_no_status?.text = detailMessage.status
                context?.let {
                    tv_date?.text = detailMessage.getInputDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.CLAIMINSURANCE) {
            tv_title?.text = "Info Pengajuan Klaim Asuransi"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.detail_claim_insurance_submission)))
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailClaim = Gson().fromJson(detailInboxResponse.content, ClaimInsuranceMessage::class.java)
                tv_message_content?.text = detailClaim.message
                tv_no_req?.text = detailClaim.ticketNumber
                tv_no_status?.text = detailClaim.status
                context?.let {
                    tv_date?.text = detailClaim.getInputDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.CREDITAPPLICATION) {
            tv_title?.text = "Info Pengajuan Kredit"
            //menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.detail_contrack_menu)))
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailCredit = Gson().fromJson(detailInboxResponse.content, CreditApplicationMessage::class.java)
                tv_message_content?.text = detailCredit.message

                if (detailInboxResponse.contentHtml != "<pre></pre>" || detailInboxResponse.contentHtml != "") {
                    tv_message_content?.visibility = View.INVISIBLE
                    wv_message_content?.visibility = View.VISIBLE
                    wv_message_content?.loadData(detailInboxResponse.contentHtml, "text/html; charset=utf-8", "UTF-8")
                }

                tv_no_req?.text = detailCredit.ticketNumber
                tv_no_status?.text = detailCredit.status
                context?.let {
                    tv_date?.text = detailCredit.getSubmissionDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.PLAFONDAPPLICATION) {
            tv_title?.text = "Info Pengajuan Plafond"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailCredit = Gson().fromJson(detailInboxResponse.content, DetailMessagePlafondSubmission::class.java)
                tv_message_content?.text = detailCredit.message
                tv_no_req?.text = detailCredit.ticketNumber
                tv_no_status?.text = detailCredit.status
                context?.let {
                    tv_date?.text = detailCredit.getInputDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.REPAYMENT) {
            tv_title?.text = "Info Pengajuan Pelunasan Maju"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailCredit = Gson().fromJson(detailInboxResponse.content, RepaymentRequestMessage::class.java)
                tv_message_content?.text = detailCredit.message
                tv_no_req?.text = detailCredit.ticketNumber
                tv_no_status?.text = detailCredit.status
                context?.let {
                    tv_date?.text = detailCredit.getSubmissionDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.REPAYMENTREQUEST) {
            tv_title?.text = "Info Request Pelunasan Maju"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailCredit = Gson().fromJson(detailInboxResponse.content, DetailMessagePlafondSubmission::class.java)
                tv_message_content?.text = detailCredit.message
                tv_no_req?.text = detailCredit.ticketNumber
                tv_no_status?.text = detailCredit.status
                context?.let {
                    tv_date?.text = detailCredit.getInputDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.REPAYMENTREQUEST) {
            tv_title?.text = "Info Request Pelunasan Maju"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailCredit = Gson().fromJson(detailInboxResponse.content, DetailMessagePlafondSubmission::class.java)
                tv_message_content?.text = detailCredit.message
                tv_no_req?.text = detailCredit.ticketNumber
                tv_no_status?.text = detailCredit.status
                context?.let {
                    tv_date?.text = detailCredit.getInputDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == ConfigVar.COLLATERALREQUEST) {
            tv_title?.text = "Info Request Ambil Jaminan"
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.detail_collateral_request)))
            menuMidAdapter.add(MenuProfile(name = context?.getString(R.string.contact_us)))
            try {
                val detailCredit = Gson().fromJson(detailInboxResponse.content, DetailMessagePlafondSubmission::class.java)
                tv_message_content?.text = detailCredit.message
                tv_no_req?.text = detailCredit.ticketNumber
                tv_no_status?.text = detailCredit.status
                context?.let {
                    tv_date?.text = detailCredit.getInputDateUI(it.getMonths())
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        }

        tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()

        if (detailInboxResponse.type == "promo") {
            tv_title?.text = "Promo Andalanku"
            tv_message_title?.visibility = View.VISIBLE
            tv_message_title?.text = detailInboxResponse.title
            try {
                //val content = Gson().fromJson(detailInboxResponse.content, DetailMessagePromo::class.java)
                tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()
                container_mid?.visibility = View.GONE
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == "information blast") {
            tv_title?.text = "Info Andalanku"
            tv_message_title?.visibility = View.VISIBLE
            tv_message_title?.text = detailInboxResponse.title
            try {
                //val content = Gson().fromJson(detailInboxResponse.content, DetailMessagePromo::class.java)
                if (detailInboxResponse.contentHtml == "<pre></pre>") {
                    try {
                        val inboxMessage = Gson().fromJson(detailInboxResponse.content, DetailMessagePlafondSubmission::class.java)
                        tv_message_content?.text = inboxMessage.message
                    } catch (e: Exception) {
                        e("error parsing message", detailInboxResponse.content)
                    }
                }
                 else {
                    tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()
                }
                container_mid?.visibility = View.GONE
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == "update profile") {
            tv_title?.text = "Informasi"
            tv_title?.text = detailInboxResponse.title
            try {
                //val content = Gson().fromJson(detailInboxResponse.content, DetailMessagePromo::class.java)
                tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()
                container_mid?.visibility = View.GONE
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        } else if (detailInboxResponse.type == "direct message") {
            tv_title?.text = "Direct Message"
            try {
                //val content = Gson().fromJson(detailInboxResponse.content, DetailMessagePromo::class.java)
                tv_message_title?.visibility = View.VISIBLE
                tv_message_title?.text = detailInboxResponse.title
                tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()
                container_mid?.visibility = View.GONE
                container_mid_reply_button.visibility = View.VISIBLE
                if (detailInboxResponse.firstMedia != null) {
                    getFileAttachment(detailInboxResponse)
                    container_mid_download_button.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        }
        else if (detailInboxResponse.type == "informasi") {
            tv_title?.text = "Informasi"
            try {
                //val content = Gson().fromJson(detailInboxResponse.content, DetailMessagePromo::class.java)
                tv_message_title?.visibility = View.VISIBLE
                tv_message_title?.text = detailInboxResponse.title
                tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()
                container_mid?.visibility = View.GONE
                //container_mid_reply_button.visibility = View.VISIBLE
                /*if (detailInboxResponse.firstMedia != "") {
                    getFileAttachment(detailInboxResponse)
                    container_mid_download_button.visibility = View.VISIBLE
                }*/
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        }

        else if (detailInboxResponse.type == "agent registration") {
            tv_title?.text = "Pendaftaran Agen"
            try {
                //val content = Gson().fromJson(detailInboxResponse.content, DetailMessagePromo::class.java)
                tv_message_title?.visibility = View.VISIBLE
                tv_message_title?.text = detailInboxResponse.title
                tv_message_content?.text = detailInboxResponse.contentHtml?.convertStringToHTML()
                container_mid?.visibility = View.GONE
                container_mid_reply_button.visibility = View.GONE
                if (detailInboxResponse.firstMedia != null) {
                    getFileAttachment(detailInboxResponse)
                    container_mid_download_button.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e("error parsing message", detailInboxResponse.content)
            }
        }
    }

    override fun showWait() {
        if (::alertDialogProgress.isInitialized) {
            alertDialogProgress.show()
        }
    }

    override fun removeWait() {
        if (::alertDialogProgress.isInitialized) {
            alertDialogProgress.dismiss()
        }
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    fun getFileAttachment(detailInboxResponse: DetailInboxResponse) {
        link = detailInboxResponse.firstMedia?:""
        inputDate = detailInboxResponse.date?:""
        inputDate = inputDate.replace("-", "")
        inputDate = inputDate.replace(" ", "")
        inputDate = inputDate.replace(":", "")
    }

    private fun hasStoragePermission (): Boolean {
        context?.let {
            return EasyPermissions.hasPermissions(it, STORAGE)
        }
        return false
    }

    private fun doDownload() {
        if (link.isNotBlank()) {
            filename = "inbox-attachment-${inputDate}.pdf"
            PRDownloader.download(link, path, filename)
                .build()
                .start(object: OnDownloadListener {
                    override fun onDownloadComplete() {
                        Toast.makeText(context, getString(R.string.success_download), Toast.LENGTH_LONG).show()
                        openFile(path, filename)
                    }
                    override fun onError(error: Error?) {
                        if (error?.isConnectionError == true) {
                            Toast.makeText(context, getString(R.string.failed_download_connection_error), Toast.LENGTH_LONG).show()
                        } else if (error?.isServerError == true) {
                            Toast.makeText(context, getString(R.string.failed_download_server_error), Toast.LENGTH_LONG).show()
                        } else {
                            Toast.makeText(context, getString(R.string.failed_download), Toast.LENGTH_LONG).show()
                        }
                    }
                })
        } else {
            e("tag", "error_polis")
            Toast.makeText(context, getString(R.string.failed_download_server_error), Toast.LENGTH_LONG).show()
        }
    }

    private fun openFile(path: String, filename: String) {
        context?.let {
            val file = File("$path/$filename")
            val target = Intent(Intent.ACTION_VIEW)
            val uri = FileProvider.getUriForFile(it,
                BuildConfig.APPLICATION_ID + ".provider",
                file)
            target.setDataAndType(uri, "application/pdf")
            target.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            target.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)

            val intent = Intent.createChooser(target, "Buka File Attachment")
            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException) {
                // Instruct the user to install a PDF reader here, or something
            }
        }
    }
}
