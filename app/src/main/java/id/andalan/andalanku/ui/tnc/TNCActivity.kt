package id.andalan.andalanku.ui.tnc

import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.preferences.PreferencesUtil
import kotlinx.android.synthetic.main.activity_tnc.*
import kotlinx.android.synthetic.main.toolbar_login.*
import javax.inject.Inject

class TNCActivity : BaseApp() {

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tnc)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = "Syarat dan Ketentuan"

        deps.inject(this)

        val legalDocumentResponse = preferencesUtil.getLegalDocument()

        legalDocumentResponse?.let {response ->
//            loadData(yourData, "text/html; charset=utf-8", "UTF-8");
            val data = if (response.legalDocumentList.size > 0) response.legalDocumentList[1].content?:"" else ""
            webview?.loadData(data, "text/html; charset=utf-8", "UTF-8")
        }
    }
}
