package id.andalan.andalanku.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.andalan.andalanku.model.response.DetailAndalanFinancialResponse

class ViewModelAndalanDetail: ViewModel() {
    val currentAndalanFinancialResponse: MutableLiveData<DetailAndalanFinancialResponse> by lazy {
        MutableLiveData<DetailAndalanFinancialResponse>()
    }
}