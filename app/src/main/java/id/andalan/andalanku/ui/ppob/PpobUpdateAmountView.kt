package id.andalan.andalanku.ui.ppob

import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.ui.base.BaseView

interface PpobUpdateAmountView: BaseView {
    fun onUpdateAmount(ppobUpdateAmountResponse: PpobUpdateAmountResponse)
}