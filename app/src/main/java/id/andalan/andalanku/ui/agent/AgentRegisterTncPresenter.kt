package id.andalan.andalanku.ui.agent

import id.andalan.andalanku.model.request.*
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class AgentRegisterTncPresenter @Inject constructor(private val service: Services): BasePresenter<AgentRegisterTncView>() {
    override fun onCreate() {

    }

    fun sendAgentRegistration(sendAgenRegistrationRequest: SendAgenRegistrationRequest) {
        view.showWait()
        val subscription = service.sendAgentRegistration(sendAgenRegistrationRequest)
            .subscribe(object : Subscriber<AgentRegistrationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(agentRegistrationResponse: AgentRegistrationResponse) {
                    view.onResponseSend(agentRegistrationResponse)
                }
            })
        subscriptions.add(subscription)
    }
}