package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.CarTrim
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_default_text.view.*

class CarTrimAdapter(val onItemClickListener: OnClickItem): BaseAdapter<CarTrim, CarTrimAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_default_text
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<CarTrim>(itemView) {
        override fun bind(data: CarTrim) {
            itemView.setOnClickListener {
                onItemClickListener.onClick(data)
            }
            itemView.tv_name.text = data.name
        }
    }

    interface OnClickItem {
        fun onClick(carTrim: CarTrim)
    }
}