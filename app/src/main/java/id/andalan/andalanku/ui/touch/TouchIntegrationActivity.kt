package id.andalan.andalanku.ui.touch

import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.toolbar_login.*

class TouchIntegrationActivity : BaseApp() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_touch_integration)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title?.text = getString(R.string.integrasi_touch_id)
    }
}
