package id.andalan.andalanku.ui.login

import android.content.Intent
import android.os.Bundle
import android.util.Log.e
import android.view.View
import android.widget.Toast
import com.an.biometric.BiometricCallback
import com.an.biometric.BiometricManager
import id.andalan.andalanku.R
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_AGGREMENT_LIST
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_INBOX
import id.andalan.andalanku.model.request.EventBus.Companion.LOAD_PERSONAL_INFO
import id.andalan.andalanku.model.response.LoginResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.password.ForgotPasswordActivity
import id.andalan.andalanku.ui.register.RegisterActivity
import id.andalan.andalanku.utils.AlertDialogUtil
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.toolbar_login.*
import org.greenrobot.eventbus.EventBus
import javax.inject.Inject


class LoginActivity : BaseApp(), BiometricCallback, LoginView {
    private var personalInfoUser: PersonalInformationResponse? = null

    override fun getProfileInformation(personalInformationResponse: PersonalInformationResponse) {
    }

    companion object {
        val REQUEST_LOGIN = 511
        val RESPONSE_LOGIN = 512
    }
    private val TAG = LoginActivity::class.java.simpleName

    @Inject
    lateinit var preferencesUtil: PreferencesUtil
    @Inject
    lateinit var presenter: LoginPresenter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        deps.inject(this)
        presenter.view = this

        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.GONE

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.login_account)

        btn_sign_up.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }

        btn_login.setOnClickListener {
            val email = et_email.text.toString()
            val password = et_password.text.toString()
            if (email.isNotBlank() && password.isNotBlank()) {
                presenter.doLogin(email, password)
            } else {
                AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                    override fun onClickCancel() {
                    }

                    override fun onClickOk() {
                    }

                }, this, getString(R.string.security_account), getString(R.string.login_form_not_complete))
            }
        }

        btn_finger_print.setOnClickListener {
            BiometricManager.BiometricBuilder(this)
                .setTitle(getString(R.string.login_account))
                .setSubtitle(getString(R.string.login_with_fingerprint_subtitle))
                .setDescription("")
                .setNegativeButtonText(getString(R.string.cancel))
                .build()
                .authenticate(this)
        }

        btn_forgot_password.setOnClickListener {
            startActivityForResult(Intent(this@LoginActivity, ForgotPasswordActivity::class.java), ForgotPasswordActivity.REQUEST_FORGOT)
            finish()
        }
    }

    override fun onGetResponse(loginResponse: LoginResponse) {
        if (loginResponse.status == ConfigVar.SUCCESS) {
            preferencesUtil.putIdNumber(loginResponse.idCustomer?:0)
            preferencesUtil.putUser(loginResponse)
            val user = preferencesUtil.getUser()
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_PERSONAL_INFO))
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_AGGREMENT_LIST))
            EventBus.getDefault().post(id.andalan.andalanku.model.request.EventBus(type = LOAD_INBOX))
            user?.isVerify = true
            preferencesUtil.putUser(user?: LoginResponse())
            preferencesUtil.putFirstOpen(false)

            finish()
        } else {
            AlertDialogUtil.buildDefaultAlertDialog(object: AlertDialogUtil.AlertDialogView{
                override fun onClickCancel() {
                }

                override fun onClickOk() {
                }

            }, this, getString(R.string.security_account), loginResponse.message?:"")
        }
    }

    override fun showWait() {
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.VISIBLE
    }

    override fun removeWait() {
        val loadView = findViewById<View>(R.id.loadingPanel)
        loadView.visibility = View.GONE
    }

    override fun onFailure(appErrorMessage: String?) {
        Toast.makeText(this@LoginActivity, "Terjadi Masalah Pada Koneksi", Toast.LENGTH_SHORT).show()
    }

    override fun onSdkVersionNotSupported() {
        e(TAG, "SDK Not Support")
    }

    override fun onBiometricAuthenticationNotSupported() {
        e(TAG, "biometric authentication not support")
    }

    override fun onBiometricAuthenticationNotAvailable() {
        e(TAG, "biometric authentication not available")
    }

    override fun onBiometricAuthenticationPermissionNotGranted() {
        e(TAG, "biometric authentication not granted")
    }

    override fun onBiometricAuthenticationInternalError(error: String?) {
        e(TAG, "biometric authentication on error ${error?:""}")
    }

    override fun onAuthenticationFailed() {
        e(TAG, "biometric authentication failed")
    }

    override fun onAuthenticationCancelled() {
        e(TAG, "biometric authentication canceled")
    }

    override fun onAuthenticationSuccessful() {
        e(TAG, "biometric authentication successful")
        val user = preferencesUtil.getLogoutUser()
        user?.let {
            if (it.customerName.isNullOrBlank()){
                Toast.makeText(this@LoginActivity, "Mohon login menggunakan email dan password terlebih dahulu", Toast.LENGTH_SHORT).show()
            } else {
                preferencesUtil.putUser(user)
                finish()
            }
        }
    }

    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
        e(TAG, "biometric authentication help $helpCode ${helpString.toString()}")
    }

    override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
        e(TAG, "biometric authentication error $errorCode ${errString.toString()}")
    }
}
