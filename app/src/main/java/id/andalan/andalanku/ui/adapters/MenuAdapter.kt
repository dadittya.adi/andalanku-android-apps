package id.andalan.andalanku.ui.adapters

import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R
import id.andalan.andalanku.model.ui.Menu
import id.andalan.andalanku.ui.base.BaseAdapter
import id.andalan.andalanku.ui.base.BaseViewHolder
import kotlinx.android.synthetic.main.item_menu_preloader.view.*

class MenuAdapter(val onItemClickListener: onClickItem): BaseAdapter<Menu, MenuAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MenuAdapter.ViewHolder {
        val view = getView(parent, viewType)
        return ViewHolder(view)
    }

    override fun getItemResourceLayout(viewType: Int): Int {
        return R.layout.item_menu_preloader
    }

    inner class ViewHolder(itemView: View): BaseViewHolder<Menu>(itemView) {
        override fun bind(data: Menu) {
            itemView.setOnClickListener {
                onItemClickListener.onClickMenu(data)
            }
            itemView.ic_logo.setImageResource(data.image)
            itemView.title_text.text = data.name
        }

    }

    interface onClickItem {
        fun onClickMenu(menu: Menu)
    }
}