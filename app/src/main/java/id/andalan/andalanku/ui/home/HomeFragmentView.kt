package id.andalan.andalanku.ui.home

import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.ui.base.BaseView

interface HomeFragmentView: BaseView {
    fun onGetNewsResponse(newsResponse: NewsResponse)
    fun onGetPromoResponse(promoResponse: PromoResponse?)
    fun onRemoveWaitGetPartner()
    fun onGetPartnerResponse(partnerListResponse: PartnerListResponse)
    fun onRemoveWaitGetNews()
    fun onErrorGetNews(message: String)
    fun onRemoveWaitGetPromo()
    fun onErrorGetPromo(message: String)
    fun onRemoveWaitGetSlider()
    fun onErrorGetSlider(message: String)
    fun onGetSliderResponse(sliderResponse: SliderResponse)
    fun onGetAgent(agentResponse: AgentResponse)
}