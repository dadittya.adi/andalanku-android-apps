package id.andalan.andalanku.ui.loan_application

import id.andalan.andalanku.model.request.SendLoanApplicationRequest
import id.andalan.andalanku.model.response.LoanApplicationResponse
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class CompleteDocumentPresenter @Inject constructor(private val service: Services): BasePresenter<CompleteDocumentView>() {
    override fun onCreate() {

    }

    fun sendLoanapplication(loanApplicationRequest: SendLoanApplicationRequest) {
        view.showWait()
        val subscription = service.sendLoanApplication(loanApplicationRequest)
            .subscribe(object : Subscriber<LoanApplicationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(loanApplicationResponse: LoanApplicationResponse) {
                    view.onResponseSend(loanApplicationResponse)
                }
            })
        subscriptions.add(subscription)
    }
}