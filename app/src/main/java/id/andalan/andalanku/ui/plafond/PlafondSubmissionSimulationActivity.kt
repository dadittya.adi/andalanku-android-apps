package id.andalan.andalanku.ui.plafond

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.PlafondSimulationRequest
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.PlafondSimulationResponse
import id.andalan.andalanku.model.ui.Occupation
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.ui.adapters.OccupationAdapter
import id.andalan.andalanku.utils.AlertDialogCustom
import id.andalan.andalanku.utils.convertToRpFormat
import id.andalan.andalanku.utils.getNumberOnly
import com.sothree.slidinguppanel.SlidingUpPanelLayout
import kotlinx.android.synthetic.main.activity_plafond_submission_simulation.*
import kotlinx.android.synthetic.main.item_drag_view.*
import kotlinx.android.synthetic.main.toolbar_login.*
import kotlinx.android.synthetic.main.top_drag_view.*
import javax.inject.Inject

class PlafondSubmissionSimulationActivity : BaseApp(), PlafondSubmissionSimulationView {
    lateinit var occupationAdapter: OccupationAdapter
    private var occupationChoosed: Occupation? = null

    @Inject
    lateinit var presenter: PlafondSimulationSubmissionPresenter
    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    private lateinit var alertDialogProgress: AlertDialogCustom
    private var personalInfo: PersonalInformationResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plafond_submission_simulation)

        alertDialogProgress = AlertDialogCustom(this, null)
        alertDialogProgress.setView(LayoutInflater.from(this).inflate(R.layout.layout_progress_loading, null, false))
        alertDialogProgress.create()
        alertDialogProgress.setCancelable(true)

        deps.inject(this)
        presenter.view = this
        presenter.onCreate()

        personalInfo = preferencesUtil.getProfile()
        val salary = (personalInfo?.monthlyIncome?.getNumberOnly()?:0).convertToRpFormat()
        et_salary?.setText(salary)
        val additionalSalary = (personalInfo?.additionalIncome?.getNumberOnly()?:0).convertToRpFormat()
        et_additional_salary?.setText(additionalSalary)
        et_source_income?.setText(personalInfo?.sideJob)

        sliding_layout?.setFadeOnClickListener { hideSlideLayout() }
        sliding_layout?.isTouchEnabled = false
        btn_close_drag_view?.setOnClickListener {
            hideSlideLayout()
        }
        rv_drag_view.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this,
                androidx.recyclerview.widget.LinearLayoutManager.VERTICAL,
                false
            )
        rv_drag_view.itemAnimator =
            androidx.recyclerview.widget.DefaultItemAnimator()

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.plafond_simulation_andalanku)

        occupationAdapter = OccupationAdapter(object: OccupationAdapter.OnClickItem{
            override fun onClick(occupation: Occupation) {
                occupationChoosed = occupation
                et_job.setText(occupation.name)
                hideSlideLayout()
            }
        })

        et_job?.setOnClickListener {
            if (occupationAdapter.getData().isEmpty()) { presenter.getOccupationList() }
            tv_title_drag_view.text = getString(R.string.job)
            et_drag_view.visibility = View.GONE
            rv_drag_view.adapter = occupationAdapter
            showSlideLayout()
        }

        et_salary?.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    if (et_salary?.text.toString() != price.convertToRpFormat()) et_salary?.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_salary?.setSelection(text.length - 2)
                }
            }
        })

        et_additional_salary?.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val numString = s.toString()
                if (numString.isNotEmpty()) {
                    val price = numString.getNumberOnly()
                    if (et_additional_salary.text.toString() != price.convertToRpFormat()) et_additional_salary.setText(price.convertToRpFormat())
                    val text = price.convertToRpFormat()
                    et_additional_salary.setSelection(text.length - 2)
                }
            }
        })

        btn_next?.setOnClickListener {
            if (cb_term_condition?.isChecked == true){
                val mainSalary = et_salary.text.toString().getNumberOnly()
                val sideJob = et_source_income.text.toString()
                val secondarySalary = et_additional_salary.text.toString().getNumberOnly()
                var additionalIncome = "0"
                if (secondarySalary.toString() != "" ) {
                    additionalIncome = secondarySalary.toString()
                }

                val simulationRequest = PlafondSimulationRequest(
                    occupationId = occupationChoosed?.id?:"",
                    monthlyIncome = mainSalary.toString(),
                    sideJob = sideJob,
                    additionalIsncome = additionalIncome
                )
                if (simulationRequest.isValid()) {
                    occupationChoosed ?.let { preferencesUtil.putJob(it) }
                    preferencesUtil.putSideJob(sideJob)
                    preferencesUtil.putAdditionalIncome( additionalIncome)
                    preferencesUtil.putSimulationPlafond(simulationRequest)
                    presenter.sendPlafondSimulation(simulationRequest)
                } else {
                    Toast.makeText(this, getString(R.string.form_is_empty), Toast.LENGTH_SHORT).show()
                }
            } else {
                Toast.makeText(this, getString(R.string.info_check_term_condition), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun hideSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.COLLAPSED
    }

    private fun showSlideLayout() {
        sliding_layout?.panelState = SlidingUpPanelLayout.PanelState.EXPANDED
        hideKeyboard(this)
    }

    override fun onGetSimulationPlafond(plafondSimulationResponse: PlafondSimulationResponse) {
        if (plafondSimulationResponse.status == ConfigVar.SUCCESS) {
            val intent = Intent(this@PlafondSubmissionSimulationActivity, SimulationPlafondActivity::class.java)
            intent.putExtra(SimulationPlafondActivity.RESULT_PLAFOND, plafondSimulationResponse.plafond)
            startActivity(intent)
        } else {
            Toast.makeText(this, plafondSimulationResponse.message, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onGetOccupation(list: MutableList<Occupation>) {
        occupationAdapter.add(list)
        personalInfo?.occupation?.let {
            et_job?.setText(it)
            occupationChoosed = list.find {item ->
                item.name == it
            }
        }
    }

    override fun showWait() {
        alertDialogProgress.show()
    }

    override fun removeWait() {
        alertDialogProgress.dismiss()
    }

    override fun onFailure(appErrorMessage: String?) {
    }

    fun hideKeyboard(activity: Activity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
