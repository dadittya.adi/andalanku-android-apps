package id.andalan.andalanku.ui.account

import id.andalan.andalanku.model.request.SendAllAddressRequest
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.model.request.UpdatePersonalRequest
import id.andalan.andalanku.model.response.LocationResponse
import id.andalan.andalanku.model.response.PersonalInformationResponse
import id.andalan.andalanku.model.response.UpdateAddressResponse
import id.andalan.andalanku.model.response.UpdateProfileResponse
import id.andalan.andalanku.model.ui.AddressName
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class UpdatePersonalInfoPresenter @Inject constructor(private val service: Services): BasePresenter<UpdatePersonalInfoView>() {
    override fun onCreate() {
        getListProvince()
        getPersonalInfoUser()
    }

    fun sendUpdatedData(updateAddressRequest: UpdateAddressRequest) {
        view.showWait()
        val subscription = service.sendUpdateAddress(updateAddressRequest).subscribe(object : Subscriber<UpdateAddressResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                //view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(updateAddressResponse: UpdateAddressResponse) {
                view.getResponseUpdatePersonalInfo(updateAddressResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun sendUpdatedAddress(sendAllAddressRequest: SendAllAddressRequest) {
        view.showWait()
        val subscription = service.sendAllAddress(sendAllAddressRequest).subscribe(object : Subscriber<UpdateAddressResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                //view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(updateAllAddressResponse: UpdateAddressResponse) {
                view.getResponseUpdateAllAddress(updateAllAddressResponse)
            }
        })
        subscriptions.add(subscription)
    }

    fun getListProvince() {
        view.showWait()
        val subscription = service.getProvince()
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetProvince(locationResponse.provinceList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListCity(id: String) {
        view.showWait()
        val subscription = service.getCity(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetCity(locationResponse.cityList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListSubDistrict(id: String) {
        view.showWait()
        val subscription = service.getDistrict(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetSubdistrict(locationResponse.districtList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getListVillage(id: String) {
        view.showWait()
        val subscription = service.getVillage(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetVillage(locationResponse.villageList?: arrayListOf())
                }
            })
        subscriptions.add(subscription)
    }

    fun getZipCode(id: String) {
        view.showWait()
        val subscription = service.getZipCode(id)
            .subscribe(object : Subscriber<LocationResponse>() {
                override fun onCompleted() {
                    view.removeWait()
                }

                override fun onError(e: Throwable) {
                    view.removeWait()
                    view.onFailure(e.localizedMessage?:"")
                }

                override fun onNext(locationResponse: LocationResponse) {
                    view.onGetZipCode(locationResponse.zipCode)
                }
            })
        subscriptions.add(subscription)
    }

    fun getPersonalInfoUser() {
        view.showWait()
        val subscription = service.getPersonalInfo().subscribe(object : Subscriber<PersonalInformationResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.onFailure(e.localizedMessage?:"")
                view.removeWait()
            }

            override fun onNext(personalInformationResponse: PersonalInformationResponse) {
                //view.getProfileInformation(personalInformationResponse)
                view.removeWait()
            }
        })
        subscriptions.add(subscription)
    }
}