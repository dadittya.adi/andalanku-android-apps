package id.andalan.andalanku.ui.account

import android.Manifest
import android.os.Bundle
import id.andalan.andalanku.BaseApp
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.activity_account_setting.*
import kotlinx.android.synthetic.main.toolbar_login.*
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import android.Manifest.permission
import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import com.karumi.dexter.Dexter
import com.karumi.dexter.listener.PermissionRequest
import android.provider.MediaStore
import android.content.Intent
import android.net.Uri
import java.io.File
import java.nio.file.Files.exists
import android.provider.OpenableColumns
import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.app.Activity
import androidx.annotation.Nullable
import java.io.IOException
import androidx.core.content.ContextCompat
import android.util.Log.d
import id.andalan.andalanku.preferences.PreferencesUtil
import id.andalan.andalanku.utils.GlideApp
import id.andalan.andalanku.utils.ImagePickerActivity
import id.andalan.andalanku.utils.ekstensions.loadImage
import javax.inject.Inject


class AccountSettingActivity : BaseApp() {
    private val TAG = AccountSettingActivity::class.java.simpleName
    private var fileName: String = ""

    private val REQUEST_IMAGE_CAPTURE = 112
    private val REQUEST_IMAGE = 100

    @Inject
    lateinit var preferencesUtil: PreferencesUtil

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_setting)

        deps.inject(this)

        setSupportActionBar(toolbar_login)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        tv_title.text = getString(R.string.setting_account)

        val user = preferencesUtil.getProfile()

        et_name?.setText(user?.customerName)
        et_ktp?.setText(user?.idNumber)
        et_phone?.setText(user?.phoneNumber)
        et_email?.setText(user?.email)

        btn_edit_photo?.setOnClickListener {
            openImagePicker()
        }
        btn_update_personal_info?.setOnClickListener {
            startActivity(Intent(this, UpdatePersonalInfoActivity::class.java))
        }

    }

    private fun openImagePicker() {
        Dexter.withActivity(this)
            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .withListener(object : MultiplePermissionsListener {
                override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                    if (report.areAllPermissionsGranted()) {
                        showImagePickerOptions()
                    } else {
                        // TODO - handle permission denied case
                    }
                }

                override fun onPermissionRationaleShouldBeShown(permissions: List<PermissionRequest>, token: PermissionToken) {
                    token.continuePermissionRequest()
                }
            }).check()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                val uri = data!!.getParcelableExtra<Uri>("path")
                try {
                    // You can update this bitmap to your server
                    //val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)

                    // loading profile image from local cache
                    loadProfile(uri.toString())
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
    }

    private fun loadProfile(url: String) {
        if (url.isNotEmpty()) {
            loadImage(this, url, iv_photo_profile)
        }
//        imgProfile.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent))
    }

    private fun showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, object: ImagePickerActivity.PickerOptionListener{
            override fun onTakeCameraSelected() {
                launchCameraIntent()
            }

            override fun onChooseGallerySelected() {
                launchGalleryIntent()
            }

        })
    }

    private fun launchCameraIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 500)
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 500)

        startActivityForResult(intent, REQUEST_IMAGE)
    }

    private fun launchGalleryIntent() {
        val intent = Intent(this, ImagePickerActivity::class.java)
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE)

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true)
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1) // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1)
        startActivityForResult(intent, REQUEST_IMAGE)
    }
}
