package id.andalan.andalanku.ui.dana

import id.andalan.andalanku.model.response.CarBrandsResponse
import id.andalan.andalanku.model.response.CarTrimsResponse
import id.andalan.andalanku.model.response.CarTypesResponse
import id.andalan.andalanku.model.response.UsedCarListResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.ui.base.BaseView

interface ChooseDocumentAgunanView: BaseView {
    fun onGetTypeCar(list: MutableList<PickerItem>)
    fun onGetCarTrim(list: MutableList<PickerItem>)
    fun onGetCarBrands(list: MutableList<PickerItem>)
    fun onGetUsedCarsList(usedCarListResponse: UsedCarListResponse)
}