package id.andalan.andalanku.ui.payment

import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.response.AgreementListResponse
import id.andalan.andalanku.model.response.PaymentInquiryResponse
import id.andalan.andalanku.model.ui.PickerItem
import id.andalan.andalanku.networking.Services
import id.andalan.andalanku.ui.base.BasePresenter
import rx.Subscriber
import javax.inject.Inject

class InstallmentPaymentPresenter @Inject constructor(private val service: Services): BasePresenter<InstallmentPaymentView>() {
    override fun onCreate() {
        getAggreementNumber()
    }

    fun getAggreementNumber() {
        view.showWait()
        val subscription = service.getAgreementList().subscribe(object : Subscriber<AgreementListResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(agreementListResponse: AgreementListResponse) {
                if (agreementListResponse.status == ConfigVar.SUCCESS) {
                    val list: MutableList<PickerItem> = arrayListOf()
                    agreementListResponse.agreementList?.let {
                        it.forEach {item ->
                            list.add(PickerItem(name = item.agreementNo, contract_status = item.contract_status, isChoosed = false))
                        }
                    }
                    view.onGetContractNumber(list)
                }
            }
        })
        subscriptions.add(subscription)
    }

    fun getPaymentDetail(agreementNumber: String) {
        view.showWait()
        val subscription = service.paymentInquiry(agreementNumber).subscribe(object : Subscriber<PaymentInquiryResponse>() {
            override fun onCompleted() {
                view.removeWait()
            }

            override fun onError(e: Throwable) {
                view.removeWait()
                view.onFailure(e.localizedMessage?:"")
            }

            override fun onNext(paymentInquiryResponse: PaymentInquiryResponse) {
                view.onGetPaymentDetail(paymentInquiryResponse)

            }
        })
        subscriptions.add(subscription)
    }
}