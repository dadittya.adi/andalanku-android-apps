package id.andalan.andalanku.ui.address

import id.andalan.andalanku.model.response.UpdateAddressResponse
import id.andalan.andalanku.model.ui.Location
import id.andalan.andalanku.ui.base.BaseView

interface UpdateAddressView: BaseView {
    fun onGetProvince(mutableList: MutableList<Location>)
    fun onGetCity(mutableList: MutableList<Location>)
    fun onGetSubdistrict(mutableList: MutableList<Location>)
    fun onGetVillage(mutableList: MutableList<Location>)
    fun onGetResponseUpdate(updateAddressResponse: UpdateAddressResponse)
    fun onGetZipCode(zipCode: String)
}