package id.andalan.andalanku.factory

import android.content.Context
import androidx.lifecycle.LifecycleOwner
import com.skydoves.balloon.ArrowConstraints
import com.skydoves.balloon.ArrowOrientation
import com.skydoves.balloon.Balloon
import com.skydoves.balloon.BalloonAnimation
import com.skydoves.balloon.createBalloon
import id.andalan.andalanku.R
import id.andalan.andalanku.utils.BalloonUtils

class SimulationTipsBalloonFactory: Balloon.Factory() {
    override fun create(context: Context, lifecycle: LifecycleOwner?): Balloon {
        return createBalloon(context) {
            setLayout(R.layout.layout_baloon_simulation)
            setArrowSize(10)
            setArrowOrientation(ArrowOrientation.BOTTOM)
            setArrowConstraints(ArrowConstraints.ALIGN_ANCHOR)
            setArrowPosition(0.5f)
            isRtlSupport(BalloonUtils.isRtlLayout())
            setCornerRadius(4f)
            setElevation(0)
            setBackgroundColorResource(R.color.whiteFive)
            setBalloonAnimation(BalloonAnimation.CIRCULAR)
            setDismissWhenTouchOutside(true)
            setDismissWhenShowAgain(true)
            setLifecycleOwner(lifecycle)
        }
    }
}