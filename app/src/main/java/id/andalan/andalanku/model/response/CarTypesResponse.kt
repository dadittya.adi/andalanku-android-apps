package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.CarType

/*
{
    "status": "success",
    "car_type_list": [
        {
            "id_car_type": "13",
            "id_car_brand": "9",
            "name": "Agya",
            "is_active": "1",
            "lowestPrice": "109 juta",
            "car_type_image": [
                "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_2058353324f6f3d1.png",
                "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_bcebe4b9d0f9f1f4.jpg",
                "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_a75eceea4bb041c2.jpg"
            ]
        },
        {
            "id_car_type": "12",
            "id_car_brand": "9",
            "name": "Avanza",
            "is_active": "1",
            "lowestPrice": "149 juta",
            "car_type_image": [
                "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_01306fe5565b5da2.jpg",
                "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_0fcc4bbe3bd652c4.jpg",
                "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_7ae70a5f68fc74b5.jpg"
            ]
        }
    ]
}
 */
class CarTypesResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("car_type_list") val carTypeList: MutableList<CarType>? = arrayListOf()
)