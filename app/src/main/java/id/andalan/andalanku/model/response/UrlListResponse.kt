package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Complain
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory

/*
{
    status: "success",
    web_url: "https://dev.andalanku.id",
    api_url: "http://devapi.andalanku.id",
    cms_url: "http://devcms.andalanku.id"
}
 */
class UrlListResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("web_url") val webUrl: String? = "https://andalanku.id/",
    @SerializedName("api_url") val apiUrl: String? = "https://api.andalanku.id",
    @SerializedName("cms_url") val cmsUrl: String? = "https://cms.andalanku.id"
)