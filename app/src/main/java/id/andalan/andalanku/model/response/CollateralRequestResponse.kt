package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","message":"Add collateral request successful","ticket_number":"CRQ6356649"}
class CollateralRequestResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("ticket_number") val ticketNumber: String? = ""
)