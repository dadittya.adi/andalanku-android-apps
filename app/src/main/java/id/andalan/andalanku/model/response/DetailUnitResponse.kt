package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","asset_description":"Toyota Agya G MT 1200","manufacturing_year":"2018","colour":"Kuning","license_plate":"AB 1156 OU","chasis_no":"FCKGW-RHQQ2-YXRKT-8TG6W-2B7Q8","engine_no":"V2C47-MK7JD-3R89F-D2KXW-VPK3J"}
/*
"asset_description": "DAIHATSU AYLA AYLA-GQQFJ-QL",
            "manufacturing_year": 2017,
            "colour": "SILVER",
            "license_plate": "-",
            "chasis_no": "MHKS4DB3JHJ025808",
            "engine_no": "1KRA397337"
 */

class DetailUnitResponse(
    @SerializedName("asset_description") val assetDescription: String? = "",
    @SerializedName("manufacturing_year") val manufacturingYear: String? = "",
    @SerializedName("colour") val colour: String? = "",
    @SerializedName("license_plate") val licensePlate: String? = "",
    @SerializedName("chasis_no") val chasisNo: String? = "",
    @SerializedName("engine_no") val engineNo: String? = ""
)