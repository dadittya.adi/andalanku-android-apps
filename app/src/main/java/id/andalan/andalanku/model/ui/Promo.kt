package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize
import java.util.*

/*
{
"id_promo":"2",
"promo_date":"2019-03-27 15:35:11",
"title":"Promo Andalan 1",
"content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
<br \/>
<br \/>
<br \/>
<br \/>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
"image":"http:\/\/depanbelih.id\/andalanku-cms\/user_files\/promo_image\/andalanku_0d628b5d09051799.jpg"}
 */
@SuppressLint("ParcelCreator")
@Parcelize
class Promo(
    @SerializedName("id_promo") val idPromo: String? = "",
    @SerializedName("promo_date") val promoDate: String? = "",
    @SerializedName("title") val title: String? = "",
    @SerializedName("content") val content: String? = "",
    @SerializedName("image") val image: String? = "",
    @SerializedName("promo_type_name") val promo_type_name: String? = "",
    @SerializedName("slug") val slug: String? = ""
): Parcelable, PromoHomeUi, IsEmptyObject {
    override fun getPromoHome(): PromoHome {
        return PromoHome(
            idPromo,
            image,
            promoDate
        )
    }

    override fun isEmpty(): Boolean {
        return idPromo.isNullOrBlank()
    }

    fun getDateUI(listMonth: Array<String>): String {
        if (promoDate.isNullOrBlank()) return ""
        //var result = ""
        val date = promoDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}