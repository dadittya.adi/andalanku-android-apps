package id.andalan.andalanku.model.request

import android.annotation.SuppressLint
import android.os.Parcelable
import id.andalan.andalanku.model.ui.UsedCar
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class CreditSimulationRequest(
    val type: String? = "",
    val otr: String? = "",
    val dp: String? = "",
    val tenor: String? = "",
    val asuransi: String? = "",
    val wilayah: String? = "",
    val car_price: String? = ""
) : Parcelable {
    @IgnoredOnParcel
    var dpAmount: Long = 0
    fun isValid(): Boolean {
        var result = true
        when {
            type.isNullOrBlank() -> result = false
            otr.isNullOrBlank() -> result = false
            dp.isNullOrBlank() -> result = false
            tenor.isNullOrBlank() -> result = false
            asuransi.isNullOrBlank() -> result = false
            wilayah.isNullOrBlank() -> result = false
        }

        return result
    }

    fun isValidKMG(): Boolean {
        var result = true
        when {
            type.isNullOrBlank() -> result = false
            otr.isNullOrBlank() -> result = false
            dp.isNullOrBlank() -> result = false
            tenor.isNullOrBlank() -> result = false
            asuransi.isNullOrBlank() -> result = false
            wilayah.isNullOrBlank() -> result = false
        }

        return result
    }

    @IgnoredOnParcel
    var usedCar = UsedCar()
}