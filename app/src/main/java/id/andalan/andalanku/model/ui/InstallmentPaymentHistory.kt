package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

/*{
            "id": "103",
            "id_customer": "156",
            "transaction_code": "3ARNOVD976ZK",
            "inquiry_date": "2020-07-20 05:26:48",
            "contract_no": "114115181598",
            "police_number": "B1158NYK",
            "installment_number": "16",
            "tenor": "48",
            "description": "Pembayaran Angusran",
            "amount": "2922000",
            "admin_fee": "110",
            "penalty": "0",
            "total_amount": "2922110",
            "due_date": "2020-02-20",
            "periode": "02-2020",
            "payment_status": "PAYMENT",
            "sql_insert_detail": null,
            "payment_datetime": null,
            "debit_from_name": null,
            "debit_from": null,
            "credit_to_name": null,
            "credit_to": null,
            "product_code": null,
            "payment_amount": null,
            "currency": null
        }
 */
@SuppressLint("ParcelCreator")
@Parcelize
class InstallmentPaymentHistory (
    @SerializedName("id") val id: String? = "",
    @SerializedName("date") val     date: String? = "",
    @SerializedName("order_id") val orderId: String? = "",
    @SerializedName("info_1") val info1: String? = "",
    @SerializedName("info_2") val info2: String? = "",
    @SerializedName("info_3") val info3: String? = "",
    @SerializedName("info_4") val info4: String? = "",
    @SerializedName("info_5") val info5: String? = "",
    @SerializedName("amount") val amount: String? ="0",
    @SerializedName("status") val status: String? = ""
): Parcelable {
    fun getInstallmentPaymentTransaction(): InstallmentPaymentTransaction {
        return InstallmentPaymentTransaction(
            id = id,
            date = date,
            orderId = orderId,
            info1 = info1,
            info2 = info2,
            info3 = info3,
            info4 = info4,
            info5 = info5,
            amount = amount,
            status = status
        )
    }

    fun getInquiryDateUI(listMonth: Array<String>): String {
        if (date.isNullOrBlank()) return ""
        //var result = ""
        val date = date.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}