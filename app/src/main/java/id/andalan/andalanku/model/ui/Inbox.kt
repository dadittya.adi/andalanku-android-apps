package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

// {"id":"2","customer_id":"6","date":"2019-07-06 05:53:32","type":"credit application","title":"Pengajuan Kredit","status":"1"}
@SuppressLint("ParcelCreator")
@Parcelize
class Inbox (
    @SerializedName("id") val id: String? = "",
    @SerializedName("customer_id") val customerId: String? = "",
    @SerializedName("date") val date: String? = "",
    @SerializedName("type") val type: String? = "",
    @SerializedName("title") val title: String? = "",
    @SerializedName("display_date") val displayDate: String? = "",
    @SerializedName("message") val message: String? = "",
    @SerializedName("status") val status: String? = "",
    @SerializedName("inbox_media") val complainMedia: MutableList<String>? = arrayListOf()
): Parcelable {
    fun getDateUI(yesterday: String, listDay: Array<String>, listMonth: Array<String>): String {
        if (date.isNullOrBlank()) return ""
        //var result = ""
        val date = date.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(yesterday, listDay, listMonth)

        return date.datetimeToString(yesterday, listDay, listMonth)
    }

    override fun toString(): String {
        return title?:""
    }
}
