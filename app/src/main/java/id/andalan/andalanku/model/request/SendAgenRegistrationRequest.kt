package id.andalan.andalanku.model.request

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class SendAgenRegistrationRequest(
    @SerializedName("id_card_image") var imageIdCard: String? = "",
    @SerializedName("type_id_card_image") var typeImageIdCard: String? = "",

    @SerializedName("id_card_number") var idCardNumber: String? = "",
    @SerializedName("province_id") var provinceId: String? = "",
    @SerializedName("city_id") var cityId: String? = "",
    @SerializedName("district_id") var districtId: String? = "",
    @SerializedName("village_id") var villageId: String? = "",
    @SerializedName("address") var address: String? = "",
    @SerializedName("rt") var rt: String? = "",
    @SerializedName("rw") var rw: String? = "",
    @SerializedName("zip_code") var zipCode: String? = "",
    // domicile
    @SerializedName("province_id_domicile") var provinceIdDomicile: String? = "",
    @SerializedName("city_id_domicile") var cityIdDomicile: String? = "",
    @SerializedName("district_id_domicile") var districtIdDomicile: String? = "",
    @SerializedName("village_id_domicile") var villageIdDomicile: String? = "",
    @SerializedName("address_domicile") var addressDomicile: String? = "",
    @SerializedName("rt_domicile") var rtDomicile: String? = "",
    @SerializedName("rw_domicile") var rwDomicile: String? = "",
    @SerializedName("zip_code_domicile") var zipCodeDomicile: String? = "",
    // office
    @SerializedName("province_id_office") var provinceIdOffice: String? = "",
    @SerializedName("regency_id_office") var cityIdOffice: String? = "",
    @SerializedName("district_id_office") var districtIdOffice: String? = "",
    @SerializedName("village_id_office") var villageIdOffice: String? = "",
    @SerializedName("address_office") var addressOffice: String? = "",
    @SerializedName("rt_office") var rtOffice: String? = "",
    @SerializedName("rw_office") var rwOffice: String? = "",
    @SerializedName("zip_code_office") var zipCodeOffice: String? = "",

    @SerializedName("area_code") var areaCode: String? = "",
    @SerializedName("phone") var phone: String? = "",
    @SerializedName("phone_number") var mobilePhone: String? = "",

    @SerializedName("npwp") val npwp: String? = "",
    @SerializedName("npwp_image") val npwp_image: String? = "",
    @SerializedName("npwp_image_type") val npwpImageType: String? = "",

    @SerializedName("gender") val gender: String? = "",
    @SerializedName("birth_place") val birthPlace: String? = "",
    @SerializedName("birth_date") val birthDate: String? = "",
    @SerializedName("agent_name") val agentName: String? = "",
    @SerializedName("email") val email: String? = "",
    @SerializedName("occupation_id") val occupationId: String? = "",
    //bank
    @SerializedName("bank_name") var bankName: String? = "",
    @SerializedName("bank_branch_name") var bankBranchName: String? = "",
    @SerializedName("bank_account_number") var bankAccountNumber: String? = "",
    @SerializedName("bank_account_holder") var bankAccountHolder: String? = "",
    @SerializedName("bank_account_image") var bankAccountImage: String? = "",
    @SerializedName("bank_image_type") var bankImageType: String? = "",

    @SerializedName("branch_id") var branchId: String? = "",
    @SerializedName("status_pernikahan") var statusPernikahan: String? = "",
    @SerializedName("pendidikan_terakhir") var pendidikanTerakhir: String? = "",
    @SerializedName("status_rumah_tinggal") var statusRumahTinggal: String? = "",
    @SerializedName("kerabat_afi") var kerabatAfi: String? = "",
    @SerializedName("kerabat_afi_jabatan") var kerabatAfiJabatan: String? = "",
    @SerializedName("lama_tinggal_tahun") var lamaTinggalTahun: String? = "",
    @SerializedName("lama_tinggal_bulan") var lamaTinggalBulan: String? = "",
    @SerializedName("nik_upline") var nikUpline: String? = "",
    @SerializedName("upline_name") var nameUpline: String? = ""
):  Parcelable {
    fun isValidFormCompleteData(): String {
        if (branchId.isNullOrBlank()) return "Cabang AFI Harus Diisi"
        if (agentName.isNullOrBlank()) return "Nama Agent Harus Diisi"
        if (gender.isNullOrBlank()) return "Jenis Kelamin Harus Diisi"
        if (birthPlace.isNullOrBlank()) return "Tempat Lahir Harus Diisi"
        if (birthDate.isNullOrBlank()) return "Tanggal Lahir Harus Diisi"
        if (idCardNumber.isNullOrBlank()) return "No KTP Harus Diisi"
        if (mobilePhone.isNullOrBlank()) return "Nomor HP Harus Diisi"
        if (pendidikanTerakhir.isNullOrBlank()) return "Pendidikan Terakhir Harus Diisi"
        if (statusPernikahan.isNullOrBlank()) return "Status Pernikahan Harus Diisi"
        if (statusRumahTinggal.isNullOrBlank()) return "Status Tempat Tinggal Harus Diisi"
        if (lamaTinggalTahun.isNullOrBlank()) return "Lama Tinggal Harus Diisi"
        if (lamaTinggalBulan.isNullOrBlank()) return "Lama Tinggal Harus Diisi"
        if (email.isNullOrBlank()) return "Email Harus Diisi"
        if (occupationId.isNullOrBlank()) return "Pekerjaan Harus Diisi"
        if (npwp.isNullOrBlank()) return "NPWP Harus Diisi"
        if (npwp_image.isNullOrBlank()) return "Foto NPWP Harus Disii"
        if (provinceId.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (cityId.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (districtId.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (villageId.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (address.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (rt.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (rw.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (zipCode.isNullOrBlank()) return "Alamat KTP Belum Lengkap"
        if (imageIdCard.isNullOrBlank()) return "Foto KTP Harus Diisi"
        if (provinceIdDomicile.isNullOrBlank()) return "Alamat Domisili Belum Lengkap"
        if (cityIdDomicile.isNullOrBlank()) return "Alamat Domisili Belum Lengkap"
        if (districtIdDomicile.isNullOrBlank()) return "Alamat Domisli Belum Lengkap"
        if (villageIdDomicile.isNullOrBlank()) return "Alamat Domisili Belum Lengkap"
        if (addressDomicile.isNullOrBlank()) return "Alamat Domisili Belum Lengkap"
        if (rtDomicile.isNullOrBlank()) return "Alamat Domisili Belum Lengkap"
        if (rwDomicile.isNullOrBlank()) return "Alamat Domisili Belum Lengkap"
        if (bankName.isNullOrBlank()) return "Bank Harus Diisi"
        if (bankBranchName.isNullOrBlank()) return "Cabang Bank Harus Diisi"
        if (bankAccountNumber.isNullOrBlank()) return "Nomor Rekening Harus Diisi"
        if (bankAccountHolder.isNullOrBlank()) return "Atas Nama Rekening Harus Diisi"
        if (bankAccountImage.isNullOrBlank()) return "Foto Cover Buku Tabungan Harus Diisi"
        if (npwp.length<15) return "NPWP 15 digit"

        return ""
    }

    fun isValidBank(): Boolean {
        if (bankName.isNullOrBlank()) return false
        if (bankAccountNumber.isNullOrBlank()) return false
        if (bankAccountHolder.isNullOrBlank()) return false
        //if (bankAccountImage.isNullOrBlank()) return false

        return true
    }
}
