package id.andalan.andalanku.model.ui

import com.google.android.material.textfield.TextInputLayout

class TILExtension(
    val til: TextInputLayout,
    val error: String = ""
)