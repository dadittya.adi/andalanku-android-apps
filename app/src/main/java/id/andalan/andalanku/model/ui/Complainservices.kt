package id.andalan.andalanku.model.ui

import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate

class Complainservices(
    val id: String? = null,
    val category: String? = null,
    val date: String? = null, // "2019-05-06 09:54:39",
    val state: String? = null
) {
    fun getDateUI(listMonth: Array<String>): String {
        if (date.isNullOrBlank()) return ""
        //var result = ""
        val date = date.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}