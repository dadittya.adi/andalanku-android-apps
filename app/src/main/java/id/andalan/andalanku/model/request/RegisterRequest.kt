package id.andalan.andalanku.model.request

import com.google.gson.annotations.SerializedName

class RegisterRequest(
    @SerializedName("customer_name") val customerName: String? = "",
    @SerializedName("id_number") val idNumber: String? = "",
    @SerializedName("email") val email: String? = "",
    @SerializedName("password") val password: String? = "",
    @SerializedName("phone_number") val phoneNumber: String? = ""
)
