package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/*
{
    "id_car_type": "13",
    "id_car_brand": "9",
    "name": "Agya",
    "is_active": "1",
    "lowestPrice": "109 juta",
    "car_type_image": [
        "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_2058353324f6f3d1.png",
        "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_bcebe4b9d0f9f1f4.jpg",
        "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_a75eceea4bb041c2.jpg"
    ]
}
 */
@SuppressLint("ParcelCreator")
@Parcelize
class CarType (
    @SerializedName("id_car_type") val idCarType: String? = "",
    @SerializedName("id_car_brand") val idCarBrand: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("lowestPrice") val lowestPrice: String? = "",
    @SerializedName("car_type_image") val images: MutableList<String>? = arrayListOf(),
    @SerializedName("is_active") val isActive: String? = ""
): Parcelable {
    fun isEmpty(): Boolean {
        return idCarType.isNullOrEmpty()
    }
}