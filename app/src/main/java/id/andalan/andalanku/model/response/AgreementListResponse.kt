package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Agreement

// {"status":"success","agreement_list":[{"agreement_no":"1730105861","insurance_category":"ARK"}]}
class AgreementListResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("agreement_list") val agreementList: MutableList<Agreement>? = arrayListOf()
)