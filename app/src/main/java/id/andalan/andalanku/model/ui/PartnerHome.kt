package id.andalan.andalanku.model.ui

class PartnerHome (
    val id: String? = "",
    val link: String? = "",
    val name: String? = "",
    val url: String? = ""
)