package id.andalan.andalanku.model.ui

import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate

class TransactionHistory(
    val id: String? = null,
    val paymentStatus: String? = null,
    val inquiryDate: String? = null, // "2019-05-06 09:54:39",
    val agreementNumber: String? = null,
    val installmentNumber: String? = null,
    val periode: String? = null,
    val transactionCode: String? = null,
    val totalPayment: String?
) {
    fun getDateUI(listMonth: Array<String>): String {
        if (inquiryDate.isNullOrBlank()) return ""
        //var result = ""
        val date = inquiryDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}