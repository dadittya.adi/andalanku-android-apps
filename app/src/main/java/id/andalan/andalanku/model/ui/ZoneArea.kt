package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class ZoneArea(
    val id: Int? = 0,
    val name: String = ""
): Parcelable {
    fun getZoneName(zone: String): String {
        return "$zone $id"
    }
}