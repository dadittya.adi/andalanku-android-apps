package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.Collateral
import id.andalan.andalanku.model.ui.CollateralRequest

// {"status":"success","insurance_claim_list":[
// {"id":"2","ticket_number":"CRQ1934651","customer_id":"6","request_date":"2019-06-21 12:56:28","agreement_number":"1730105861","taking_date":"2019-07-11","status":"ON PROCESS","customer_name":"Donovan"}
// ]}
class CollateralListResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("insurance_claim_list") val collaterallist: MutableList<CollateralRequest>? = arrayListOf()
)