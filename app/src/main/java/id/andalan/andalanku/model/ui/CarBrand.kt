package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//{
//      "id_car_brand":"9",
//      "name":"Toyota",
//      "image":"http:\/\/andalanku-cms.depanbelih.id\/user_files\/car_brand_image\/andalanku_35b8a22f77019577.png",
//      "is_active":"1"
//  }
@SuppressLint("ParcelCreator")
@Parcelize
class CarBrand (
    @SerializedName("id_car_brand") val idCarBrand: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("image") val image: String? = "",
    @SerializedName("is_active") val isActive: String? = ""
): Parcelable {
    fun getBrands(): Brands {
        return Brands(
            id = idCarBrand,
            name = name,
            urlImage = image
        )
    }
}