package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import id.andalan.andalanku.R
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Brands(
    val id: String? = "",
    val name: String? = "",
    val urlImage: String? = "",
    val image: Int? = R.drawable.ic_default_image
): Parcelable, IsEmptyObject {
    override fun isEmpty(): Boolean {
        return id == ""
    }

    override fun equals(other: Any?): Boolean {
        return id == (other as Brands).id
    }
}