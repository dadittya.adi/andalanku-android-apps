package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Promo

/*
 * {"status":"success",
 * "promo_list":[]}
 *
 */
class PromoResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("promo_list") val promoList: MutableList<Promo>
)