package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","kartu_piutang":"http:\/\/andalanku-api.depanbelih.id\/files\/example.pdf"}
class DetailPaymentHistoryResponse(
    @SerializedName("status") val status: String? = ConfigVar.SUCCESS,
    @SerializedName("kartu_piutang") val kartuPiutang: String? = "",
    @SerializedName("message") val message: String? = ""
)