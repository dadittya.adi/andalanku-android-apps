package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Bpkb(
    val owners: String? = "",
    val carNumber: String? = "",
    val brand: String? = "",
    val type: String? = "",
    val builtYear: String? = "",
    val model: String? = "",
    val price: String? = ""
) : Parcelable {
    fun isValid(): Boolean{
        var result = true
        if (owners.isNullOrBlank()) result = false
        if (carNumber.isNullOrBlank()) result = false
        if (brand.isNullOrBlank()) result = false
        if (type.isNullOrBlank()) result = false
        if (model.isNullOrBlank()) result = false
        if (builtYear.isNullOrBlank()) result = false
        if (price.isNullOrBlank()) result = false
        return result
    }

    override fun toString(): String {
        return "$owners, $carNumber, $brand, $type, $builtYear, $model"
    }
}