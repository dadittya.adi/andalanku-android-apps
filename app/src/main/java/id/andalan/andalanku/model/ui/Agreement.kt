package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// {"agreement_no":"1730105861","insurance_category":"ARK"}
@SuppressLint("ParcelCreator")
@Parcelize
class Agreement(
    @SerializedName("agreement_no") val agreementNo: String? = "",
    @SerializedName("contract_status") val contract_status: String? = "",
    @SerializedName("insurance_category") val insuranceCategory: String? = ""
): Parcelable {
    override fun equals(other: Any?): Boolean {
        return (other as Agreement).agreementNo == agreementNo
        return (other as Agreement).contract_status == contract_status
    }

    override fun toString(): String {
        return agreementNo?:""
        return contract_status?:""
    }
}