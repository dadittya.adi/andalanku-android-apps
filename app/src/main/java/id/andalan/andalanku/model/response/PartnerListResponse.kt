package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Partner

/*
    {
    "status": "success",
    "partner_list": []
    }
 */
class PartnerListResponse (
    val status: String? = ConfigVar.FAILED,
    @SerializedName("partner_list") val partnerList: MutableList<Partner>? = arrayListOf()
)