package id.andalan.andalanku.model.request

import com.google.gson.annotations.SerializedName

class PlafondApplicationRequest(
    @SerializedName("occupation_id") val occupationId: String = "",
    @SerializedName("monthly_income") val monthlyIncome: String = "",
    @SerializedName("side_job") val sideJob: String = "",
    @SerializedName("additional_income") val additionalIsncome: String = "",
    @SerializedName("andalan_branch_id") val andalanBranchId: String = "",
    @SerializedName("survey_date") val surveyDate: String = "",
    @SerializedName("survey_time") val surveyTime: String = "",
    // legal
    @SerializedName("province_id") var provinceId: String? = "",
    @SerializedName("city_id") var cityId: String? = "",
    @SerializedName("district_id") var districtId: String? = "",
    @SerializedName("village_id") var villageId: String? = "",
    @SerializedName("address") var address: String? = "",
    @SerializedName("rt") var rt: String? = "",
    @SerializedName("rw") var rw: String? = "",
    @SerializedName("zip_code") var zipCode: String? = "",
    // domicile
    @SerializedName("province_id_domicile") var provinceIdDomicile: String? = "",
    @SerializedName("city_id_domicile") var cityIdDomicile: String? = "",
    @SerializedName("district_id_domicile") var districtIdDomicile: String? = "",
    @SerializedName("village_id_domicile") var villageIdDomicile: String? = "",
    @SerializedName("address_domicile") var addressDomicile: String? = "",
    @SerializedName("rt_domicile") var rtDomicile: String? = "",
    @SerializedName("rw_domicile") var rwDomicile: String? = "",
    @SerializedName("zip_code_domicile") var zipCodeDomicile: String? = "",
    // office
    @SerializedName("province_id_office") var provinceIdOffice: String? = "",
    @SerializedName("city_id_office") var cityIdOffice: String? = "",
    @SerializedName("district_id_office") var districtIdOffice: String? = "",
    @SerializedName("village_id_office") var villageIdOffice: String? = "",
    @SerializedName("address_office") var addressOffice: String? = "",
    @SerializedName("rt_office") var rtOffice: String? = "",
    @SerializedName("rw_office") var rwOffice: String? = "",
    @SerializedName("zip_code_office") var zipCodeOffice: String? = "",

    var fotoKtp: String? = "",
    var typeFotoKtp: String? = "",
    var fotoSlipGaji: String? = "",
    var typeFotoSlipGaji: String? = ""
) {
    fun isValid(): Boolean {
        if (occupationId.isBlank()) return false
        if (monthlyIncome.isBlank()) return false
        if (andalanBranchId.isBlank()) return false
        if (surveyDate.isBlank()) return false
        if (surveyTime.isBlank()) return false
        return true
    }
}