package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Location

class LocationResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("province_list") val provinceList: MutableList<Location>? = arrayListOf(),
    @SerializedName("city_list") val cityList: MutableList<Location>? = arrayListOf(),
    @SerializedName("district_list") val districtList: MutableList<Location>? = arrayListOf(),
    @SerializedName("village_list") val villageList: MutableList<Location>? = arrayListOf(),
    @SerializedName("zip_code") val zipCode: String
)