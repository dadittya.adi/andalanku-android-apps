package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","polis":"http:\/\/andalanku-api.depanbelih.id\/files\/example.pdf"}
class AgreementDocumentResponse(
    @SerializedName("andalan_agreement_document") val agreementDocument: String? = "",
    @SerializedName("agreement_document_message") val message: String? = ""
)