package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

// {\"ticket_number\":\"C146464655\",
// \"category\":\"Lain-lain\",
// \"input_date\":\"2019-07-06 13:30:05\",
// \"status\":\"SUBMITTED\",
// \"complain_message\":\"pesan\",
// \"processed_status\":\"PENDING\",
// \"processed_date\":\"\",
// \"solved_status\":\"PENDING\",
// \"solved_date\":\"\",
// \"complain_media\":[\"http://cms.andalanku.id/user_files/media_image/andalanku_6fe15bfcf4602815.png\"]}

@SuppressLint("ParcelCreator")
@Parcelize
class ComplainMessage (
    @SerializedName("ticket_number") val ticketNumber: String? = "",
    @SerializedName("category") val category: String? = "",
    @SerializedName("status") val status: String? = "",
    @SerializedName("input_date") val inputDate: String? = "",
    @SerializedName("complain_message") val complainMessage: String? = "",
    @SerializedName("processed_status") val processedStatus: String? = "",
    @SerializedName("processed_date") val processedDate: String? = "",
    @SerializedName("solved_status") val solvedStatus: String? = "",
    @SerializedName("solved_date") val solvedDate: String? = ""
    //@SerializedName("complain_media") val complainMedia: MutableList<String>? = arrayListOf()
): Parcelable {
    fun getInputDateUI(listMonth: Array<String>): String {
        if (inputDate.isNullOrBlank()) return ""
        //var result = ""
        val date = inputDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }

    fun getProcessedDateUI(listMonth: Array<String>): String {
        if (processedDate.isNullOrBlank()) return ""
        //var result = ""
        val date = processedDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}