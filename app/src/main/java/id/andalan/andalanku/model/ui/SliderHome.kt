package id.andalan.andalanku.model.ui

import android.graphics.Bitmap
import android.graphics.drawable.Drawable

class SliderHome (
    val id: String? = "",
    val idPromo: String? = "",
    val promoSlug: String? = "",
    val url: String? = "",
    val drawableImage: Drawable? = null
)