package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.LegalDocument

class LegalDocumentResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("legal_document_list") val legalDocumentList: MutableList<LegalDocument> = arrayListOf()
)