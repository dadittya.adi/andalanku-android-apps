package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//  {
//            "id": "1",
//            "name": "Pulsa",
//            "icon": "ic_pulsa",
//            "class": "id.andalan.andalanku.ui.ppob.PpobPulsaActivity",
//            "title": "Isi Ulang Ponsel",
//            "extra_1": "pulsa",
//            "extra_2": null,
//            "extra_3": null,
//            "ppob_group": "top_up"
//        }
@SuppressLint("ParcelCreator")
@Parcelize
class PpobGroup (
    @SerializedName("id") val id: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("icon") val icon: String? = "",
    @SerializedName("class") val className: String? = "",
    @SerializedName("title") val title: String? = "",
    @SerializedName("extra_1") val extra1: String? = "",
    @SerializedName("extra_2") val extra2: String? = "",
    @SerializedName("extra_3") val extra3: String? = "",
    @SerializedName("ppob_group") val group: String? = ""
): Parcelable {
    fun getBillDetails(): PpobGroup {
        return PpobGroup(
            id = id,
            name = name,
            icon = icon,
            className = className,
            title = title,
            extra1 = extra1,
            extra2 = extra2,
            extra3 = extra3,
            group = group
        )
    }
    
}
