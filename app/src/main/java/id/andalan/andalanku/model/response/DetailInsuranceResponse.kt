package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","polis":"http:\/\/andalanku-api.depanbelih.id\/files\/example.pdf"}
class DetailInsuranceResponse(
    @SerializedName("polis") val polis: String? = "",
    @SerializedName("detail_insurance_message") val message: String? = ""
)