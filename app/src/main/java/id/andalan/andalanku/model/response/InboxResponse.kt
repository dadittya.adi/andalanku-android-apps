package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Inbox

// {"status":"success","inbox_list":[]}
class InboxResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("inbox_list") val listInbox: MutableList<Inbox>? = arrayListOf(),
    @SerializedName("unread_count") val unreadCount: Int? = 0
)