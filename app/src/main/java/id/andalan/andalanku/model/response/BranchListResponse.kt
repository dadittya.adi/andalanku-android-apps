package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Branch

// {"status":"success","branch_list":[]}
class BranchListResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("branch_list") val branchList: MutableList<Branch>? = arrayListOf()
)