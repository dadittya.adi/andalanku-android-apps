package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class CreditSimulation(
    val dp: String? = "",
    val tenor: String? = "",
    val pokokHutang: String? = "",
    val angsuran: String? = "",
    val tipeAsuransi: String? = "",
    val rateAsuransi: String? = "",
    val premiAsuransi: String? = "",
    val provisi: String? = "",
    val biayaAdmin: String? = "",
    val bungaFlat: String? = "",
    val totalHutang: String? = ""
) : Parcelable, IsEmptyObject {
    override fun isEmpty(): Boolean {
        return totalHutang.isNullOrEmpty()
    }
}