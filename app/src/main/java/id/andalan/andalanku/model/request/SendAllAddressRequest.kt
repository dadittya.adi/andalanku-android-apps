package id.andalan.andalanku.model.request

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize

class SendAllAddressRequest(
    @SerializedName("id_card_image") var imageIdCard: String? = "",
    @SerializedName("type_id_card_image") var typeImageIdCard: String? = "",

    @SerializedName("id_card_number") var idCardNumber: String? = "",
    @SerializedName("province_id") var provinceId: String? = "",
    @SerializedName("city_id") var cityId: String? = "",
    @SerializedName("district_id") var districtId: String? = "",
    @SerializedName("village_id") var villageId: String? = "",
    @SerializedName("address") var address: String? = "",
    @SerializedName("rt") var rt: String? = "",
    @SerializedName("rw") var rw: String? = "",
    @SerializedName("zip_code") var zipCode: String? = "",
    // domicile
    @SerializedName("province_id_domicile") var provinceIdDomicile: String? = "",
    @SerializedName("city_id_domicile") var cityIdDomicile: String? = "",
    @SerializedName("district_id_domicile") var districtIdDomicile: String? = "",
    @SerializedName("village_id_domicile") var villageIdDomicile: String? = "",
    @SerializedName("address_domicile") var addressDomicile: String? = "",
    @SerializedName("rt_domicile") var rtDomicile: String? = "",
    @SerializedName("rw_domicile") var rwDomicile: String? = "",
    @SerializedName("zip_code_domicile") var zipCodeDomicile: String? = "",
    // office
    @SerializedName("province_id_office") var provinceIdOffice: String? = "",
    @SerializedName("city_id_office") var cityIdOffice: String? = "",
    @SerializedName("district_id_office") var districtIdOffice: String? = "",
    @SerializedName("village_id_office") var villageIdOffice: String? = "",
    @SerializedName("address_office") var addressOffice: String? = "",
    @SerializedName("rt_office") var rtOffice: String? = "",
    @SerializedName("rw_office") var rwOffice: String? = "",
    @SerializedName("zip_code_office") var zipCodeOffice: String? = "",

    @SerializedName("area_code") var areaCode: String? = "",
    @SerializedName("phone") var phone: String? = "",
    @SerializedName("mobile_phone") var mobilePhone: String? = "",

    @SerializedName("npwp") val npwp: String? = "",
    @SerializedName("npwp_image") val npwp_image: String? = "",
    @SerializedName("gender") val gender: String? = "",
    @SerializedName("birth_place") val birthPlace: String? = "",
    @SerializedName("birth_date") val birthDate: String? = "",
    @SerializedName("avatar") val avatar: String? = "",
    @SerializedName("customer_name") val customer_name: String? = "",
    //bank
    @SerializedName("bank_name") var bankName: String? = "",
    @SerializedName("bank_account_number") var bankAccountNumber: String? = "",
    @SerializedName("bank_account_holder") var bankAccountHolder: String? = "",
    @SerializedName("bank_account_image") var bankAccountImage: String? = ""
) : Parcelable {
    fun validFormCompleteData(): String {
        if (npwp != null) {
            if (npwp.length<15) return "NPWP 15 digit"
        }

        return ""
    }
}