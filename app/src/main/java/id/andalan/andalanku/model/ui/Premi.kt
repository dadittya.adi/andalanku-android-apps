package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Premi(
    val id: Int? = 0,
    val type: String? = "",
    val name: String? = ""
):Parcelable