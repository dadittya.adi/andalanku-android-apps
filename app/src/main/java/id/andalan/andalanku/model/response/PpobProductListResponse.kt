package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Complain
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory
import id.andalan.andalanku.model.ui.PpobProduct

/*
{
    "status": "success",
    "complain_list": [
    ]
}
 */
class PpobProductListResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("product_list") val ppobProduct: MutableList<PpobProduct>? = arrayListOf()
)