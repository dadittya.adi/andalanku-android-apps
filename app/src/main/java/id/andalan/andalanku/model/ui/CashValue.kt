package id.andalan.andalanku.model.ui

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

/*
{
    "AgreementNo": "114115170008",
    "CashValue": 20380612.5
}
 */
class CashValue (
    @SerializedName("AgreementNo") val AgreementNo: String? = "",
    @SerializedName("CashValue") val CashValue: Double? = 0.0
)
