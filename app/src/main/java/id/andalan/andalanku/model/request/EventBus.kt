package id.andalan.andalanku.model.request

import java.util.*

class EventBus (
    val type: String,
    val objects: Any? = null
) {
    companion object {
        val LOAD_PERSONAL_INFO = "load_personal_info"
        val LOAD_INBOX = "load_inbox"
        val LOAD_AGGREMENT_LIST = "load_aggrement_list"
        val LOAD_UNREAD_MESSAGE = "load_unread_message"
    }
}