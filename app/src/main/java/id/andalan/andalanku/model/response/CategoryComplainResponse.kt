package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","complain_category_list":["Marketing","Collection","Operation","Pelelangan","Hukum","Pelayanan","Lain-lain"]}
class CategoryComplainResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("complain_category_list") val categoryList: MutableList<String>? = arrayListOf()
)