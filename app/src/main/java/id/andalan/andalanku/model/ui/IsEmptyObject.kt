package id.andalan.andalanku.model.ui

interface IsEmptyObject {
    fun isEmpty(): Boolean
}