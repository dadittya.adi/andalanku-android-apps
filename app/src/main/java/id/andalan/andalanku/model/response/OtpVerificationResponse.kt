package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

//{"status":"success","message":"Login successful","id_customer":"3"}
class OtpVerificationResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("id_customer") val idCustomer: Int? = 0
)