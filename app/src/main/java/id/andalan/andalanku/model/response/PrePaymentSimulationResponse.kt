package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","message":"Prepayment simulation successful","totalPelunasan":null}
class PrePaymentSimulationResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("totalPelunasan") val totalPelunasan: String? = "",
    @SerializedName("sisaPinjaman") val sisaPinjaman: String? = ""

)