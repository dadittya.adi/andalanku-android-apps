package id.andalan.andalanku.model.request

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.model.ui.IsEmptyObject

class UpdatePersonalRequest (
    @SerializedName("id_customer") val idCustomer: Int? = 0,
    @SerializedName("address_type") val addressType: String? = "",
    @SerializedName("gender") val gender: String? = "",
    @SerializedName("address") val address: String? = "",
    @SerializedName("rt") val rt: String? = "",
    @SerializedName("rw") val rw: String? = "",
    @SerializedName("kelurahan") val kelurahan: String? = "",
    @SerializedName("kecamatan") val kecamatan: String? = "",
    @SerializedName("city") val city: String? = "",
    @SerializedName("zip_code") val zipCode: String? = "",
    @SerializedName("phone_number") val phoneNumber: String? = "",
    @SerializedName("mobile_number") val mobileNumber: String? = "",
    @SerializedName("npwp") val npwp: String? = "",
    @SerializedName("npwp_image") val npwp_image: String? = "",
    @SerializedName("nip") val nip: String? = ""
) {
    fun isValid(): Boolean {
        var result = true
        if (idCustomer == 0) result = false
        if (addressType.isNullOrEmpty()) result = false
        if (address.isNullOrEmpty()) result = false
        if (rt.isNullOrEmpty()) result = false
        if (rw.isNullOrEmpty()) result = false
        if (kelurahan.isNullOrEmpty()) result = false
        if (kecamatan.isNullOrEmpty()) result = false
        if (city.isNullOrEmpty()) result = false
        if (zipCode.isNullOrEmpty()) result = false
        if (phoneNumber.isNullOrEmpty()) result = false
        if (mobileNumber.isNullOrEmpty()) result = false

        return result
    }
}