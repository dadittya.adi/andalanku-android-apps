package id.andalan.andalanku.model.request

class InsuranceClaimRequest(
    val idCustomer: String? = "",
    val agreement_number: String? = "",
    val claim_type: String? = "",
    val event_case: String? = "",
    val event_date: String? = "",
    val location: String? = "",
    val notes: String? = ""
){
    fun isValid(): Boolean {
        if (agreement_number.isNullOrBlank()) return false
        if (claim_type.isNullOrBlank()) return false
        if (event_case.isNullOrBlank()) return false
        if (event_date.isNullOrBlank()) return false
        if (location.isNullOrBlank()) return false
        if (notes.isNullOrBlank()) return false

        return true
    }

    override fun toString(): String {
        return "$idCustomer - $agreement_number - $claim_type - $event_case - $event_date - $location - $notes"
    }
}