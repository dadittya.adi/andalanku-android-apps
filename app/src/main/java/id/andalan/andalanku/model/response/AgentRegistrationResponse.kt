package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"afis_response":null,"status":"success","message":"Profile update request successful","ticket_number":"UP539765311"}
class AgentRegistrationResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = ""
)