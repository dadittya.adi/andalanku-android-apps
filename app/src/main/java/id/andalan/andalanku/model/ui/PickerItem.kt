package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class PickerItem(
    var id: String? = null,
    var name: String? = null,
    var contract_status: String? = null,
    var isChoosed: Boolean? = false
) : Parcelable {
    @IgnoredOnParcel
    var realObject:Any? = null

    override fun equals(other: Any?): Boolean {
        return name == (other as PickerItem).name
        return contract_status == (other as PickerItem).contract_status
    }

    override fun hashCode(): Int {
        var result = id?.hashCode() ?: 0
        result = 31 * result + (name?.hashCode() ?: 0)
        result = 31 * result + (contract_status?.hashCode() ?: 0)
        result = 31 * result + (isChoosed?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return name?:""
        return contract_status?:""
    }
}