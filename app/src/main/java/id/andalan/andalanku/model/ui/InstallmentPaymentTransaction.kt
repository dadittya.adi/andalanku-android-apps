package id.andalan.andalanku.model.ui

import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate

class InstallmentPaymentTransaction(
    val id: String? = null,
    val date: String? = null,
    val orderId: String? = null,
    val info1: String? = null,
    val info2: String? = null,
    val info3: String? = null,
    val info4: String? = null,
    val info5: String? = null,
    val amount: String? = null,
    val status: String?
) {
    fun getDateUI(listMonth: Array<String>): String {
        if (date.isNullOrBlank()) return ""
        //var result = ""
        val date = date.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}