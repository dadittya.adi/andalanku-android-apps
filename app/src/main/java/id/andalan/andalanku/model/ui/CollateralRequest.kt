package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

//// {"id":"2","ticket_number":"CRQ1934651","customer_id":"6","request_date":"2019-06-21 12:56:28","agreement_number":"1730105861","taking_date":"2019-07-11","status":"ON PROCESS","customer_name":"Donovan"}
@SuppressLint("ParcelCreator")
@Parcelize
class CollateralRequest (
    @SerializedName("id") val id: String? = "",
    @SerializedName("ticket_number") val ticketNumber: String? = "",
    @SerializedName("customer_id") val customerId: String? = "",
    @SerializedName("request_date") val requestDate: String? = "",
    @SerializedName("agreement_number") val agreementNumber: String? = "",
    @SerializedName("taking_date") val takingDate: String? = "",
    @SerializedName("status") val status: String? = "",
    @SerializedName("customer_name") val customerName: String? = "",
    @SerializedName("ready_date") val readyDate: String? = ""
): Parcelable {
    fun getRequestDateUI(listMonth: Array<String>): String {
        if (requestDate.isNullOrBlank()) return ""
        //var result = ""
        val date = requestDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }

    fun getTakingDateUI(listMonth: Array<String>): String {
        if (takingDate.isNullOrBlank()) return ""
        //var result = ""
        val date = takingDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }

    fun getReadyDateUI(listMonth: Array<String>): String {
        if (readyDate.isNullOrBlank()) return ""
        //var result = ""
        val date = readyDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }
}