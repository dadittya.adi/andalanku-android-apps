package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//{
//    "id_answer": "6",
//    "id_faq": "2",
//    "image": "http://andalanku-cms.depanbelih.id/user_files/faq_image/andalanku_8773dba463193ca2.jpg",
//    "description": "Silahkan menghubungi customer service Andalan Finance atau melalui aplikasi Andalanku",
//    "is_active": "TRUE"
//}
@SuppressLint("ParcelCreator")
@Parcelize
class Faq (
    @SerializedName("id_answer") val idAnswer: String? = "",
    @SerializedName("counter") val counter: String? = "",
    @SerializedName("id_faq") val idFaq: String? = "",
    @SerializedName("image") val image: String? = "",
    @SerializedName("description") val description: String? = "",
    @SerializedName("is_active") val isActive: Boolean? = false
): Parcelable