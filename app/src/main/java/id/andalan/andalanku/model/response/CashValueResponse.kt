package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.CashValue

/*
{
    "status": "success",
    "cash_value_list": [
        {
            "AgreementNo": "114115170008",
            "CashValue": 20380612.5
        },
        {
            "AgreementNo": "1730105861",
            "CashValue": 11291411.25
        }
    ],
    "total_cash_value": 31672023.75
}
 */
class CashValueResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("cash_value_list") val cashValueList: MutableList<CashValue>? = arrayListOf(),
    @SerializedName("total_cash_value") val totalCashValue: Double? = 0.0
)