package id.andalan.andalanku.model.ui

interface CreditSimulationUI {
    fun getCreditSimulation(): CreditSimulation
}