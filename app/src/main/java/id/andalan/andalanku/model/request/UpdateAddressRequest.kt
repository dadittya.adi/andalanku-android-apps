package id.andalan.andalanku.model.request

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class UpdateAddressRequest(
    @SerializedName("id_customer") val idCustomer: Int? = 0,
    @SerializedName("address_type") val addressType: String? = "",
    @SerializedName("address") val address: String? = "",
    @SerializedName("rt") val rt: String? = "",
    @SerializedName("rw") val rw: String? = "",
    @SerializedName("province_id") val provinceId: String? = "",
    @SerializedName("regency_id") val regencyId: String? = "",
    @SerializedName("district_id") val districtId: String? = "",
    @SerializedName("village_id") val villageId: String? = "",
    @SerializedName("zip_code") val zipCode: String? = "",
    @SerializedName("phone") val phoneNumber: String? = "",
    @SerializedName("mobile_phone") val mobileNumber: String? = "",
    @SerializedName("nip") val nip: String? = "",
    @SerializedName("id_card_image") val idCardImage: String? = "",
    @SerializedName("type_id_card_image") var typeIdCardImage: String? = "",
    @SerializedName("npwp") val npwp: String? = "",
    @SerializedName("npwp_image") val npwp_image: String? = "",
    @SerializedName("gender") val gender: String? = "",
    @SerializedName("birth_place") val birthPlace: String? = "",
    @SerializedName("birth_date") val birthDate: String? = "",
    @SerializedName("provinceName") val provinceName: String? = "",
    @SerializedName("cityName") val cityName: String? = "",
    @SerializedName("villageName") val villageName: String? = "",
    @SerializedName("districtName") val districtName: String? = "",
    @SerializedName("avatar") val avatar: String? = "",
    @SerializedName("id_card_number") val id_card_number: String? = "",
    @SerializedName("home_phone_number") val home_phone_number: String? = "",
    @SerializedName("customer_name") val customer_name: String? = "",
    //bank
    @SerializedName("bank_name") var bankName: String? = "",
    @SerializedName("bank_account_number") var bankAccountNumber: String? = "",
    @SerializedName("bank_account_holder") var bankAccountHolder: String? = "",
    @SerializedName("bank_account_image") var bankAccountImage: String? = ""
): Parcelable {

    fun isValid(): Boolean {
        var result = true
        if (idCustomer == 0) result = false
        if (addressType.isNullOrEmpty()) result = false
        if (address.isNullOrEmpty()) result = false
        if (rt.isNullOrEmpty()) result = false
        if (rw.isNullOrEmpty()) result = false
        if (provinceId.isNullOrEmpty()) result = false
        if (regencyId.isNullOrEmpty()) result = false
        if (districtId.isNullOrEmpty()) result = false
        if (villageId.isNullOrEmpty()) result = false
        if (zipCode.isNullOrEmpty()) result = false

        return result
    }

    override fun toString(): String {
        return "$idCustomer - $addressType - $address - $rt - $rw - $provinceId - $regencyId - $districtId - $zipCode - $phoneNumber - $idCardImage"
    }
}