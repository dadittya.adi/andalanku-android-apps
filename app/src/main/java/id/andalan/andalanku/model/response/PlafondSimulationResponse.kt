package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","message":"Plafond simulation successfully","plafond":"90.000.000"}
class PlafondSimulationResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("plafond") val plafond: String? = ""
)