package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.News

/**
 *
 *
 * {"status":"success","news_list":[]}
 *
 */
class NewsResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("news_list") val newsList: MutableList<News>? = arrayListOf()
)