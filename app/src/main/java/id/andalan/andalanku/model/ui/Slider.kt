package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.graphics.drawable.Drawable
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.InputStream
import java.net.URL

/*
{
"id_promo":"2",
"promo_date":"2019-03-27 15:35:11",
"title":"Promo Andalan 1",
"content":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
<br \/>
<br \/>
<br \/>
<br \/>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?",
"image":"http:\/\/depanbelih.id\/andalanku-cms\/user_files\/promo_image\/andalanku_0d628b5d09051799.jpg"}
 */
@SuppressLint("ParcelCreator")
@Parcelize
class Slider(
    @SerializedName("id_slider") val idSlider: String? = "",
    @SerializedName("promo_title") val promoTitle: String? = "",
    @SerializedName("id_promo") val idPromo: String? = "",
    @SerializedName("promo_slug") val promoSlug: String? = "",
    @SerializedName("image") val image: String? = ""
): Parcelable, SliderHomeUi, IsEmptyObject {
        override fun getSliderHome(): SliderHome {
            return SliderHome(
                idSlider,
                idPromo,
                promoSlug,
                image
                //loadImageFromWebURL(image)
            )
    }

    override fun isEmpty(): Boolean {
        return idSlider.isNullOrBlank()
    }

    fun loadImageFromWebURL(url: String?): Drawable? {
        return try {
            val iStream: InputStream = URL(url).content as InputStream
            Drawable.createFromStream(iStream, "src name")
        } catch (e: java.lang.Exception) {
            null
        }
    }
}