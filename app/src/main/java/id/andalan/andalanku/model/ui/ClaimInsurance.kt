package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize
import java.util.*

// {"id":"2",
// "ticket_number":"ISC2085190",
// "customer_id":"6",
// "claim_date":"2019-06-18 13:36:48",
// "agreement_number":"1730105861",
// "claim_type":"TLO",
// "event_case":"ST",
// "event_date":"2019-06-01",
// "location":"solo",
// "notes":"pesan",
// "status":"ON PROCESS",
// "customer_name":"Donovan"}

@SuppressLint("ParcelCreator")
@Parcelize
class ClaimInsurance(
    @SerializedName("id") val id: String? = "",
    @SerializedName("ticket_number") val ticketNumber:String? = "",
    @SerializedName("customer_id") val customerId: String? = "",
    @SerializedName("claim_date") val claimDate: String? = "",
    @SerializedName("agreement_number") val agreementNumber: String? = "",
    @SerializedName("claim_type") val claimType: String? = "",
    @SerializedName("event_case") val eventCase: String? = "",
    @SerializedName("event_date") val eventDate: String? = "",
    @SerializedName("location") val location: String? = "",
    @SerializedName("notes") val notes: String? = "",
    @SerializedName("status") val status: String? = "",
    @SerializedName("customer_name") val customerName: String? = "",
    @SerializedName("pic") val pic: String? = "",
    @SerializedName("media_list") val mediaList: MutableList<String>? = arrayListOf()
): Parcelable {
    fun getClaimDateUI(listMonth: Array<String>): String {
        if (claimDate.isNullOrBlank()) return ""
        //var result = ""
        val date = claimDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }

    fun getEventDateUI(listMonth: Array<String>): String {
        if (eventDate.isNullOrBlank()) return ""
        //var result = ""
        val date = eventDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }

    fun getInsuranceType(): String {
        var result = ""
        if (claimType == "TLO") {
            result = "Total Loss Only"
        }
        if (claimType == "ARK") {
            result = "All Risk"
        }

        return result
    }

    fun getClaimEventCase(): String {
        var result = ""
        if (eventCase == "TLO") {
            result = "Total Loss"
        }
        if (eventCase == "A") {
            result = "Accident"
        }
        if (eventCase == "ST") {
            result = "Stolen"
        }

        return result
    }
}