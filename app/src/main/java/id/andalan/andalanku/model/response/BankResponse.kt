package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Bank
import id.andalan.andalanku.model.ui.BankBranch

class BankResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("bank_list") val bankList: MutableList<Bank>? = arrayListOf()
)