package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","message":"Plafond application successfully","plafond":"99.000.000"}
class PlafondApplicationResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("plafond") val plafond: String? = ""
)