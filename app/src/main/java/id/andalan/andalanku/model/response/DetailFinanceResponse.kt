package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.stringToDate

// {"status":"success","otr":"143000000","down_payment":"50000000","insurance_capitalized":"3500000","net_finance":"96500000","agreement_date":"2018-01-10","effective_date":"2018-01-12","maturity_date":"2023-01-12","tenor":"60","installment_amount":"2500000"}
// ﻿{"status":"success","otr":129250000,"down_payment":14540625,"insurance_capitalized":19781725,"net_finance":134491100,"agreement_date":"2017-04-26T00:00:00","effective_date":"2017-04-28T00:00:00","maturity_date":"2022-04-26T00:00:00","tenor":60,"installment_amount":3396000}
class DetailFinanceResponse(
    @SerializedName("otr") val otr: String? = "",
    @SerializedName("down_payment") val downPayment: String? = "",
    @SerializedName("insurance_capitalized") val insuranceCapitalized: String? = "",
    @SerializedName("net_finance") val netFinance: String? = "",
    @SerializedName("agreement_date") val agreementDate: String? = "",
    @SerializedName("effective_date") val effectiveDate: String? = "",
    @SerializedName("maturity_date") val maturityDate: String? = "",
    @SerializedName("tenor") val tenor: String? = "",
    @SerializedName("installment_amount") val installmentAmount: String? = "",
    @SerializedName("next_installment_due_date") val NextInstallmentDueDate : String? = ""
) {
    fun getAgreementDateUI(listMonth: Array<String>): String {
        if (agreementDate.isNullOrBlank()) return ""
        //var result = ""
        val date = agreementDate.stringToDate("yyyy-MM-dd")

        return date.dateToString(listMonth)
    }

    fun getMaturityDateUI(listMonth: Array<String>): String {
        if (maturityDate.isNullOrBlank()) return ""
        //var result = ""
        val date = maturityDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }

    fun getMaturityDateReq(): String {
        if (maturityDate.isNullOrBlank()) return ""
        //var result = ""
        val date = maturityDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString("yyyy-MM-dd")

        return date.dateToString("yyyy-MM-dd")
    }

    fun getEffectiveDateUI(listMonth: Array<String>): String {
        if (effectiveDate.isNullOrBlank()) return ""
        //var result = ""
        val date = effectiveDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }

    fun getNextDueDateUI(listMonth: Array<String>): String {
        if (NextInstallmentDueDate.isNullOrBlank()) return ""
        //var result = ""
        val date = NextInstallmentDueDate.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }
}