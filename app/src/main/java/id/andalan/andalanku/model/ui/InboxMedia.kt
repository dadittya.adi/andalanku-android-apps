package id.andalan.andalanku.model.ui

import com.google.gson.annotations.SerializedName
/*
    {
        "url":"http:\/\/localhost:8888\/andalanku-cms\/user_files\/inbox_files\/andalanku_3594d957051430db.pdf",
        "file_type":"pdf"
    }
 */

class InboxMedia {
    @SerializedName("url") val url: String? = ""
    @SerializedName("file_type") val name: String? = ""
}