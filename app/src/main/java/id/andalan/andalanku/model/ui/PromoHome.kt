package id.andalan.andalanku.model.ui

import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate

class PromoHome (
    val id: String? = "",
    val url: String? = "",
    val date: String? = "" // 2019-03-27 15:35:11
) {
    fun getDateUI(listMonth: Array<String>): String {
        if (date.isNullOrBlank()) return ""
        //var result = ""
        val date = date.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}