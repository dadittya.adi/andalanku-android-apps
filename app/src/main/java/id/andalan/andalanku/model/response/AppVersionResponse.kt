package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.CashValue

/*
{
status: "success",
Andriod Application Version: "1",
iOS Application Version: "1"
}
 */
class AppVersionResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("Andriod Application Version") val latestAndroidVersion: Int? = 0
)