package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Complain

/*
{
    "status": "success",
    "complain_list": [
    ]
}
 */
class ComplainListResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("complain_list") val listComplain: MutableList<Complain>? = arrayListOf()
)