package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.request.UpdateAddressRequest
import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.stringToDate

/*
{"status":"success","customer_name":"Donovan","andalan_customer_id":"0009C20170403827","id_number":"3374130701800001_1","email":"user@example.com","phone_number":"081286365322_1",
"home_phone_number":"081286365322","place_of_birth":"JAKARTA PUSAT","date_of_birth":"1980-01-07","mother_name":"SITI HARYATI","occupation":"Karyawan Swasta","monthly_income":"25000000",
"additional_income":"0","side_job":"gojek","gender":"L","agent_code":"900011908000097","referral_code":"3D4F5","bank_name":"MANDIRI","bank_account_number":"123991230","bank_account_holder":"atas nama",
"customer_type":"AGENT","nip":"","npwp":"","address_domicile":"Potorono Bantul","rt_domicile":"07","rw_domicile":"20","village_id_domicile":"3402130006","village_name_domicile":"POTORONO",
"district_id_domicile":"3402130","district_name_domicile":"BANGUNTAPAN","city_id_domicile":"3402","city_name_domicile":"KABUPATEN BANTUL","province_id_domicile":"34","province_name_domicile":"DI YOGYAKARTA",
"zip_code_domicile":"688189","address_legal":"KENCONOWUNGU TENGAH IV\/12","rt_legal":"004","rw_legal":"005","village_id_legal":"3324150007","village_name_legal":"KARANGAYU","district_id_legal":"3374140",
"district_name_legal":"SEMARANG BARAT","city_id_legal":"3374","city_name_legal":"KOTA SEMARANG","province_id_legal":"33","province_name_legal":"JAWA TENGAH","zip_code_legal":"50142","address_office":"",
"rt_office":"","rw_office":"","village_id_office":"0","village_name_office":null,"district_id_office":"0","district_name_office":null,"city_id_office":"0","city_name_office":null,"province_id_office":"0",
"province_name_office":null,"zip_code_office":"","avatar":"http:\/\/cms.andalanku.id\/user_files\/customer_document_image\/andalanku_7b26bca09ddaa69a.png","foto_ktp":"","foto_npwp":"","foto_kk":"",
"foto_slip_gaji":"","foto_rekening_koran":"","foto_keuangan_lainnya":""}
 */
class PersonalInformationResponse(
    @SerializedName("status") val status: String? = ConfigVar.SUCCESS,
    @SerializedName("customer_type") val customerType: String? = ConfigVar.FAILED,
    @SerializedName("nip") val nip: String? = "",
    @SerializedName("customer_name") val customerName: String? = "",
    @SerializedName("andalan_customer_id") val andalanCustomerId: String? = "",
    @SerializedName("id_number") val idNumber: String? = "",
    @SerializedName("email") val email: String? = "",
    @SerializedName("address") val address: String? = "",
    @SerializedName("rt") val rt: String? = "",
    @SerializedName("rw") val rw: String? = "",
    @SerializedName("village_name") val villageName: String? = "",
    @SerializedName("district_name") val districtName: String? = "",
    @SerializedName("city_name") val cityName: String? = "",
    @SerializedName("province_name") val provinceName: String? = "",
    @SerializedName("zip_code") val zipCode: String? = "",
    @SerializedName("gender") val gender: String? = "",
    @SerializedName("address_legal") val addressLegal: String? = "",
    @SerializedName("rt_legal") val rtLegal: String? = "",
    @SerializedName("rw_legal") val rwLegal: String? = "",
    @SerializedName("village_name_legal") val villageNameLegal: String? = "",
    @SerializedName("district_name_legal") val districtNameLegal: String? = "",
    @SerializedName("city_name_legal") val cityNameLegal: String? = "",
    @SerializedName("province_name_legal") val provinceNameLegal: String? = "",
    @SerializedName("zip_code_legal") val zipCodeLegal: String? = "",
    @SerializedName("village_id_legal") val villageIdLegal: String? = "",
    @SerializedName("district_id_legal") val districtIdLegal: String? = "",
    @SerializedName("city_id_legal") val cityIdLegal: String? = "",
    @SerializedName("province_id_legal") val provinceIdLegal: String? = "",
    @SerializedName("address_domicile") val addressDomicile: String? = "",
    @SerializedName("rt_domicile") val rtDomicile: String? = "",
    @SerializedName("rw_domicile") val rwDomicile: String? = "",
    @SerializedName("village_name_domicile") val villageNameDomicile: String? = "",
    @SerializedName("district_name_domicile") val districtNameDomicile: String? = "",
    @SerializedName("city_name_domicile") val cityNameDomicile: String? = "",
    @SerializedName("province_name_domicile") val provinceNameDomicile: String? = "",
    @SerializedName("zip_code_domicile") val zipCodeDomicile: String? = "",
    @SerializedName("village_id_domicile") val villageIdDomicile: String? = "",
    @SerializedName("district_id_domicile") val districtIdDomicile: String? = "",
    @SerializedName("city_id_domicile") val cityIdDomicile: String? = "",
    @SerializedName("province_id_domicile") val provinceIdDomicile: String? = "",
    @SerializedName("address_office") val addressOffice: String? = "",
    @SerializedName("rt_office") val rtOffice: String? = "",
    @SerializedName("rw_office") val rwOffice: String? = "",
    @SerializedName("village_name_office") val villageNameOffice: String? = "",
    @SerializedName("district_name_office") val districtNameOffice: String? = "",
    @SerializedName("city_name_office") val cityNameOffice: String? = "",
    @SerializedName("province_name_office") val provinceNameOffice: String? = "",
    @SerializedName("zip_code_office") val zipCodeOffice: String? = "",
    @SerializedName("village_id_office") val villageIdOffice: String? = "",
    @SerializedName("district_id_office") val districtIdOffice: String? = "",
    @SerializedName("city_id_office") val cityIdOffice: String? = "",
    @SerializedName("province_id_office") val provinceIdOffice: String? = "",
    @SerializedName("area_code") val areaCode: String? = "",
    @SerializedName("phone_number") val phoneNumber: String? = "",
    @SerializedName("home_phone_number") val homePhoneNumber: String? = "",
    @SerializedName("place_of_birth") val placeOfBirth: String? = "",
    @SerializedName("date_of_birth") val dateOfBirth: String? = "",
    @SerializedName("mother_name") val motherName: String? = "",
    @SerializedName("occupation") val occupation: String? = "",
    @SerializedName("monthly_income") val monthlyIncome: String? = "",
    @SerializedName("additional_income") val additionalIncome: String? = "",
    @SerializedName("agent_code") val agentCode: String? = "",
    @SerializedName("referral_code") val referralCode: String? = "",
    @SerializedName("side_job") val sideJob: String? = "",
    @SerializedName("bank_name") val bankName: String? = "",
    @SerializedName("bank_account_number") val bankAccountNumber: String? = "",
    @SerializedName("bank_account_holder") val bankAccountHolder: String? = "",
    @SerializedName("bank_account_image") val bankAccountImage: String? = "",
    @SerializedName("avatar") val avatar: String? = "",
    @SerializedName("foto_ktp") val fotoKtp: String? = "",
    @SerializedName("foto_npwp") val fotoNpwp: String? = "",
    @SerializedName("npwp") val npwp: String? = "",
    @SerializedName("foto_kk") val fotoKk: String? = "",
    @SerializedName("foto_slip_gaji") val fotoSlipGaji: String? = "",
    @SerializedName("foto_rekening_koran") val fotoRekeningKoran: String? = "",
    @SerializedName("foto_keuangan_lainnya") val fotoKeuanganLainnya: String? = ""
) {
    fun getDOBUI(listMonth: Array<String>): String {
        if (dateOfBirth.isNullOrBlank()) return ""
        //var result = ""
        val date = dateOfBirth.stringToDate("yyyy-MM-dd")
        //result = date.dateToString(listMonth)

        return date.dateToString(listMonth)
    }

    fun getAddressLegal(): UpdateAddressRequest {
        return UpdateAddressRequest(
            phoneNumber = phoneNumber,
            provinceId = provinceIdLegal,
            regencyId = cityIdLegal,
            districtId = districtIdLegal,
            villageId = villageIdLegal,
            address = addressLegal,
            rt = rtLegal,
            rw = rwLegal,
            zipCode = zipCodeLegal,
            provinceName = provinceNameLegal,
            cityName = cityNameLegal,
            districtName = districtNameLegal,
            idCardImage = fotoKtp,
            villageName = villageNameLegal
        )
    }

    fun getAddressDomicile(): UpdateAddressRequest {
        return UpdateAddressRequest(
            phoneNumber = phoneNumber,
            provinceId = provinceIdDomicile,
            regencyId = cityIdDomicile,
            districtId = districtIdDomicile,
            villageId = villageIdDomicile,
            address = addressDomicile,
            rt = rtDomicile,
            rw = rwDomicile,
            zipCode = zipCodeDomicile,
            provinceName = provinceNameDomicile,
            cityName = cityNameDomicile,
            districtName = districtNameDomicile,
            villageName = villageNameDomicile
        )
    }

    fun isDomicileEqualKTP(): Boolean {
        if (provinceIdDomicile != provinceIdLegal) return false
        if (cityIdDomicile!= cityIdLegal) return false
        if (districtIdDomicile != districtIdLegal) return false
        if (villageIdDomicile != villageIdLegal) return false
        if (addressDomicile != addressLegal) return false
        if (rtDomicile != rtLegal) return false
        if (rwDomicile != rwLegal) return false
        if (zipCodeDomicile != zipCodeLegal) return false
        return true
    }

    fun getAddressOffice(): UpdateAddressRequest {
        return UpdateAddressRequest(
            phoneNumber = phoneNumber,
            provinceId = provinceIdOffice,
            regencyId = cityIdOffice,
            districtId = districtIdOffice,
            villageId = villageIdOffice,
            address = addressOffice,
            rt = rtOffice,
            rw = rwOffice,
            zipCode = zipCodeOffice,
            provinceName = provinceNameOffice,
            cityName = cityNameOffice,
            districtName = districtNameOffice,
            villageName = villageNameOffice
        )
    }

    override fun toString(): String {
        return "$status, $customerType, $customerName"
    }
}