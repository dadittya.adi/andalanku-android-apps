package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Complain
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory

/*
{
    "status": "success",
    "complain_list": [
    ]
}
 */
class InstallmentPaymentHistoryListResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("transaction_history_list") val listInstallmentPaymentHistory: MutableList<InstallmentPaymentHistory>? = arrayListOf()
)