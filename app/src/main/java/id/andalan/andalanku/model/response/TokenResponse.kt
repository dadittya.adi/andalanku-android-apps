package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class TokenResponse(
    val status: String? = ConfigVar.FAILED,
    val message: String? = "",
    @SerializedName("token_number") val tokenNumber: String? = null
)