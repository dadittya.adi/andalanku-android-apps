package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class BankBranch (
    @SerializedName("bank_branch_id") val id: String? = "",
    @SerializedName("bank_code") val bankCode: String? = "",
    @SerializedName("bank_branch_name") val bankBranchName: String? = ""
): Parcelable