package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import id.andalan.andalanku.utils.convertToRpFormat
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Cars (
    val id: String? = "",
    val urlImage: String? = "",
    val name: String? = "",
    val brand: String? = "",
    val price: String? = "",
    val type: String? = ""
) : Parcelable, IsEmptyObject {
    override fun isEmpty(): Boolean {
        return id == ""
    }

    fun getPriceRp(): String {
        return (price?:"0").toLong().convertToRpFormat()
    }

    fun getFullName(): String {
        return "$brand $name"
    }

    override fun equals(other: Any?): Boolean {
        return (other as Cars).id == id
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }
}