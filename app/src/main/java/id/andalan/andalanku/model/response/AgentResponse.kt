package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class AgentResponse (
    @SerializedName("agent_code") val agentCode: String? = ""
)