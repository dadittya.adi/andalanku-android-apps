package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

/*
{
    "id_complain": "11",
    "ticket_number": "C177208810",
    "category": "Marketing",
    "user_id": "2",
    "phone": "08176884055",
    "message": "Test e-complain",
    "status": "ON PROCESS",
    "submitted_date": "2019-05-06 09:54:39",
    "on_process_date": "2019-05-06 11:14:27",
    "processed_by": "1",
    "solved_date": null,
    "solved_by": null,
    "solution": null,
    "customer_name": "Handi Artiawan",
    "media_list": [
        "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_64e714d81ed4592f.png",
        "http://andalanku-cms.depanbelih.id/user_files/media_image/andalanku_2e881a1b7cb05f1d.png"
    ]
}
 */
@SuppressLint("ParcelCreator")
@Parcelize
class Complain (
    @SerializedName("id_complain") val idComplain: String? = "",
    @SerializedName("ticket_number") val ticketNumber: String? = "",
    @SerializedName("category") val category: String? = "",
    @SerializedName("phone") val phone: String? = "",
    @SerializedName("user_id") val userId: String? = "",
    @SerializedName("message") val message: String? = "",
    @SerializedName("status") val status: String? = ConfigVar.ONPROCESS,
    @SerializedName("submitted_date") val submittedDate: String? = "",
    @SerializedName("on_process_date") val onProcessDate: String? = "",
    @SerializedName("processed_by") val processedBy: String? = "",
    @SerializedName("solved_date") val solvedDate: String? = "",
    @SerializedName("solved_by") val solvedBy: String? = "",
    @SerializedName("solution") val solution: String? = "",
    @SerializedName("customer_name") val costumerName: String? = "",
    @SerializedName("media_list") val mediaList: MutableList<String>? = arrayListOf()
): Parcelable {
    fun getComplainService(): Complainservices {
        return Complainservices(
            id = ticketNumber,
            category = category,
            date = submittedDate,
            state = status
        )
    }

    fun getSubmittedDateUI(listMonth: Array<String>): String {
        if (submittedDate.isNullOrBlank()) return ""
        //var result = ""
        val date = submittedDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }

    fun getOnProcessDateUI(listMonth: Array<String>): String {
        if (onProcessDate.isNullOrBlank()) return ""
        //var result = ""
        val date = onProcessDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}