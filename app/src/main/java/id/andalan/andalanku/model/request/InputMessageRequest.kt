package id.andalan.andalanku.model.request

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class InputMessageRequest(
    @SerializedName("destination_id") val destination_id: String? = "",
    @SerializedName("title") val title: String? = "",
    @SerializedName("sender_id") val sender_id: String? = "",
    @SerializedName("content") val content: String? = ""

) {
    fun isValid(): Boolean {
        var result = true
        if (destination_id?.isBlank() == true) result = false
        if (title?.isBlank() == true) result = false
        if (content?.isBlank() == true) result = false
        return result
    }
}