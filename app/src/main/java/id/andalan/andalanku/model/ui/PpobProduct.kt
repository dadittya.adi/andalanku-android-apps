package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

/*{
        "code": "PLIDPT05",
        "name": "Pulsa Rp 5.000",
        "logo": "",
        "amount": "5000",
        "biller": "Indosat",
        "category": "Pulsa",
        "active": "1",
        "type": "prepaid"
 */
@SuppressLint("ParcelCreator")
@Parcelize
class PpobProduct (
    @SerializedName("code") val code: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("logo") val logo: String? = "",
    @SerializedName("amount") val amount: String? = "",
    @SerializedName("biller") val biller: String? = "",
    @SerializedName("category") val category: String? = "",
    @SerializedName("active") val active: String? = "",
    @SerializedName("validity") val validity: String? = "",
    @SerializedName("note") val note: String? = ""
): Parcelable {
    fun getPpobProduct(): PpobProduct {
        return PpobProduct(
            code = code,
            name = name,
            logo = logo,
            amount = amount,
            biller = biller,
            category = category,
            active = active,
            validity = validity,
            note = note
        )
    }
}