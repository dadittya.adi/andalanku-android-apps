package id.andalan.andalanku.model.ui

class MenuProfile(
    val type: String? = null,
    val name: String? = null,
    val point: String? = null,
    val rightText: String? = null,
    val leftDrawable: Int? = null
)