package id.andalan.andalanku.model.ui

/*
"{\"message\":\"Andalan Finance lagi bagi-bagi voucher untuk karyawan PT. Andalan Finance Indonesia nih!  ud83eudd73\n\nYuk ikutan, caranya gampang:\n1. Follow account Instagram @andalanku_id u2611\n2. Komen kelebihan Andalan Finance di salah satu post ud83dudcac\n3. Like post tersebut ud83dudc4dud83cudffb\n4. Mention temanmu sebanyak-banyaknya. ud83dudc6b\n.\nTotal hadiah senilai Rp 500.000, berupa saldo GoPay\/OVO\/Pulsa HP. Untuk 10 pemenang dengan komen terbaik @ Rp50.000\n\nPeriode hingga 8 September 2019.\",\"image\":\"http:\/\/cms.andalanku.id\/user_files\/promo_image\/andalanku_4a17b40aa3bb83fc.jpg\"}"}
 */
class DetailMessagePromo(
    val message: String? = "",
    val image: String? = ""
)