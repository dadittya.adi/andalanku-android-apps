package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Complain
import id.andalan.andalanku.model.ui.InstallmentPaymentHistory

/*
{
    "status": "success",
    "message": "Employee found",
    "employeenik": "3171082001710007",
    "employeename": "Fikri Zulfikar"
}
 */
class NipCheckResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("employeenik") val employeeNik: String? = "",
    @SerializedName("employeename") val employeeName: String? = ""
)