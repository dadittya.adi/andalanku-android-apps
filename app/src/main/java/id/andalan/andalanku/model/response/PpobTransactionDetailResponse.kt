package id.andalan.andalanku.model.response

import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.PpobBillBillDetails
import id.andalan.andalanku.model.ui.PpobBillCustomerDetails
import id.andalan.andalanku.model.ui.PpobBillProductDetails
import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize


/*
{
    "status": "success",
    "complain_list": [
    ]
}
 */
@Parcelize
class PpobTransactionDetailResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("transaction_id") val transactionId: String? = "",
    @SerializedName("ref_number") val refNumber: String? = "",
    @SerializedName("date") val transactionDate: String? = "",
    @SerializedName("account_number") val accountNumber: String? = "",
    @SerializedName("amount") val amount: String? = "",
    @SerializedName("total_admin") val totalAdmin: String? = "",
    @SerializedName("processing_fee") val processingFee: String? = "",
    @SerializedName("product_code") val productCode: String? = "",
    @SerializedName("product_name") val productName: String? = "",
    @SerializedName("category") val category: String? = "",
    @SerializedName("token") val token: String? = "",
    @SerializedName("type") val type: String? = "",
    @SerializedName("message") val message: String? = "",
    @SerializedName("transaction_status") val transactionStatus: Boolean? = true,
    @SerializedName("customer_details") val listBillCustomerDetails: MutableList<PpobBillCustomerDetails>? = arrayListOf(),
    //@SerializedName("customer_details") val listBillCustomerDetails: String = "",
    @SerializedName("bill_details") val listBillBillDetails: MutableList<PpobBillBillDetails>? = arrayListOf(),
    //@SerializedName("bill_details") val listBillBillDetails: String,
    @SerializedName("product_details") val listBillProductDetails: MutableList<PpobBillProductDetails>? = arrayListOf()
    //@SerializedName("product_details") val listBillProductDetails: String
): Parcelable {
    fun getTransactionDate(listMonth: Array<String>): String {
        if (transactionDate.isNullOrBlank()) return ""
        //var result = ""
        val date = transactionDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.dateToString(listMonth)

    }
}