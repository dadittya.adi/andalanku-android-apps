package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// {"AssetCode":"TOY.FOR.A013","AssetName":"TOYOTA ALL NEW FORTUNER 2.4 VRZ A\/T","ManufacturingYear":2017,"Area":901,"Price":625000000}
@SuppressLint("ParcelCreator")
@Parcelize
class UsedCar (
    @SerializedName("AssetCode") val AssetCode: String? = "",
    @SerializedName("AssetName") val AssetName: String? = "",
    @SerializedName("ManufacturingYear") val ManufacturingYear: String? = "",
    @SerializedName("Area") val Area: String? = "",
    @SerializedName("Price") val Price: String? = ""
): Parcelable