package id.andalan.andalanku.model.ui

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class LegalDocument (
    @SerializedName("type") val type: String? = "",
    @SerializedName("content") val content: String? = ""
)
