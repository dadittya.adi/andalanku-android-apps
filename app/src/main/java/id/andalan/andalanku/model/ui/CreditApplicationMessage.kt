package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

// {\"message\":\"Dear Konsumen yang terhormat, \\r\\n\\r\\nTerima kasih telah menggunakan aplikasi Andalanku. Kami telah menerima pengajuan kredit dari Anda dan akan segera memproses permohonan Anda.\",
// \"ticket_number\":\"PKASS5QOTL\",
// \"status\":\"Input Aplikasi\",
// \"tangal_pengajuan\":\"2019-07-06 05:53:32\"}"
@SuppressLint("ParcelCreator")
@Parcelize
class CreditApplicationMessage (
    @SerializedName("ticket_number") val ticketNumber: String? = "",
    @SerializedName("status") val status: String? = "",
    @SerializedName("tanggal_pengajuan") val tanggalPengajuan: String? = "",
    @SerializedName("message") val message: String? = ""
): Parcelable {
    fun getSubmissionDateUI(listMonth: Array<String>): String {
        if (tanggalPengajuan.isNullOrBlank()) return ""
        //var result = ""
        val date = tanggalPengajuan.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}