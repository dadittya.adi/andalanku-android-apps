package id.andalan.andalanku.model.request

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class InputComplainRequest(
    @SerializedName("category") val category: String? = "",
    @SerializedName("phone") val phone: String? = "",
    @SerializedName("message") val message: String? = ""
) {
    fun isValid(): Boolean {
        var result = true
        if (category?.isBlank() == true) result = false
        if (phone?.isBlank() == true) result = false
        if (message?.isBlank() == true) result = false
        return result
    }
}