package id.andalan.andalanku.model.response

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.dateToString
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize


// {"status":"success","date":"2019-07-06 13:30:05","title":"E-Complain","type":"e complain","content":"{\"ticket_number\":\"C146464655\",\"category\":\"Lain-lain\",\"input_date\":\"2019-07-06 13:30:05\",\"status\":\"SUBMITTED\",\"complain_message\":\"pesan\",\"processed_status\":\"PENDING\",\"processed_date\":\"\",\"solved_status\":\"PENDING\",\"solved_date\":\"\",\"complain_media\":[\"http:\/\/cms.andalanku.id\/user_files\/media_image\/andalanku_6fe15bfcf4602815.png\"]}"}
@SuppressLint("ParcelCreator")
@Parcelize
class DetailPromoResponse(
    @SerializedName("status") val status: String? = "",
    @SerializedName("id_promo") val id_promo: String? = "",
    @SerializedName("promo_date") val promoDate: String? = "",
    @SerializedName("promo_start_date") val startDate: String? = "",
    @SerializedName("promo_expired_date") val endDate: String? = "",
    @SerializedName("title") val title: String? = "",
    @SerializedName("promo_type_name") val type: String? = "",
    @SerializedName("content") val content: String? = "",
    @SerializedName("content_html") val contentHtml: String? = "",
    @SerializedName("image") val image: String? = ""

): Parcelable {
    fun getDatePromoUI(listMonth: Array<String>): String {
        if (promoDate.isNullOrBlank()) return ""
        //var result = ""
        val date = promoDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)

    }
    fun getDateStartPromoUI(listMonth: Array<String>): String {
        if (startDate.isNullOrBlank()) return ""
        //var result = ""
        val date = startDate.stringToDate("yyyy-MM-dd")
        //result = date.datetimeToString(listMonth)

        return date.dateToString(listMonth)

    }

    fun getDateEndPromoUI(listMonth: Array<String>): String {
        if (endDate.isNullOrBlank()) return ""
        //var result = ""
        val date = endDate.stringToDate("yyyy-MM-dd")
        //result = date.datetimeToString(listMonth)

        return date.dateToString(listMonth)

    }

}