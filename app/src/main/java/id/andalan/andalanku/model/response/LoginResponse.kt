package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class LoginResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("id_customer") val idCustomer: Int? = 0,
    @SerializedName("customer_name") val customerName: String? = "",
    @SerializedName("id_number") val idNumber: String? = "",
    @SerializedName("email") val email: String? = "",
    var isVerify: Boolean? = false
) {
    override fun toString(): String {
        return "$idCustomer - $customerName - $isVerify"
    }
}