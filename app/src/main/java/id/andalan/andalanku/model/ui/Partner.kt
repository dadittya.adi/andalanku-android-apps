package id.andalan.andalanku.model.ui

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.model.ui.PartnerHome

/*
{
    "id_partner": "5",
    "name": "Cars World",
    "url": "https://carsworld.id/",
    "image": "http://andalanku-cms.depanbelih.id/user_files/partner_image/andalanku_4adbca12124c9f24.png"
}
 */
class Partner (
    @SerializedName("id_partner") val idPartner: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("url") val url: String? = "",
    @SerializedName("image") val image: String? = ""
) {
    fun getPartnerHome(): PartnerHome {
        return PartnerHome(id = idPartner, link = url, url = image, name = name)
    }
}
