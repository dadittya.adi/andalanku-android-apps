package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

class DetailAndalanFinancialResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("andalan_detail_unit") val andalanDetailUnit: List<DetailUnitResponse>? = arrayListOf(),
    @SerializedName("andalan_detail_financial") val andalanDetailFinancial: List<DetailFinanceResponse>? = arrayListOf(),
    @SerializedName("andalan_detail_payment_history") val andalanDetailPaymentHistory: List<DetailPaymentHistoryResponse>? = arrayListOf(),
    @SerializedName("andalan_detail_insurance") val andalanDetailInsurance: List<DetailInsuranceResponse>? = arrayListOf(),
    @SerializedName("andalan_agreement_document") val andalanAgreementDocumument: List<AgreementDocumentResponse>? = arrayListOf()
)