package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// {"BranchID":"110","Name":"PEMUDA","Address":"JL. PEMUDA RAYA,  RUKO GRAHA MAS PEMUDA BLOK AD\/8 JAKARTA TIMUR RT\/RW:0\/0 RAWAMANGUN PULO GADUNG","Phone":"(021) 29574854","fax":"(021) 29574854","Zipcode":"13220","City":"JAKARTA TIMUR","Provinsi":"DKI JAKARTA"}
@SuppressLint("ParcelCreator")
@Parcelize
class Branch(
    @SerializedName("id") val branchID: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("address") val address: String? = "",
    @SerializedName("phone") val phone: String? = "",
    @SerializedName("fax") val fax: String? = "",
    @SerializedName("zipcode") val zipcode: String? = "",
    @SerializedName("city") val city: String? = "",
    @SerializedName("provinsi") val provinsi: String? = ""
): Parcelable