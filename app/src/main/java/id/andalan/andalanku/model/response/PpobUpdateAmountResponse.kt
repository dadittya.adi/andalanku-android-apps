package id.andalan.andalanku.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.*
import kotlinx.android.parcel.Parcelize

@Parcelize
class PpobUpdateAmountResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val mesage: String? = ""
): Parcelable