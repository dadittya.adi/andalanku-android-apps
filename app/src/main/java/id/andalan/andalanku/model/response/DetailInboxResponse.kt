package id.andalan.andalanku.model.response

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.model.ui.InboxMedia
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize


// {"status":"success","date":"2019-07-06 13:30:05","title":"E-Complain","type":"e complain","content":"{\"ticket_number\":\"C146464655\",\"category\":\"Lain-lain\",\"input_date\":\"2019-07-06 13:30:05\",\"status\":\"SUBMITTED\",\"complain_message\":\"pesan\",\"processed_status\":\"PENDING\",\"processed_date\":\"\",\"solved_status\":\"PENDING\",\"solved_date\":\"\",\"complain_media\":[\"http:\/\/cms.andalanku.id\/user_files\/media_image\/andalanku_6fe15bfcf4602815.png\"]}"}
@SuppressLint("ParcelCreator")
@Parcelize
class DetailInboxResponse(
    @SerializedName("status") val status: String? = "",
    @SerializedName("date") val date: String? = "",
    @SerializedName("title") val title: String? = "",
    @SerializedName("type") val type: String? = "",
    @SerializedName("content") val content: String? = "",
    @SerializedName("content_html") val contentHtml: String? = "",
    @SerializedName("first_media") val firstMedia: String? = ""

): Parcelable {
    fun getDateUI(listMonth: Array<String>): String {
        if (date.isNullOrBlank()) return ""
        //var result = ""
        val date = date.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)

    }
}