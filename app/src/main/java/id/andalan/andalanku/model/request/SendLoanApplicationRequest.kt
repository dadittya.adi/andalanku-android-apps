package id.andalan.andalanku.model.request

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/*
id_customer:3
product:NEW CAR
otr:115000000
dp_percent:24
dp_amount:27600000
plafond:5000000
tenor:60
insurance:TLO
region:3
collateral_type:BPKB
collateral:TOYOTA/AGYA/bensin
collateral_year:2018
collateral_owner:Khal Drogo
collateral_paper_number:AB 1234 BG
place_of_birth:Sukoharjo
date_of_birth:1999-01-01
id_number:12376871263873123
mother_name:khaleshi
home_phone_number:0856490213981
phone_number:0856490213981
occupation_id:1
monthly_income:5000000
additional_income:1000000
side_job:Kuli
province_id:33
city_id:3313
district_id:3313110
village_id:3313110001
address:jl bener
rt:06
rw:04
zip_code:45324
foto_ktp:
foto_kk:
foto_slip_gaji:
foto_rekening_koran:
foto_keuangan_lainnya:
 */
@SuppressLint("ParcelCreator")
@Parcelize
class SendLoanApplicationRequest (
    @SerializedName("id_customer") var idCustomer: String? = "",
    @SerializedName("product") var product: String? = "",
    @SerializedName("otr") var otr: String? = "",
    @SerializedName("dp_percent") var dpPercent: String? = "",
    @SerializedName("recieved_funds") var recievedFunds: String? = "",
    @SerializedName("installment") var installment: String? = "",
    @SerializedName("dp_amount") var dpAmount: String? = "",
    @SerializedName("plafond") var plafond: String? = "",
    @SerializedName("tenor") var tenor: String? = "",
    @SerializedName("insurance") var insurance: String? = "",
    @SerializedName("region") var region: String? = "",
    @SerializedName("asset_code") var assetCode: String? = "",
    @SerializedName("collateral_type") var collateralType: String? = "",
    @SerializedName("collateral") var collateral: String? = "",
    @SerializedName("collateral_year") var collateralYear: String? = "",
    @SerializedName("collateral_owner") var collateralOwner: String? = "",
    @SerializedName("collateral_paper_number") var collateralPaperNumber: String? = "",
    @SerializedName("place_of_birth") var placeOfBirth: String? = "",
    @SerializedName("date_of_birth") var dateOfBirth: String? = "",
    @SerializedName("id_number") var idNumber: String? = "",
    @SerializedName("mother_name") var motherName: String? = "",
    @SerializedName("area_code") var areaCode: String? = "",
    @SerializedName("home_phone_number") var homePhoneNumber: String? = "",
    @SerializedName("phone_number") var phoneNumber: String? = "",
    @SerializedName("occupation_id") var occupationId: String? = "",
    @SerializedName("monthly_income") var monthlyIncome: String? = "",
    @SerializedName("additional_income") var additionalIncome: String? = "",
    @SerializedName("side_job") var sideJob: String? = "",
    @SerializedName("province_id") var provinceId: String? = "",
    @SerializedName("city_id") var cityId: String? = "",
    @SerializedName("district_id") var districtId: String? = "",
    @SerializedName("village_id") var villageId: String? = "",
    @SerializedName("address") var address: String? = "",
    @SerializedName("rt") var rt: String? = "",
    @SerializedName("rw") var rw: String? = "",
    @SerializedName("zip_code") var zipCode: String? = "",
    // domicile
    @SerializedName("province_id_domicile") var provinceIdDomicile: String? = "",
    @SerializedName("city_id_domicile") var cityIdDomicile: String? = "",
    @SerializedName("district_id_domicile") var districtIdDomicile: String? = "",
    @SerializedName("village_id_domicile") var villageIdDomicile: String? = "",
    @SerializedName("address_domicile") var addressDomicile: String? = "",
    @SerializedName("rt_domicile") var rtDomicile: String? = "",
    @SerializedName("rw_domicile") var rwDomicile: String? = "",
    @SerializedName("zip_code_domicile") var zipCodeDomicile: String? = "",
    // office
    @SerializedName("province_id_office") var provinceIdOffice: String? = "",
    @SerializedName("city_id_office") var cityIdOffice: String? = "",
    @SerializedName("district_id_office") var districtIdOffice: String? = "",
    @SerializedName("village_id_office") var villageIdOffice: String? = "",
    @SerializedName("address_office") var addressOffice: String? = "",
    @SerializedName("rt_office") var rtOffice: String? = "",
    @SerializedName("rw_office") var rwOffice: String? = "",
    @SerializedName("zip_code_office") var zipCodeOffice: String? = "",

    @SerializedName("foto_ktp") var fotoKtp: String? = "",
    @SerializedName("type_foto_ktp") var typeFotoKtp: String? = "",
    @SerializedName("foto_kk") var fotoKk: String? = "",
    @SerializedName("type_foto_kk") var typeFotoKk: String? = "",
    @SerializedName("foto_slip_gaji") var fotoSlipGaji: String? = "",
    @SerializedName("type_foto_slip_gaji") var typeFotoSlipGaji: String? = "",
    @SerializedName("foto_rekening_koran") var fotoRekeningKoran: String? = "",
    @SerializedName("type_foto_rekening_koran") var typeFotoRekeningKoran: String? = "",
    @SerializedName("foto_keuangan_lainnya") var fotoKeuanganLainya: String? = "",
    @SerializedName("type_foto_keuangan_lainnya") var typeFotoKeuanganLainya: String? = "",
    @SerializedName("branch_id") var branchId: String? = "",
    @SerializedName("survey_date") var surveyDate: String? = "",
    @SerializedName("survey_date") var surveyClock: String? = "",
    @SerializedName("loan_application_referal") var loanApplicationReferal: String? = "",
    @SerializedName("email") var email: String? = "",
    @SerializedName("customer_name") var customerName: String? = "",
    @SerializedName("gender") var gender: String? = "",

    //bank
    @SerializedName("bank_name") var bankName: String? = "",
    @SerializedName("bank_account_number") var bankAccountNumber: String? = "",
    @SerializedName("bank_account_holder") var bankAccountHolder: String? = "",
    @SerializedName("bank_account_image") var bankAccountImage: String? = "",
    @SerializedName("type_bank_account_image") var typeBankAccountImage: String? = ""

): Parcelable {
    fun isValidFormCompleteData(): Boolean {
        if (placeOfBirth.isNullOrBlank()) return false
        if (dateOfBirth.isNullOrBlank()) return false
        if (idNumber.isNullOrBlank()) return false
        if (motherName.isNullOrBlank()) return false
        if (phoneNumber.isNullOrBlank()) return false
        if (occupationId.isNullOrBlank()) return false
        if (monthlyIncome.isNullOrBlank()) return false
        if (provinceId.isNullOrBlank()) return false
        if (cityId.isNullOrBlank()) return false
        if (districtId.isNullOrBlank()) return false
        if (villageId.isNullOrBlank()) return false
        if (address.isNullOrBlank()) return false
        if (rt.isNullOrBlank()) return false
        if (rw.isNullOrBlank()) return false
        if (zipCode.isNullOrBlank()) return false

        return true
    }

    fun isValidBank(): Boolean {
        if (bankName.isNullOrBlank()) return false
        if (bankAccountNumber.isNullOrBlank()) return false
        if (bankAccountHolder.isNullOrBlank()) return false
        if (bankAccountImage.isNullOrBlank()) return false

        return true
    }
}