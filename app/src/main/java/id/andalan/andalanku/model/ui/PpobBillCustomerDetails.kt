package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// {"id":"2","customer_id":"6","date":"2019-07-06 05:53:32","type":"credit application","title":"Pengajuan Kredit","status":"1"}
@SuppressLint("ParcelCreator")
@Parcelize
class PpobBillCustomerDetails (
    @SerializedName("key") val key: String? = "",
    @SerializedName("value") val value: String? = ""
): Parcelable {
    fun getCustomerDetails(): PpobBillCustomerDetails {
        return PpobBillCustomerDetails(
            key = key,
            value = value
        )
    }
}
