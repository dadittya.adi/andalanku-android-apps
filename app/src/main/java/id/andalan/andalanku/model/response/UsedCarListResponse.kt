package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.UsedCar

// {"status":"success","car_price_list":[
// {"AssetCode":"TOY.FOR.A012","AssetName":"TOYOTA ALL NEW FORTUNER 2.4 G 4x4 A\/T","ManufacturingYear":2017,"Area":901,"Price":554000000},
// {"AssetCode":"TOY.FOR.A013","AssetName":"TOYOTA ALL NEW FORTUNER 2.4 VRZ A\/T","ManufacturingYear":2017,"Area":901,"Price":625000000}
// ]}
class UsedCarListResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("car_price_list") val carPriceList: MutableList<UsedCar>? = arrayListOf()
)