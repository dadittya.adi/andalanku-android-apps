package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.ClaimInsurance

// {"status":"success","insurance_claim_list":[
// {"id":"3","ticket_number":"ISC5222581","customer_id":"6","claim_date":"2019-06-18 15:30:44","agreement_number":"1730105861","claim_type":"TLO","event_case":"TLO","event_date":"2019-06-18","location":"kota","notes":"","status":"ON PROCESS","customer_name":"Donovan"},
// {"id":"2","ticket_number":"ISC2085190","customer_id":"6","claim_date":"2019-06-18 13:36:48","agreement_number":"1730105861","claim_type":"TLO","event_case":"ST","event_date":"2019-06-01","location":"solo","notes":"pesan","status":"ON PROCESS","customer_name":"Donovan"}
// ]}
class ClaimInsuranceListResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = "",
    @SerializedName("insurance_claim_list") val insuranceClaimList: MutableList<ClaimInsurance>? = arrayListOf()
)
