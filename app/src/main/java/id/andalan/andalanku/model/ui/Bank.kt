package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Bank (
    @SerializedName("bank_id") val id: String? = "",
    @SerializedName("short_name") val bankName: String? = ""
    //@SerializedName("short_name") val bankName: String? = ""
): Parcelable