package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//    {
//        "id_faq": "2",
//        "question": "Bagaimana cara mengajukan kredit mobil?",
//        "is_active": "TRUE",
//        "faq_answer_list": [
//        ]
//    }
@SuppressLint("ParcelCreator")
@Parcelize
class TutorialParent(
    @SerializedName("id_tutorial") val idTutorial: String? = "",
    @SerializedName("question") val question: String? = "",
    @SerializedName("is_active") val isActive: Boolean? = false,
    @SerializedName("tutorial_answer_list") val tutorialAnswerList: MutableList<Tutorial>? = arrayListOf()
):Parcelable