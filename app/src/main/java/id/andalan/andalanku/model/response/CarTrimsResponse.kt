package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.CarTrim
import id.andalan.andalanku.model.ui.CarType

/*
{
    "status": "success",
    "car_trim_list": [
        {
            "id_car_trim": "16",
            "id_car_type": "13",
            "name": "E AT 1200cc",
            "price": "115000000",
            "is_active": "1"
        },
        {
            "id_car_trim": "17",
            "id_car_type": "13",
            "name": "E MT 1200cc",
            "price": "109000000",
            "is_active": "1"
        },
        {
            "id_car_trim": "10",
            "id_car_type": "13",
            "name": "G AT 1200cc",
            "price": "125000000",
            "is_active": "1"
        },
        {
            "id_car_trim": "8",
            "id_car_type": "13",
            "name": "G MT 1200 cc",
            "price": "120000000",
            "is_active": "1"
        },
        {
            "id_car_trim": "11",
            "id_car_type": "13",
            "name": "TRD AT 1200 cc",
            "price": "137000000",
            "is_active": "1"
        },
        {
            "id_car_trim": "9",
            "id_car_type": "13",
            "name": "TRD MT 1200 cc",
            "price": "135000000",
            "is_active": "1"
        }
    ]
}
 */
class CarTrimsResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("car_trim_list") val carTrimList: MutableList<CarTrim>? = arrayListOf()
)