package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

// {
// "credit_type":"ARREAR",
// "dp":"120.000.000",
// "tenor":"48",
// "pokok_hutang":"80.000.000",
// "angsuran":"2.527.000",
// "tipe_asuransi":"ARK",
// "rate_asuransi":"0.48%",
// "premi_asuransi":"3.760.000",
// "provisi":"1.600.000",
// "biaya_admin":"4.550.000",
// "bunga_flat":"41.280.000",
// "total_hutang":"121.296.000"
// }
@SuppressLint("ParcelCreator")
@Parcelize
class CreditSimulationO(
    @SerializedName("credit_type") val creditType: String? = "",
    val dp: String? = "",
    val tenor: String? = "",
    @SerializedName("pokok_hutang") val pokokHutang: String? = "",
    val angsuran: String? = "",
    @SerializedName("tipe_asuransi") val tipeAsuransi: String? = "",
    @SerializedName("rate_asuransi") val rateAsuransi: String? = "",
    @SerializedName("premi_asuransi") val premiAsuransi: String? = "",
    val provisi: String? = "",
    @SerializedName("biaya_admin") val biayaAdmin: String? = "",
    @SerializedName("bunga_flat") val bungaFlat: String? = "",
    @SerializedName("total_hutang") val totalHutang: String? = "",
    @SerializedName("dana_diterima") val danaDiterima: String? = ""
): Parcelable