package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Slider

/*
 * {"status":"success",
 * "promo_list":[]}
 *
 */
class SliderResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("slider_list") val sliderList: MutableList<Slider>
)