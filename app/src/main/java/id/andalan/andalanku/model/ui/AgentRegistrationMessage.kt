package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

// {\"ticket_number\":\"ISC8686475\",
// \"input_date\":\"2019-07-06 14:30:32\",
// \"status\":\"SUBMITTED\",
// \"message\":\"Dear Konsumen, \\r\\n\\r\\nKonsumen yang terhormat, terima kasih atas kepercayaan Anda kepada Andalan Finance. Keluhan Anda akan segera kami tindak lanjuti dalam kurun waktu 2 hari kerja.\"}"
@SuppressLint("ParcelCreator")
@Parcelize
class AgentRegistrationMessage (
    @SerializedName("message") val message: String? = ""
): Parcelable