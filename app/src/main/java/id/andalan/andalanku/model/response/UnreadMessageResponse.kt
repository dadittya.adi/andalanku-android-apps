package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

/*
{
    "status":"success",
    "unread_count":4
}
 */
class UnreadMessageResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("unread_count") val unreadCount: Int? = 0
)