package id.andalan.andalanku.model.ui

import id.andalan.andalanku.R

class Menu (
    val type: String? = null,
    val name: String? = null,
    val image: Int = R.drawable.ic_default_image
)