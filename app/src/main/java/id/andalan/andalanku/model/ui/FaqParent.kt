package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

//    {
//        "id_faq": "2",
//        "question": "Bagaimana cara mengajukan kredit mobil?",
//        "is_active": "TRUE",
//        "faq_answer_list": [
//        ]
//    }
@SuppressLint("ParcelCreator")
@Parcelize
class FaqParent(
    @SerializedName("id_faq") val idFaq: String? = "",
    @SerializedName("question") val question: String? = "",
    @SerializedName("is_active") val isActive: Boolean? = false,
    @SerializedName("faq_answer_list") val faqAnswerList: MutableList<Faq>? = arrayListOf()
):Parcelable