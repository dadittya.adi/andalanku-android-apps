package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/*
{
    "id_car_trim": "11",
    "id_car_type": "13",
    "name": "TRD AT 1200 cc",
    "price": "137000000",
    "is_active": "1"
},
 */
@SuppressLint("ParcelCreator")
@Parcelize
class CarTrim (
    @SerializedName("id_car_trim") val idCarTrim: String? = "",
    @SerializedName("id_car_type") val idCarType: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("price") val price: String? = "",
    @SerializedName("asset_code") val assetCode: String? = "",
    @SerializedName("is_active") val isActive: String? = ""
): Parcelable