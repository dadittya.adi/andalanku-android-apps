package id.andalan.andalanku.model.ui

class AddressName(
    val id: String? = "",
    val name: String? = "",
    val type: String? = ""
){
    override fun toString(): String {
        return name?:""
    }
}