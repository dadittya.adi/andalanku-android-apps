package id.andalan.andalanku.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.*
import kotlinx.android.parcel.Parcelize

@Parcelize
class PpobGroupResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("last_purchase") val lastPurchase: MutableList<PpobGroup>? = arrayListOf(),
    @SerializedName("tagihan") val tagihan: MutableList<PpobGroup>? = arrayListOf(),
    @SerializedName("top_up") val topUp: MutableList<PpobGroup>? = arrayListOf(),
    @SerializedName("hiburan") val hiburan: MutableList<PpobGroup>? = arrayListOf()
): Parcelable