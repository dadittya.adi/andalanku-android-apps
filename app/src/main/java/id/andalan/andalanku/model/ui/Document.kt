package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Document (
    var id: String? = "",
    var hintText: String? = "",
    var urlImage: String? = "",
    var typeUrlImage: String? = "",
    var infoText: String? = ""
): Parcelable {
    override fun equals(other: Any?): Boolean {
        return id == (other as Document).id
    }
}