package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"message":"Loan application successful","ticket_number":"PKAOUO2N5I"}
class LoanApplicationResponse (
    @SerializedName("status") val status: String? = ConfigVar.SUCCESS,
    @SerializedName("message") val message: String? = "",
    @SerializedName("ticket_number") val ticketNumber: String? = ""
)