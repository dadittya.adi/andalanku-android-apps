package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.FaqParent

//{
//    "status": "success",
//    "faq_list": [
//    ]
//}
class FaqResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("faq_list") val faqParentList: MutableList<FaqParent>? = arrayListOf()
)