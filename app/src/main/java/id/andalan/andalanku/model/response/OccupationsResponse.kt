package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.Occupation

/*
{
    "status": "success",
    "occupation_list": [
        {
            "id": "1",
            "name": "Karyawan Swasta",
            "is_active": "1"
        },
        {
            "id": "2",
            "name": "Pegawai Negeri Sipil",
            "is_active": "1"
        },
        {
            "id": "5",
            "name": "Profesional",
            "is_active": "1"
        },
        {
            "id": "3",
            "name": "TNI / Polri",
            "is_active": "1"
        },
        {
            "id": "4",
            "name": "Wirausaha",
            "is_active": "1"
        }
    ]
}
 */
class OccupationsResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("occupation_list") val occupationList: MutableList<Occupation>? = arrayListOf()
)