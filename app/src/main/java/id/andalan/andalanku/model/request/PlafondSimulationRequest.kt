package id.andalan.andalanku.model.request

import com.google.gson.annotations.SerializedName

class PlafondSimulationRequest(
    @SerializedName("occupation_id") val occupationId: String = "",
    @SerializedName("monthly_income") val monthlyIncome: String = "",
    @SerializedName("side_job") val sideJob: String = "",
    @SerializedName("additional_income") val additionalIsncome: String = "0"
) {
    fun isValid(): Boolean {
        if (occupationId.isBlank()) return false
        if (monthlyIncome.isBlank()) return false
        return true
    }
}