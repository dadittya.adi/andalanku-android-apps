package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.CarBrand

//{
// "status":"success",
// "car_brand_list":[
//  {
//      "id_car_brand":"9",
//      "name":"Toyota",
//      "image":"http:\/\/andalanku-cms.depanbelih.id\/user_files\/car_brand_image\/andalanku_35b8a22f77019577.png",
//      "is_active":"1"
//  }
// ]
// }
class CarBrandsResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("car_brand_list") val CarBrandList: MutableList<CarBrand>? = arrayListOf()
)