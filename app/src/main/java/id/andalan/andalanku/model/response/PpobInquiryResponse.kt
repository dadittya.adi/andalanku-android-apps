package id.andalan.andalanku.model.response

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.*
import kotlinx.android.parcel.Parcelize

/*
{
    "status": "success",
    "complain_list": [
    ]
}
 */
@Parcelize
class PpobInquiryResponse (
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("transaction_date") val transactionDate: String? = "",
    @SerializedName("order_id") val orderId: String? = "",
    @SerializedName("account_number") val accountNumber: String? = "",
    @SerializedName("zone_id") val zoneId: String? = "",
    @SerializedName("month") val month: String? = "",
    @SerializedName("inquiry_id") val inquiryId: String? = "",
    @SerializedName("product_code") val productCode: String? = "",
    @SerializedName("customer_name") val customerName: String? = "",
    @SerializedName("product_name") val productName: String? = "",
    @SerializedName("category") val category: String? = "",
    @SerializedName("amount") val amount: String? = "",
    @SerializedName("total_admin") val totalAdmin: String? = "",
    @SerializedName("processing_fee") val processingFee: String? = "",
    @SerializedName("validity") val validity: String? = "",
    @SerializedName("payment_url") val paymentUrl: String? = "",
    @SerializedName("message") val message: String? = "",
    @SerializedName("customer_details") val listBillCustomerDetails: MutableList<PpobBillCustomerDetails>? = arrayListOf(),
    @SerializedName("bill_details") val listBillBillDetails: MutableList<PpobBillBillDetails>? = arrayListOf(),
    @SerializedName("product_details") val listBillProductDetails: MutableList<PpobBillProductDetails>? = arrayListOf()
): Parcelable {
    fun getBill(): String? {
        val bill = listBillBillDetails?.filter { it.key == "Jumlah Tagihan" }
        val tagihan = bill?.get(0)?.value
        return tagihan
    }

    fun getAdminFee(): String? {
        val bill = listBillBillDetails?.filter { it.key == "Biaya Administrasi" }
        val admin = bill?.get(0)?.value
        return admin
    }

    fun getName(): String? {
        val customer = listBillCustomerDetails?.filter { it.key == "Nama Pelanggan" }
        val name = customer?.get(0)?.value
        return name
    }

    fun getDate(): String? {
        val customer = listBillCustomerDetails?.filter { it.key == "Waktu & Tanggal Pembayaran" }
        val date = customer?.get(0)?.value
        return date
    }

    fun getAccNumber(): String? {
        val customer = listBillCustomerDetails?.filter { it.key == "Nomor Pelanggan" }
        val accNumber = customer?.get(0)?.value
        return accNumber
    }

    fun getCompany(): String? {
        val product = listBillProductDetails?.filter { it.key == "Perusahaan" }
        val company = product?.get(0)?.value
        return company
    }
}