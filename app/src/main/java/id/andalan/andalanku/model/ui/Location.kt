package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Location (
    @SerializedName("id") val id: String? = "",
    @SerializedName("province_id") val provinceId: String? = "",
    @SerializedName("regency_id") val regencyId: String? = "",
    @SerializedName("district_id") val districtId: String? = "",
    @SerializedName("name") val name: String? = ""
): Parcelable