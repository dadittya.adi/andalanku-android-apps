package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import id.andalan.andalanku.utils.roundedNumber
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Tenor(
    val id: Int? = 0
): Parcelable {
    fun getMonth(month: String): String {
        return "$id $month"
    }

    fun getYear(year: String): String {
        val tempYear: Double = id?.toDouble()?.div(12) ?:0.0
        if (tempYear.toInt() < tempYear && tempYear.toInt() + 1 > tempYear) {
            return "${tempYear.roundedNumber()} $year"
        } else {
            return "${tempYear.roundedNumber().toInt()} $year"
        }
    }
}