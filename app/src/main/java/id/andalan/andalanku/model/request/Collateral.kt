package id.andalan.andalanku.model.request

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Collateral(
    @SerializedName("collateral_type") var collateralType: String? = "",
    @SerializedName("collateral") var collateral: String? = "",
    @SerializedName("collateral_year") var collateralYear: String? = "",
    @SerializedName("collateral_owner") var collateralOwner: String? = "",
    @SerializedName("collateral_paper_number") var collateralPaperNumber: String? = "",
    @SerializedName("collateral_price") var collateralPrice: String? = "",
    @SerializedName("asset_code") var assetCode: String? = ""
): Parcelable