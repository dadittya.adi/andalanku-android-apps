package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@SuppressLint("ParcelCreator")
@Parcelize
class Sertipikat(
    val type: String? = "",
    val sertipikatNumber: String? = "",
    val largeArea: String? = "",
    val ownersArea: String? = "",
    val builtYear: String? = ""
): Parcelable {
    fun isValid(): Boolean {
        var result = true
        if (sertipikatNumber.isNullOrBlank()) result = false
        if (largeArea.isNullOrBlank()) result = false
        if (ownersArea.isNullOrBlank()) result = false
        if (type.isNullOrBlank()) result = false
        if (builtYear.isNullOrBlank()) result = false
        return result
    }
}