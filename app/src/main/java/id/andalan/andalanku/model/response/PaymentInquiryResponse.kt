package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success",
// "message":"Payment inquiry successful",
// "transaction_code":"X96719NXAD15",
// "contract_no":"114175160458",
// "police_number":"B1759WVM",
// "customer":"Test",
// "installment_number":45,
// "tenor":36,
// "description":"Pembayaran Angusran",
// "amount":2868000,
// "admin_fee":110,
// "penalty":0,
// "total_amount":2868110,
// "due_date":"14-01-2020",
// "periode":"01-2020",
// "Info_text":"TOTAL TAGIHAN SUDAH TERMASUK DENDA,\r\nJIKA ADA. INFORMASI LEBIH LANJUT\r\nHUBUNGI PT.ANDALAN FINANCE INDONESIA\r\nCABANG ALAM SUTERA 29008535\r\n"}
class PaymentInquiryResponse(
    @SerializedName("status") val status: String? = ConfigVar.SUCCESS,
    @SerializedName("message") val message: String? = "",
    @SerializedName("transaction_code") val transactionCode: String? = "",
    @SerializedName("contract_no") val agreementNumber: String? = "",
    @SerializedName("police_number") val policeNumber: String? = "",
    @SerializedName("customer") val name: String? = "",
    @SerializedName("installment_number") val installmentSequence: String? = "",
    @SerializedName("tenor") val tenor: String? = "",
    @SerializedName("amount") val amount: String? = "",
    @SerializedName("admin_fee") val adminFee: String? = "",
    @SerializedName("penalty") val penalty: String? = "",
    @SerializedName("total_amount") val totalAmount: String? = "",
    @SerializedName("due_date") val dueDate: String? = "",
    @SerializedName("periode") val periode: String? = "",
    @SerializedName("Info_text") val infoText: String? = "",
    @SerializedName("asset_description") val assetDescription: String? = "",
    @SerializedName("payment_url") val paymentUrl: String? = ""
)