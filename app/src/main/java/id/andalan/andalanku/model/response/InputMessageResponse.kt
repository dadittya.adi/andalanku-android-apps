package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

// {"status":"success","message":"Add e-complain successful","ticket_number":"C221013744"}
class InputMessageResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("message") val message: String? = ""
)