package id.andalan.andalanku.model.ui

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.utils.datetimeToString
import id.andalan.andalanku.utils.stringToDate
import kotlinx.android.parcel.Parcelize

// {\"ticket_number\":\"PFS1524313\",\"input_date\":\"2019-07-12 07:36:39\",\"status\":\"SUBMITTED\",\"message\":\"Dear Konsumen, \\r\\n\\r\\nKonsumen yang terhormat, pengajuan Anda sudah kami terima. Kami akan segera menindaklanjuti permohonan Anda.\"}"
@SuppressLint("ParcelCreator")
@Parcelize
class DetailMessagePlafondSubmission (
    @SerializedName("ticket_number") val ticketNumber: String? = "",
    @SerializedName("input_date") val inputDate: String? = "",
    @SerializedName("status") val status: String? = "",
    @SerializedName("message") val message: String? = ""
): Parcelable {
    fun getInputDateUI(listMonth: Array<String>): String {
        if (inputDate.isNullOrBlank()) return ""
        //var result = ""
        val date = inputDate.stringToDate("yyyy-MM-dd HH:mm:ss")
        //result = date.datetimeToString(listMonth)

        return date.datetimeToString(listMonth)
    }
}