package id.andalan.andalanku.model.response

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar
import id.andalan.andalanku.model.ui.FaqParent
import id.andalan.andalanku.model.ui.TutorialParent

//{
//    "status": "success",
//    "faq_list": [
//    ]
//}
class TutorialResponse(
    @SerializedName("status") val status: String? = ConfigVar.FAILED,
    @SerializedName("tutorial_list") val tutorialParentList: MutableList<TutorialParent>? = arrayListOf()
)