package id.andalan.andalanku.model.ui

import com.google.gson.annotations.SerializedName
import id.andalan.andalanku.config.ConfigVar

/*
{
    "id": "1",
    "name": "Karyawan Swasta",
    "is_active": "1"
}
 */
class Occupation (
    @SerializedName("id") val id: String? = "",
    @SerializedName("name") val name: String? = "",
    @SerializedName("is_active") val isActive: String? = ""
)