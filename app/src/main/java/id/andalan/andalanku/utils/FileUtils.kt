package id.andalan.andalanku.utils

import android.content.Context
import android.os.Environment
import androidx.core.content.ContextCompat
import android.os.Environment.MEDIA_MOUNTED



class FileUtils {
    companion object {
        fun getRootDirPath(): String {
            return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath
        }
    }
}