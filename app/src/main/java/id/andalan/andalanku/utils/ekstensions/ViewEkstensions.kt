package id.andalan.andalanku.utils.ekstensions

import android.content.Context
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.MemoryCategory
import com.bumptech.glide.load.engine.DiskCacheStrategy
import id.andalan.andalanku.R
import id.andalan.andalanku.utils.GlideApp
import android.widget.LinearLayout



internal fun loadImage(context: Context,
                       url: String,
                       imageView: ImageView
) {
    fun setMemoryCategory(context: Context) {
        Glide.get(context).setMemoryCategory(MemoryCategory.NORMAL)
    }
    setMemoryCategory(context)
    GlideApp.with(context)
        .load(url)
        .dontAnimate()
        .placeholder(R.drawable.ic_default_image)
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
        .into(imageView)
}

fun ViewGroup.setHeight(height: Int) {
    val params = this.layoutParams
    params.height = height
    this.layoutParams = params
}