package id.andalan.andalanku.utils

import android.content.Context
import id.andalan.andalanku.R
import java.text.SimpleDateFormat
import java.util.*

fun String.stringToDate(format: String): Date = SimpleDateFormat(format, Locale.ENGLISH).parse(this)
fun Date.dateToString(format: String): String = SimpleDateFormat(format, Locale.ENGLISH).format(this)
fun Date.dateToString(months: Array<String>): String {
    val calender = Calendar.getInstance()
    calender.time = this
    val year = calender.get(Calendar.YEAR)
    val month = calender.get(Calendar.MONTH)
    val day = calender.get(Calendar.DAY_OF_MONTH)

    return "$day ${months[month]} $year"
}

fun Date.datetimeToString(months: Array<String>): String {
    val calender = Calendar.getInstance()
    calender.time = this
    val year = calender.get(Calendar.YEAR)
    val month = calender.get(Calendar.MONTH)
    val day = calender.get(Calendar.DAY_OF_MONTH)
    val hour = this.dateToString("HH:mm")

    return "$day ${months[month]} $year ${hour}"
}

fun Date.datetimeToString(yesterday: String, days: Array<String>, months: Array<String>): String {
    val calender = Calendar.getInstance()
    calender.time = this
    //val year = calender.get(Calendar.YEAR)
    val month = calender.get(Calendar.MONTH)
    val day = calender.get(Calendar.DAY_OF_MONTH)
    val calendarNow = Calendar.getInstance()

    val newTime = calendarNow.timeInMillis - calender.timeInMillis
    val oneDay = 24 * 60 * 60 * 1000
    val twoDay = 24 * 60 * 60 * 1000 * 2
    val threeDay = 24 * 60 * 60 * 1000 * 3

    return when {
        newTime < oneDay -> this.dateToString("HH:mm")
        newTime < twoDay -> yesterday
        newTime < threeDay -> {
            val dayOfWeek = calender.get(Calendar.DAY_OF_WEEK)
            days[dayOfWeek-1]
        }
        else -> "$day ${months[month]}"
    }
}



fun Context.getMonths(): Array<String> {
    return arrayOf(
        this.getString(R.string.january),
        this.getString(R.string.february),
        this.getString(R.string.march),
        this.getString(R.string.april),
        this.getString(R.string.may),
        this.getString(R.string.june),
        this.getString(R.string.july),
        this.getString(R.string.august),
        this.getString(R.string.september),
        this.getString(R.string.october),
        this.getString(R.string.november),
        this.getString(R.string.desember)
    )
}

fun Context.getMonthsShort(): Array<String> {
    return arrayOf(
        this.getString(R.string.jan),
        this.getString(R.string.feb),
        this.getString(R.string.mar),
        this.getString(R.string.apr),
        this.getString(R.string.mai),
        this.getString(R.string.jun),
        this.getString(R.string.jul),
        this.getString(R.string.aug),
        this.getString(R.string.sep),
        this.getString(R.string.oct),
        this.getString(R.string.nov),
        this.getString(R.string.des)
    )
}

fun Context.getDays(): Array<String> {
    return arrayOf(
        this.getString(R.string.sunday),
        this.getString(R.string.monday),
        this.getString(R.string.tuesday),
        this.getString(R.string.wednesday),
        this.getString(R.string.thursday),
        this.getString(R.string.friday),
        this.getString(R.string.saturday)
    )
}

