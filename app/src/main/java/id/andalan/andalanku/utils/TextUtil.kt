@file:Suppress("DEPRECATION")

package id.andalan.andalanku.utils

import android.os.Build
import android.text.Html
import android.text.Spanned
import java.text.DecimalFormat
import java.util.regex.Pattern

fun String.convertStringToHTML(): Spanned {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(this, Html.FROM_HTML_MODE_COMPACT)
    } else {
        //this.convertStringToHTML()
        //noinspection deprecation
        Html.fromHtml(this)
    }
}

fun Long.convertToRpFormat(): String {
    val formatter = DecimalFormat("#,###")
    val tempText = formatter.format(this)
    val result = tempText.replace(",", ".")

    return "Rp ${result},-"
}

fun Long.convertToMoneyFormat(): String {
    val formatter = DecimalFormat("#,###")
    val tempText = formatter.format(this)
    val result = tempText.replace(",", ".")

    return "${result}"
}

fun Double.roundedNumber(): Double {
    val tempNumber: Double = Math.round(this * 100).toDouble()
    return  tempNumber / 100
}

fun Long.getValue(percentage: Double): Long {

    return (this * percentage / 100).toLong()
}

fun Long.getPercentage(value: Long): String {
    val formatter = DecimalFormat("#.##")

    return formatter.format(this.toDouble() / value.toDouble() * 100)
}

fun String.getNumberOnly(): Long {
    var result = "0"
    val m = Pattern.compile("[0-9]+").matcher(this)
    while (m.find()) {
        result += m.group()
    }

    return result.toLong()
}