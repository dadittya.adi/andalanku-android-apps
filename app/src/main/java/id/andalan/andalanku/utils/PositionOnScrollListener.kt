package id.andalan.andalanku.utils

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SCROLL_STATE_IDLE


abstract class PositionOnScrollListener(private val mLinearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager) : androidx.recyclerview.widget.RecyclerView.OnScrollListener() {
    private var position = -1
    private var isToLeft = false
    private var isToRight = false
    override fun onScrollStateChanged(recyclerView: androidx.recyclerview.widget.RecyclerView, newState: Int) {
        super.onScrollStateChanged(recyclerView, newState)
        if (newState == SCROLL_STATE_IDLE) {
            position = getCurrentItem(mLinearLayoutManager)
            currentPosition(position, isToLeft, isToRight)
        }
    }

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        isToRight = dx > 0
        isToLeft = dx < 0
    }

    abstract fun currentPosition(position: Int, isToLeft: Boolean, isToRight: Boolean)

    private fun getCurrentItem(mLinearLayoutManager: androidx.recyclerview.widget.LinearLayoutManager): Int {
        return mLinearLayoutManager.findFirstVisibleItemPosition()
    }
}