package id.andalan.andalanku.utils

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import id.andalan.andalanku.R
import kotlinx.android.synthetic.main.layout_succes_send_update.view.*


class AlertDialogUtil {
    companion object {
        fun buildDefaultAlertDialog(alertDialogView: AlertDialogView, context: Context, title: String, message: String): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes) { dialog, _ ->
                    alertDialogView.onClickOk()
                    dialog.dismiss()
                }
                .show()
        }

        fun buildAlertDialog(alertDialogView: AlertDialogView, context: Context, title: String, message: String): AlertDialog {
            return AlertDialog.Builder(context)
                .setTitle(title)
                .setCancelable(false)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes) { dialog, _ ->
                    alertDialogView.onClickOk()
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    alertDialogView.onClickCancel()
                    dialog.dismiss()
                }
                .show()
        }

        fun buildAlertDialogSuccessUpdate(context: Context, parent: ViewGroup?): AlertDialog {
            val successForm = LayoutInflater.from(context).inflate(R.layout.layout_succes_send_update, parent, false)
            val dialogBuilder = AlertDialog.Builder(context)
            dialogBuilder.setView(successForm)
            return AlertDialog.Builder(context)
                .setView(successForm)
                .show()
        }
    }

    interface AlertDialogView {
        fun onClickOk()
        fun onClickCancel()
    }
}