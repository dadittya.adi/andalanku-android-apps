package id.andalan.andalanku.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.andalan.andalanku.R

class AlertDialogCustom(context: Context, parent: ViewGroup?) {
    val defaultView = LayoutInflater.from(context).inflate(R.layout.layout_custom_dialog, parent, false)
    val dialogBuilder = AlertDialog.Builder(context)
    private lateinit var alertDialog: AlertDialog
    private lateinit var viewDialog: View

    fun setView(view: View? = null) {
        viewDialog = view?:defaultView
        dialogBuilder.setView(view?:defaultView)
    }

    fun getView(): View {
        return viewDialog
    }

    fun create() {
        alertDialog = dialogBuilder.create()
    }

    fun show() {
        alertDialog.show()
    }

    fun dismiss() {
        if (alertDialog.isShowing) {
            alertDialog.dismiss()
        }
    }

    fun setCancelable(flag: Boolean) {
        alertDialog.setCancelable(flag)
    }
}