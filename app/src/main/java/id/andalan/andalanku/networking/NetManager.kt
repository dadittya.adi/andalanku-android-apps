package id.andalan.andalanku.networking

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager

class NetManager(private var context: Context) {
    val isConnectedToInternet: Boolean
        @SuppressLint("MissingPermission")
        get() {
            val conManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val ni = conManager.activeNetworkInfo
            return ni != null && ni.isConnected
        }
}