package id.andalan.andalanku.networking

import id.andalan.andalanku.model.response.*
import okhttp3.RequestBody
import retrofit2.http.*
import rx.Observable

interface NetworkService {
    @FormUrlEncoded
    @POST("get_token")
    fun getToken(@Field("api_key") apiKey: String,
                 @Field("api_secret") apiSecret: String)
            : Observable<TokenResponse>

    @FormUrlEncoded
    @POST("register")
    fun register(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Field("customer_name") customerName: String? = "",
                 @Field("id_number") idNumber: String? = "",
                 @Field("email") email: String? = "",
                 @Field("password") password: String? = "",
                 @Field("phone_number") phoneNumber: String? = ""): Observable<RegisterResponse>

    @FormUrlEncoded
    @POST("set_pin")
    fun createPin(@Header("api_key") apiKey: String,
                  @Header("token_number") tokenNumber: String,
                  @Header("latitude") latitude: String,
                  @Header("longitude") longitude: String,
                  @Field("id_customer") idCustomer: Int? = 0,
                  @Field("pin_number") pinNumber: String? = ""): Observable<CreatePinResponse>

    @FormUrlEncoded
    @POST("change_pin")
    fun changePin(@Header("api_key") apiKey: String,
                  @Header("token_number") tokenNumber: String,
                  @Header("latitude") latitude: String,
                  @Header("longitude") longitude: String,
                  @Field("id_customer") idCustomer: Int? = 0,
                  @Field("old_pin_number") oldPinNumber: String? = "",
                  @Field("new_pin_number") newPinNumber: String? = ""): Observable<CreatePinResponse>

    @FormUrlEncoded
    @POST("reset_pin")
    fun resetPin(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Field("id_customer") idCustomer: Int? = 0,
                 @Field("email") email: String? = ""): Observable<ResetPinResponse>

    @FormUrlEncoded
    @POST("change_password")
    fun changePassword(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Header("id_customer") idCustomerHeader: Int,
                       @Header("latitude") latitude: String,
                       @Header("longitude") longitude: String,
                       @Field("id_customer") idCustomer: Int? = 0,
                       @Field("old_password") oldPassword: String? = "",
                       @Field("new_password") newPassword: String? = "",
                       @Field("confirm_new_password") confirmNewPassword: String? = ""): Observable<ChangePasswordResponse>

    @FormUrlEncoded
    @POST("reset_password")
    fun resetPassword(@Header("api_key") apiKey: String,
                      @Header("token_number") tokenNumber: String,
                      @Field("email") email: String): Observable<ResetPasswordResponse>

    @FormUrlEncoded
    @POST("login")
    fun login(@Header("api_key") apiKey: String,
              @Header("token_number") tokenNumber: String,
              @Field("email") email: String? = "",
              @Field("password") password: String? = ""): Observable<LoginResponse>

    @FormUrlEncoded
    @POST("validate_otp")
    fun verificationOtp(@Header("api_key") apiKey: String,
                        @Header("token_number") tokenNumber: String,
                        @Field("id_costumer") idCustomer: Int,
                        @Field("otp_code") otp: String): Observable<OtpVerificationResponse>

    @POST("branch_list")
    fun branchList(@Header("api_key") apiKey: String,
                   @Header("token_number") tokenNumber: String): Observable<BranchListResponse>

    @FormUrlEncoded
    @POST("advance_branch_list")
    fun advaceBranchList(@Header("api_key") apiKey: String,
                         @Header("token_number") tokenNumber: String,
                         @Header("latitude") latitude: String,
                         @Header("longitude") longitude: String,
                         @Field("id_city") idCity: String): Observable<BranchListResponse>

    @FormUrlEncoded
    @POST("news_list")
    fun listNews(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Field("keyword") keyword: String,
                 @Field("page") page: Int): Observable<NewsResponse>

    @FormUrlEncoded
    @POST("promo_list")
    fun listPromo(@Header("api_key") apiKey: String,
                  @Header("token_number") tokenNumber: String,
                  @Field("keyword") keyword: String,
                  @Field("page") page: Int): Observable<PromoResponse>

    @FormUrlEncoded
    @POST("promo_detail")
    fun getDetailPromo(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Header("id_customer") idCustomer1: Int,
                       @Header("latitude") latitude: String,
                       @Header("longitude") longitude: String,
                       @Field("slug") slug: String): Observable<DetailPromoResponse>

    @POST("slider_list")
    fun listSlider(@Header("api_key") apiKey: String,
                  @Header("token_number") tokenNumber: String): Observable<SliderResponse>

    @POST("partner_list")
    fun listParner(@Header("api_key") apiKey: String,
                   @Header("token_number") tokenNumber: String): Observable<PartnerListResponse>

    @POST("faq_list")
    fun listFaq(@Header("api_key") apiKey: String,
                @Header("token_number") tokenNumber: String): Observable<FaqResponse>

    @POST("car_brand_list")
    fun listBrandsCar(@Header("api_key") apiKey: String,
                      @Header("token_number") tokenNumber: String): Observable<CarBrandsResponse>

    @FormUrlEncoded
    @POST("car_type_list")
    fun listTypesCar(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Field("id_car_brand") idBrandCar: String): Observable<CarTypesResponse>

    @FormUrlEncoded
    @POST("car_trim_list")
    fun listTrimsCar(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Field("id_car_type") idTypeCar: String): Observable<CarTrimsResponse>

    @FormUrlEncoded
    @POST("bank_list")
    fun listBank(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Field("keyword") keyword: String): Observable<BankResponse>

    @FormUrlEncoded
    @POST("bank_branch_list")
    fun listBankBranch(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Field("bank_id") bankId: String,
                 @Field("keyword") keyword: String): Observable<BankBranchResponse>

    @POST("province_list")
    fun listProvince(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String): Observable<LocationResponse>

    @FormUrlEncoded
    @POST("city_list")
    fun listCity(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Field("id_province") idProvince: String): Observable<LocationResponse>

    @FormUrlEncoded
    @POST("district_list")
    fun listDistrict(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Field("id_city") idCity: String): Observable<LocationResponse>

    @FormUrlEncoded
    @POST("village_list")
    fun listVillage(@Header("api_key") apiKey: String,
                    @Header("token_number") tokenNumber: String,
                    @Field("id_district") idDistrict: String): Observable<LocationResponse>

    @FormUrlEncoded
    @POST("zip_code")
    fun zipCode(@Header("api_key") apiKey: String,
                    @Header("token_number") tokenNumber: String,
                    @Field("id_village") idVillage: String): Observable<LocationResponse>

    @FormUrlEncoded
    @POST("credit_simulation")
    fun getCreditSimulation(@Header("api_key") apiKey: String,
                            @Header("token_number") tokenNumber: String,
                            @Field("type") type: String,
                            @Field("otr") otr: String,
                            @Field("dp") dp: String,
                            @Field("tenor") tenor: String,
                            @Field("asuransi") asuransi: String,
                            @Field("wilayah") wilayah: String,
                            @Field("car_price") car_price: String): Observable<CreditSimulationResponse>

    @FormUrlEncoded
    @POST("andalan_update_profile")
    fun sendUpdatedPersonalData(@Header("api_key") apiKey: String,
                                @Header("token_number") tokenNumber: String,
                                @Field("id_customer") idCustomer: Int,
                                @Field("address_type") addressType: String,
                                @Field("address") address: String,
                                @Field("rt") rt: String,
                                @Field("rw") rw: String,
                                @Field("kelurahan") kelurahan: String,
                                @Field("kecamatan") kecamatan: String,
                                @Field("city") city: String,
                                @Field("zip_code") zipCode: String,
                                @Field("phone_number") phoneNumber: String,
                                @Field("mobile_number") mobilePhone: String): Observable<UpdateProfileResponse>

    @FormUrlEncoded
    @POST("profile_update_request")
    fun sendUpdatedAddress(@Header("api_key") apiKey: String,
                           @Header("token_number") tokenNumber: String,
                           @Header("id_customer") idCustomerHeader: Int,
                           @Header("latitude") latitude: String,
                           @Header("longitude") longitude: String,
                           @Field("id_customer") idCustomer: Int,
                           @Field("address_type") addressType: String,
                           @Field("address") address: String,
                           @Field("rt") rt: String,
                           @Field("rw") rw: String,
                           @Field("province_id") provinceId: String,
                           @Field("regency_id") regencyId: String,
                           @Field("district_id") districtId: String,
                           @Field("village_id") villageId: String,
                           @Field("zip_code") zipCode: String,
                           @Field("phone") phoneNumber: String,
                           @Field("id_card_image") idCardImage: String,
                           @Field("npwp") npwp: String,
                           @Field("npwp_image") npwpImage: String,
                           @Field("nip") nip: String,
                           @Field("gender") gender: String,
                           @Field("birth_place") birthPlace: String,
                           @Field("birth_date") birthDate: String,
                           @Field("avatar") avatar: String,
                           @Field("bank_name") bankName: String,
                           @Field("bank_account_number") bankAccountNumber: String,
                           @Field("bank_account_holder") bankAccountHolder: String,
                           @Field("bank_account_image") bankAccountImage: String,
                           @Field("id_card_number") id_card_number: String,
                           @Field("home_phone_number") home_phone_number: String,
                           @Field("customer_name") customer_name: String) : Observable<UpdateAddressResponse>

    @FormUrlEncoded
    @POST("agent_registration")
    fun sendAgentRegistration(@Header("api_key") apiKey: String,
                           @Header("token_number") tokenNumber: String,
                           @Header("id_customer") idCustomerHeader: Int,
                           @Header("latitude") latitude: String,
                           @Header("longitude") longitude: String,
                           @Field("id_customer") idCustomer: Int,
                           @Field("address_type") addressType: String,
                           @Field("address") address: String,
                           @Field("rt") rt: String,
                           @Field("rw") rw: String,
                           @Field("province_id") provinceId: String,
                           @Field("regency_id") regencyId: String,
                           @Field("district_id") districtId: String,
                           @Field("village_id") villageId: String,
                           @Field("zip_code") zipCode: String,
                           @Field("phone") phoneNumber: String,
                           @Field("id_card_image") idCardImage: String,
                           @Field("npwp") npwp: String,
                           @Field("npwp_image") npwpImage: String,
                           @Field("nip") nip: String,
                           @Field("gender") gender: String,
                           @Field("birth_place") birthPlace: String,
                           @Field("birth_date") birthDate: String,
                           @Field("avatar") avatar: String,
                           @Field("bank_name") bankName: String,
                           @Field("bank_branch_name") bankBranchName: String,
                           @Field("bank_account_number") bankAccountNumber: String,
                           @Field("bank_account_holder") bankAccountHolder: String,
                           @Field("bank_account_image") bankAccountImage: String,
                           @Field("id_card_number") id_card_number: String,
                           @Field("home_phone_number") home_phone_number: String,
                           @Field("customer_name") customer_name: String) : Observable<UpdateAddressResponse>

    @FormUrlEncoded
    @POST("customer_address_update")
    fun sendUpdatedOnlyAddress(@Header("api_key") apiKey: String,
                               @Header("token_number") tokenNumber: String,
                               @Header("id_customer") idCustomerHeader: Int,
                               @Header("latitude") latitude: String,
                               @Header("longitude") longitude: String,
                               @Field("id_customer") idCustomer: Int,
                               @Field("address_type") addressType: String,
                               @Field("address") address: String,
                               @Field("rt") rt: String,
                               @Field("rw") rw: String,
                               @Field("province_id") provinceId: String,
                               @Field("regency_id") regencyId: String,
                               @Field("district_id") districtId: String,
                               @Field("village_id") villageId: String,
                               @Field("zip_code") zipCode: String,
                               @Field("id_card_image") idCardImage: String): Observable<UpdateAddressResponse>

    @POST("complain_category_list")
    fun getCategoryComplainList(@Header("api_key") apiKey: String,
                                @Header("token_number") tokenNumber: String): Observable<CategoryComplainResponse>

    @FormUrlEncoded
    @POST("e_complain_list")
    fun getComplainList(@Header("api_key") apiKey: String,
                        @Header("token_number") tokenNumber: String,
                        @Field("id_customer") idCustomer: Int,
                        @Field("status") status: String?): Observable<ComplainListResponse>

    @FormUrlEncoded
    @POST("agent_code")
    fun getAgentCode(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Field("id_customer") idCustomer: Int): Observable<AgentResponse>

    @FormUrlEncoded
    @POST("inbox_list")
    fun getInboxList(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Header("id_customer") idCustomer1: Int,
                     @Header("latitude") latitude: String,
                     @Header("longitude") longitude: String,
                     @Field("id_customer") idCustomer: Int,
                     @Field("page") page: String?): Observable<InboxResponse>

    @FormUrlEncoded
    @POST("inbox_detail")
    fun getDetailInbox(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Header("id_customer") idCustomer1: Int,
                       @Header("latitude") latitude: String,
                       @Header("longitude") longitude: String,
                       @Field("id_customer") idCustomer: Int,
                       @Field("id_inbox") id_inbox: String?): Observable<DetailInboxResponse>

    @POST("input_e_complain")
    fun sendComplain(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Header("id_customer") idCustomer: Int,
                     @Header("latitude") latitude: String,
                     @Header("longitude") longitude: String,
                     @Body body: RequestBody): Observable<InputComplainResponse>

    @POST("send_dm")
    fun sendMessage(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Header("id_customer") idCustomer: Int,
                     @Header("latitude") latitude: String,
                     @Header("longitude") longitude: String,
                     @Body body: RequestBody): Observable<InputMessageResponse>

    @POST("insurance_claim")
    fun insuranceClaim(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Header("id_customer") idCustomer: Int,
                       @Header("latitude") latitude: String,
                       @Header("longitude") longitude: String,
                       @Body body: RequestBody): Observable<InsuranceClaimResponse>

    @POST("get_legal_document")
    fun getLegalDocument(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Header("id_customer") idCustomer: Int,
                       @Header("latitude") latitude: String,
                       @Header("longitude") longitude: String): Observable<LegalDocumentResponse>

    @POST("occupation_list")
    fun getOccupationList(@Header("api_key") apiKey: String,
                          @Header("token_number") tokenNumber: String): Observable<OccupationsResponse>

    @POST("loan_application")
    fun sendLoanApplication(@Header("api_key") apiKey: String,
                            @Header("token_number") tokenNumber: String,
                            @Body body: RequestBody): Observable<LoanApplicationResponse>

    @FormUrlEncoded
    @POST("collateral_request")
    fun getCollateralRequest(@Header("api_key") apiKey: String,
                             @Header("token_number") tokenNumber: String,
                             @Field("id_customer") idCustomer: Int,
                             @Field("agreement_number") AgreementNumber: String,
                             @Field("taking_date") takingDate: String): Observable<CollateralRequestResponse>

    @FormUrlEncoded
    @POST("agreement_list")
    fun getAgreementList(@Header("api_key") apiKey: String,
                         @Header("token_number") tokenNumber: String,
                         @Field("id_customer") idCustomer: Int): Observable<AgreementListResponse>

    @FormUrlEncoded
    @POST("prepayment_simulation")
    fun getPrepaymentSimulation(@Header("api_key") apiKey: String,
                             @Header("token_number") tokenNumber: String,
                             @Field("id_customer") idCustomer: Int,
                             @Field("agreement_no") AgreementNumber: String,
                             @Field("prepayment_date") prepaymentDate: String): Observable<PrePaymentSimulationResponse>

    @FormUrlEncoded
    @POST("prepayment_request")
    fun getPrepaymentRequest(@Header("api_key") apiKey: String,
                             @Header("token_number") tokenNumber: String,
                             @Field("id_customer") idCustomer: Int,
                             @Field("agreement_no") AgreementNumber: String,
                             @Field("prepayment_date") prepaymentDate: String): Observable<PrepaymentRequest>

    @FormUrlEncoded
    @POST("andalan_detail_unit")
    fun getAndalanDetailUnit(@Header("api_key") apiKey: String,
                             @Header("token_number") tokenNumber: String,
                             @Field("agreement_no") AgreementNumber: String): Observable<DetailUnitResponse>

    @FormUrlEncoded
    @POST("andalan_detail_financial")
    fun getAndalanDetailFinancial(@Header("api_key") apiKey: String,
                                  @Header("token_number") tokenNumber: String,
                                  @Field("agreement_no") AgreementNumber: String): Observable<DetailFinanceResponse>

    @FormUrlEncoded
    @POST("andalan_detail_insurance")
    fun getAndalanDetailInsurance(@Header("api_key") apiKey: String,
                                  @Header("token_number") tokenNumber: String,
                                  @Header("id_customer") idCustomer: Int,
                                  @Field("agreement_no") AgreementNumber: String): Observable<DetailInsuranceResponse>

    @FormUrlEncoded
    @POST("andalan_detail_payment_history")
    fun getAndalanDetailPaymentHistory(@Header("api_key") apiKey: String,
                                       @Header("token_number") tokenNumber: String,
                                       @Field("id_customer") idCustomer: Int,
                                       @Field("agreement_no") AgreementNumber: String): Observable<DetailPaymentHistoryResponse>

    @FormUrlEncoded
    @POST("andalan_financial")
    fun getAndalanFinancial(@Header("api_key") apiKey: String,
                            @Header("token_number") tokenNumber: String,
                            @Header("id_customer") idCustomer2: Int,
                            @Header("latitude") latitude: String,
                            @Header("longitude") longitude: String,
                            @Field("id_customer") idCustomer: Int,
                            @Field("agreement_no") AgreementNumber: String): Observable<DetailAndalanFinancialResponse>

    @FormUrlEncoded
    @POST("used_car_price")
    fun getUsedCarList(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Header("id_customer") idCustomer: Int,
                       @Header("latitude") latitude: String,
                       @Header("longitude") longitude: String,
                       @Field("year") year: String,
                       @Field("keyword") keyword: String): Observable<UsedCarListResponse>

    @FormUrlEncoded
    @POST("personal_information")
    fun getPersonalInformation(@Header("api_key") apiKey: String,
                               @Header("token_number") tokenNumber: String,
                               @Header("id_customer") idCustomer: Int,
                               @Header("latitude") latitude: String,
                               @Header("longitude") longitude: String,
                               @Field("id_customer") idCustomer2: String): Observable<PersonalInformationResponse>

    @FormUrlEncoded
    @POST("plafond_simulation")
    fun sendPlafondSimulation(@Header("api_key") apiKey: String,
                              @Header("token_number") tokenNumber: String,
                              @Header("latitude") latitude: String,
                              @Header("longitude") longitude: String,
                              @Field("id_customer") idCustomer: String,
                              @Field("occupation_id") occupationId: String,
                              @Field("monthly_income") monthlyIncome: String,
                              @Field("side_job") sideJob: String,
                              @Field("additional_income") additionalIsncome: String): Observable<PlafondSimulationResponse>

    @POST("plafond_application")
    fun sendPlafondApplication(@Header("api_key") apiKey: String,
                               @Header("token_number") tokenNumber: String,
                               @Header("latitude") latitude: String,
                               @Header("longitude") longitude: String,
                               @Header("id_customer") idCustomer: String,
                               @Body body: RequestBody): Observable<PlafondApplicationResponse>

    @POST("update_profile_all_address")
    fun sendProfileAllAddress(@Header("api_key") apiKey: String,
                               @Header("token_number") tokenNumber: String,
                               @Header("latitude") latitude: String,
                               @Header("longitude") longitude: String,
                               @Header("id_customer") idCustomer: String,
                               @Body body: RequestBody): Observable<UpdateAddressResponse>

    @POST("agent_registration")
    fun sendAgentRegistration(@Header("api_key") apiKey: String,
                              @Header("token_number") tokenNumber: String,
                              @Header("latitude") latitude: String,
                              @Header("longitude") longitude: String,
                              @Header("id_customer") idCustomer: String,
                              @Body body: RequestBody): Observable<AgentRegistrationResponse>

    @FormUrlEncoded
    @POST("insurance_claim_list")
    fun getInsuranceClaimList(@Header("api_key") apiKey: String,
                              @Header("token_number") tokenNumber: String,
                              @Header("id_customer") idCustomer: Int,
                              @Header("latitude") latitude: String,
                              @Header("longitude") longitude: String,
                              @Field("id_customer") idCustomer2: Int,
                              @Field("status") status: String): Observable<ClaimInsuranceListResponse>

    @FormUrlEncoded
    @POST("collateral_request")
    fun sendCollateralRequest(@Header("api_key") apiKey: String,
                              @Header("token_number") tokenNumber: String,
                              @Header("id_customer") idCustomer: Int,
                              @Header("latitude") latitude: String,
                              @Header("longitude") longitude: String,
                              @Field("id_customer") idCustomer2: Int,
                              @Field("agreement_number") agreementNumber: String,
                              @Field("taking_date") takingDate: String): Observable<CollateralRequestResponse>

    @FormUrlEncoded
    @POST("cash_value")
    fun getCashValue(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Header("id_customer") idCustomer: Int,
                     @Header("latitude") latitude: String,
                     @Header("longitude") longitude: String,
                     @Field("id_customer") idCustomer2: Int): Observable<CashValueResponse>

    @FormUrlEncoded
    @POST("inbox_check")
    fun getUnreadMessage(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Header("id_customer") idCustomer: Int,
                     @Header("latitude") latitude: String,
                     @Header("longitude") longitude: String,
                     @Field("id_customer") idCustomer2: Int): Observable<UnreadMessageResponse>

    @FormUrlEncoded
    @POST("collateral_request_list")
    fun getCollateralRequestList(@Header("api_key") apiKey: String,
                                 @Header("token_number") tokenNumber: String,
                                 @Header("id_customer") idCustomer: Int,
                                 @Header("latitude") latitude: String,
                                 @Header("longitude") longitude: String,
                                 @Field("id_customer") idCustomer2: Int,
                                 @Field("status") status: String): Observable<CollateralListResponse>


    @POST("tutorial_list")
    fun listTutorial(@Header("api_key") apiKey: String,
                @Header("token_number") tokenNumber: String): Observable<TutorialResponse>

    @FormUrlEncoded
    @POST("reset_pin_using_password")
    fun resetPinWithPassword(@Header("api_key") apiKey: String,
                             @Header("token_number") tokenNumber: String,
                             @Header("latitude") latitude: String,
                             @Header("longitude") longitude: String,
                             @Field("id_customer") idCustomer: Int? = 0,
                             @Field("password") password: String? = "",
                             @Field("pin_number") pinNumber: String? = ""): Observable<CreatePinResponse>

    @POST("get_app_version")
    fun getLatestAppVersion(@Header("api_key") apiKey: String,
                     @Header("token_number") tokenNumber: String,
                     @Header("id_customer") idCustomer: Int,
                     @Header("latitude") latitude: String,
                     @Header("longitude") longitude: String): Observable<AppVersionResponse>

    @FormUrlEncoded
    @POST("payment_inquiry")
    fun paymentInquiry(@Header("api_key") apiKey: String,
                                  @Header("token_number") tokenNumber: String,
                                  @Field("contract_no") AgreementNumber: String,
                                  @Field("id_customer") idCustomer: Int): Observable<PaymentInquiryResponse>
    @FormUrlEncoded
    @POST("transaction_history")
    fun getPaymentHistory(
        @Header("api_key") apiKey: String,
        @Header("token_number") tokenNumber: String,
        @Field("category") category: String,
        @Field("status") status: String,
        @Field("id_customer") idCustomer: Int
    ): Observable<InstallmentPaymentHistoryListResponse>

    @POST("get_url")
    fun getUrlList(@Header("api_key") apiKey: String,
                                     @Header("token_number") tokenNumber: String): Observable<UrlListResponse>

    @FormUrlEncoded
    @POST("customer_fcm")
    fun updateFcmToken(@Header("api_key") apiKey: String,
                   @Header("token_number") tokenNumber: String,
                   @Field("id_customer") idCustomer: Int,
                   @Field("fcm_token") fcmToken: String): Observable<UpdateTokenResponse>

    @FormUrlEncoded
    @POST("nip_check")
    fun nipCheck(@Header("api_key") apiKey: String,
                       @Header("token_number") tokenNumber: String,
                       @Field("nip") fcmToken: String): Observable<NipCheckResponse>

    @POST("product_list")
    fun ppobProductList(@Header("api_key") apiKey: String,
                 @Header("token_number") tokenNumber: String,
                 @Body body: RequestBody): Observable<PpobProductListResponse>

    @POST("ppob_inquiry")
    fun ppobInquiry(@Header("api_key") apiKey: String,
                        @Header("token_number") tokenNumber: String,
                        @Body body: RequestBody): Observable<PpobInquiryResponse>

    @POST("emoney_biller")
    fun ppobEmoneyBiller(@Header("api_key") apiKey: String,
                        @Header("token_number") tokenNumber: String,
                        @Body body: RequestBody): Observable<PpobProductListResponse>

    @POST("ppob_transaction_detail")
    fun ppobTransactionDetail(@Header("api_key") apiKey: String,
                    @Header("token_number") tokenNumber: String,
                    @Body body: RequestBody): Observable<PpobTransactionDetailResponse>

    @POST("ppob_group")
    fun ppobGroup(@Header("api_key") apiKey: String,
                    @Header("token_number") tokenNumber: String,
                    @Body body: RequestBody): Observable<PpobGroupResponse>

    @POST("ppob_update_amount")
    fun ppobUpdateAmount(@Header("api_key") apiKey: String,
                  @Header("token_number") tokenNumber: String,
                  @Body body: RequestBody): Observable<PpobUpdateAmountResponse>

}
