package id.andalan.andalanku.networking

class AuthenticationException(message: String) : RuntimeException(message)