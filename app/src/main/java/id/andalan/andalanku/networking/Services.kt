package id.andalan.andalanku.networking

import android.util.Base64
import android.util.Base64OutputStream
import id.andalan.andalanku.BuildConfig
import id.andalan.andalanku.model.request.*
import id.andalan.andalanku.model.response.*
import id.andalan.andalanku.preferences.PreferencesUtil
import okhttp3.MultipartBody
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream
import android.graphics.BitmapFactory
import android.graphics.Bitmap

class Services @Inject constructor(private val networkService: NetworkService, private val preferencesUtil: PreferencesUtil) {
    fun getTokenNumber(): Observable<TokenResponse> {
        return networkService.getToken(BuildConfig.API_KEY, BuildConfig.API_SECRET)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doRegister(registerRequest: RegisterRequest): Observable<RegisterResponse> {
        return networkService.register(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            registerRequest.customerName,
            registerRequest.idNumber,
            registerRequest.email,
            registerRequest.password,
            registerRequest.phoneNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doCreatePin(pinNumber: String): Observable<CreatePinResponse> {
        return networkService.createPin(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            preferencesUtil.getIdNumber()?:0,
            pinNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doChangePin(oldPinNumber: String, newPinNumber: String): Observable<CreatePinResponse> {
        return networkService.changePin(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            preferencesUtil.getIdNumber()?:0,
            oldPinNumber,
            newPinNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doResetPin(): Observable<ResetPinResponse> {
        return networkService.resetPin(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getIdNumber()?:0,
            preferencesUtil.getUser()?.email
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doChangePassword(password: String, newPassword: String, confirmNewPassword: String): Observable<ChangePasswordResponse> {
        return networkService.changePassword(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            preferencesUtil.getIdNumber()?:0,
            password,
            newPassword,
            confirmNewPassword
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doResetPassword(email: String): Observable<ResetPasswordResponse> {
        return networkService.resetPassword(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            email
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doLogin(email: String, password: String): Observable<LoginResponse> {
        return networkService.login(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            email,
            password
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doVerificationOtp(otp: String): Observable<OtpVerificationResponse> {
        return networkService.verificationOtp(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getIdNumber()?:0,
            otp
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getNews(page: Int, keyword: String): Observable<NewsResponse> {
        return networkService.listNews(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            keyword,
            page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getListFaq(): Observable<FaqResponse> {
        return networkService.listFaq(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getPromo(page: Int, keyword: String): Observable<PromoResponse> {
        return networkService.listPromo(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            keyword,
            page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDetailPromo(slug: String): Observable<DetailPromoResponse> {
        return networkService.getDetailPromo(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            slug
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getSlider(): Observable<SliderResponse> {
        return networkService.listSlider(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getPartner(): Observable<PartnerListResponse> {
        return networkService.listParner(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getCreditSimulation(type: String,
                            otr: String,
                            dp: String,
                            tenor: String,
                            asuransi: String,
                            wilayah: String,
                            car_price: String = ""): Observable<CreditSimulationResponse> {
        return networkService.getCreditSimulation(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            type,
            otr,
            dp,
            tenor,
            asuransi,
            wilayah,
            car_price
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendUpdatedPersonalInfo(updatePersonalRequest: UpdatePersonalRequest): Observable<UpdateProfileResponse> {
        return networkService.sendUpdatedPersonalData(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            updatePersonalRequest.idCustomer?:0,
            updatePersonalRequest.addressType?:"",
            updatePersonalRequest.address?:"",
            updatePersonalRequest.rt?:"",
            updatePersonalRequest.rw?:"",
            updatePersonalRequest.kelurahan?:"",
            updatePersonalRequest.kecamatan?:"",
            updatePersonalRequest.city?:"",
            updatePersonalRequest.zipCode?:"",
            updatePersonalRequest.phoneNumber?:"",
            updatePersonalRequest.mobileNumber?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendUpdateAddress(updateAddressRequest: UpdateAddressRequest): Observable<UpdateAddressResponse> {
        return networkService.sendUpdatedAddress(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            updateAddressRequest.idCustomer?:0,
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            updateAddressRequest.idCustomer?:0,
            updateAddressRequest.addressType?:"",
            updateAddressRequest.address?:"",
            updateAddressRequest.rt?:"",
            updateAddressRequest.rw?:"",
            updateAddressRequest.provinceId?:"",
            updateAddressRequest.regencyId?:"",
            updateAddressRequest.districtId?:"",
            updateAddressRequest.villageId?:"",
            updateAddressRequest.zipCode?:"",
            updateAddressRequest.phoneNumber?:"",
            if (updateAddressRequest.idCardImage.isNullOrBlank()) ""
                else convertImageFileToBase64(File(updateAddressRequest.idCardImage)),
            updateAddressRequest.npwp?:"",
            if (updateAddressRequest.npwp_image.isNullOrBlank()) ""
                else convertImageFileToBase64(File(updateAddressRequest.npwp_image)),
            updateAddressRequest.nip?:"",
            updateAddressRequest.gender?:"",
            updateAddressRequest.birthPlace?:"",
            updateAddressRequest.birthDate?:"",
            if (updateAddressRequest.avatar.isNullOrBlank()) ""
            else convertImageFileToBase64(File(updateAddressRequest.avatar)),
            updateAddressRequest.bankName?:"",
            updateAddressRequest.bankAccountNumber?:"",
            updateAddressRequest.bankAccountHolder?:"",
            updateAddressRequest.bankAccountImage?:"",
            updateAddressRequest.id_card_number?:"",
            updateAddressRequest.home_phone_number?:"",
             updateAddressRequest.customer_name?:""


        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendAgentRequest(updateAddressRequest: UpdateAddressRequest): Observable<UpdateAddressResponse> {
        return networkService.sendUpdatedAddress(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            updateAddressRequest.idCustomer?:0,
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            updateAddressRequest.idCustomer?:0,
            updateAddressRequest.addressType?:"",
            updateAddressRequest.address?:"",
            updateAddressRequest.rt?:"",
            updateAddressRequest.rw?:"",
            updateAddressRequest.provinceId?:"",
            updateAddressRequest.regencyId?:"",
            updateAddressRequest.districtId?:"",
            updateAddressRequest.villageId?:"",
            updateAddressRequest.zipCode?:"",
            updateAddressRequest.phoneNumber?:"",
            if (updateAddressRequest.idCardImage.isNullOrBlank()) ""
            else convertImageFileToBase64(File(updateAddressRequest.idCardImage)),
            updateAddressRequest.npwp?:"",
            if (updateAddressRequest.npwp_image.isNullOrBlank()) ""
            else convertImageFileToBase64(File(updateAddressRequest.npwp_image)),
            updateAddressRequest.nip?:"",
            updateAddressRequest.gender?:"",
            updateAddressRequest.birthPlace?:"",
            updateAddressRequest.birthDate?:"",
            if (updateAddressRequest.avatar.isNullOrBlank()) ""
            else convertImageFileToBase64(File(updateAddressRequest.avatar)),
            updateAddressRequest.bankName?:"",
            updateAddressRequest.bankAccountNumber?:"",
            updateAddressRequest.bankAccountHolder?:"",
            updateAddressRequest.bankAccountImage?:"",
            updateAddressRequest.id_card_number?:"",
            updateAddressRequest.home_phone_number?:"",
            updateAddressRequest.customer_name?:""


        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendUpdateOnlyAddress(updateAddressRequest: UpdateAddressRequest): Observable<UpdateAddressResponse> {
        return networkService.sendUpdatedOnlyAddress(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            updateAddressRequest.idCustomer?:0,
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            updateAddressRequest.idCustomer?:0,
            updateAddressRequest.addressType?:"",
            updateAddressRequest.address?:"",
            updateAddressRequest.rt?:"",
            updateAddressRequest.rw?:"",
            updateAddressRequest.provinceId?:"",
            updateAddressRequest.regencyId?:"",
            updateAddressRequest.districtId?:"",
            updateAddressRequest.villageId?:"",
            updateAddressRequest.zipCode?:"",
            if (updateAddressRequest.idCardImage.isNullOrBlank()) ""
            else convertImageFileToBase64(File(updateAddressRequest.idCardImage))
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getCategoryComplain(): Observable<CategoryComplainResponse> {
        return networkService.getCategoryComplainList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getAgentCode(): Observable<AgentResponse> {
        return networkService.getAgentCode(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getComplainList(status: String?): Observable<ComplainListResponse> {
        return networkService.getComplainList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            idCustomer = preferencesUtil.getUser()?.idCustomer?:0,
            status = status
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getInboxList(page: String?): Observable<InboxResponse> {
        return networkService.getInboxList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0,
            page
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDetailMessage(idMessage: String?): Observable<DetailInboxResponse> {
        return networkService.getDetailInbox(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0,
            idMessage
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendComplain(complainRequest: InputComplainRequest, media: List<String>): Observable<InputComplainResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("category", complainRequest.category?:"")
        builder.addFormDataPart("phone", complainRequest.phone?:"")
        builder.addFormDataPart("message", complainRequest.message?:"")

        for (i in 0 until media.size) {
            val file = File(media[i])
            builder.addFormDataPart("media[$i]", convertImageFileToBase64(file))
        }

        return networkService.sendComplain(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendMessage(messageRequest: InputMessageRequest, media: List<String>): Observable<InputMessageResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("destination_id", messageRequest.destination_id?:"")
        builder.addFormDataPart("sender_id", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("content", messageRequest.content?:"")
        builder.addFormDataPart("title", messageRequest.title?:"")


        for (i in 0 until media.size) {
            val file = File(media[i])
            builder.addFormDataPart("media[$i]", convertImageFileToBase64(file))
        }

        return networkService.sendMessage(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendInsuranceClaim(insuranceClaimRequest: InsuranceClaimRequest, media: List<String>): Observable<InsuranceClaimResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("agreement_number", insuranceClaimRequest.agreement_number?:"")
        builder.addFormDataPart("claim_type", insuranceClaimRequest.claim_type?:"")
        builder.addFormDataPart("event_case", insuranceClaimRequest.event_case?:"")
        builder.addFormDataPart("event_date", insuranceClaimRequest.event_date?:"")
        builder.addFormDataPart("location", insuranceClaimRequest.location?:"")
        builder.addFormDataPart("notes", insuranceClaimRequest.notes?:"")

        for (i in 0 until media.size) {
            val file = File(media[i])
            builder.addFormDataPart("media[$i]", convertImageFileToBase64(file))
        }

        return networkService.insuranceClaim(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getBrandsCar(): Observable<CarBrandsResponse> {
        return networkService.listBrandsCar(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getTypesCar(idBrand: String): Observable<CarTypesResponse> {
        return networkService.listTypesCar(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            idBrand
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getTrimCar(idType: String): Observable<CarTrimsResponse> {
        return networkService.listTrimsCar(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            idType
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getBranchList(): Observable<BranchListResponse> {
        return networkService.branchList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getAdvanceBranchList(idCity: String): Observable<BranchListResponse> {
        return networkService.advaceBranchList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            idCity
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getBank(keyword: String): Observable<BankResponse> {
        return networkService.listBank(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            keyword
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getBankBranch(id: String?, keyword: String): Observable<BankBranchResponse> {
        return networkService.listBankBranch(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            id!!,
            keyword
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getProvince(): Observable<LocationResponse> {
        return networkService.listProvince(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getCity(id: String): Observable<LocationResponse> {
        return networkService.listCity(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDistrict(id: String): Observable<LocationResponse> {
        return networkService.listDistrict(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getVillage(id: String): Observable<LocationResponse> {
        return networkService.listVillage(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getZipCode(id: String): Observable<LocationResponse> {
        return networkService.zipCode(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            id
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getOccupation(): Observable<OccupationsResponse> {
        return networkService.getOccupationList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendLoanApplication(loanApplicationRequest: SendLoanApplicationRequest): Observable<LoanApplicationResponse> {
        val builder = MultipartBody.Builder()

        var fotoAccountCover = ""
        if (!loanApplicationRequest.bankAccountImage.isNullOrBlank()) {
            if (loanApplicationRequest.bankAccountImage?.take(4) == "http") {
                fotoAccountCover = loanApplicationRequest.bankAccountImage!!
            }
            else {
                val bm = BitmapFactory.decodeFile(loanApplicationRequest.bankAccountImage)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                val b = baos.toByteArray()
                fotoAccountCover = Base64.encodeToString(b, Base64.DEFAULT)
            }
        }

        //val fotoKtp = if (loanApplicationRequest.typeFotoKtp == "file") convertImageFileToBase64(File(loanApplicationRequest.fotoKtp)) else loanApplicationRequest.fotoKtp
        var fotoKtp = ""
        if (!loanApplicationRequest.fotoKtp.isNullOrBlank()) {
            if (loanApplicationRequest.fotoKtp?.take(4) == "http") {
                fotoKtp = loanApplicationRequest.fotoKtp!!
            }
            else {
                val bm = BitmapFactory.decodeFile(loanApplicationRequest.fotoKtp)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                val b = baos.toByteArray()
                fotoKtp = Base64.encodeToString(b, Base64.DEFAULT)
            }
        }
        //val fotoKk = if (loanApplicationRequest.typeFotoKk == "file") convertImageFileToBase64(File(loanApplicationRequest.fotoKk)) else loanApplicationRequest.fotoKk
        var fotoKk = ""
        if (!loanApplicationRequest.fotoKk.isNullOrBlank()) {
            if (loanApplicationRequest.fotoKk?.take(4) == "http") {
                fotoKk = loanApplicationRequest.fotoKk!!
            }
            else {
                val bm = BitmapFactory.decodeFile(loanApplicationRequest.fotoKk)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                val b = baos.toByteArray()
                fotoKk = Base64.encodeToString(b, Base64.DEFAULT)
            }
        }
        //val fotoSlipGaji = if (loanApplicationRequest.typeFotoSlipGaji == "file") convertImageFileToBase64(File(loanApplicationRequest.fotoSlipGaji)) else loanApplicationRequest.fotoSlipGaji
        var fotoSlipGaji = ""
        if (!loanApplicationRequest.fotoSlipGaji.isNullOrBlank()) {
            if (loanApplicationRequest.fotoSlipGaji?.take(4) == "http") {
                fotoSlipGaji = loanApplicationRequest.fotoSlipGaji!!
            }
            else {
                val bm = BitmapFactory.decodeFile(loanApplicationRequest.fotoSlipGaji)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                val b = baos.toByteArray()
                fotoSlipGaji = Base64.encodeToString(b, Base64.DEFAULT)
            }
        }
        //val fotoRekKoran = if (loanApplicationRequest.typeFotoRekeningKoran == "file") convertImageFileToBase64(File(loanApplicationRequest.fotoRekeningKoran)) else loanApplicationRequest.fotoRekeningKoran
        var fotoRekKoran = ""
        if (!loanApplicationRequest.fotoRekeningKoran.isNullOrBlank()) {
            if (loanApplicationRequest.fotoRekeningKoran?.take(4) == "http") {
                fotoRekKoran = loanApplicationRequest.fotoRekeningKoran!!
            }
            else {
                val bm = BitmapFactory.decodeFile(loanApplicationRequest.fotoRekeningKoran)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                val b = baos.toByteArray()
                fotoRekKoran = Base64.encodeToString(b, Base64.DEFAULT)
            }
        }
        //val fotoKeuanganLainnya = if (loanApplicationRequest.typeFotoKeuanganLainya == "file") convertImageFileToBase64(File(loanApplicationRequest.fotoKeuanganLainya)) else loanApplicationRequest.fotoKeuanganLainya
        var fotoKeuanganLainnya = ""
        if (!loanApplicationRequest.fotoKeuanganLainya.isNullOrBlank()) {
            if (loanApplicationRequest.fotoKeuanganLainya?.take(4) == "http") {
                fotoKeuanganLainnya = loanApplicationRequest.fotoKeuanganLainya!!
            }
            else {
                val bm = BitmapFactory.decodeFile(loanApplicationRequest.fotoKeuanganLainya)
                val baos = ByteArrayOutputStream()
                bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
                val b = baos.toByteArray()
                fotoKeuanganLainnya = Base64.encodeToString(b, Base64.DEFAULT)
            }
        }

        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("product", loanApplicationRequest.product?:"")
        builder.addFormDataPart("otr", loanApplicationRequest.otr?:"")
        builder.addFormDataPart("dp_percent", loanApplicationRequest.dpPercent?:"")
        builder.addFormDataPart("dp_amount", loanApplicationRequest.dpAmount?:"")
        builder.addFormDataPart("plafond", loanApplicationRequest.plafond?:"")
        builder.addFormDataPart("tenor", loanApplicationRequest.tenor?:"")
        builder.addFormDataPart("insurance", loanApplicationRequest.insurance?:"")
        builder.addFormDataPart("recieved_funds", loanApplicationRequest.recievedFunds?:"")
        builder.addFormDataPart("installment", loanApplicationRequest.installment?:"")
        builder.addFormDataPart("region", loanApplicationRequest.region?:"")
        builder.addFormDataPart("asset_code", loanApplicationRequest.assetCode?:"")
        builder.addFormDataPart("collateral_type", loanApplicationRequest.collateralType?:"")
        builder.addFormDataPart("collateral", loanApplicationRequest.collateral?:"")
        builder.addFormDataPart("collateral_year", loanApplicationRequest.collateralYear?:"")
        builder.addFormDataPart("collateral_owner", loanApplicationRequest.collateralOwner?:"")
        builder.addFormDataPart("collateral_paper_number", loanApplicationRequest.collateralPaperNumber?:"")
        builder.addFormDataPart("branch_id", loanApplicationRequest.branchId?:"")
        builder.addFormDataPart("survey_date", loanApplicationRequest.surveyDate?:"")
        builder.addFormDataPart("survey_clock", loanApplicationRequest.surveyClock?:"")
        builder.addFormDataPart("loan_application_referal", loanApplicationRequest.loanApplicationReferal?:"")
        builder.addFormDataPart("email", loanApplicationRequest.email?:"")
        builder.addFormDataPart("customer_name", loanApplicationRequest.customerName?:"")
        builder.addFormDataPart("gender", loanApplicationRequest.gender?:"")
        builder.addFormDataPart("place_of_birth", loanApplicationRequest.placeOfBirth?:"")
        builder.addFormDataPart("date_of_birth", loanApplicationRequest.dateOfBirth?:"")
        builder.addFormDataPart("id_number", loanApplicationRequest.idNumber?:"")
        builder.addFormDataPart("mother_name", loanApplicationRequest.motherName?:"")
        builder.addFormDataPart("area_code", loanApplicationRequest.areaCode?:"")
        builder.addFormDataPart("home_phone_number", loanApplicationRequest.homePhoneNumber?:"")
        builder.addFormDataPart("phone_number", loanApplicationRequest.phoneNumber?:"")
        builder.addFormDataPart("occupation_id", loanApplicationRequest.occupationId?:"")
        builder.addFormDataPart("monthly_income", loanApplicationRequest.monthlyIncome?:"")
        builder.addFormDataPart("additional_income", loanApplicationRequest.additionalIncome?:"")
        builder.addFormDataPart("side_job", loanApplicationRequest.sideJob?:"")
        // legal
        builder.addFormDataPart("province_id", loanApplicationRequest.provinceId?:"")
        builder.addFormDataPart("city_id", loanApplicationRequest.cityId?:"")
        builder.addFormDataPart("district_id", loanApplicationRequest.districtId?:"")
        builder.addFormDataPart("village_id", loanApplicationRequest.villageId?:"")
        builder.addFormDataPart("address", loanApplicationRequest.address?:"")
        builder.addFormDataPart("rt", loanApplicationRequest.rt?:"")
        builder.addFormDataPart("rw", loanApplicationRequest.rw?:"")
        builder.addFormDataPart("zip_code", loanApplicationRequest.zipCode?:"")
        // domicile
        builder.addFormDataPart("province_id_domicile", loanApplicationRequest.provinceIdDomicile?:"")
        builder.addFormDataPart("city_id_domicile", loanApplicationRequest.cityIdDomicile?:"")
        builder.addFormDataPart("district_id_domicile", loanApplicationRequest.districtIdDomicile?:"")
        builder.addFormDataPart("village_id_domicile", loanApplicationRequest.villageIdDomicile?:"")
        builder.addFormDataPart("address_domicile", loanApplicationRequest.addressDomicile?:"")
        builder.addFormDataPart("rt_domicile", loanApplicationRequest.rtDomicile?:"")
        builder.addFormDataPart("rw_domicile", loanApplicationRequest.rwDomicile?:"")
        builder.addFormDataPart("zip_code_domicile", loanApplicationRequest.zipCodeDomicile?:"")
        // office
        builder.addFormDataPart("province_id_office", loanApplicationRequest.provinceIdOffice?:"")
        builder.addFormDataPart("city_id_office", loanApplicationRequest.cityIdOffice?:"")
        builder.addFormDataPart("district_id_office", loanApplicationRequest.districtIdOffice?:"")
        builder.addFormDataPart("village_id_office", loanApplicationRequest.villageIdOffice?:"")
        builder.addFormDataPart("address_office", loanApplicationRequest.addressOffice?:"")
        builder.addFormDataPart("rt_office", loanApplicationRequest.rtOffice?:"")
        builder.addFormDataPart("rw_office", loanApplicationRequest.rwOffice?:"")
        builder.addFormDataPart("zip_code_office", loanApplicationRequest.zipCodeOffice?:"")

        builder.addFormDataPart("bank_name", loanApplicationRequest.bankName?:"")
        builder.addFormDataPart("bank_account_number", loanApplicationRequest.bankAccountNumber?:"")
        builder.addFormDataPart("bank_account_holder", loanApplicationRequest.bankAccountHolder?:"")

        builder.addFormDataPart("bank_account_image", fotoAccountCover)
        builder.addFormDataPart("foto_ktp", fotoKtp)
        builder.addFormDataPart("foto_kk", fotoKk)
        builder.addFormDataPart("foto_slip_gaji", fotoSlipGaji)
        builder.addFormDataPart("foto_rekening_koran", fotoRekKoran)
        builder.addFormDataPart("foto_keuangan_lainnya", fotoKeuanganLainnya)

        return networkService.sendLoanApplication(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getAgreementList(): Observable<AgreementListResponse> {
        return networkService.getAgreementList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getPrePaymentSimulation(agreementNumber: String, prepaymentDate: String): Observable<PrePaymentSimulationResponse> {
        return networkService.getPrepaymentSimulation(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            agreementNumber,
            prepaymentDate
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getPrePaymentRequest(agreementNumber: String, prepaymentDate: String): Observable<PrepaymentRequest> {
        return networkService.getPrepaymentRequest(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            agreementNumber,
            prepaymentDate
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDetailUnit(agreementNumber: String): Observable<DetailUnitResponse> {
        return networkService.getAndalanDetailUnit(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            agreementNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDetailFinancial(agreementNumber: String): Observable<DetailFinanceResponse> {
        return networkService.getAndalanDetailFinancial(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            agreementNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDetailPaymentHistory(agreementNumber: String): Observable<DetailPaymentHistoryResponse> {
        return networkService.getAndalanDetailPaymentHistory(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            agreementNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getAndalanFinancial(agreementNumber: String): Observable<DetailAndalanFinancialResponse> {
        return networkService.getAndalanFinancial(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0,
            agreementNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getDetailInsurance(agreementNumber: String): Observable<DetailInsuranceResponse> {
        return networkService.getAndalanDetailInsurance(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            agreementNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getUsedCarList(year: String, keyword: String): Observable<UsedCarListResponse> {
        return networkService.getUsedCarList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            year,
            keyword
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getPersonalInfo(): Observable<PersonalInformationResponse> {
        return networkService.getPersonalInformation(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            (preferencesUtil.getUser()?.idCustomer?:0).toString()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getClaimInsuranceList(status: String): Observable<ClaimInsuranceListResponse> {
        return networkService.getInsuranceClaimList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0,
            status
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendCollateralRequest(agreementNumber: String, takingDate: String): Observable<CollateralRequestResponse> {
        return networkService.sendCollateralRequest(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0,
            agreementNumber,
            takingDate
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getLegalDocument(): Observable<LegalDocumentResponse> {
        return networkService.getLegalDocument(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0"
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getCollateralRequestList(status: String): Observable<CollateralListResponse> {
        return networkService.getCollateralRequestList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0,
            status
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getCashValue(): Observable<CashValueResponse> {
        return networkService.getCashValue(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getUnreadMessage(): Observable<UnreadMessageResponse> {
        return networkService.getUnreadMessage(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            preferencesUtil.getUser()?.idCustomer?:0
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendPlafondSimulation(plafondSimulationRequest: PlafondSimulationRequest): Observable<PlafondSimulationResponse> {
        return networkService.sendPlafondSimulation(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            (preferencesUtil.getUser()?.idCustomer?:0).toString(),
            plafondSimulationRequest.occupationId,
            plafondSimulationRequest.monthlyIncome,
            plafondSimulationRequest.sideJob,
            plafondSimulationRequest.additionalIsncome
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendPlafondApplication(plafondApplicationRequest: PlafondApplicationRequest): Observable<PlafondApplicationResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", (preferencesUtil.getUser()?.idCustomer?:0).toString())
        builder.addFormDataPart("occupation_id", plafondApplicationRequest.occupationId)
        builder.addFormDataPart("monthly_income", plafondApplicationRequest.monthlyIncome)
        builder.addFormDataPart("side_job", plafondApplicationRequest.sideJob)
        builder.addFormDataPart("additional_income", plafondApplicationRequest.additionalIsncome)
        builder.addFormDataPart("andalan_branch_id", plafondApplicationRequest.andalanBranchId)
        builder.addFormDataPart("survey_date", plafondApplicationRequest.surveyDate)
        builder.addFormDataPart("survey_time", plafondApplicationRequest.surveyTime)
        // legal
        builder.addFormDataPart("city_id", plafondApplicationRequest.cityId?:"")
        builder.addFormDataPart("district_id", plafondApplicationRequest.districtId?:"")
        builder.addFormDataPart("village_id", plafondApplicationRequest.villageId?:"")
        builder.addFormDataPart("address", plafondApplicationRequest.address?:"")
        builder.addFormDataPart("rt", plafondApplicationRequest.rt?:"")
        builder.addFormDataPart("rw", plafondApplicationRequest.rw?:"")
        builder.addFormDataPart("zip_code", plafondApplicationRequest.zipCode?:"")
        // domicile
        builder.addFormDataPart("province_id_domicile", plafondApplicationRequest.provinceIdDomicile?:"")
        builder.addFormDataPart("city_id_domicile", plafondApplicationRequest.cityIdDomicile?:"")
        builder.addFormDataPart("district_id_domicile", plafondApplicationRequest.districtIdDomicile?:"")
        builder.addFormDataPart("village_id_domicile", plafondApplicationRequest.villageIdDomicile?:"")
        builder.addFormDataPart("address_domicile", plafondApplicationRequest.addressDomicile?:"")
        builder.addFormDataPart("rt_domicile", plafondApplicationRequest.rtDomicile?:"")
        builder.addFormDataPart("rw_domicile", plafondApplicationRequest.rwDomicile?:"")
        builder.addFormDataPart("zip_code_domicile", plafondApplicationRequest.zipCodeDomicile?:"")
        // office
        builder.addFormDataPart("province_id_office", plafondApplicationRequest.provinceIdOffice?:"")
        builder.addFormDataPart("city_id_office", plafondApplicationRequest.cityIdOffice?:"")
        builder.addFormDataPart("district_id_office", plafondApplicationRequest.districtIdOffice?:"")
        builder.addFormDataPart("village_id_office", plafondApplicationRequest.villageIdOffice?:"")
        builder.addFormDataPart("address_office", plafondApplicationRequest.addressOffice?:"")
        builder.addFormDataPart("rt_office", plafondApplicationRequest.rtOffice?:"")
        builder.addFormDataPart("rw_office", plafondApplicationRequest.rwOffice?:"")
        builder.addFormDataPart("zip_code_office", plafondApplicationRequest.zipCodeOffice?:"")

        val fotoKtp = if (plafondApplicationRequest.typeFotoKtp == "file") convertImageFileToBase64(File(plafondApplicationRequest.fotoKtp)) else plafondApplicationRequest.fotoKtp
        val fotoSlipGaji = if (plafondApplicationRequest.typeFotoSlipGaji == "file") convertImageFileToBase64(File(plafondApplicationRequest.fotoSlipGaji)) else plafondApplicationRequest.fotoSlipGaji

        builder.addFormDataPart("foto_ktp", fotoKtp?:"")
        builder.addFormDataPart("foto_slip_gaji", fotoSlipGaji?:"")
        return networkService.sendPlafondApplication(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            (preferencesUtil.getUser()?.idCustomer?:0).toString(),
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendAllAddress(sendAllAddressRequest: SendAllAddressRequest): Observable<UpdateAddressResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", (preferencesUtil.getUser()?.idCustomer?:0).toString())
        //val e = e("type_id_card", sendAllAddressRequest.typeImageIdCard)
        val imageIdCard = if (sendAllAddressRequest.typeImageIdCard == "file") {convertImageFileToBase64(File(sendAllAddressRequest.imageIdCard))} else ""
        builder.addFormDataPart("id_card_image", imageIdCard)
        // legal
        builder.addFormDataPart("province_id", sendAllAddressRequest.provinceId?:"")
        builder.addFormDataPart("regency_id", sendAllAddressRequest.cityId?:"")
        builder.addFormDataPart("district_id", sendAllAddressRequest.districtId?:"")
        builder.addFormDataPart("village_id", sendAllAddressRequest.villageId?:"")
        builder.addFormDataPart("address", sendAllAddressRequest.address?:"")
        builder.addFormDataPart("rt", sendAllAddressRequest.rt?:"")
        builder.addFormDataPart("rw", sendAllAddressRequest.rw?:"")
        builder.addFormDataPart("zip_code", sendAllAddressRequest.zipCode?:"")
        // domicile
        builder.addFormDataPart("province_id_domicile", sendAllAddressRequest.provinceIdDomicile?:"")
        builder.addFormDataPart("regency_id_domicile", sendAllAddressRequest.cityIdDomicile?:"")
        builder.addFormDataPart("district_id_domicile", sendAllAddressRequest.districtIdDomicile?:"")
        builder.addFormDataPart("village_id_domicile", sendAllAddressRequest.villageIdDomicile?:"")
        builder.addFormDataPart("address_domicile", sendAllAddressRequest.addressDomicile?:"")
        builder.addFormDataPart("rt_domicile", sendAllAddressRequest.rtDomicile?:"")
        builder.addFormDataPart("rw_domicile", sendAllAddressRequest.rwDomicile?:"")
        builder.addFormDataPart("zip_code_domicile", sendAllAddressRequest.zipCodeDomicile?:"")
        // office
        builder.addFormDataPart("province_id_office", sendAllAddressRequest.provinceIdOffice?:"")
        builder.addFormDataPart("regency_id_office", sendAllAddressRequest.cityIdOffice?:"")
        builder.addFormDataPart("district_id_office", sendAllAddressRequest.districtIdOffice?:"")
        builder.addFormDataPart("village_id_office", sendAllAddressRequest.villageIdOffice?:"")
        builder.addFormDataPart("address_office", sendAllAddressRequest.addressOffice?:"")
        builder.addFormDataPart("rt_office", sendAllAddressRequest.rtOffice?:"")
        builder.addFormDataPart("rw_office", sendAllAddressRequest.rwOffice?:"")
        builder.addFormDataPart("zip_code_office", sendAllAddressRequest.zipCodeOffice?:"")

        builder.addFormDataPart("area_code", sendAllAddressRequest.areaCode?:"")
        builder.addFormDataPart("phone", sendAllAddressRequest.phone?:"")
        builder.addFormDataPart("mobile_phone", sendAllAddressRequest.mobilePhone?:"")
        builder.addFormDataPart("id_card_number", sendAllAddressRequest.idCardNumber?:"")

        if (!sendAllAddressRequest.npwp_image.isNullOrBlank()) {
            val bm = BitmapFactory.decodeFile(sendAllAddressRequest.npwp_image)
            val baos = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
            val b = baos.toByteArray()
            val imageNpwp = Base64.encodeToString(b, Base64.DEFAULT)

            builder.addFormDataPart("npwp_image", imageNpwp)
        }
        else {
            builder.addFormDataPart("npwp_image", "")
        }

        builder.addFormDataPart("npwp", sendAllAddressRequest.npwp?:"")
        builder.addFormDataPart("gender", sendAllAddressRequest.gender?:"")
        builder.addFormDataPart("birth_place", sendAllAddressRequest.birthPlace?:"")
        builder.addFormDataPart("birth_date", sendAllAddressRequest.birthDate?:"")
        if (!sendAllAddressRequest.avatar.isNullOrBlank()) {
            builder.addFormDataPart("avatar", convertImageFileToBase64(File(sendAllAddressRequest.avatar)))
        }
        builder.addFormDataPart("bank_name", sendAllAddressRequest.bankName?:"")
        builder.addFormDataPart("bank_account_number", sendAllAddressRequest.bankAccountNumber?:"")
        builder.addFormDataPart("bank_account_holder", sendAllAddressRequest.bankAccountHolder?:"")
        builder.addFormDataPart("customer_name", sendAllAddressRequest.customer_name?:"")
        if (!sendAllAddressRequest.bankAccountImage.isNullOrBlank()) {
            val bm = BitmapFactory.decodeFile(sendAllAddressRequest.bankAccountImage)
            val baos = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
            val b = baos.toByteArray()
            val imageBank = Base64.encodeToString(b, Base64.DEFAULT)
            //val imageBank = convertImageFileToBase64(File(sendAllAddressRequest.bankAccountImage))
            builder.addFormDataPart("bank_account_image", imageBank)
        }
        else {
            builder.addFormDataPart("bank_account_image", "")
        }

        return networkService.sendProfileAllAddress(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            (preferencesUtil.getUser()?.idCustomer?:0).toString(),
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun sendAgentRegistration(sendAgenRegistrationRequest: SendAgenRegistrationRequest): Observable<AgentRegistrationResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", (preferencesUtil.getUser()?.idCustomer?:0).toString())
        //val e = e("type_id_card", sendAllAddressRequest.typeImageIdCard)
        val imageIdCard = if (sendAgenRegistrationRequest.typeImageIdCard == "file") {convertImageFileToBase64(File(sendAgenRegistrationRequest.imageIdCard))} else sendAgenRegistrationRequest.imageIdCard
        builder.addFormDataPart("id_card_image", imageIdCard?:"")
        // legal
        builder.addFormDataPart("province_id", sendAgenRegistrationRequest.provinceId?:"")
        builder.addFormDataPart("city_id", sendAgenRegistrationRequest.cityId?:"")
        builder.addFormDataPart("district_id", sendAgenRegistrationRequest.districtId?:"")
        builder.addFormDataPart("village_id", sendAgenRegistrationRequest.villageId?:"")
        builder.addFormDataPart("address", sendAgenRegistrationRequest.address?:"")
        builder.addFormDataPart("rt", sendAgenRegistrationRequest.rt?:"")
        builder.addFormDataPart("rw", sendAgenRegistrationRequest.rw?:"")
        builder.addFormDataPart("zip_code", sendAgenRegistrationRequest.zipCode?:"")
        // domicile
        builder.addFormDataPart("province_id_domicile", sendAgenRegistrationRequest.provinceIdDomicile?:"")
        builder.addFormDataPart("city_id_domicile", sendAgenRegistrationRequest.cityIdDomicile?:"")
        builder.addFormDataPart("district_id_domicile", sendAgenRegistrationRequest.districtIdDomicile?:"")
        builder.addFormDataPart("village_id_domicile", sendAgenRegistrationRequest.villageIdDomicile?:"")
        builder.addFormDataPart("address_domicile", sendAgenRegistrationRequest.addressDomicile?:"")
        builder.addFormDataPart("rt_domicile", sendAgenRegistrationRequest.rtDomicile?:"")
        builder.addFormDataPart("rw_domicile", sendAgenRegistrationRequest.rwDomicile?:"")
        builder.addFormDataPart("zip_code_domicile", sendAgenRegistrationRequest.zipCodeDomicile?:"")
        // office
        builder.addFormDataPart("province_id_office", sendAgenRegistrationRequest.provinceIdOffice?:"")
        builder.addFormDataPart("city_id_office", sendAgenRegistrationRequest.cityIdOffice?:"")
        builder.addFormDataPart("district_id_office", sendAgenRegistrationRequest.districtIdOffice?:"")
        builder.addFormDataPart("village_id_office", sendAgenRegistrationRequest.villageIdOffice?:"")
        builder.addFormDataPart("address_office", sendAgenRegistrationRequest.addressOffice?:"")
        builder.addFormDataPart("rt_office", sendAgenRegistrationRequest.rtOffice?:"")
        builder.addFormDataPart("rw_office", sendAgenRegistrationRequest.rwOffice?:"")
        builder.addFormDataPart("zip_code_office", sendAgenRegistrationRequest.zipCodeOffice?:"")

        builder.addFormDataPart("area_code", sendAgenRegistrationRequest.areaCode?:"")
        builder.addFormDataPart("home_phone_number", sendAgenRegistrationRequest.phone?:"")
        builder.addFormDataPart("phone_number", sendAgenRegistrationRequest.mobilePhone?:"")
        builder.addFormDataPart("id_card_number", sendAgenRegistrationRequest.idCardNumber?:"")
        builder.addFormDataPart("nik_upline", sendAgenRegistrationRequest.nikUpline?:"")
        builder.addFormDataPart("upline_name", sendAgenRegistrationRequest.nameUpline?:"")

        /*if (!sendAgenRegistrationRequest.npwp_image.isNullOrBlank()) {
            val bm = BitmapFactory.decodeFile(sendAgenRegistrationRequest.npwp_image)
            val baos = ByteArrayOutputStream()
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos) //bm is the bitmap object
            val b = baos.toByteArray()
            val imageNpwp = Base64.encodeToString(b, Base64.DEFAULT)

            builder.addFormDataPart("npwp_image", imageNpwp)
        }
        else {
            builder.addFormDataPart("npwp_image", "")
        }*/

        val imageNpwp = if (sendAgenRegistrationRequest.npwpImageType == "file") {convertImageFileToBase64(File(sendAgenRegistrationRequest.npwp_image))} else sendAgenRegistrationRequest.npwp_image
        builder.addFormDataPart("npwp_image", imageNpwp?:"")

        builder.addFormDataPart("npwp", sendAgenRegistrationRequest.npwp?:"")
        builder.addFormDataPart("gender", sendAgenRegistrationRequest.gender?:"")
        builder.addFormDataPart("place_of_birth", sendAgenRegistrationRequest.birthPlace?:"")
        builder.addFormDataPart("date_of_birth", sendAgenRegistrationRequest.birthDate?:"")
        builder.addFormDataPart("bank_name", sendAgenRegistrationRequest.bankName?:"")
        builder.addFormDataPart("bank_branch_name", sendAgenRegistrationRequest.bankBranchName?:"")
        builder.addFormDataPart("bank_account_number", sendAgenRegistrationRequest.bankAccountNumber?:"")
        builder.addFormDataPart("bank_account_holder", sendAgenRegistrationRequest.bankAccountHolder?:"")

        val imageBank = if (sendAgenRegistrationRequest.bankImageType == "file") {convertImageFileToBase64(File(sendAgenRegistrationRequest.bankAccountImage))} else sendAgenRegistrationRequest.bankAccountImage
        builder.addFormDataPart("bank_account_image", imageBank?:"")

        builder.addFormDataPart("agent_name", sendAgenRegistrationRequest.agentName?:"")
        builder.addFormDataPart("branch_id", sendAgenRegistrationRequest.branchId?:"")
        builder.addFormDataPart("status_pernikahan", sendAgenRegistrationRequest.statusPernikahan?:"")
        builder.addFormDataPart("pendidikan_terakhir", sendAgenRegistrationRequest.pendidikanTerakhir?:"")
        builder.addFormDataPart("status_rumah_tinggal", sendAgenRegistrationRequest.statusRumahTinggal?:"")
        builder.addFormDataPart("kerabat_afi", sendAgenRegistrationRequest.kerabatAfi?:"")
        builder.addFormDataPart("kerabat_afi_jabatan", sendAgenRegistrationRequest.kerabatAfiJabatan?:"")
        builder.addFormDataPart("lama_tinggal_tahun", sendAgenRegistrationRequest.lamaTinggalTahun?:"")
        builder.addFormDataPart("lama_tinggal_bulan", sendAgenRegistrationRequest.lamaTinggalBulan?:"")
        builder.addFormDataPart("branch_id", sendAgenRegistrationRequest.branchId?:"")
        builder.addFormDataPart("email", sendAgenRegistrationRequest.email?:"")
        builder.addFormDataPart("occupation_id", sendAgenRegistrationRequest.occupationId?:"")

        return networkService.sendAgentRegistration(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0",
            (preferencesUtil.getUser()?.idCustomer?:0).toString(),
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    private fun convertImageFileToBase64(imageFile: File): String {
        return FileInputStream(imageFile).use { inputStream ->
            ByteArrayOutputStream().use { outputStream ->
                Base64OutputStream(outputStream, Base64.DEFAULT).use { base64FilterStream ->
                    inputStream.copyTo(base64FilterStream)
                    base64FilterStream.flush()
                    outputStream.toString()
                }
            }
        }
    }

    fun getListTutorial(): Observable<TutorialResponse> {
        return networkService.listTutorial(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun doResetPinWithPassword(password: String, pinNumber: String): Observable<CreatePinResponse> {
        return networkService.resetPinWithPassword(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getLatitude()?:"",
            preferencesUtil.getLongitude()?:"",
            preferencesUtil.getIdNumber()?:0,
            password,
            pinNumber
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getLatestAppVersion(): Observable<AppVersionResponse> {
        return networkService.getLatestAppVersion(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            preferencesUtil.getLatitude()?:"0",
            preferencesUtil.getLongitude()?:"0"
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }
    fun paymentInquiry(agreementNumber: String): Observable<PaymentInquiryResponse> {
        return networkService.paymentInquiry(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            agreementNumber,
            preferencesUtil.getUser()?.idCustomer?:0

        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getPaymentHistoryList(category: String, status: String): Observable<InstallmentPaymentHistoryListResponse> {
        return networkService.getPaymentHistory(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            category,
            status,
            idCustomer = preferencesUtil.getUser()?.idCustomer?:0
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun getUrlList(): Observable<UrlListResponse> {
        return networkService.getUrlList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:""
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun updateFcmToken(fcmToken: String): Observable<UpdateTokenResponse> {
        return networkService.updateFcmToken(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            preferencesUtil.getUser()?.idCustomer?:0,
            fcmToken
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun NipCheck(nip: String): Observable<NipCheckResponse> {
        return networkService.nipCheck(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            nip
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun PpobProductList(category: String, biller: String = "", keyword: String = ""): Observable<PpobProductListResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("category", category)
        builder.addFormDataPart("biller", biller)
        builder.addFormDataPart("keyword", keyword)
        return networkService.ppobProductList(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun PpobInquiry(accountNumber: String, productCode: String, category: String, zoneId: String = "", month: String = ""): Observable<PpobInquiryResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("account_number", accountNumber)
        builder.addFormDataPart("product_code", productCode)
        builder.addFormDataPart("zone_id", zoneId)
        builder.addFormDataPart("month", month)
        builder.addFormDataPart("transaction_group", category)
        return networkService.ppobInquiry(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun PpobEmoneyBiller(category: String): Observable<PpobProductListResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("category", category)
        return networkService.ppobEmoneyBiller(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun PpobTransactionDetail(transactionId: String): Observable<PpobTransactionDetailResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        builder.addFormDataPart("order_id", transactionId)
        return networkService.ppobTransactionDetail(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun PpobGroup(): Observable<PpobGroupResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("id_customer", preferencesUtil.getUser()?.idCustomer.toString())
        return networkService.ppobGroup(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

    fun PpobUpdateAmount(orderId : String, amount: String): Observable<PpobUpdateAmountResponse> {
        val builder = MultipartBody.Builder()
        builder.setType(MultipartBody.FORM)
        builder.addFormDataPart("order_id", orderId)
        builder.addFormDataPart("amount", amount)
        return networkService.ppobUpdateAmount(
            BuildConfig.API_KEY,
            preferencesUtil.getAccessToken()?:"",
            builder.build()
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .onErrorResumeNext { throwable -> Observable.error(throwable) }
    }

}