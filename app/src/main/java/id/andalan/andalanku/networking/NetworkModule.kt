package id.andalan.andalanku.networking

import android.app.Application
import android.util.Log.e
import id.andalan.andalanku.BuildConfig
import id.andalan.andalanku.exception.ErrorInterceptor
import id.andalan.andalanku.preferences.PreferencesUtil
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.io.File
import javax.inject.Singleton

@Module
class NetworkModule(private val application: Application, internal var cacheFile: File) {
    @Provides
    @Singleton
    internal fun provideCall(): Retrofit {
        //var cache: Cache? = null
        //cache = Cache(cacheFile, (10 * 1024 * 1024).toLong())
        val httpLoggingInterceptor = HttpLoggingInterceptor()
            .setLevel(HttpLoggingInterceptor.Level.BODY)

        val errorInterceptor =
            ErrorInterceptor(NetManager(application.applicationContext))
        e("net module", "called")

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor { chain ->
                val original = chain.request()

                val request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .build()

                val response = chain.proceed(request)
                response.cacheResponse()
                response
            }
            .addInterceptor(httpLoggingInterceptor)
            .addInterceptor(errorInterceptor)
            .addInterceptor(ChuckInterceptor(application.applicationContext))
            .build()

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASEURL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun providesNetworkService(retrofit: Retrofit): NetworkService {
        return retrofit.create(NetworkService::class.java)
    }

    @Provides
    @Singleton
    fun providesService(networkService: NetworkService, preferencesUtil: PreferencesUtil): Services {
        return Services(networkService, preferencesUtil)
    }
}